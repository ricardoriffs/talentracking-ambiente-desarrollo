<?php
//file: index.php
$yii = dirname(__FILE__) . '/../yii/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

require_once($yii);
$conf = include($config);

//buka koneksi database
$connection = new CDbConnection(
        $conf['components']['db']['connectionString'],
        $conf['components']['db']['username'],
        $conf['components']['db']['password']
);
$connection->setActive(true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <style type="text/css">
.divContentBlue{background-color:#F2F2F2;border-color:#CCC;border-image:initial;border-style:solid;border-width:1px;color:#1F497D;padding:5px}
.tablecontents2{background-color:#CFD8F1;border:medium none #FFF;color:#000;font-family:Arial, Helvetica, sans-serif;font-size:10pt;font-weight:400}
.whitelinks{linkcolor:#fff;vlinkcolor:#fff}
.grid-view-loading{background:url(loading.gif) no-repeat}
.grid-view{padding:15px 0}
.grid-view table.items{background:#FFF;border:1px #D0E3EF solid;border-collapse:collapse;width:100%}
.grid-view table.items th,.grid-view table.items td{border:1px #FFF solid;font-size:.9em;padding:.3em}
.grid-view table.items th{background:url(bg.gif) repeat-x scroll left top #FFF;color:#FFF;text-align:center}
.grid-view table.items th a{color:#EEE;font-weight:700;text-decoration:none}
.grid-view table.items th a:hover{color:#FFF}
.grid-view table.items th a.asc{background:url(up.gif) right center no-repeat;padding-right:10px}
.grid-view table.items th a.desc{background:url(down.gif) right center no-repeat;padding-right:10px}
.grid-view table.items tr.even{background:#F8F8F8}
.grid-view table.items tr.odd{background:#E5F1F4}
.grid-view table.items tr.selected{background:#BCE774}
.grid-view table.items tr:hover{background:#ECFBD4}
.grid-view .button-column{text-align:center;width:60px}
.grid-view .checkbox-column{width:15px}
.grid-view .summary{margin:0 0 5px;text-align:right}
.grid-view .pager{margin:5px 0 0;text-align:right}
.grid-view .empty{font-style:italic}
.grid-view .filters input,.grid-view .filters select{border:1px solid #ccc;width:100%}
.grid-view .link-column img,.grid-view .button-column img{border:0}
        </style>
    </head>
    <body>
        <diV class = "divContentBlue">
            <div class="row"><h2 align="center" class="Estilo2"> Lista de Archivos</h2></div><br/>
            <?php
//Asignamos la ruta a la variable path
            $r = $_GET['r'];

            $paux = substr($r, 1, strlen($r));
            $varupload = substr($r, 0, 1);
            if ($varupload == 'f') {
                $path = 'upload/fa/' . $paux . '/';
                echo 'Tipo Archivos : FORMATO AUTORIZACIONES';
            } elseif ($varupload == 'd') {
                $path = 'upload/dc/' . $paux . '/';
                echo 'Tipo Archivos : DESCRIPCION CARGO';
            } elseif ($varupload == 'x') {

                $command = $connection->createCommand("SELECT c.fechaInsert, c.idCandidato, c.id_proceso, c.hojavida, c.tipo_hojavida, c.nombre_hojavida, c.formaprobacion, c.tipo_formaprobacion, c.nombre_formaprobacion FROM candidatos c WHERE c.idCandidato=" . $paux);
                $requisicion = $command->queryRow();

                $command = $connection->createCommand("SELECT p.ofertaid FROM procesos p WHERE p.id=" . $requisicion['id_proceso']);
                $oferta = $command->queryRow();

                echo 'Tipo Archivos : DOCUMENTOS DE CANDIDATO';
                echo nl2br("\n");
                //echo 'Requisici&oacute;n Numero :' . $requisicion['RequisicionID'] . '  ';
                echo 'Proceso N&uacute;mero :' . $oferta['ofertaid'];
                echo nl2br("\n");
                echo nl2br("\n");
                echo nl2br("\n");
            } else {

                echo nl2br("\n");
                echo 'Requisicion Numero :' . $paux . '  ';
                echo nl2br("\n");
                echo nl2br("\n");
                echo nl2br("\n");
                //asignamos a $directorio el objeto dir creado con la ruta
                $directorio = dir($path);

                //$directorio->close();
// open this directory
                $myDirectory = opendir($path);

// get each entry
                $i = 1;
                while ($entryName = readdir($myDirectory)) {

                    //echo $path.'/'.$entryName;

                    $l = strlen($entryName);

                    if ($l > 3) {
                        //echo '*****';

                        $ext = split("[/\\.]", $entryName);
                        $n = count($ext) - 1;
                        $ext = $ext[$n];

                        //$ext = substr($entryName, -3, 3);
                        $entryNameT = $paux . '-' . $i . '.' . $ext;
                        //echo $path.'/'.$entryNameT;
                        rename($path . '/' . $entryName, $path . '/' . $entryNameT);
                        $dirArray[] = $entryNameT;
                        $dirArrayT[] = array('date' => filemtime($path . '/' . $entryNameT), 'file' => $entryNameT);

                        $i = $i + 1;
                    } else {
                        $dirArray[] = $entryName;
                        $dirArrayT[] = array('date' => filemtime($path . '/' . $entryName), 'file' => $entryName);
                    }
                    //
                }

// close directory
                closedir($myDirectory);

                $indexCount = count($dirArray) - 2;
                $indexCountT = count($dirArrayT) - 2;

                $indexCount = count($dirArray);

                sort($dirArray);

            } ?>
            
            <TABLE border=1 cellpadding=5 cellspacing=0 class=tablecontents2>
            <tr>
                <th>Archivo</th>
                <th>Tipo Archivo</th>
                <th>Tama&ntilde;o</th>
                <th>Fecha Creaci&oacute;n</th>
            </tr>

            <?php if ($varupload == 'f') : $tol = $indexCount - 2; ?>
                Total Archivos : <?php echo $tol ?><br>
                <?php for ($index = 0; $index < $indexCount; $index++) : ?>
                    <?php if (substr("$dirArray[$index]", 0, 1) != ".") : // don't list hidden files ?>
                        <tr>
                            <td><a href="<?php echo "$path$dirArray[$index]" ?>"><?php echo $dirArray[$index] ?></a></td>
                            <td><?php echo filetype($path . '/' . $dirArray[$index]) ?></td>
                            <td><?php echo filesize($path . '/' . $dirArray[$index]) ?></td>
                            <td><?php echo date("M d Y H:i", filemtime($path . '/' . $dirArray[$index])) ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endfor; ?>
            <?php elseif ($varupload == 'd') : ?>
                Total Archivos : 1<br>
                <?php if ($indexCountT > 0) : $arc = $dirArrayT[$indexCountT]['file']; ?>
                <tr>
                    <td><a href="<?php echo "$path$arc" ?>"><?php echo $arc ?></a></td>
                    <td><?php echo filetype($path . '/' . $dirArrayT [$indexCountT] ['file']) ?></td>
                    <td><?php echo filesize($path . '/' . $dirArrayT [$indexCountT] ['file']) ?></td>
                    <td><?php echo date("M d Y H:i", filemtime($path . '/' . $dirArrayT [$indexCountT] ['file'])) ?></td>
                 </tr>
                <?php endif; ?>
            <?php elseif ($varupload == 'x') : ?>
                <tr>
                    <td><a href="index.php?r=candidatos/verHojaVida&id=<?php echo $paux ?>" target='blank'><?php echo $requisicion['nombre_hojavida'] ?></a></td>
                    <td><?php echo $requisicion['tipo_hojavida'] ?></td>
                    <td></td>
                    <td><?php echo $requisicion['fechaInsert'] ?></td>
                </tr>
                <tr>
                    <td><a href="verFormAprobacion.php?idc=<?php echo $paux ?>" target='blank'><?php echo $requisicion['nombre_formaprobacion'] ?></a></td>
                    <td><?php echo $requisicion['tipo_formaprobacion'] ?></td>
                    <td></td>
                    <td><?php echo $requisicion['fechaInsert'] ?></td>
                </tr>
            <?php endif; ?>
            </TABLE><?php $connection->setActive(false) ?>
        </div>
        <p><br></p>
    </body>
</html>
