<?php 

$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

require_once($yii);
$conf = include($config);

$connection=new CDbConnection(
        $conf['components']['db']['connectionString'],
        $conf['components']['db']['username'],
        $conf['components']['db']['password']
);
$connection->setActive(true);

$datos = NULL;

$idc = $_GET["idc"];

if($idc > 0){ 
    
    $command=$connection->createCommand("SELECT c.foto, c.tipo_foto, c.nombre_foto FROM candidatos c WHERE c.idCandidato=".$idc);
    $datos = $command->queryRow();    
    
    $foto = $datos['foto'];
    $tipo_foto = $datos['tipo_foto'];
//    $nombre_foto = $datos['nombre_foto'];
    
    header("Content-type: ".$tipo_foto);    
//    header("Content-Disposition: attachment; filename=". $nombre_foto);    
    
    echo $foto;       
}

?>