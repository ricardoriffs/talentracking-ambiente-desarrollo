<?php 

header("Location: index.php?r=candidatos/verHojaVida&id="
        . filter_input(INPUT_GET, "idc"));

$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

require_once($yii);
$conf = include($config);

$connection=new CDbConnection(
        $conf['components']['db']['connectionString'],
        $conf['components']['db']['username'],
        $conf['components']['db']['password']
);
$connection->setActive(true);

$datos = NULL;

$idc = $_GET["idc"];

if($idc > 0){
    
    $command=$connection->createCommand(""
            . "SELECT c.hojavida, c.tipo_hojavida, c.nombre_hojavida "
            . "FROM candidatos c "
            . "WHERE c.idCandidato=".$idc);
    $datos = $command->queryRow();    
    
    $hojavida = $datos['hojavida'];
    $tipo_hojavida = $datos['tipo_hojavida'];
    $nombre_hojavida = $datos['nombre_hojavida'];
    
    header("Content-type: ".$tipo_hojavida);    
    header("Content-Disposition: attachment; filename=". $nombre_hojavida);    
    
    echo $hojavida;    
}