<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
.divContentBlue {
  background-color:#F2F2F2;
  border-bottom-color:#CCCCCC;
  border-bottom-style:solid;
  border-bottom-width:1px;
  border-image:initial;
  border-left-color:#CCCCCC;
  border-left-style:solid;
  border-left-width:1px;
  border-right-color:#CCCCCC;
  border-right-style:solid;
  border-right-width:1px;
  border-top-color:#CCCCCC;
  border-top-style:solid;
  border-top-width:1px;
  color:#1F497D;
  padding-bottom:5px;
  padding-left:5px;
  padding-right:5px;
  padding-top:5px;
}
.tablecontents2 {
font-family: Arial, Helvetica, sans-serif;
background-color: #CFD8F1;
border: medium none #FFFFFF;
font-weight: normal;
color: #000000;
font-size: 10pt;
}
.whitelinks {
link color: #ffffff
vlink color: #ffffff
}
.grid-view-loading
{
	background:url(loading.gif) no-repeat;
}

.grid-view
{
	padding: 15px 0;
}

.grid-view table.items
{
	background: white;
	border-collapse: collapse;
	width: 100%;
	border: 1px #D0E3EF solid;
}

.grid-view table.items th, .grid-view table.items td
{
	font-size: 0.9em;
	border: 1px white solid;
	padding: 0.3em;
}

.grid-view table.items th
{
	color: white;
	background: url("bg.gif") repeat-x scroll left top white;
	text-align: center;
}

.grid-view table.items th a
{
	color: #EEE;
	font-weight: bold;
	text-decoration: none;
}

.grid-view table.items th a:hover
{
	color: #FFF;
}

.grid-view table.items th a.asc
{
	background:url(up.gif) right center no-repeat;
	padding-right: 10px;
}

.grid-view table.items th a.desc
{
	background:url(down.gif) right center no-repeat;
	padding-right: 10px;
}

.grid-view table.items tr.even
{
	background: #F8F8F8;
}

.grid-view table.items tr.odd
{
	background: #E5F1F4;
}

.grid-view table.items tr.selected
{
	background: #BCE774;
}

.grid-view table.items tr:hover
{
	background: #ECFBD4;
}

.grid-view .link-column img
{
	border: 0;
}

.grid-view .button-column
{
	text-align: center;
	width: 60px;
}

.grid-view .button-column img
{
	border: 0;
}

.grid-view .checkbox-column
{
	width: 15px;
}

.grid-view .summary
{
	margin: 0 0 5px 0;
	text-align: right;
}

.grid-view .pager
{
	margin: 5px 0 0 0;
	text-align: right;
}

.grid-view .empty
{
	font-style: italic;
}

.grid-view .filters input,
.grid-view .filters select
{
	width: 100%;
	border: 1px solid #ccc;
}


</style>
</head>

<body>
     
<diV class = "divContentBlue">   
        <div class="row">
	<h2 align="center" class="Estilo2"> Documentos de politicas y procedimientos de seleccion</h2>
        </div>



<?php 
      
      //Asignamos la ruta a la variable path
        $r= $_GET['r'];
         
print("<TABLE border=1 cellpadding=5 cellspacing=0 class=tablecontents2 \n");
print("<TR><TD><a href=\"p1.pdf\">P-TH-001 Procedimiento de Seleccion 2012</a></td>");		
print("<TR><TD><a href=\"p2.pdf\">P-TH-002 procedimeinto Oferta Interna de Vacantes 2012</a></td>");		
print("<TR><TD><a href=\"p3.pdf\">P-TH-003 Procedimiento de Seleccion Estudiantes en Practica - 2012</a></td>");		
print("<TR><TD><a href=\"p4.pdf\">Presentacion proceso de seleccion induccion</a></td>");		
print("<td>");
?>

</div>
</body>
</html>
