<?php 

$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

require_once($yii);
$conf = include($config);

$connection=new CDbConnection(
        $conf['components']['db']['connectionString'],
        $conf['components']['db']['username'],
        $conf['components']['db']['password']
);
$connection->setActive(true);

$datos = NULL;

$idc = $_GET["idc"];

if($idc > 0){
    
    $command=$connection->createCommand("SELECT c.formaprobacion, c.tipo_formaprobacion, c.nombre_formaprobacion FROM candidatos c WHERE c.idCandidato=".$idc);
    $datos = $command->queryRow();  
    
    $formaprobacion = $datos['formaprobacion'];
    $tipo_formaprobacion = $datos['tipo_formaprobacion'];
    $nombre_formaprobacion = $datos['nombre_formaprobacion'];
     
    header("Content-type: ".$tipo_formaprobacion);    
    header("Content-Disposition: attachment; filename=". $nombre_formaprobacion);        
    
    echo $formaprobacion;    
}

?>