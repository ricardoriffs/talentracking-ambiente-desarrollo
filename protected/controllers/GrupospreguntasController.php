<?php

class GrupospreguntasController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','asignarCompetencias','deleteCompetencia'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Grupospreguntas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Grupospreguntas']))
		{
			$model->attributes=$_POST['Grupospreguntas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idGrupo));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Grupospreguntas']))
		{
			$model->attributes=$_POST['Grupospreguntas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idGrupo));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionDeleteCompetencia($id)
	{
		$this->loadModelCompetencia($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('asignarCompetencias'));
	}        

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Grupospreguntas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Grupospreguntas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Grupospreguntas']))
			$model->attributes=$_GET['Grupospreguntas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Grupospreguntas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	public function loadModelCompetencia($id)
	{
		$model=CompetenciasEntrevista::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}        

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='grupospreguntas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
	public function actionAsignarCompetencias()
	{
                //$idProceso = $_GET["idP"];
		$model=new CompetenciasEntrevista;              
		
		$criteria = new CDbCriteria;
		$criteria->select = 'id, CompaniaID';
		$criteria->condition = 'username=:username';
		$criteria->params = array(':username' => Yii::app()->user->name);                        
		$usuario = Usuario::model()->find($criteria);                  
                
		$competenciasAsignadas = CompetenciasEntrevista::model()->findAll(array('condition'=> "idCompania =".$usuario->CompaniaID));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CompetenciasEntrevista']))
		{                   
			//$model->attributes=$_POST['CompetenciasEntrevista'];
                        $grupo_id = $_POST['CompetenciasEntrevista']['idGrupo'];
			$model->idCompania = $usuario->CompaniaID;  
                        $model->idGrupo = $grupo_id;
					
			if($model->validate()){                    
				$model->save();
				$this->redirect(array('asignarCompetencias'));
			}    
		}

		$this->render('asignarCompetencias',array(
			'model'=>$model,
                        'competencias'=>$competenciasAsignadas
		));
	}        
	
	public function actionObtenerValidadores()
	{
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('asignarCompetencias'));
		$data = NULL;	
		//if (Yii::app()->request->isAjaxRequest)
                if(isset($_POST['idGrupo']))
		{
                        $comp_id = $_POST['idGrupo'];
			//$comp_id = Yii::app()->request->getParam( 'grupoid' );
			//$comp_id = $_POST['CompetenciasEntrevista']['idGrupo'];
			//if($comp_id != ''){				
				$data = Grupospreguntas::model()->findByPk($comp_id);
				//$preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')); 
				
				//if (!empty($data)){
					//echo CHtml::encode($data->idGrupo);
				  echo CHtml::encode($data->idGrupo);				
				//} else {
				//	echo '0';
				//}				
			//}
		}
	}	
}
