<?php

class CompaniasprocesosexternosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(''),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Companiasprocesosexternos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Companiasprocesosexternos']))
		{
			$model->attributes=$_POST['Companiasprocesosexternos'];
                        
                        $model->FechaCreacion = new CDbExpression('NOW()');
                        $model->FechaModificacion = NULL;

                        $criteria = new CDbCriteria;
                        $criteria->select = 'id, CompaniaID';
                        $criteria->condition = 'username=:username';
                        $criteria->params = array(':username' => Yii::app()->user->name);                        
                        $usuario = Usuario::model()->find($criteria);
                        
                        $model->UsuarioCreacion = $usuario->id;
                        $model->CompaniaID = $usuario->CompaniaID;
                        $model->UsuarioModificacion = NULL;                           
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->CompaniasProcesosExternosID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Companiasprocesosexternos']))
		{
			$model->attributes=$_POST['Companiasprocesosexternos'];
                        
                        $model->FechaModificacion = new CDbExpression('NOW()');

                        $criteria = new CDbCriteria;
                        $criteria->select = 'id';
                        $criteria->condition = 'username=:username';
                        $criteria->params = array(':username' => Yii::app()->user->name);                        
                        $usuario = Usuario::model()->find($criteria);
                        
                        $model->UsuarioModificacion = $usuario->id;                          
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->CompaniasProcesosExternosID));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
//		$this->loadModel($id)->delete();
                $model=$this->loadModel($id);
                $model->Estado = 0;
                $model->save(); 

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Companiasprocesosexternos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Companiasprocesosexternos('search');
		$model->unsetAttributes();  // clear any default values
                
                $criteria=new CDbCriteria;     
                $criteria->select = 'id, CompaniaID, rol';
                $criteria->condition = 'username=:username';
                $criteria->params = array(':username' => Yii::app()->user->name);                       
                $usuario = Usuario::model()->find($criteria);
                
                if($usuario->rol != 'admin'){
                    $model->CompaniaID = $usuario->CompaniaID; 
                    $model->Estado = 1;
                }                  
                
		if(isset($_GET['Companiasprocesosexternos']))
			$model->attributes=$_GET['Companiasprocesosexternos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Companiasprocesosexternos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='companiasprocesosexternos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
