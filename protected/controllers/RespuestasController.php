<?php

class RespuestasController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','RegistrarEncuesta','IngresoCandidato','MisEncuestas'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','registrarEntrevista'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Respuestas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Respuestas']))
		{
			$model->attributes=$_POST['Respuestas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idRespuesta));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Respuestas']))
		{
			$model->attributes=$_POST['Respuestas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idRespuesta));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Respuestas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Respuestas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Respuestas']))
			$model->attributes=$_GET['Respuestas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Respuestas the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Respuestas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Respuestas $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='respuestas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
	public function actionRegistrarEntrevista(){
            
            $idCandidato = $_GET["idC"];
//            $idCompania = $_GET["idE"]; 
            $idProceso = $_GET["idP"];
            $idEncuesta = 75;
			
			$criteria = new CDbCriteria;
            $criteria->select = 'id, CompaniaID';
            $criteria->condition = 'username=:username';
            $criteria->params = array(':username' => Yii::app()->user->name);                        
            $usuario = Usuario::model()->find($criteria);			
			$idCompania = $usuario->CompaniaID;
			
            $proceso = Procesos::model()->findByPk($idProceso); 
            $candidato = Candidatos::model()->findByPk($idCandidato);
            $encuestas = Encuestas::model()->findByPk($idEncuesta);      
            
            $criteria = new CDbCriteria;
            $criteria->condition = 'idCompania=:idCompania';
            $criteria->params = array(':idCompania' => $idCompania);
            $competencias = CompetenciasEntrevista::model()->findAll($criteria);                   

            $criteria1 = new CDbCriteria;
            $criteria1->condition = 'tipoOpcion=:tipoOpcion';
            $criteria1->params = array(':tipoOpcion' => 'entrevista');
            $opciones = Opciones::model()->findAll($criteria1);   
            
            $model= new Respuestas;        
            $model1 = new ResultadoEntrevista;

            if(isset($_POST['Respuestas'])){
                   
//                for($i=1;$i<=5;$i++){
               
            foreach($competencias as $l=>$competencia):               
               
                $preguntas =Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC'));               
               
                foreach ($preguntas as $i=>$pregunta):               

//                    foreach ($opciones as $j => $opcion):

                        if(isset($_POST['Respuestas']['idOpcion'][$l][$i])){

                                 $opcion = $_POST['Respuestas']['idOpcion'][$l][$i];
                            
                                 $model= new Respuestas;
//                                 $model->attributes=$_POST['Respuestas'][$l][$i][$j];
                                 $model->idPregunta = $pregunta->idPregunta;
                                 $model->idCandidato = $candidato->idCandidato;
//                                 $model->valor = $opcion->texto;
                                 $model->idOpcion = $opcion;
                                 $model->idProceso = $proceso->id;
                                 $consultaOpciones = Opciones::model()->findByPk($opcion);
                                 $model->valor = $consultaOpciones->texto;    
                                 $model->observaciones = $_POST['Respuestas']['observaciones'][$l][$i];
                                 $model->fechaInsert = new CDbExpression('NOW()');
                                 $model->save();                                   
                            }            
//                    endforeach;  
                endforeach;  
            endforeach;
            //$this->redirect(array('view','id'=>$model->idRespuesta));             
//                }  
                if(isset($_POST['ResultadoEntrevista'])){
                    
                    $model1->attributes = $_POST['ResultadoEntrevista'];
                    $model1->idCandidato = $candidato->idCandidato;
                    $model1->idProceso = $idProceso;                  
                    $model1->idUsuarioEvaluador = $usuario->id;
                        
                    $model1->save();        
                }            
            }            
            
            $this->render('registrarEntrevista',array(
                    'proceso'=>$proceso,
                    'competencias'=>$competencias,
                    'candidato'=>$candidato,     
                    'opciones'=>$opciones,
                    'encuestas'=>$encuestas,    
                    'model'=>$model,
                    'model1'=>$model1
            ));              
        }     
        
        public function actionRegistrarEncuesta(){
            
            //$idCandidato = $_GET["idC"];
            //$idProceso = $_GET["idP"];
            $idEncuesta = $_GET["idE"];
            $opciones = array();
            
            $encuestas = Encuestas::model()->findByPk($idEncuesta);
            $idProceso = $encuestas->idProceso;
            $proceso = Procesos::model()->findByPk($idProceso); 
            
            $identificacion = Yii::app()->user->identificacion;
            $candidato = RegistroCandidatos::model()->findByAttributes(array('identificacion'=>$identificacion));
//            $criteria = new CDbCriteria;
//            $criteria->condition = 'idProceso=:idProceso';
//            $criteria->params = array(':idProceso' => $idProceso);
//            $encuestas = Encuestas::model()->find($criteria);        
			
//            $criteria2 = new CDbCriteria;
//            $criteria2->select = 'id, CompaniaID';
//            $criteria2->condition = 'username=:username';
//            $criteria2->params = array(':username' => Yii::app()->user->name);                        
//            $usuario = Usuario::model()->find($criteria2);			
//            $idCompania = $usuario->CompaniaID;
			
            //$candidato = Candidatos::model()->findByPk($idCandidato);        
            
//            $criteria3 = new CDbCriteria;
//            $criteria3->condition = 'idCompania=:idCompania';
//            $criteria3->params = array(':idCompania' => $idCompania);
//            $competencias = CompetenciasEntrevista::model()->findAll($criteria3);
            
            $criteria4 = new CDbCriteria;
            $criteria4->condition = 'idEncuesta=:idEncuesta';
            $criteria4->params = array(':idEncuesta' => $idEncuesta);
            $grupos = Grupospreguntas::model()->find($criteria4);         
            $idGrupo = $grupos->idGrupo;

            $criteria5 = new CDbCriteria;
            $criteria5->condition = 'idGrupo=:idGrupo';
            $criteria5->params = array(':idGrupo' => $idGrupo);
            $preguntas = Preguntas::model()->findAll($criteria5);            
            
            foreach ($preguntas as $k=>$pregunta):
//                $criteria1 = new CDbCriteria;
//                $criteria1->condition = 'idGrupo=:idGrupo';
//                $criteria1->params = array(':idGrupo' => $idGrupo);
//                $opciones = Opciones::model()->findAll($criteria1);  
                $opciones[$k] = Opciones::model()->findAll(array('condition'=> "idGrupo =" . $idGrupo . " AND idPregunta =" . $pregunta->idPregunta,'order' => 'orden ASC'));               
            endforeach;
            //$preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $idGrupo,'order' => 'orden ASC')); 
            
            $model= new Respuestas;        
            $model1 = new ResultadoEntrevista;

            if(isset($_POST['Respuestas'])){                           
               
                foreach ($preguntas as $i=>$pregunta):               

                        if(isset($_POST['Respuestas']['idOpcion'][$i])){

                                 $opcion = $_POST['Respuestas']['idOpcion'][$i];
                            
                                 $model= new Respuestas;
                                 $model->idPregunta = $pregunta->idPregunta;
                                 $model->idCandidato = $candidato->idCandidato;
                                 $model->idOpcion = $opcion;
                                 $model->idProceso = $proceso->id;
                                 $consultaOpciones = Opciones::model()->findByPk($opcion);
                                 $model->valor = $consultaOpciones->texto;    
                                 $model->fechaInsert = new CDbExpression('NOW()');
                                 $model->save();                                   
                            }             
                endforeach;         
            
           if($model->save()){
               $this->render('agradecimiento', array(
                  'oferta'=>$proceso->ofertaid
               ));
               Yii::app()->end();
           }      
//                if(isset($_POST['ResultadoEntrevista'])){
//                    
//                    $model1->attributes = $_POST['ResultadoEntrevista'];
//                    $model1->idCandidato = $candidato->idCandidato;
//                    $model1->idProceso = $idProceso;                  
//                    $model1->idUsuarioEvaluador = $usuario->id;
//                        
//                    $model1->save();        
//                }            
            }            
            
            $this->render('registrarEncuesta',array(
                    'proceso'=>$proceso,
                    'candidato'=>$candidato,     
                    'opciones'=>$opciones,
                    'encuestas'=>$encuestas,    
                    'model'=>$model,
                    'model1'=>$model1,
                    'preguntas'=>$preguntas
            ));             
        }
        
        public function actionIngresoCandidato(){
           
                $model = new IngresoCandidatos();
            
		if(isset($_POST['IngresoCandidatos'])){
                    
                    $model->attributes=$_POST['IngresoCandidatos'];
                    // validate user input and redirect to the previous page if valid
                    if($model->validate() && $model->ingreso())
                            //$this->redirect(array('Respuestas/registrarEncuesta&idC=8&idP=296'));    
                            $this->redirect(array('Respuestas/misEncuestas'));    
                 }
                
                $this->render('ingreso',array(
                    'model'=>$model
                ));
        }
        
        public function actionMisEncuestas(){
            
            $identificacion = Yii::app()->user->identificacion;
	    $encuestas=new Encuestas('search');
            $encuestas->unsetAttributes();  // clear any default values
            $idEncuesta = 76;   
            
            $candidato = RegistroCandidatos::model()->findByAttributes(array('identificacion'=>$identificacion));
            //$encuestas = Encuestas::model()->findByAttributes(array('idProceso'=>$candidato->id_proceso));
            //Respuestas::model()->findByAttributes(array('idCandidato'=>$candidato->idCandidato)); 
            //$respuestas = Respuestas::model()->with('idPregunta0')->findAll();

            $criteria = new CDbCriteria;
            $criteria->join='LEFT JOIN preguntas p ON p.idPregunta=t.idPregunta';
            $criteria->condition = 'idCandidato=:idCandidato';
            $criteria->params = array(':idCandidato' => $candidato->idCandidato);        
            $respuestas = Respuestas::model()->findAll($criteria);                  
            $cantidad_respuestas = count($respuestas);
            
            $criteria4 = new CDbCriteria;
            $criteria4->condition = 'idEncuesta=:idEncuesta';
            $criteria4->params = array(':idEncuesta' => $idEncuesta);
            $grupos = Grupospreguntas::model()->find($criteria4);         
            $idGrupo = $grupos->idGrupo;

            $criteria5 = new CDbCriteria;
            $criteria5->condition = 'idGrupo=:idGrupo';
            $criteria5->params = array(':idGrupo' => $idGrupo);
            $preguntas = Preguntas::model()->findAll($criteria5);           
            $cantidad_preguntas = count($preguntas);                    
            
            if($cantidad_respuestas == $cantidad_preguntas)
                $estado = 'Resuelta';
            elseif($cantidad_respuestas == null)
                 $estado = 'Sin_resolver'; 
            elseif($cantidad_respuestas < $cantidad_preguntas)
                 $estado = 'Incompleta';            
            
            $this->render('misEncuestas',array(
                'candidato'=>$candidato, 
                'encuestas'=>$encuestas,
                'estado'=>$estado
            ));            
        }
}

		//$model=new Respuestas('search');
		//$model->unsetAttributes();  
