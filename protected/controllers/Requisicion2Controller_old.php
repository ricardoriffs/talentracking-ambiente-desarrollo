<?php

class Requisicion2Controller extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */



	public function actioncrearejecucion()
	{
		
                $id = $_GET['id'];
                $modelACT=new Ejecucionactividad;                
		$modelACT->actividadflujoID=5;
     		$modelACT->fecha=new CDbExpression('NOW()');
               
 
		$model1=$this->loadModel($id);
                
                $modelACT->usuarioID=Yii::app()->user->name;
                $modelACT->observacion=$model1->Observaciones; 
		
                // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		//if(isset($_POST['Requisicion1']))
		//{
                      $model1->actividadID =5;   
                      if ($model1->save()){   
			 //$model1->attributes=$_POST['Requisicion1'];                        
                  $modelACT->RequisionID=$model1->RequisicionID;                          



            $modelRP=new requisicionproceso;                
		$modelRP->RequisicionID=$id;
            //TipoProcesoSeleccionID
            $modelRP->TipoProcesoSeleccionID=$model1->TipoProcesoSeleccionID;
            $modelRP->datosofertacargo=$model1->NombreCargo; 
            $modelRP->datosofertaunidadnegocio=$model1->UnidadNegocioID;
            $modelRP->datosofertareportaa  =$model1->JefeInmediatoVacante; 
            $modelRP->username  =$model1->UsuarioIDResponsable;
            $modelRP->save();

                         
			if($modelACT->save())
				//$this->redirect(array('view','id'=>$model1->RequisicionID));
                             $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin','view'=>subsubr4n1));
                      }              
                      //$model1=$this->update($id);
		//}
            //requisicionproceso
            
            
     		//$modelRP->fecha=new CDbExpression('NOW()');
      
		$this->render('update',array(
			'model'=>$model1,
		));

}










	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','crearejecucion'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Requisicion2;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requisicion2']))
		{
			$model->attributes=$_POST['Requisicion2'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->RequisicionID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requisicion2']))
		{
			$model->attributes=$_POST['Requisicion2'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->RequisicionID));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Requisicion2');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Requisicion2('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requisicion2']))
			$model->attributes=$_GET['Requisicion2'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Requisicion2::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requisicion2-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
