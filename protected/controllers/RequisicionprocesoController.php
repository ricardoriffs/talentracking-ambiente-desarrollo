<?php

class RequisicionprocesoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','uptstatus'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Requisicionproceso;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requisicionproceso']))
		{
			$model->attributes=$_POST['Requisicionproceso'];
                        
                       
			if($model->save())
				$this->redirect(array('view','id'=>$model->requisionprocesoID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requisicionproceso']))
		{
			$model->attributes=$_POST['Requisicionproceso'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->requisionprocesoID));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        public function actionUptstatus()
	{
                $cp = NULL;
                $idRP = $_GET['idRP'];
                $idP = $_GET['idP'];
                $idR = $_GET['idR'];
                $AC = $_GET['AC'];
                $cp = $_GET['cp'];
                $id1 = 'M1';

        if ($id1=='M1') {
            
            if($cp == 1){ 
                $criteria=new CDbCriteria;
                $criteria->compare('RequisicionID', $idR);
                $criteria->compare('requisionprocesoID', $idRP);                
                $criteria->compare('tipocomentarioID', '5.CIERRE PROCESO');               	
                $comentarios = Requisicionprocesocomentarios::model()->find($criteria);
                
                if(empty($comentarios))
                    $this->redirect(array('admin','cp'=>"MSJ"));         
            }
            
                $modelACT1=new Ejecucionactividad;
                $model=$this->loadModel($idRP);
                //echo $AC;

              if ($AC=='C') {
                if ($model->estado==0) {
                    $model->estado=1; 
                    $modelACT1->actividadflujoID=6; 
                } else { 
                  if ($model->estado==2) {
                       $model->estado=1; 
                       $modelACT1->actividadflujoID=6; 
                   } 
                 } 
                }   
                

              if ($AC=='F') {
                if ($model->estado==0) {
                    $model->estado=2; 
                    $modelACT1->actividadflujoID=7; 
                } else { 
                  if ($model->estado==2) {
                       $model->estado=0; 
                       $modelACT1->actividadflujoID=5; 
                   } 
                 } 
                }   

	           
                        //$modelACT1->actividadflujoID=6; 
	     		$modelACT1->fecha=new CDbExpression('NOW()');
        	        $modelACT1->RequisionID = (int) $idR; 
                        $usuario = Yii::app()->user->name;
                        
                        //echo $usuario;
 
               		 //$modelACT->requisicionactividadID = 0;
                	$modelACT1->usuarioID=$usuario;
                	$modelACT1->observacion="CERRADA POR COORDINADOR"; 
                         		
                       $modelACT1->save();
                       $errors = $modelACT1->getErrors();
                       //echo $errors;  

		//**************************************************


               
		//**************************************************


        $resultsR = Requisicion::model()->findAll(array('condition'=> "RequisicionID =" . $idR ));
        
        $continue = 0; 
        foreach ($resultsR as $resultR)
        {

          //echo $resultR->actividadID . "Req " . $resultR->RequisicionID; 
          //$resultR->actividadID=$resultR->actividadID+1;
          //echo $resultR->actividadID . "Req " . $resultR->RequisicionID;    
          $resultR->save();
          $errors = $resultR->getErrors();
          //echo $errors;  
  
         
 
         }  









        $results = Procesos::model()->findAll(array('condition'=> "id ='" . $idP . "'" ,'order' => 'id ASC'));
        
        $continue = 0; 
        foreach ($results as $result)
        {
        /*if ($model->estado==1) {
          $result->activo = "N";
         } 
        if ($model->estado==2) {
          $result->activo = "N";
         } 


         if ($model->estado== 0)  {
          $result->activo = "S";
         }  
         */

         $result->activo = "N";

             if ($result->save())
                     $continue = 1;

         }  



       




              //  $modelPROCESO=$proceso->loadModel($idP);; 

                  if($model->save())
				$this->redirect(array('admin'));
                  else 
	                        $this->redirect(array('admin','id1'=>"ERROR"));
         
  }else { 

       $this->redirect(array('admin','id1'=>"MSJ"));
   }



           	
}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Requisicionproceso');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Requisicionproceso('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requisicionproceso']))
			$model->attributes=$_GET['Requisicionproceso'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Requisicionproceso::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requisicionproceso-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
  }