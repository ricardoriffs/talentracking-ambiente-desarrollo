<?php

class AdminController extends Controller
{
    public function actionSuggestCity()
    {
        $res = array();
        $term = Yii::app()->getRequest()->getParam('q', false);
        if ($term) {
            // test table is for the sake of this example
            $sql = "SELECT t1.city_id id, CONCAT( t1.city_name , ' (' , t2.country_name , ')' ) label "
                    . "FROM prm_city t1 "
                    . "JOIN prm_country t2 ON t2.country_id = t1.country_id "
                    . "WHERE LCASE(t1.city_name) LIKE :name";
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

}
