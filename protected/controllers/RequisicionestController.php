<?php

class RequisicionestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	
    public function actionOneRowspan()
    {
        $dp = new CActiveDataProvider('Requisicionest', array(
            'sort'=>array(
                'attributes'=>array(
                      'UnidadNegocioID',
                 ),
                 'defaultOrder' => 'UnidadNegocioID',  
            ),
            'pagination' => array(
              'pagesize' => 30,
            ),
        )); 
        
        //$this->render('view', array('dp' => $dp));
        $this->render('admin',array(
			'model'=>$model,
		)); 

        //$this->render('admin',array(
	//		'dp'=>$dp
	//	));
    } 


    public function actionOneRowspan2()
    {
        $dp = new CActiveDataProvider('Requisicionest', array(
            'sort'=>array(
                'attributes'=>array(
                      'UnidadNegocioID',
                 ),
                 'defaultOrder' => 'UnidadNegocioID',  
            ),
            'pagination' => array(
              'pagesize' => 30,
            ),
        )); 
        
        $this->render('onerowspan2', array('dp' => $dp));
    } 



      public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','OneRowspan','OneRowspan2','consultar'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Requisicionest;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requisicionest']))
		{
			$model->attributes=$_POST['Requisicionest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->RequisicionID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                //echo "ENTRE ADMIN".$model->f1;
		if(isset($_POST['Requisicionest']))
		{
			$model->attributes=$_POST['Requisicionest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->RequisicionID));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Requisicionest');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.


	 */


        public function actionConsultar()
	{


            $model=new Requisicionest;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='consulta-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['Requisicionest']))
		{
			$model->attributes=$_POST['Requisicionest'];
			
				//$this->redirect(Yii::app()->user->returnUrl);
                                //echo Yii::app()->baseUrl."/index.php?r=requisicionest/admin&view=".$model->fid."&f1=".$model->f1."&f2=".$model->f2;
                                $this->redirect(Yii::app()->baseUrl."/index.php?r=requisicionest/admin&view=".$model->fid."&f1=".$model->f1."&f2=".$model->f2);

		}
		// display the login form
		//$this->render('admin',array(
		//	'model'=>$model,'view'=>$id,'f1'=>$model->f1,'f2'=>$f2
		//));
	}	


         
	public function actionAdmin()
	{
		$model=new Requisicionest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requisicionest']))
			$model->attributes=$_GET['Requisicionest'];
                //echo "ENTRE ADMIN".$model->f1;
                //$f1="20120701"; 
                $i++; 
		$this->render('admin',array(
			'model'=>$model,'view'=>$id,'f1'=>$f1,'f2'=>$f2
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Requisicionest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requisicionest-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
