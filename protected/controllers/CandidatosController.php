<?php

class CandidatosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('indexCandidatos', 'registrarCandidatos', 'actualizarCandidatos', 'agradecimiento', 'verificarRegAnteriorCandidato'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update', 'enviarCorreo', 'verHojaVida',
                    'verProceso', 'verPrefiltro', 'verCandidatosPruebas', 'verCandidatosEntrevista', 'verCandidatosElegidos',
                    'ajaxEnviarPruebas', 'ajaxCitarEntrevista', 'ajaxElegirCandidatos'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Candidatos;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Candidatos'])) {
            $model->attributes = $_POST['Candidatos'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->idCandidato));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Candidatos'])) {
            $model->attributes = $_POST['Candidatos'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->idCandidato));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Candidatos');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Candidatos('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Candidatos']))
            $model->attributes = $_GET['Candidatos'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Candidatos::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function cargarModeloCandidatos($id) {
        $model = RegistroCandidatos::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'candidatos-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionRegistrarCandidatos() {

        $model = new RegistroCandidatos;
        $model->unsetAttributes();
        $obj_candidato = NULL;

        if (isset($_POST['RegistroCandidatos'])) {

            $model->attributes = $_POST['RegistroCandidatos'];
//                        $model->foto = CUploadedFile::getInstance($this, 'foto');
//                        $hojavida = CUploadedFile::getInstance($this, 'hojavida');
//                        $formaprobacion = CUploadedFile::getInstance($this, 'formaprobacion');

            $num_Proceso = $_POST['RegistroCandidatos']['id_proceso'];

            $criteria = new CDbCriteria;
            $criteria->condition = 'ofertaid=:ofertaid';
            $criteria->params = array(':ofertaid' => $num_Proceso);
            $oferta = Procesos::model()->find($criteria);

            $tipoContratoID = $model->tipoContratoID;

            if ($tipoContratoID <= 6)
                $model->tipocontrato = 'Interno';
            elseif ($tipoContratoID >= 7)
                $model->tipocontrato = 'Externo';

            if ($model->validate()) {

                $model->id_proceso = $oferta->id;

                if ($model->identificacion) {

                    $id = $model->identificacion;
                    $obj_candidato = $this->verificarRegAnteriorCandidato($id);

                    $proceso_anterior = Procesos::model()->findByPk($obj_candidato->id_proceso);

                    if ($obj_candidato != NULL) {

                        $estado = 0;

                        if ($obj_candidato->estado == 0) {
                            $estado = 'Registrado';
                        } elseif ($obj_candidato->estado == 1) {
                            $estado = 'Pre-seleccionado';
                        } elseif ($obj_candidato->estado == 2) {
                            $estado = 'Descartado';
                        } elseif ($obj_candidato->estado == 3) {
                            $estado = 'Para entrevista';
                        } elseif ($obj_candidato->estado == 4) {
                            $estado = 'Elegible';
                        } elseif ($obj_candidato->estado == 5) {
                            $estado = 'Contratado';
                        }
                    }
                }
                if ($model->save(false)) {

                    //$rnd = rand(0,9999);
                    $fileName = $model->idCandidato . "-" . $model->nombre_foto;
                    $model->ft->saveAs(Yii::app()->basePath . '/../upload/fotos/' . $fileName);

                    $fileName = $model->idCandidato . "-" . $model->nombre_hojavida;
                    $model->hv->saveAs(Yii::app()->basePath . '/../upload/hojasvida/' . $fileName);

                    $MetadatosRel = $_POST['MetadatosRel'];
                    $MetadatosAdi = $_POST['MetadatosAdi'];

                    for ($row = 0; $row < count($MetadatosRel, 0); $row++) {

                        $valor = $MetadatosRel[$row]['valor'];

                        $Metadatos = new Metadatos;
                        $Metadatos->idcandidatos = $model->idCandidato;
                        $Metadatos->tipocampo = 1;
                        $Metadatos->valor = $valor;
                        $Metadatos->save();
                    }

                    for ($row = 0; $row < count($MetadatosAdi, 0); $row++) {

                        $valor = $MetadatosAdi[$row]['valor'];

                        $Metadatos = new Metadatos;
                        $Metadatos->idcandidatos = $model->idCandidato;
                        $Metadatos->tipocampo = 2;
                        $Metadatos->valor = $valor;
                        $Metadatos->save();
                    }

                    $this->render('verRegCandidatos', array(
                        'model' => $model,
                        'oferta' => $oferta,
                        'obj_candidato' => $obj_candidato,
                        'proceso' => $proceso_anterior,
                        'estado' => $estado,
                    ));
                    Yii::app()->end();
                }
            }
        }

        $this->render('crearCandidatos', array(
            'model' => $model,
        ));
    }

    public function actionActualizarCandidatos($id) {

        $model = $this->cargarModeloCandidatos($id);
        $obj_candidato = NULL;

        $Oferta = Procesos::model()->findByPk($model->id_proceso);
        $model->id_proceso = $Oferta->ofertaid;

        if (isset($_POST['RegistroCandidatos'])) {

            $model->attributes = $_POST['RegistroCandidatos'];
            $model->foto = CUploadedFile::getInstance($this, 'foto');
            $model->hojavida = CUploadedFile::getInstance($this, 'hojavida');
            $model->formaprobacion = CUploadedFile::getInstance($this, 'formaprobacion');

            $num_Proceso = $_POST['RegistroCandidatos']['id_proceso'];
            $criteria = new CDbCriteria;
            $criteria->condition = 'ofertaid=:ofertaid';
            $criteria->params = array(':ofertaid' => $num_Proceso);
            $oferta = Procesos::model()->find($criteria);

            $tipoContratoID = $model->tipoContratoID;

            if ($tipoContratoID <= 6)
                $model->tipocontrato = 'Interno';
            elseif ($tipoContratoID >= 7)
                $model->tipocontrato = 'Externo';

            if ($model->validate()) {

                $model->id_proceso = $oferta->id;

                if ($model->identificacion) {

                    $id = $model->identificacion;
                    $obj_candidato = $this->verificarRegAnteriorCandidato($id);

                    $proceso_anterior = Procesos::model()->findByPk($obj_candidato->id_proceso);

                    if ($obj_candidato != NULL) {

                        $estado = 0;

                        if ($obj_candidato->estado == 0) {
                            $estado = 'Registrado';
                        } elseif ($obj_candidato->estado == 1) {
                            $estado = 'Pre-seleccionado';
                        } elseif ($obj_candidato->estado == 2) {
                            $estado = 'Descartado';
                        } elseif ($obj_candidato->estado == 3) {
                            $estado = 'Para entrevista';
                        } elseif ($obj_candidato->estado == 4) {
                            $estado = 'Elegible';
                        } elseif ($obj_candidato->estado == 5) {
                            $estado = 'Contratado';
                        }
                    }
                }
                if ($model->save(false)) {

                    $fileName = $model->idCandidato . "-" . $model->nombre_foto;
                    $model->ft->saveAs(Yii::app()->basePath . '/../upload/fotos/' . $fileName);

                    $fileName = $model->idCandidato . "-" . $model->nombre_hojavida;
                    $model->hv->saveAs(Yii::app()->basePath . '/../upload/hojasvida/' . $fileName);

                    $MetadatosRel = $_POST['MetadatosRel'];
                    $MetadatosAdi = $_POST['MetadatosAdi'];

                    if ($MetadatosRel) {

                        Metadatos::model()->deleteAll(array('condition' => 'idcandidatos=:idcandidatos', 'params' => array(':idcandidatos' => $model->idCandidato)));

                        for ($row = 0; $row < count($MetadatosRel, 0); $row++) {

                            $valor = $MetadatosRel[$row]['valor'];

                            $Metadatos = new Metadatos;
                            $Metadatos->idcandidatos = $model->idCandidato;
                            $Metadatos->tipocampo = 1;
                            $Metadatos->valor = $valor;
                            $Metadatos->save();
                        }
                    }

                    if ($MetadatosAdi) {

                        for ($row = 0; $row < count($MetadatosAdi, 0); $row++) {

                            $valor = $MetadatosAdi[$row]['valor'];

                            $Metadatos = new Metadatos;
                            $Metadatos->idcandidatos = $model->idCandidato;
                            $Metadatos->tipocampo = 2;
                            $Metadatos->valor = $valor;
                            $Metadatos->save();
                        }
                    }
                    $this->render('verRegCandidatos', array(
                        'model' => $model,
                        'oferta' => $oferta,
                        'obj_candidato' => $obj_candidato,
                        'proceso' => $proceso_anterior,
                        'estado' => $estado,
                    ));
                    Yii::app()->end();
                }
            }
        }

        $this->render('actualizarCandidatos', array(
            'model' => $model
        ));
    }

    public function actionAgradecimiento($id) {

//                $idCandidato = $_GET['id'];
        //$model = Candidatos::model()->findByPk($idCandidato);
        $model = $this->cargarModeloCandidatos($id);
        $ident = $model->identificacion;
        $model->estado = 0;
        $model->save(false);

        $candidatos = RegistroCandidatos::model()->findAll(array('condition' => 'identificacion=:identificacion', 'params' => array(':identificacion' => $ident)));

        foreach ($candidatos as $candidato):
            if ($candidato->idCandidato != $model->idCandidato)
                $candidato->estado = 2;
            $candidato->save(false);
        endforeach;

        $idOferta = $_GET['of'];
        $oferta = Procesos::model()->findByPk($idOferta);

        $this->render('agradecimiento', array(
            'model' => $model,
            'oferta' => $oferta,
        ));
    }

    public function actionEnviarCorreo() {

        $idCandidato = $_GET['id'];
        $model = $this->loadModel($idCandidato);

        $para = $model->mail1 . '; fgutierrez@talentracking.com';
        //$para      = $enviadoa.';plataforma@seleccionpacific.com;dpacheco@pacificrubiales.com.co';
        $titulo = 'Felicitaciones. Usted continua a la siguiente etapa del proceso de selección para el cargo "' . $model->nombreCargo . '"';
        $mensaje = "\r\n" . '
            Estimado  ' . $model->nombres . "\r\n" . "\r\n" . '
            Para Pacific Rubiales Energy, ha sido un gusto poder contar con usted como
            participante en el proceso de selección para el cargo "' . $model->nombreCargo . '",
            que nos ha permitido identificar, en su caso, importantes cualidades,
            conocimientos y experiencia de valor para la organización.  A partir de
            este momento, el área de Selección finaliza las labores de diagnóstico
            y evaluación, considerándolo a usted como uno de los finalistas dentro del proceso.
            En calidad de candidato elegible, le remitimos ahora a la fase que cubre el
            área de Contratación, quien lo contactará para realizar actividades
            asociadas a exámenes y solicitud de documentos adicionales que permitan
            tener una definición de su posible vinculación a la compañía, tema que no
            quedará determinado hasta que dicha área no finalice su proceso de análisis.
            ' . "\r\n" . "\r\n" . '
            Agradecemos el tiempo que nos ha brindado para permitir conocerle y la
            aceptación de nuestra invitación a hacer parte del proceso.  Esperamos que
            el mismo le haya aportado una visión general sobre nuestra organización y su
            interés de construir cada vez un mejor futuro para Colombia y nuestras zonas
            de operación.
            ' . "\r\n" . "\r\n" . '
            El área de Contratación lo estará contactando próximamente para definir los
            próximos pasos en su proceso de evaluación.
            ' . "\r\n" . "\r\n" . '
            Reciba un cordial saludo,
            ' . "\r\n" . '
            Selección Corporativa – Gerencia de Desarrollo Organizacional
            ' . "\r\n" . '
            Pacific Rubiales Energy';
        $cabeceras = 'From: plataforma@seleccionpacific.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

        mail($para, $titulo, $mensaje, $cabeceras);

        $this->render('envioExitoso');
    }

    public function actionIndexCandidatos() {

        $this->render('indexCandidatos');
    }

    public function verificarRegAnteriorCandidato($id) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'identificacion=:identificacion';
        $criteria->params = array(':identificacion' => $id);
        $identi_repetida = RegistroCandidatos::model()->find($criteria);

        return $identi_repetida;
    }

    public function actionVerProceso() {

        $app = NULL;
        $id_proceso = NULL;

        $app = $_GET["APP"];
        $id_proceso = $_GET["idP"];

        $proceso = Procesos::model()->findByPk($id_proceso);

        $candidatos = new RegistroCandidatos('search');
        $candidatos->unsetAttributes();  // clear any default values
        if (isset($_GET['RegistroCandidatos']))
            $candidatos->attributes = $_GET['RegistroCandidatos'];

        $this->render('verProceso', array(
            'candidatos' => $candidatos,
            'proceso' => $proceso,
        ));
    }

    public function actionVerPreFiltro() {

        $app = NULL;
        $id_proceso = NULL;

        if (filter_has_var(INPUT_GET, "APP")) {
            $app = filter_input(INPUT_GET, "APP");
        }

        $id_proceso = $_GET["idP"];
        $conteo_estado = array();
        $estado = 0;

        $proceso = Procesos::model()->findByPk($id_proceso);

        $candidatos = new RegistroCandidatos('search_prefiltro');
        $candidatos->unsetAttributes();

        while ($estado <= 5) {
            $conteo_estado[$estado] = RegistroCandidatos::model()->count(array(
                'condition' => "id_proceso ='" . $id_proceso . "' and estado ='" . $estado . "'"
            ));


            $estado++;
        }

        if (isset($_GET['RegistroCandidatos']))
            $candidatos->attributes = $_GET['RegistroCandidatos'];

        $this->render('verPrefiltro', array(
            'candidatos' => $candidatos,
            'proceso' => $proceso,
            'conteo_estado' => $conteo_estado,
        ));
    }

    public function actionVerCandidatosPruebas() {

        $app = NULL;
        $id_proceso = NULL;

        $app = $_GET["APP"];
        $id_proceso = $_GET["idP"];
        $conteo_estado = array();
        $estado = 1;
        $flujo = $estado;

        $proceso = Procesos::model()->findByPk($id_proceso);

        $candidatos = new RegistroCandidatos('searchFlujoSeleccion');
        $candidatos->unsetAttributes();

        $report_candidato = RegistroCandidatos::model()->findall(array('condition' => "id_proceso ='" . $id_proceso . "' and estado ='" . $estado . "'"));

        while ($estado <= 3) {
            if ($estado != 2)
                $conteo_estado[$estado] = RegistroCandidatos::model()->count(array('condition' => "id_proceso ='" . $id_proceso . "' and estado ='" . $estado . "'"));
            $estado++;
        }

        if (isset($_GET['RegistroCandidatos']))
            $candidatos->attributes = $_GET['RegistroCandidatos'];

        $this->render('verCandidatosPruebas', array(
            'proceso' => $proceso,
            'candidatos' => $candidatos,
            'report_candidato' => $report_candidato,
            'conteo_estado' => $conteo_estado,
            'flujo' => $flujo,
        ));
    }

    public function actionVerCandidatosEntrevista() {

        $app = NULL;
        $id_proceso = NULL;

        $app = $_GET["APP"];
        $id_proceso = $_GET["idP"];
        $conteo_estado = array();
        $estado = 3;
        $flujo = $estado;

        $proceso = Procesos::model()->findByPk($id_proceso);

        $candidatos = new RegistroCandidatos('searchFlujoSeleccion');
        $candidatos->unsetAttributes();

        while ($estado <= 4) {
            $conteo_estado[$estado] = RegistroCandidatos::model()->count(array('condition' => "id_proceso ='" . $id_proceso . "' and estado ='" . $estado . "'"));
            $estado++;
        }

        if (isset($_GET['RegistroCandidatos']))
            $candidatos->attributes = $_GET['RegistroCandidatos'];

        $this->render('verCandidatosEntrevista', array(
            'proceso' => $proceso,
            'candidatos' => $candidatos,
            'conteo_estado' => $conteo_estado,
            'flujo' => $flujo,
        ));
    }

    public function actionVerCandidatosElegidos() {

        $app = NULL;
        $id_proceso = NULL;

        $app = $_GET["APP"];
        $id_proceso = $_GET["idP"];
        $conteo_estado = array();
        $estado = 4;
        $flujo = $estado;

        $proceso = Procesos::model()->findByPk($id_proceso);

        $candidatos = new RegistroCandidatos('searchFlujoSeleccion');
        $candidatos->unsetAttributes();

        if ($estado == 4) {
            $conteo_estado[$estado] = RegistroCandidatos::model()->count(array('condition' => "id_proceso ='" . $id_proceso . "' and estado ='" . $estado . "'"));
        }

        if (isset($_GET['RegistroCandidatos']))
            $candidatos->attributes = $_GET['RegistroCandidatos'];

        $this->render('verCandidatosElegidos', array(
            'proceso' => $proceso,
            'candidatos' => $candidatos,
            'conteo_estado' => $conteo_estado,
            'flujo' => $flujo,
        ));
    }

    public function actionAjaxEnviarPruebas() {

        $act = $_GET['act'];
        $candidatos = array();
        $candidatos = $_POST['idCandidatos'];
        $idProceso = $_GET['idP'];

        if (isset($candidatos)) {

            foreach ($candidatos as $key => $candidato) {

                $model = $this->cargarModeloCandidatos($candidato);
                if ($act == 'doEnviarPruebas')
                    $model->estado = 1;
                if ($act == 'doDescartar')
                    $model->estado = 2;

                if ($model->save(false)) {
                    /* @var $candidato Candidatos */

//                        print_r($candidatos);
//                        print implode(',',$candidatos);
                } else
                    throw new Exception("Lo sentimos, no fué posible realizar la acción", 500);
            }
            $proceso = $model->id_proceso;
            $this->redirect("index.php?r=candidatos/verPrefiltro&idP=$proceso");
        } else
            $this->redirect("index.php?r=candidatos/verPrefiltro&idP=$idProceso");
        //throw new Exception("Lo sentimos, no fué posible realizar la acción",500);
    }

    public function actionAjaxCitarEntrevista() {

        $act = $_GET['act'];
        $candidatos = array();
        $candidatos = $_POST['idCandidatos'];
        $idProceso = $_GET['idP'];

        if (isset($candidatos)) {

            foreach ($candidatos as $key => $candidato) {

                $model = $this->cargarModeloCandidatos($candidato);
                if ($act == 'doCitarEntrevista')
                    $model->estado = 3;
                if ($act == 'doDescartar')
                    $model->estado = 2;

                if ($model->save(false)) {
                    /* @var $candidato Candidatos */

//                        print_r($candidatos);
//                        print implode(',',$candidatos);
                } else
                    throw new Exception("Lo sentimos, no fué posible realizar la acción", 500);
            }
            $proceso = $model->id_proceso;
            $this->redirect("index.php?r=candidatos/verCandidatosPruebas&idP=$proceso");
        } else
            $this->redirect("index.php?r=candidatos/verCandidatosPruebas&idP=$idProceso");
    }

    public function actionAjaxElegirCandidatos() {

        $act = $_GET['act'];
        $candidatos = array();
        $candidatos = $_POST['idCandidatos'];
        $idProceso = $_GET['idP'];

        if (isset($candidatos)) {

            foreach ($candidatos as $key => $candidato) {

                $model = $this->cargarModeloCandidatos($candidato);
                if ($act == 'doElegirCandidatos')
                    $model->estado = 4;
                if ($act == 'doDescartar')
                    $model->estado = 2;

                if ($model->save(false)) {
                    /* @var $candidato Candidatos */

//                        print_r($candidatos);
//                        print implode(',',$candidatos);
                } else
                    throw new Exception("Lo sentimos, no fué posible realizar la acción", 500);
            }
            $proceso = $model->id_proceso;
            $this->redirect("index.php?r=candidatos/verCandidatosEntrevista&idP=$proceso");
        } else
            $this->redirect("index.php?r=candidatos/verCandidatosEntrevista&idP=$idProceso");
    }

    public function actionVerHojaVida($id) {
        /* @var $candidato Candidatos */
        $candidato = $this->loadModel($id);

        header("Content-type: " . $candidato->tipo_hojavida);
        header("Content-Disposition: attachment; filename="
                . $candidato->nombre_hojavida);
        die($candidato->hojavida);
    }

}
