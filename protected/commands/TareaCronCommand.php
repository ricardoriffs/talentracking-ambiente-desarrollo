<?php

class TareaCronCommand extends CConsoleCommand {
/* Declaramos propiedades, etc */
    
    public function run($argumentos) {
        
////////////////////////

        $diaNow = date('d-m-Y');        
        
        $criteria=new CDbCriteria;                      
        $match = 'gerente';  
        $match1 = 'liderseleccion';        
        $criteria->addSearchCondition('rol', $match);
        $criteria->addSearchCondition('rol', $match1 , true, 'OR');        
        $gerentes = Usuario::model()->findall($criteria);   
        
        foreach ($gerentes as $gerente):
            
            $criteria2=new CDbCriteria;   
            if(strpos($gerente->username, "gerente") !== false):        
                $criteria2->condition='actividadID<=3';                   
                $criteria2->compare('UsuarioIDResponsable',$gerente->username); 
            endif;
            if(strpos($gerente->username, "liderseleccion") !== false):
                $criteria2->condition='actividadID=4';                     
            endif;            
            $requis_gerente = Requisicion::model()->findall($criteria2);
            $i = 0;
            $array_requi = array();                
            
            foreach ($requis_gerente as $requi_gerente):
                $fecha_creacion = $requi_gerente->fechacreacion;      
        
//                $diaf = date("d-m-Y",strtotime($fecha_creacion));
                  $fecha = date_create($fecha_creacion);
                  $diaf = date_format($fecha, 'd-m-Y'); 
//                $diaf = "20-03-2013";
//                $difdias = (($diaNow) - ($diaf))/86400;
                $difdias = floor((strtotime($diaNow)-strtotime($diaf))/86400);
                
                if($difdias > 5):
                    
//                    $array_requi[$i][0] = $requi_gerente->RequisicionID;
//                    $array_requi[$i][1] = $requi_gerente->UnidadNegocio->Nombre;
//                    $array_requi[$i][2] = $requi_gerente->AreaVicepresidencia->Nombre;
//                    $array_requi[$i][3] = $requi_gerente->fechacreacion; 	
//                    $array_requi[$i][4] = $requi_gerente->usuariocreacion;
//                    $array_requi[$i][5] = $requi_gerente->NombreCargo;
//                    
                      $array_requi[$i] = '
                              <tr>  
                                <td>'.$requi_gerente->RequisicionID.'</td>
                                <td>'.$requi_gerente->unidadnegocio->Nombre.'</td>
                                <td>'.$requi_gerente->areavicepresidencia->Nombre.'</td>
                                <td>'.$requi_gerente->fechacreacion.'</td>
                                <td>'.$requi_gerente->usuariocreacion.'</td>  
                                <td>'.$requi_gerente->NombreCargo.'</td>                                      
                                <td>'.$difdias.'</td>                                      
                              </tr>
                            ';
                
    //                $array_correos = usuario::model()->find(array('condition'=> "username ='" . $gerentes->UsuarioIDResponsable . "'"));
                    $i++;
                endif;
                
            endforeach;
            
            if($i > 0):
                $para = $gerente->mail.';fgutierrez@talentracking.com';
                $titulo = 'Usted tiene requisiciones vencidas por procesar - www.seleccionpacific.com';
                $mensaje = "\r\n" . 'Las siguientes requisiciones tienen más de 5 días de haber sido asignadas:' . "\r\n" . "\r\n" .
                    '<table border="1">
                        <tr>
                            <th>Requisición ID</th>
                            <th>Unidad Negocio</th>
                            <th>Área Vicepresidencia</th>
                            <th>Fecha Creación</th>
                            <th>Usuario Creación</th>
                            <th>Nombre Cargo</th>
                            <th>Días asignado</th>
                        </tr>
                        '.
                           implode("", $array_requi)
                         .'
                    </table>'. "\r\n" . 'Nota: Por favor acceda al Sistema www.seleccionpacific.com para procesarlas';
                $cabeceras = 'From: plataforma@seleccionpacific.com' . "\r\n" .
                'MIME-Version: 1.0' . "\r\n" .     
                'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                mail($para, $titulo, $mensaje, $cabeceras);
            endif;
            
        endforeach;            
///////////////////////////////////            
        
    }
} 

?>