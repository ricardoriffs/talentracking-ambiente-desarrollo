<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'3t Console Application',
	// application components
	'components'=>array(
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=mysql.talentracking.com;port=3306;dbname=humantra_encuestas',
			'emulatePrepare' => true,
			'username' => 'utalent',
			'password' => 'jamahure4',
			'charset' => 'utf8',
		),            
	),
);