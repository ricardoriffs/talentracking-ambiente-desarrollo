<?php
/* @var $this GrupospreguntasController */
/* @var $model Grupospreguntas */

$this->breadcrumbs=array(
	'Grupospreguntases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Grupospreguntas', 'url'=>array('index')),
	array('label'=>'Manage Grupospreguntas', 'url'=>array('admin')),
);
?>

<h1>Create Grupospreguntas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>