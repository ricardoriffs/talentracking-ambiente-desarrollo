<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>


<?php
$this->breadcrumbs=array(
	'CompetenciasEntrevistaes'=>array('index'),
	'Create',
);
?>

<!--<h1>Asignar Competencias para la Entrevista</h1>-->

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'CompetenciasEntrevista-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con asterisco <span class="required">(*)</span> son requeridos.</p>

<p><?php echo 'NOTA: Asigne máximo 3 competencias para construir el formulario de entrevista.'; ?></p>

	<?php echo $form->errorSummary($model); ?>
 <div class = "divContentBlue"> 
        <div style="float: right; padding: 3px;">
            <span style="font-weight: bold;">Competencias Asignadas</span>
            <table>
                <?php foreach ($competencias as $i => $competencia): ?>
                        <tr>
                            <td><?php echo ++$i.'.'. $competencia->idGrupo0->nombre; ?></td>
                            <td><div class="remove"><?php echo CHtml::link(CHtml::encode('Borrar'),array('deleteCompetencia', 'id'=>$competencia->idCompetenciasEntrevista));?></div></td>
                        </tr>              
                <?php endforeach; ?>
            </table>                           
        </div>   
	<div class="row">
		<?php echo $form->labelEx($model,'idGrupo'); ?>
		<?php echo $form->dropdownlist(
                            $model,
                            'idGrupo',
                            CHtml::listData(Grupospreguntas::model()->findAll(array('condition'=> "idEncuesta =75",'order' => 'orden ASC')), 'idGrupo', 'nombre'),
                            //array('onChange' => 'javascript:MostrarValidadores(this.selectedIndex)', 'empty'=>'-- Seleccione una opción --')
							//array(),
							array('empty'=>'-- Seleccione una opción --',
								   'ajax'=>array(
									  'type'=>'POST',								  
									  //'url'=>CController::createUrl('Grupospreguntas/obtenerValidadores'),
									  //'dataType'=>'json',
									  //'data' => array('idGrupo' => 'js:$(this).val()'),										  
									  //'submit'=>array('Grupospreguntas/obtenerValidadores'),
									  //'update'=>'#description_id',
                                                                          //'success'=>'function(){alert("ok");}',
									  'onChange' => 'js:MostrarValidadores("description_id"+$(this).val())',
                                                                          'success'=>'js:reloadGrid()',
									  //'onChange' => 'js:alert(data)',	
									  //'success' => 'js:alert(data)',
									  //'error'=> 'function(){alert("AJAX call error..!!!!!!!!!!");}',									  
							)) 							
                        );               
                ?>
		<?php echo $form->error($model,'idGrupo'); ?>
	</div>     
	<div class="row">
		<?php //echo $form->labelEx($model,'idCompania'); ?>
		<?php /*echo $form->dropdownlist(
                            $model,
                            'idCompania',
                            CHtml::listData(Companias::model()->findAll(), 'CompaniaID', 'Nombre'),
                            array('empty'=>'-- Seleccione una opción --')
                        );*/ 
                        //echo $form->hiddenField($model,'idCompania',array('size'=>10,'maxlength'=>10));
                ?>
		<?php //echo $form->error($model,'idCompania'); ?>
	</div>  
        <br/>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Agregar'); ?>
	</div>    
 </div>

<?php $this->endWidget(); ?>

<div id="description_id358" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(358); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id359" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(359); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>

<div id="description_id360" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(360); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>

<div id="description_id361" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(361); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>

<div id="description_id362" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(362); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id363" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(363); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id364" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(364); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id365" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(365); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id366" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(366); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id367" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(367); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id368" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(368); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id369" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(369); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id370" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(370); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>
<div id="description_id371" class="grid-view" style="display:none; margin-top: 15px;">
   <table class="items">
       <?php $competencia = Grupospreguntas::model()->findByPk(371); ?>
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>           			
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>			
        </tr> 
        <?php endforeach; ?>
    </table>    
</div>

<script>
function MostrarValidadores(id)
{  
    var allDivs = document.getElementsByClassName('grid-view');
    for(var i=0;i<allDivs.length;i++){
     allDivs[i].style.display = 'none';
    }
    
    document.getElementById(id).style.display = "block";    
    //var div = document.getElementById(id);
    //div.style.display = (div.style.display=='block'?'none':'block');
} 

function reloadGrid() {
    $.fn.yiiGridView.update('CompetenciasEntrevista-grid');
//    $.fn.yiiGridView.update('candidatos-grid', {
//		data: $(this).serialize()
//	});
//        	return false;
}
</script>     