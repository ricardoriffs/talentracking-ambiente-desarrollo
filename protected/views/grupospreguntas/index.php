<?php
/* @var $this GrupospreguntasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Grupospreguntases',
);

$this->menu=array(
	array('label'=>'Create Grupospreguntas', 'url'=>array('create')),
	array('label'=>'Manage Grupospreguntas', 'url'=>array('admin')),
);
?>

<h1>Grupospreguntases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
