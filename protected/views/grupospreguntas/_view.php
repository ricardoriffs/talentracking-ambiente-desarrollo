<?php
/* @var $this GrupospreguntasController */
/* @var $data Grupospreguntas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idGrupo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idGrupo), array('view', 'id'=>$data->idGrupo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEncuesta')); ?>:</b>
	<?php echo CHtml::encode($data->idEncuesta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preguntaSugerida')); ?>:</b>
	<?php echo CHtml::encode($data->preguntaSugerida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orden')); ?>:</b>
	<?php echo CHtml::encode($data->orden); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('borrado')); ?>:</b>
	<?php echo CHtml::encode($data->borrado); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::encode($data->CompaniaID); ?>
	<br />

	*/ ?>

</div>