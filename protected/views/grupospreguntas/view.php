<?php
/* @var $this GrupospreguntasController */
/* @var $model Grupospreguntas */

$this->breadcrumbs=array(
	'Grupospreguntases'=>array('index'),
	$model->idGrupo,
);

$this->menu=array(
	array('label'=>'List Grupospreguntas', 'url'=>array('index')),
	array('label'=>'Create Grupospreguntas', 'url'=>array('create')),
	array('label'=>'Update Grupospreguntas', 'url'=>array('update', 'id'=>$model->idGrupo)),
	array('label'=>'Delete Grupospreguntas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idGrupo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Grupospreguntas', 'url'=>array('admin')),
);
?>

<h1>View Grupospreguntas #<?php echo $model->idGrupo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idGrupo',
		'idEncuesta',
		'nombre',
		'descripcion',
		'preguntaSugerida',
		'orden',
		'borrado',
		'CompaniaID',
	),
)); ?>
