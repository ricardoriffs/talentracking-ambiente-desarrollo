<?php
/* @var $this GrupospreguntasController */
/* @var $model Grupospreguntas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'grupospreguntas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idEncuesta'); ?>
		<?php echo $form->textField($model,'idEncuesta'); ?>
		<?php echo $form->error($model,'idEncuesta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'preguntaSugerida'); ?>
		<?php echo $form->textArea($model,'preguntaSugerida',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'preguntaSugerida'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orden'); ?>
		<?php echo $form->textField($model,'orden'); ?>
		<?php echo $form->error($model,'orden'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'borrado'); ?>
		<?php echo $form->textField($model,'borrado',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'borrado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CompaniaID'); ?>
		<?php echo $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'CompaniaID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->