<?php
/* @var $this GrupospreguntasController */
/* @var $model Grupospreguntas */

$this->breadcrumbs=array(
	'Grupospreguntases'=>array('index'),
	$model->idGrupo=>array('view','id'=>$model->idGrupo),
	'Update',
);

$this->menu=array(
	array('label'=>'List Grupospreguntas', 'url'=>array('index')),
	array('label'=>'Create Grupospreguntas', 'url'=>array('create')),
	array('label'=>'View Grupospreguntas', 'url'=>array('view', 'id'=>$model->idGrupo)),
	array('label'=>'Manage Grupospreguntas', 'url'=>array('admin')),
);
?>

<h1>Update Grupospreguntas <?php echo $model->idGrupo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>