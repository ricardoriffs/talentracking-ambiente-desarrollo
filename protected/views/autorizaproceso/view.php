<?php
/* @var $this AutorizaprocesoController */
/* @var $model Autorizaproceso */

$this->breadcrumbs=array(
	'Autorizaprocesos'=>array('index'),
	$model->AutorizaProcesoID,
);

$this->menu=array(
//	array('label'=>'List Autorizaproceso', 'url'=>array('index')),
//	array('label'=>'Create Autorizaproceso', 'url'=>array('create')),
	array('label'=>'Editar autorización de proceso', 'url'=>array('update', 'id'=>$model->AutorizaProcesoID)),
	array('label'=>'Borrar autorización de proceso', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->AutorizaProcesoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar autorizaciones de proceso', 'url'=>array('admin')),
);
?>

<h1>Ver autorización de proceso "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'AutorizaProcesoID',
		'id',
//		'CompaniaID',
	),
)); ?>
