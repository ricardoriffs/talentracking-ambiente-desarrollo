<?php
/* @var $this AutorizaprocesoController */
/* @var $model Autorizaproceso */

$this->breadcrumbs=array(
	'Autorizaprocesos'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Autorizaproceso', 'url'=>array('index')),
	array('label'=>'Administrar autorizaciones de proceso', 'url'=>array('admin')),
);
?>

<h1>Crear autorización de proceso</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>