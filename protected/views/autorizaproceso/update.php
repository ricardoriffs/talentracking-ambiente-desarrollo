<?php
/* @var $this AutorizaprocesoController */
/* @var $model Autorizaproceso */

$this->breadcrumbs=array(
	'Autorizaprocesos'=>array('index'),
	$model->AutorizaProcesoID=>array('view','id'=>$model->AutorizaProcesoID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Autorizaproceso', 'url'=>array('index')),
//	array('label'=>'Create Autorizaproceso', 'url'=>array('create')),
	array('label'=>'Ver autorización de proceso', 'url'=>array('view', 'id'=>$model->AutorizaProcesoID)),
	array('label'=>'Administrar autorizaciones de proceso', 'url'=>array('admin')),
);
?>

<h1>Editar autorización de proceso "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>