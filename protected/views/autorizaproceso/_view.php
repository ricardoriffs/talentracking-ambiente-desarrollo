<?php
/* @var $this AutorizaprocesoController */
/* @var $data Autorizaproceso */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('AutorizaProcesoID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->AutorizaProcesoID), array('view', 'id'=>$data->AutorizaProcesoID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::encode($data->CompaniaID); ?>
	<br />


</div>