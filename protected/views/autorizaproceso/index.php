<?php
/* @var $this AutorizaprocesoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Autorizaprocesos',
);

$this->menu=array(
	array('label'=>'Create Autorizaproceso', 'url'=>array('create')),
	array('label'=>'Manage Autorizaproceso', 'url'=>array('admin')),
);
?>

<h1>Autorizaprocesos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
