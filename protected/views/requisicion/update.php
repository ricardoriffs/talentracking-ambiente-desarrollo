<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/requisicion.js', CClientScript::POS_END);

$this->breadcrumbs=array(
	'Requisicions'=>array('index'),
	$model->RequisicionID=>array('view','id'=>$model->RequisicionID),
	'Update',
);

$this->menu=array(
	
	array('label'=>'Crear Requisicion', 'url'=>array('create')),
	
	
);
?>

<h1>Actualizar Requisicion <?php echo $model->RequisicionID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model));?>
<?php //echo CHtml::button('Enviar requisicion a Gerencia Talento Humano', array('submit' => array('Crearejecucion', 'id'=>$model->RequisicionID))); ?>

<?php// echo CHtml::submitButton($model->isNewRecord ? '' : 'Enviar requisicion a Gerencia Talento Humano');
 ?>