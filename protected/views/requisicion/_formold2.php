<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicion1-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
  <diV class = "divContentBlue">   
        <div class="row">
	<h2 align="center"><strong>IDENTIFICACION DEL CARGO</strong>	</h2>
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'UnidadNegocioID'); ?>
		<?php echo $form->dropDownList($model,'UnidadNegocioID', CHtml::listData(Unidadnegocio::model()->findAll(), 'UnidadNegocioID', 'Nombre')); ?>
		<?php echo $form->error($model,'UnidadNegocioID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AutorizacionProceso'); ?>
		<?php echo $form->dropDownList($model,'AutorizacionProceso', CHtml::listData(autorizaproceso::model()->findAll(), 'id', 'id')); ?> 
		<?php echo $form->error($model,'AutorizacionProceso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreAutoriza'); ?>
		<?php echo $form->textField($model,'NombreAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NombreAutoriza'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EmailAutoriza'); ?>
		<?php echo $form->textField($model,'EmailAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'EmailAutoriza'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AreaVicepresidenciaID'); ?>
		<?php echo $form->dropDownList($model,'AreaVicepresidenciaID', CHtml::listData(AreaVicepresidencia::model()->findAll(), 'AreaVicepresidenciaID', 'Nombre')); ?>
		<?php echo $form->error($model,'AreaVicepresidenciaID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RutaArchivoAutorizacion'); ?>
         <? $this->widget('ext.EAjaxUpload.EAjaxUpload',
	
        array(
        'id'=>'uploadFile',
        'config'=>array(
               'action'=>Yii::app()->createUrl('controller/upload'),
               'allowedExtensions'=>array("php"), 
               'sizeLimit'=>10*1024*1024,// maximum file size in bytes
               'minSizeLimit'=>0,// minimum file size in bytes
               //'onComplete'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
               //'messages'=>array(
               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
               //                  'emptyError'=>"{file} is empty, please select files again without it.",
               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
               //                 ),
               //'showMessage'=>"js:function(message){ alert(message); }"
              )
             )); ?>


		<?php echo $form->textField($model,'RutaArchivoAutorizacion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'RutaArchivoAutorizacion'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'NumeroVacantes'); ?>
		<?php echo $form->textField($model,'NumeroVacantes',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NumeroVacantes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreCargo'); 
	                $criteria=new CDbCriteria;
			$criteria->select='id';  // only select the 'title' column
			$criteria->condition='tipocargo=:tipocargo';
			$criteria->params=array(':tipocargo'=>R);
			//$post=Post::model()->find($criteria); // $params is not needed 

                ?>

                <?php echo $form->dropDownList($model,'NombreCargo', CHtml::listData(cargo::model()->findall($criteria), 'id', 'id')); ?> 
		<?php// echo $form->textField($model,'NombreCargo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'NombreCargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreSolicitante'); ?>
		<?php echo $form->textField($model,'NombreSolicitante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NombreSolicitante'); ?>
	</div>

	<div class="row">
               <?php echo $form->labelEx($model,'CargoSolicitante');                           
                        $criteria=new CDbCriteria;
			$criteria->select='id';  // only select the 'title' column
			$criteria->condition='tipocargo=:tipocargo';
			$criteria->params=array(':tipocargo'=>V);
			//$post=Post::model()->find($criteria); // $params is not needed 		<?php echo $form->labelEx($model,'CargoSolicitante'); 


                ?>
		<?php //echo $form->textField($model,'CargoSolicitante',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->dropDownList($model,'CargoSolicitante', CHtml::listData(cargo::model()->findall($criteria), 'id', 'id')); ?>   
		<?php echo $form->error($model,'CargoSolicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'JefeInmediatoVacante'); ?>
		<?php echo $form->textField($model,'JefeInmediatoVacante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'JefeInmediatoVacante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LugarTrabajo'); ?>
		<?php echo $form->dropDownList($model,'LugarTrabajo', CHtml::listData(Sedescompanias::model()->findAll(), 'SedeCompaniaID', 'Nombre')); ?>
		<?php echo $form->error($model,'LugarTrabajo'); ?>
	</div>
   </div>    

  <diV class = "divContentBlue">
        <div class="row">
		<h2 align="center">INFORMARCION DEL CARGO	
	    </h2>
	    </div>

	<div class="row">
		    <?php echo $form->labelEx($model,'RutaDescripcionCargo'); ?>
                    
	<? $this->widget('ext.EAjaxUpload.EAjaxUpload',
	
        array(
        'id'=>'uploadFile2',
        'config'=>array(
               'action'=>Yii::app()->createUrl('controller/upload'),
               'allowedExtensions'=>array("php"), 
               'sizeLimit'=>10*1024*1024,// maximum file size in bytes
               'minSizeLimit'=>0,// minimum file size in bytes
               //'onComplete'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
               //'messages'=>array(
               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
               //                  'emptyError'=>"{file} is empty, please select files again without it.",
               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
               //                 ),
               //'showMessage'=>"js:function(message){ alert(message); }"
              )
             )); ?>

             
                    <?php echo $form->textField($model,'RutaDescripcionCargo',array('size'=>60,'maxlength'=>255)); ?>
		    <?php echo $form->error($model,'RutaDescripcionCargo'); ?>
	</div>

	<div class="row">
                <?php echo 'Nota: si no tiene una descripcion del cargo, haga click '?>
                <A HREF="mailto:rrhh@pacificrubiales.com.co?cc=jtatis@yahoo.com&subject=CONSULTA DESCRIPCION CARGO PARA REQUISICION">AQUI</A>
                <?php echo 'en el siguiente correo para solicitar apoyo con los gerentes de talento humano'?>

		<?php echo $form->labelEx($model,'DescripcionCargoPar'); ?>
		<?php echo $form->textarea($model,'DescripcionCargoPar',array('size'=>60,'maxlength'=>4000,'cols'=>50 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'DescripcionCargoPar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TurnoTrabajo'); ?>
		<?php// echo $form->textField($model,'TurnoTrabajo',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->dropDownList($model,'TurnoTrabajo', CHtml::listData(turno::model()->findall(), 'id', 'id')); ?>    
		<?php echo $form->error($model,'TurnoTrabajo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoContratacionID'); ?>
		<?php echo $form->dropDownList($model,'TipoContratacionID', CHtml::listData(TipoContratacion::model()->findAll(), 'TipoContratacionID', 'Nombre')); ?>
                <?php echo $form->error($model,'TipoContratacionID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TiempoContratacion'); ?>
                <?php echo $form->textField($model,'TiempoContratacion',array('size'=>50,'maxlength'=>50)); ?> 		
		<?php echo $form->error($model,'TiempoContratacion'); ?>
	</div>
</div>

 <diV class = "divContentBlue">
        <div class="row">
		<h2 align="center">ASPECTOS CLAVES DEL CARGO	
	    </h2>
	    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'AspectosImportantes'); ?>
		<?php echo $form->textarea($model,'AspectosImportantes',array('size'=>60,'maxlength'=>4000,'cols'=>50 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'AspectosImportantes'); ?>
	</div>
</div>
	<div class="row">
		<?php // echo $form->labelEx($model,'Observaciones'); ?>
		<?php //echo $form->textField($model,'Observaciones',array('size'=>60,'maxlength'=>2000)); ?>
		<?php //echo $form->error($model,'Observaciones'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'RangoSalario'); ?>
		<?php //echo $form->textField($model,'RangoSalario',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'RangoSalario'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'NivelHAY'); ?>
		<?php //echo $form->textField($model,'NivelHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'NivelHAY'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'PuntosHAY'); ?>
		<?php //echo $form->textField($model,'PuntosHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'PuntosHAY'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'TipoProcesoSeleccionID'); ?>
		<?php //echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'TipoProcesoSeleccionID'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioIDResponsable'); ?>
		<?php //echo $form->textField($model,'UsuarioIDResponsable',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'UsuarioIDResponsable'); ?>
                <?php echo 'Manifiesto que he leido las politicas de la compania en cuanto a seleccion y vinculacion'; ?>   
                <?php echo $form->checkbox($model,'Observaciones',array('name'=>politica, 'value'=>no )); ?> 
                  
 
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear Requisicion' : 'Guardar Cambios Requision'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->