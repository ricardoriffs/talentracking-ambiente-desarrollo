<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>
<script language="JavaScript1.2">

function ventanaSecundaria (URL, alto, ancho)
{ 
   window.open(URL,"ventana1","width="+ancho+",height="+alto+",scrollbars=YES") 
} 
</script>


<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/requisicion.js', CClientScript::POS_END);

$this->breadcrumbs=array(
	'Requisicions'=>array('index'),
	$model->RequisicionID,
);

$this->menu=array(
	
	array('label'=>'Crear Requisicion', 'url'=>array('create')),
	
);
?>

<h1>Vista Requisicion Cliente Interno #<?php echo $model->RequisicionID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'RequisicionID',
                array(
                                'name'=>'Unidad de Negocio',
                                'value'=>CHtml::encode($model->unidadnegocio->Nombre),
                        ),   
		'AutorizacionProceso',
		/*'NombreAutoriza',
		'EmailAutoriza',
                array (
		'name'=>'RequisionID',
		'value'=>'NombreCargo',
		'type'=>'text',
		'filter' => CHtml::listData(Requisicion::model()->findAll($criteria), 'RequisicionID', 'NombreCargo'),
	        ),*/		
		'NumeroVacantes',
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
                array(
                                'name'=>'Lugar de Trabajo',
                                'value'=>CHtml::encode($model->sedescompanias->Nombre),
                        ),	
		'DescripcionCargoPar',
		'TurnoTrabajo', 
                array(
                                'name'=>'Tipo de Contracion',
                                'value'=>CHtml::encode($model->tipocontratacion->Nombre),
                        ),		
		'TiempoContratacion',
		'AspectosImportantes',
		/*'Observaciones',
		'RangoSalario',
		'NivelHAY',
		'PuntosHAY',
		'TipoProcesoSeleccionID',
		'UsuarioIDResponsable',
                'politicaID',*/  
	),
)); ?>


<div id="pol" class="row">
  <?php echo 'Manifiesto que he leido las' ?> 
     <a href="javascript:ventanaSecundaria('\popup_noticia2.php?r=d<?echo $model->RequisicionID ?>' ,400,400)">politicas</a>
   <?php echo 'de la compania en cuanto a seleccion y vinculacion'; ?>  
   <?= CHtml::checkBox('SI',false,array('id' => 'polres')); ?> 
   <?php //CHtml::checkBox('SI',false,array('id' => 'polres', 'onchange' => 'javascript:$("#b1").toggle(); javascript:$("#b2").toggle()')); ?>
   <?php //echo CHtml::dropDownList('pol','', array('NO','SI')); array('onchange'=>'alert(value);')?>
                  
</div>


 <div class="row">
                
                <?php //echo CHtml::dropDownList('building','', array(), array('empty' =>'--please select campus first--')); ?>
                <?php //echo $form->error(Location::model(),'building'); ?>
      </div> 

            

<div id="b1" class="row buttons">
            
            <?php
              echo nl2br("\n");
              echo nl2br("\n");
                    //alert $politicaid->value;
                    if (($model->RequisicionID)!= "") {?>
                    
                      <a class="boton"  href=<?php echo 'index.php?r=requisicion/crearejecucion&id=' . $model->RequisicionID.' >' ?> Enviar requisicion a Gerencia Talento Humano </a>                        
                      
                      <?php 
                        echo nl2br("\n"); 
                        echo nl2br("\n"); 
                      ?>
                      <a class="boton"  href=<?php echo 'index.php?r=requisicion/update&id=' . $model->RequisicionID.'>' ?> Editar Requisicion  </a>                        

                 <?php   
                    echo nl2br("\n"); 
                      }
                    
                  ?>
             
</div>

<div id="b2" class="row buttons">
            
            <?php
              echo nl2br("\n");
              echo 'Usted no puede continuar el proceso si no conoce las politicas asociadas al proceso.';                    
                  ?>
             
</div>
  	
