<?php
$this->breadcrumbs=array(
	'Requisicions',
);

$this->menu=array(
	array('label'=>'Crear Requisicion', 'url'=>array('create')),
	array('label'=>'Administrar Requisicion', 'url'=>array('admin')),
);
?>

<h1>Requisicions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
