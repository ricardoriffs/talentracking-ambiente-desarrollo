<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>
<div class="form">



<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicion-form',
	'enableAjaxValidation'=>false,
)); 

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/requisicion1.js', CClientScript::POS_END);

?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios</p>

	<?php echo $form->errorSummary($model); ?>
  <diV class = "divContentBlue">   
        <div class="row">
	<h2 align="center" class="Estilo2">IDENTIFICACIÓN DEL CARGO	</h2>
        </div>


<div class="row">
		
                <?php echo $form->labelEx($model,'NombreCargo'); 
	               // $criteria=new CDbCriteria;
			//$criteria->select='id';  // only select the 'title' column
			//$criteria->condition ='CompaniaID='.$compania;
                        //$criteria->addCondition('tipocargo=:tipocargo', 'AND');
			//$criteria->params=array(':tipocargo'=>'R');
			//$post=Post::model()->find($criteria); // $params is not needed 
                      echo "Si el cargo no se encuentra en la lista, haga clic sobre Administrar Cargos, en el menú principal"; 
                ?>

         <?php //echo $form->dropDownList($model,'NombreCargo', CHtml::listData(cargo::model()->findall($criteria), 'id', 'id')); ?> 
	 <?php	$this->widget('ext.combobox.EJuiComboBox', array(
    			'model' => $model,
    			'attribute' => 'NombreCargo',
    			// data to populate the select. Must be an array.
    			'data' =>CHtml::listData(Cargo::model()->findall(array('condition'=> "CompaniaID=".$compania." AND tipocargo='R'")), 'CargoID', 'id'), 
                        //array('yii','is','fun','!'),
    			// options passed to plugin
    			'options' => array(
        		// JS code to execute on 'select' event, the selected item is
        		// available through the 'item' variable.
        		//'onSelect' => 'alert("selected value : " + item.value);',
        		// JS code to be executed on 'change' event, the input is available
        		// through the '$(this)' variable.
        		'onChange' => 'alert("changed value : " + $(this).val());',
        		// If false, field value must be present in the select.
        		// Defaults to true.
        		'allowText' => false,
    			),
    			// Options passed to the text input
    			'htmlOptions' => array('size' => 70),
		));	
        ?> 
	<?php// echo $form->textField($model,'NombreCargo',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'NombreCargo'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'UnidadNegocioID'); ?>
		<?php echo $form->dropDownList($model,'UnidadNegocioID', CHtml::listData(Unidadnegocio::model()->findAll(array('condition'=> "CompaniaID=".$compania)), 'UnidadNegocioID', 'Nombre'),array('length'=>350)); ?>
		<?php echo $form->error($model,'UnidadNegocioID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AutorizacionProceso'); ?>
		<?php echo $form->dropDownList($model,'AutorizacionProceso', CHtml::listData(Autorizaproceso::model()->findAll(array('condition'=> "CompaniaID=".$compania)), 'id', 'id')); ?> 
		<?php echo $form->error($model,'AutorizacionProceso'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'NombreAutoriza'); ?>
		<?php //echo $form->textField($model,'NombreAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'NombreAutoriza'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'EmailAutoriza'); ?>
		<?php //echo $form->textField($model,'EmailAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'EmailAutoriza'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AreaVicepresidenciaID'); ?>
		<?php echo $form->dropDownList($model,'AreaVicepresidenciaID', CHtml::listData(Areavicepresidencia::model()->findAll(array('condition'=> "CompaniaID=".$compania)), 'AreaVicepresidenciaID', 'Nombre')); ?>
		<?php echo $form->error($model,'AreaVicepresidenciaID'); ?>
	</div>

	<div class="row">
		<?php  echo $form->labelEx($model,'RutaArchivoAutorizacion'); ?>
         <? 

//    if ($model->RequisicionID!= "")
//  {
        
       

        $this->widget('ext.EAjaxUpload.EAjaxUpload',
	
        array(
        'id'=>'uploadFile',
        'config'=>array(
               'action'=>Yii::app()->createUrl('/Requisicion/Uploadfa/&id=' . $model->RequisicionID),  
               'allowedExtensions'=>array("php","doc","pdf","xls","ppt","docx","xlsx","pptx","msg"), 
               'sizeLimit'=>10*1024*1024,// maximum file size in bytes
               'minSizeLimit'=>4,// minimum file size in bytes
               'f2' => $model->RequisicionID.'_' .fileName, 
               'onComplete'=>"js:function(id, filename, responseJSON){ alert(f2); }",
               'messages'=>array(
                                 'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                 'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                 'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                'emptyError'=>"{file} is empty, please select files again without it.",
                                 'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                ),
               'showMessage'=>"js:function(message){ alert(message); }"
              )
             ));


// } else {
    //echo 'POR FAVOR CARGAR LOS ARCHIVOS CORRESPONDIENTES A - SOPORTE AUTORIZACION - EN LA MODIFICACION DE LA REQUISICION  ANTES DE ENVIAR A APROBACION';
//}
 ?>

          

		<?php // echo $form->textField($model,'RutaArchivoAutorizacion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'RutaArchivoAutorizacion'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'NumeroVacantes'); ?>
		<?php echo $form->textField($model,'NumeroVacantes',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NumeroVacantes'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'NombreSolicitante'); ?>
		<?php echo $form->textField($model,'NombreSolicitante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NombreSolicitante'); ?>
	</div>

	<div class="row">
               <?php echo $form->labelEx($model,'CargoSolicitante');                           
                        $criteria=new CDbCriteria;
			$criteria->select='id';  // only select the 'title' column
			$criteria->condition='tipocargo=:tipocargo';
			$criteria->params=array(':tipocargo'=>V);
			//$post=Post::model()->find($criteria); // $params is not needed 		<?php echo $form->labelEx($model,'CargoSolicitante'); 


                ?>
		<?php //echo $form->textField($model,'CargoSolicitante',array('size'=>50,'maxlength'=>50)); ?>
                <?php //echo $form->dropDownList($model,'CargoSolicitante', CHtml::listData(cargo::model()->findall($criteria), 'id', 'id')); ?>   
		
                <?php	$this->widget('ext.combobox.EJuiComboBox', array(
    			'model' => $model,
    			'attribute' => 'CargoSolicitante',
    			// data to populate the select. Must be an array.
    			'data' =>CHtml::listData(cargo::model()->findall(array('condition'=> "CompaniaID=".$compania." AND tipocargo='R'")), 'id', 'id'), 
                        //array('yii','is','fun','!'),
    			// options passed to plugin
    			'options' => array(
        		// JS code to execute on 'select' event, the selected item is
        		// available through the 'item' variable.
        		//'onSelect' => 'alert("selected value : " + item.value);',
        		// JS code to be executed on 'change' event, the input is available
        		// through the '$(this)' variable.
        		'onChange' => 'alert("changed value : " + $(this).val());',
        		// If false, field value must be present in the select.
        		// Defaults to true.
        		'allowText' => false,
    			),
    			// Options passed to the text input
    			'htmlOptions' => array('size' => 70),
		));	
        ?> 


                <?php echo $form->error($model,'CargoSolicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'JefeInmediatoVacante'); ?>
		<?php echo $form->textField($model,'JefeInmediatoVacante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'JefeInmediatoVacante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LugarTrabajo'); ?>
		<?php echo $form->dropDownList($model,'LugarTrabajo', CHtml::listData(Sedescompanias::model()->findAll(array('condition'=> "CompaniaID=".$compania)), 'SedeCompaniaID', 'Nombre')); ?>
		<?php echo $form->error($model,'LugarTrabajo'); ?>
	</div>
  </div>    
  
  <div>     
		<br> </br>
  
  </div>       

  <diV class = "divContentBlue">
        <div class="row">
		<h2 align="center" class="Estilo2">INFORMACIÓN DEL CARGO</h2>
	    </div>

	<div class="row">
		    <?php echo $form->labelEx($model,'RutaDescripcionCargo'); ?>


         <? 

 //   if ($model->RequisicionID!= "")
 // {
  
          
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
	
        array(
        'id'=>'uploadFile1',
        'config'=>array(
               'action'=>Yii::app()->createUrl('/Requisicion/Uploaddc/&id=' . $model->RequisicionID),  
               'allowedExtensions'=>array("php","doc","pdf","xls","ppt","docx","xlsx","pptx","msg"), 
               'sizeLimit'=>10*1024*1024,// maximum file size in bytes
               'minSizeLimit'=>4,// minimum file size in bytes
               'onComplete'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
               'messages'=>array(
                                 'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                 'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                 'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                'emptyError'=>"{file} is empty, please select files again without it.",
                                 'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                ),
               'showMessage'=>"js:function(message){ alert(message); }"
              )
             ));
 
  // $filename1 = $this1->getTempName(); 
 //echo uploadFile1=>getTempName();        
 //rename($fileName,$model->RequisicionID.$fileName);
// }else {
//    echo 'POR FAVOR CARGAR LOS ARCHIVOS CORRESPONDIENTES A - DESCRIPCION DEL CARGO - EN LA MODIFICACION DE LA REQUISICION  ANTES DE ENVIAR A APROBACION';
//} ?>


             
                    <?php // echo $form->textField($model,'RutaDescripcionCargo',array('size'=>60,'maxlength'=>255)); ?>
		    <?php echo $form->error($model,'RutaDescripcionCargo'); ?>
	</div>

	<div class="row">
                <?php echo 'Nota: si no tiene una descripción del cargo, haga click '?>
                <A HREF="mailto:rrhh@pacificrubiales.com.co?cc=jtatis@yahoo.com&subject=CONSULTA DESCRIPCIÓN CARGO PARA REQUISICIÓN">AQUÍ</A>
                <?php echo 'para solicitar apoyo con los gerentes de talento humano'?>

		<?php echo $form->labelEx($model,'DescripcionCargoPar'); ?>
		<?php echo $form->textarea($model,'DescripcionCargoPar',array('size'=>60,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'DescripcionCargoPar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TurnoTrabajo'); ?>
		<?php// echo $form->textField($model,'TurnoTrabajo',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->dropDownList($model,'TurnoTrabajo', CHtml::listData(Turno::model()->findall(array('condition'=> "CompaniaID=".$compania)), 'id', 'id')); ?>    
		<?php echo $form->error($model,'TurnoTrabajo'); ?>
	</div>



	<div id="TipoContratacionID" class="row">
		<?php echo $form->labelEx($model,'TipoContratacionID'); ?>
		<?php echo $form->dropDownList($model,'TipoContratacionID', CHtml::listData(Tipocontratacion::model()->findAll(array('condition'=> "CompaniaID=".$compania.' AND ModuloID=1')), 'TipoContratacionID', 'Nombre')); ?>
                <?php echo $form->error($model,'TipoContratacionID'); ?>
	</div>

	<div id="TiempoContratacion" class="row">
         
		
                <?php echo $form->labelEx($model,'TiempoContratacion'); ?>
                <?php echo $form->textField($model,'TiempoContratacion',array('size'=>50,'maxlength'=>50)); ?> 		
		<?php echo $form->error($model,'TiempoContratacion') . '-MESES-'; ?>
	</div>




</div>






<div>     
		<br> </br>
  
  </div>       


 <diV class = "divContentBlue">
        <div class="row">
		<h2 align="center" class="Estilo2">ASPECTOS CLAVES DEL CARGO	
	    </h2>
	    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'AspectosImportantes'); ?>
		<?php echo $form->textarea($model,'AspectosImportantes',array('size'=>60,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'AspectosImportantes'); ?>
	</div>
</div>
	<div class="row">
		<?php if ($model->Observaciones != '0'){
  
                      echo $form->labelEx($model,'Observaciones'); ?>
		<?php echo $form->textarea($model,'Observaciones',array('size'=>60,'maxlength'=>2000,'cols'=>85 ,'rows'=>10, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'Observaciones');  }?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'RangoSalario'); ?>
		<?php //echo $form->textField($model,'RangoSalario',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'RangoSalario'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'NivelHAY'); ?>
		<?php //echo $form->textField($model,'NivelHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'NivelHAY'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'PuntosHAY'); ?>
		<?php //echo $form->textField($model,'PuntosHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'PuntosHAY'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'TipoProcesoSeleccionID'); ?>
		<?php //echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'TipoProcesoSeleccionID'); ?>
	</div>

	

       <div id="Button1" class="row buttons">
 
                <?php echo CHtml::submitButton($model->isNewRecord ? '                   Crear Requisicion                          ' : '             Previsualizar Requisicion             '); ?>
                                                                                                                     
	</div>                                                 


        

<?php $this->endWidget(); ?>

</div><!-- form -->