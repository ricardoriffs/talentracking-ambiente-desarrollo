<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/requisicion.js', CClientScript::POS_END);

$this->breadcrumbs=array(
	'Requisicions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Crear Requisicion', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicion-grid', {
		data: $(this).serialize()
	});
	return false;
});
");



?>

<h1>Crear Requisicion Cliente Interno</h1>

<p>
</p>

<?php 
	echo '<BR>';
	echo '<BR>';
	echo 'En esta pagina podra crear y consultar las solicitudes de personal que esten en proceso de creacion o que han sido direccionadas a la Vicepresidencia de Talento Humano.' ;
	echo 'Para iniciar dirijase al menu crear requisicion ubicado a la derecha.';

        echo '<BR>';
	echo '<BR>';

   echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php



  $sql = 'select * from requisicion where actividadID =1';
 // $dataProvider1 = new CSqlDataProvider($sql, array(
//	'sort'=>$sort,
//	'totalItemCount'=>$count,
//	'pagination'=>array(
//		'pageSize'=>Yii::app()->params['pageSize'],
//	), 
//        		array(
//			'class'=>'CButtonColumn',
//		),
//    ));



$id = $_GET['view'];

if ($id == 'subsubr1n1'){ 
	echo "<script language='JavaScript'>alert('Gracias por utilizar la plataforma de requisicion de Pacific Rubiales, para hacer seguimiento de su solicitud haga click a seguimiento en linea desde el menu principal');</script>";

}
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicion-grid',
	 'dataProvider'=>$model->search($actividadID=1),
        //'dataProvider'=>new CSqlDataProvider($sql), 
	'filter'=>$model,
        
	'columns'=>array(
		'NombreCargo',
		array ('name'=>'UnidadNegocioID','value'=>'$data->unidadnegocio->Nombre','type'=>'text',),
		'fechacreacion',
                'NumeroVacantes',
		'NombreSolicitante',
                
	/*
		'RutaArchivoAutorizacion',
                array ('name'=>'AreaVicepresidenciaID','value'=>'$data->areasvicepresidencia->Nombre','type'=>'text',),  
		'NumeroVacantes',
                'RequisicionID', 
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
		'LugarTrabajo',
		'RutaDescripcionCargo',
		'DescripcionCargoPar',
		'TurnoTrabajo',
		'TipoContratacionID',
		'TiempoContratacion',
		'AspectosImportantes',
		'Observaciones',
		'RangoSalario',
		'NivelHAY',
		'PuntosHAY',
		'TipoProcesoSeleccionID',
		'UsuarioIDResponsable',
     */
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}{update}',                   
		), 
	
     ),


)); 




?>