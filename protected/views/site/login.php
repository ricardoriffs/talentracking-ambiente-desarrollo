<script type="text/javascript">
    function showContent() {
        element = document.getElementById("form");
        check = document.getElementById("check");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }
</script>

<script language="JavaScript1.2">

function ventanaSecundaria (URL, alto, ancho)
{ 
   window.open(URL,"ventana1","width="+ancho+",height="+alto+",scrollbars=YES") 
} 
</script>



<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);


?>

<h1>Control de Acceso</h1>







	<p>Por favor ingrese sus credenciales de acceso:</p>
<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	)); ?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>
        


	<div class="row">
		<?php echo $form->labelEx($model,'username') . "<BR>"; ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password') . "<BR>"; ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		<p class="hint">
			
		</p>
	</div>

	<div class="row rememberMe">
		<?php //echo $form->checkBox($model,'rememberMe'); ?>
		<?php //echo $form->label($model,'rememberMe'); ?>
		<?php //echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
            <?php echo "<br>"; ?> 
                            
            <?php echo "<br>"; ?>
            <?php echo "<br>"; ?>
          
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
