<?php
/* @var $this VicepresidenciasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vicepresidenciases',
);

$this->menu=array(
	array('label'=>'Create Vicepresidencias', 'url'=>array('create')),
	array('label'=>'Manage Vicepresidencias', 'url'=>array('admin')),
);
?>

<h1>Vicepresidenciases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
