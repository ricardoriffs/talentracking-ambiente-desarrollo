<?php
/* @var $this VicepresidenciasController */
/* @var $model Vicepresidencias */

$this->breadcrumbs=array(
	'Vicepresidenciases'=>array('index'),
	$model->VicePresidenciaID,
);

$this->menu=array(
//	array('label'=>'List Vicepresidencias', 'url'=>array('index')),
//	array('label'=>'Create Vicepresidencias', 'url'=>array('create')),
	array('label'=>'Editar Vicepresidencias', 'url'=>array('update', 'id'=>$model->VicePresidenciaID)),
	array('label'=>'Borrar Vicepresidencias', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->VicePresidenciaID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Vicepresidencias', 'url'=>array('admin')),
);
?>

<h1>Ver Vicepresidencia "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'VicePresidenciaID',
//		'CompaniaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ),  
	),
)); ?>
