<?php
/* @var $this VicepresidenciasController */
/* @var $model Vicepresidencias */

$this->breadcrumbs=array(
	'Vicepresidenciases'=>array('index'),
	$model->VicePresidenciaID=>array('view','id'=>$model->VicePresidenciaID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Vicepresidencias', 'url'=>array('index')),
//	array('label'=>'Create Vicepresidencias', 'url'=>array('create')),
	array('label'=>'Ver Vicepresidencia', 'url'=>array('view', 'id'=>$model->VicePresidenciaID)),
	array('label'=>'Administrar Vicepresidencias', 'url'=>array('admin')),
);
?>

<h1>Editar Vicepresidencia "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>