<?php
/* @var $this VicepresidenciasController */
/* @var $model Vicepresidencias */

$this->breadcrumbs=array(
	'Vicepresidenciases'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Vicepresidencias', 'url'=>array('index')),
	array('label'=>'Administrar Vicepresidencias', 'url'=>array('admin')),
);
?>

<h1>Crear Vicepresidencias</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>