<?php
$this->breadcrumbs=array(
	'Areavicepresidencias'=>array('index'),
	$model->AreaVicepresidenciaID=>array('view','id'=>$model->AreaVicepresidenciaID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Areavicepresidencia', 'url'=>array('index')),
	array('label'=>'Create Areavicepresidencia', 'url'=>array('create')),
	array('label'=>'View Areavicepresidencia', 'url'=>array('view', 'id'=>$model->AreaVicepresidenciaID)),
	array('label'=>'Manage Areavicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Update Areavicepresidencia <?php echo $model->AreaVicepresidenciaID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>