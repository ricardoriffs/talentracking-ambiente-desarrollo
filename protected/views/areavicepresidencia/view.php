<?php
$this->breadcrumbs=array(
	'Areavicepresidencias'=>array('index'),
	$model->AreaVicepresidenciaID,
);

$this->menu=array(
	array('label'=>'List Areavicepresidencia', 'url'=>array('index')),
	array('label'=>'Create Areavicepresidencia', 'url'=>array('create')),
	array('label'=>'Update Areavicepresidencia', 'url'=>array('update', 'id'=>$model->AreaVicepresidenciaID)),
	array('label'=>'Delete Areavicepresidencia', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->AreaVicepresidenciaID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Areavicepresidencia', 'url'=>array('admin')),
);
?>

<h1>View Areavicepresidencia #<?php echo $model->AreaVicepresidenciaID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'AreaVicepresidenciaID',
		'VicePresidenciaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
		'UsuarioCreacion',
		'FechaModificacion',
		'UsuarioModificacion',
	),
)); ?>
