<?php
$this->breadcrumbs=array(
	'Areavicepresidencias',
);

$this->menu=array(
	array('label'=>'Create Areavicepresidencia', 'url'=>array('create')),
	array('label'=>'Manage Areavicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Areavicepresidencias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
