<?php
$this->breadcrumbs=array(
	'Areavicepresidencias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Areavicepresidencia', 'url'=>array('index')),
	array('label'=>'Manage Areavicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Create Areavicepresidencia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>