<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('AreaVicepresidenciaID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->AreaVicepresidenciaID), array('view', 'id'=>$data->AreaVicepresidenciaID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VicePresidenciaID')); ?>:</b>
	<?php echo CHtml::encode($data->VicePresidenciaID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	*/ ?>

</div>