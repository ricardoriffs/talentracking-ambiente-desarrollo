<?php
$this->breadcrumbs=array(
	'Requisicionprocesoevals'=>array('index'),
	$model->requisionprocesoID=>array('view','id'=>$model->requisionprocesoID),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista Proc.Evaluacion', 'url'=>array('index')),
	array('label'=>'Crear Proc.Evaluacion', 'url'=>array('create')),
	array('label'=>'Vista Proc.Evaluacion', 'url'=>array('view', 'id'=>$model->requisionprocesoID)),
	array('label'=>'Administrar Proc.Evaluacion', 'url'=>array('admin')),
);
?>

<h1>Actualizar Proc.Evaluacion<?php echo $model->requisionprocesoID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>