<?php
$this->breadcrumbs=array(
	'Requisicionprocesoevals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista Proc.Evaluacion', 'url'=>array('index')),
	array('label'=>'Administrar Proc. Evaluacion', 'url'=>array('admin')),
);
?>

<h1>Crear Proc. Evaluacion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>