<?php
$this->breadcrumbs=array(
	'Requisicionprocesoevals',
);

$this->menu=array(
	array('label'=>'Crear Proc. Evaluacion', 'url'=>array('create')),
	array('label'=>'Administrar Proc. Evaluacion', 'url'=>array('admin')),
);
?>

<h1>Proc. Evaluacion</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
