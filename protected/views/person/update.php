<?php
$this->breadcrumbs=array(
	'people'=>array('index'),
	$model->idperson=>array('view','id'=>$model->idperson),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista Candidatos', 'url'=>array('index')),
	array('label'=>'Crear Candidatos', 'url'=>array('create')),
	array('label'=>'Vista Candidatos', 'url'=>array('view', 'id'=>$model->idperson)),
	array('label'=>'Administrar Candidatos', 'url'=>array('admin', 'requisicionprocesoIDE'=>$model->requisionprocesoID)),
);
?>

<h1>Actualizar Candidatos<?php echo $model->idperson; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>