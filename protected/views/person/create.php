<?php


 
$ida = $_GET['requisicionprocesoIDE']; 
$pr = $_GET['p']; 

$this->breadcrumbs=array(
	'people'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'Lista Candidatos', 'url'=>array('index')),
	array('label'=>'Administrar Candidatos', 'url'=>array('admin','requisicionprocesoIDE'=>$ida,'p'=>$pr)),
);
?>

<h1>Crear Candidato</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>