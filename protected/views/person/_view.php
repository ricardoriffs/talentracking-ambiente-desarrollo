<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idperson')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idperson), array('view', 'id'=>$data->idperson)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail')); ?>:</b>
	<?php echo CHtml::encode($data->mail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publicacionmedios')); ?>:</b>
	<?php echo CHtml::encode($data->publicacionmedios); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisionprocesoID')); ?>:</b>
	<?php echo CHtml::encode($data->requisionprocesoID); ?>
	<br />


</div>