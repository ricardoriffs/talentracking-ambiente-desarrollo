<div class="form">
<?php 
        $ida = $_GET['requisicionprocesoIDE']; 
	$pr = $_GET['p']; 
?> 

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'person-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail'); ?>
		<?php echo $form->textField($model,'mail',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'mail'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'publicacionmedios'); ?>
		<?php //echo $form->textField($model,'publicacionmedios',array('size'=>45,'maxlength'=>45)); ?>
		<?php //echo $form->error($model,'publicacionmedios'); ?>
	</div>

        <div class="row"> 
               <?php echo $form->labelEx($model,'publicacionmedios'); ?>
		<?php echo $form->dropDownList($model,'publicacionmedios', CHtml::listData(Companiasprocesosexternos::model()->findAll(), 'id', 'id')); ?>
                <?php //echo $form->textField($model,'publicacionmedios',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'publicacionmedios'); ?>
        </div>  
  

	<div class="row">
		<?php echo $form->labelEx($model,'requisionprocesoID');?>
		<?php echo $form->textField($model,'requisionprocesoID',array('size'=>10,'maxlength'=>10,'value'=> $ida)); ?>
		<?php echo $form->error($model,'requisionprocesoID'); ?>
                <?php $model->requisionprocesoID = $ida;?>
	</div>

	<div class="row buttons">
                
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear Candidato' : 'Guardar Candidato'); ?>
                
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->