<?php

 
$ida = $_GET['requisicionprocesoIDE']; 
$pr = $_GET['p']; 

$this->breadcrumbs=array(
	'people'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'Lista Candidatos', 'url'=>array('index')),
	array('label'=>'Crear Candidato', 'url'=>array('create','requisicionprocesoIDE'=>$ida,'p'=>$pr)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('person-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Candidatos Para Proceso : <?php echo $ida ?></h1> 

<p>
</p>

<?php echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>


</div><!-- search-form -->

<?php 
   //echo $pr;
   if ($pr == x) { ?>
	<a class="boton"  href=<?php echo 'index.php?r=requisicionprocesoproexterno/update&id=' . $ida .'>'; ?> Regresar Proceso Externo </a>
<?php } else { ?>    
<a class="boton"  href=<?php echo 'index.php?r=requisicionprocesoeval/update&id=' . $ida .'>'; ?> Regresar Proceso Evaluacion </a>
<?php } ?>    

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'person-grid',
	'dataProvider'=>$model->search($ida),
	'filter'=>$model,
	'columns'=>array(
		'nombre',
		'mail',
		'publicacionmedios',
		'idperson',
		'requisionprocesoID',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
