<?php
/* @var $this HeadhunterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Headhunters',
);

$this->menu=array(
	array('label'=>'Create Headhunter', 'url'=>array('create')),
	array('label'=>'Manage Headhunter', 'url'=>array('admin')),
);
?>

<h1>Headhunters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
