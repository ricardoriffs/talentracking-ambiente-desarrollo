<?php
/* @var $this HeadhunterController */
/* @var $model Headhunter */

$this->breadcrumbs=array(
	'Headhunters'=>array('index'),
	$model->HeadHunterID=>array('view','id'=>$model->HeadHunterID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Headhunter', 'url'=>array('index')),
//	array('label'=>'Create Headhunter', 'url'=>array('create')),
	array('label'=>'Ver Compañia Head hunter', 'url'=>array('view', 'id'=>$model->HeadHunterID)),
	array('label'=>'Administrar Compañias Head hunter', 'url'=>array('admin')),
);
?>

<h1>Editar Compañia Head hunter "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>