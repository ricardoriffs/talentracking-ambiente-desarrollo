<?php
/* @var $this HeadhunterController */
/* @var $model Headhunter */

$this->breadcrumbs=array(
	'Headhunters'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Headhunter', 'url'=>array('index')),
	array('label'=>'Administrar Compañias Head hunter', 'url'=>array('admin')),
);
?>

<h1>Crear Compañias Head hunter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>