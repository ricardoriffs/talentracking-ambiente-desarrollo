<?php
/* @var $this HeadhunterController */
/* @var $model Headhunter */

$this->breadcrumbs=array(
	'Headhunters'=>array('index'),
	$model->HeadHunterID,
);

$this->menu=array(
//	array('label'=>'List Headhunter', 'url'=>array('index')),
//	array('label'=>'Create Headhunter', 'url'=>array('create')),
	array('label'=>'Editar Compañia Head hunter', 'url'=>array('update', 'id'=>$model->HeadHunterID)),
	array('label'=>'Delete Compañia Head hunter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->HeadHunterID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Compañias Head hunter', 'url'=>array('admin')),
);
?>

<h1>Ver Compañia Head hunter "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'HeadHunterID',
		'id',
//		'CompaniaID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),  
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
