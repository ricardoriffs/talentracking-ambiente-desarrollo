<?php
/* @var $this SedescompaniasController */
/* @var $model Sedescompanias */

$this->breadcrumbs=array(
	'Sedescompaniases'=>array('index'),
	$model->SedeCompaniaID=>array('view','id'=>$model->SedeCompaniaID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Sedescompanias', 'url'=>array('index')),
//	array('label'=>'Create Sedescompanias', 'url'=>array('create')),
	array('label'=>'Ver Sede de Compañia', 'url'=>array('view', 'id'=>$model->SedeCompaniaID)),
	array('label'=>'Administrar Sedes de Compañia', 'url'=>array('admin')),
);
?>

<h1>Editar Sede de Compañia "<?php echo $model->SedeCompaniaID; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>