<?php
/* @var $this SedescompaniasController */
/* @var $model Sedescompanias */

$this->breadcrumbs=array(
	'Sedescompaniases'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Sedescompanias', 'url'=>array('index')),
	array('label'=>'Administrar Sedes de Compañia', 'url'=>array('admin')),
);
?>

<h1>Crear Sede de Compañia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>