<?php
/* @var $this SedescompaniasController */
/* @var $model Sedescompanias */

$this->breadcrumbs=array(
	'Sedescompaniases'=>array('index'),
	$model->SedeCompaniaID,
);

$this->menu=array(
//	array('label'=>'List Sedescompanias', 'url'=>array('index')),
//	array('label'=>'Create Sedescompanias', 'url'=>array('create')),
	array('label'=>'Editar Sede de Compañia', 'url'=>array('update', 'id'=>$model->SedeCompaniaID)),
	array('label'=>'Borrar Sede de Compañia', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->SedeCompaniaID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Sedes de Compañia', 'url'=>array('admin')),
);
?>

<h1>Ver Sede de Compañia "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'SedeCompaniaID',
//		'CompaniaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
