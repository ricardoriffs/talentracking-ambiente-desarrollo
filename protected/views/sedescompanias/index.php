<?php
/* @var $this SedescompaniasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sedescompaniases',
);

$this->menu=array(
	array('label'=>'Create Sedescompanias', 'url'=>array('create')),
	array('label'=>'Manage Sedescompanias', 'url'=>array('admin')),
);
?>

<h1>Sedescompaniases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
