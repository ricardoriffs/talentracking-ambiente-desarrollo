<?php
/* @var $this CargoController */
/* @var $model Cargo */

$this->breadcrumbs=array(
	'Cargos'=>array('index'),
	$model->CargoID,
);

$this->menu=array(
//	array('label'=>'List Cargo', 'url'=>array('index')),
//	array('label'=>'Create Cargo', 'url'=>array('create')),
	array('label'=>'Editar Cargo', 'url'=>array('update', 'id'=>$model->CargoID)),
	array('label'=>'Borrar Cargo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->CargoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Cargos', 'url'=>array('admin')),
);
?>

<h1>Ver Cargo "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'CargoID',
		'id',
                array ('name'=>'tipocargo','value'=>$model->obtenerTipoCargo($model->tipocargo),'type'=>'text',),   
//		'CompaniaID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),            
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
