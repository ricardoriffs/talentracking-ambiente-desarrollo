<?php
/* @var $this CargoController */
/* @var $model Cargo */

$this->breadcrumbs=array(
	'Cargos'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Cargo', 'url'=>array('index')),
	array('label'=>'Administrar Cargos', 'url'=>array('admin')),
);
?>

<h1>Crear Cargo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>