<?php
/* @var $this CargoController */
/* @var $model Cargo */

$this->breadcrumbs=array(
	'Cargos'=>array('index'),
	$model->CargoID=>array('view','id'=>$model->CargoID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Cargo', 'url'=>array('index')),
//	array('label'=>'Create Cargo', 'url'=>array('create')),
	array('label'=>'Ver Cargo', 'url'=>array('view', 'id'=>$model->CargoID)),
	array('label'=>'Administrar Cargos', 'url'=>array('admin')),
);
?>

<h1>Editar Cargo "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>