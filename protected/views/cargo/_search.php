<?php
/* @var $this CargoController */
/* @var $model Cargo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<!--	<div class="row">
		<?php //echo $form->label($model,'CargoID'); ?>
		<?php //echo $form->textField($model,'CargoID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipocargo'); ?>
                <?php 
                    echo $form->dropdownlist(
                            $model,
                            'tipocargo',
                            array('R' => 'A requerir (R)','V' => 'Vigente (V)'),
                            array('empty'=>'-- Seleccione una opción --')
                            ); 
                ?>          
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'CompaniaID'); ?>
		<?php //Secho $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FechaCreacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaCreacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)     
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'UsuarioCreacion'); ?>
		<?php //echo $form->textField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FechaModificacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaModificacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)   
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'UsuarioModificacion'); ?>
		<?php //echo $form->textField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->