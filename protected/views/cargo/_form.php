<?php
/* @var $this CargoController */
/* @var $model Cargo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cargo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con asterisco <span class="required">(*)</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipocargo'); ?>
		<?php echo $form->dropdownlist(
                            $model,
                            'tipocargo',
                            array('R' => 'A requerir (R)','V' => 'Vigente (V)'),
                            array('empty'=>'-- Seleccione una opción --')
                        ); 
                ?>            
		<?php echo $form->error($model,'tipocargo'); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->labelEx($model,'CompaniaID'); ?>
		<?php //echo $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'CompaniaID'); ?>
	</div>-->

	<div class="row">
		<?php //echo $form->labelEx($model,'FechaModificacion'); ?>
		<?php echo $form->hiddenField($model,'FechaModificacion'); ?>
		<?php echo $form->error($model,'FechaModificacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioModificacion'); ?>
		<?php echo $form->hiddenField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioModificacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'FechaCreacion'); ?>
		<?php echo $form->hiddenField($model,'FechaCreacion'); ?>
		<?php echo $form->error($model,'FechaCreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioCreacion'); ?>
		<?php echo $form->hiddenField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioCreacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->