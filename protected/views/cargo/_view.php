<?php
/* @var $this CargoController */
/* @var $data Cargo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CargoID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CargoID), array('view', 'id'=>$data->CargoID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::encode($data->id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipocargo')); ?>:</b>
	<?php echo CHtml::encode($data->tipocargo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::encode($data->CompaniaID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	*/ ?>

</div>