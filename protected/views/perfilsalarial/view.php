<?php
$this->breadcrumbs=array(
	'Perfilsalarials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Perfilsalarial', 'url'=>array('index')),
	array('label'=>'Create Perfilsalarial', 'url'=>array('create')),
	array('label'=>'Update Perfilsalarial', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Perfilsalarial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Perfilsalarial', 'url'=>array('admin')),
);
?>

<h1>View Perfilsalarial #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
	),
)); ?>
