<?php
$this->breadcrumbs=array(
	'Perfilsalarials',
);

$this->menu=array(
	array('label'=>'Create Perfilsalarial', 'url'=>array('create')),
	array('label'=>'Manage Perfilsalarial', 'url'=>array('admin')),
);
?>

<h1>Perfilsalarials</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
