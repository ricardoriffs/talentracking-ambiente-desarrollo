<?php
$this->breadcrumbs=array(
	'Perfilsalarials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Perfilsalarial', 'url'=>array('index')),
	array('label'=>'Create Perfilsalarial', 'url'=>array('create')),
	array('label'=>'View Perfilsalarial', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Perfilsalarial', 'url'=>array('admin')),
);
?>

<h1>Update Perfilsalarial <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>