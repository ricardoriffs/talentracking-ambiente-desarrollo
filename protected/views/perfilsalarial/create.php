<?php
$this->breadcrumbs=array(
	'Perfilsalarials'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Perfilsalarial', 'url'=>array('index')),
	array('label'=>'Manage Perfilsalarial', 'url'=>array('admin')),
);
?>

<h1>Create Perfilsalarial</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>