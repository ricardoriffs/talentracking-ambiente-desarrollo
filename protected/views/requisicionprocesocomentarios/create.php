<?php
$this->breadcrumbs=array(
	'Requisicionprocesocomentarioses'=>array('index'),
	'Create',
);
$rp = $_GET['rp']; 
$re = $_GET['re'];

$this->menu=array(
	//array('label'=>'List Requisicionprocesocomentarios', 'url'=>array('index')),
	array('label'=>'Administrar Comentarios', 'url'=>array('admin','re'=>$re,'rp'=>$rp)),
);
?>

<h1>Crear Comentario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>