<?php
$this->breadcrumbs=array(
	'Requisicionprocesocomentarioses'=>array('index'),
	$model->id,
);

$rp = $_GET['rp']; 
$re = $_GET['re'];

$this->menu=array(
	//array('label'=>'List Requisicionprocesocomentarios', 'url'=>array('index')),
	//array('label'=>'Create Requisicionprocesocomentarios', 'url'=>array('create')),
	//array('label'=>'Update Requisicionprocesocomentarios', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Requisicionprocesocomentarios', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Comentarios', 'url'=>array('admin','re'=>$model->RequisicionID,'rp'=>$model->requisionprocesoID)),
);
?>

<h1>View Requisicionprocesocomentarios #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'requisionprocesoID',
		'RequisicionID',
		'comentarios',
		'fecha',
		'username',
	),
)); ?>
