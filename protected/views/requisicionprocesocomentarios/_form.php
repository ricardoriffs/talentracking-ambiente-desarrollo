<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicionprocesocomentarios-form',
	'enableAjaxValidation'=>false,
)); 


$rp = $_GET['rp']; 
$re = $_GET['re']; 
$id = $_GET['id']; 


if ($re == '') {
	$resultsC = Requisicionprocesocomentarios::model()->findAll(array('condition'=> "id=" . $id,'order' => 'RequisicionID ASC'));
        foreach ($resultsC as $resultC)
 	{
           $re = $resultC->RequisicionID;
       } 

}

if (strlen($model->requisionprocesoID) > 0) {


} else {

$model->requisionprocesoID = $rp;
$model->RequisicionID = $re;

}

$model->username = Yii::app()->user->name;
$dat = date("Y-m-d h:m:s");
$model->fecha = $dat;

/* Busco el cargo de la requisicion*/
$cargo = '******';
$results = Requisicion::model()->findAll(array('condition'=> "RequisicionID=" . $re,'order' => 'UnidadNegocioID ASC'));

 foreach ($results as $result)
 {
      $cargo = $result->NombreCargo;
 }
 
 echo $cargo;  
?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'requisionprocesoID'); ?>
		<?php echo $form->textField($model,'requisionprocesoID',array('size'=>10,'maxlength'=>10,'readonly'=>true)); ?>
		<?php echo $form->error($model,'requisionprocesoID');?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RequisicionID'); ?>
		<?php echo $form->textField($model,'RequisicionID',array('size'=>10,'maxlength'=>10,'readonly'=>true)); ?>
		<?php echo $form->error($model,'RequisicionID'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'tipocomentarioID'); ?>
		<?php
                     if ($rp== 0) {   
                           $model->tipocomentarioID = "PREVIO PROCESO";                      
                           echo $form->dropDownList($model,'tipocomentarioID', CHtml::listData(Tipocomentario::model()->findAll(), 'id', 'id'),array('readonly'=>true)); 
                           
                     } Else {
                           echo $form->dropDownList($model,'tipocomentarioID', CHtml::listData(Tipocomentario::model()->findAll(), 'id', 'id'));
                     }    
                ?> 
		<?php echo $form->error($model,'tipocomentarioID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comentarios'); ?>
		<?php echo $form->textarea($model,'comentarios',array('size'=>1000,'maxlength'=>3000,'cols'=>85 ,'rows'=>10)); ?>
                <?php //echo $form->textarea($model,'DescripcionCargoPar',array('size'=>60,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?> 
		<?php echo $form->error($model,'comentarios'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php //echo $form->textField($model,'fecha',array('readonly'=>true)); ?>
                  <?php $this->widget('application.extensions.timepicker.timepicker', array(
		     'model'=>$model,
		     'name'=>'fecha',
	
		   )); ?>  
      	

                    &nbsp;(Formato de la fecha : AAAA-MM-DD)
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128,'readonly'=>true)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear comentario' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->