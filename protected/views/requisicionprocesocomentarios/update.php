<?php
$this->breadcrumbs=array(
	'Requisicionprocesocomentarioses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>'List Requisicionprocesocomentarios', 'url'=>array('index')),
	//array('label'=>'Create Requisicionprocesocomentarios', 'url'=>array('create')),
	//array('label'=>'View Requisicionprocesocomentarios', 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>'Manage Requisicionprocesocomentarios', 'url'=>array('admin')),
);
?>

<h1>Actualizar Comentarios <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>