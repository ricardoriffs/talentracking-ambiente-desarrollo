<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'RequisicionID'); ?>
		<?php echo $form->textField($model,'RequisicionID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UnidadNegocioID'); ?>
		<?php echo $form->textField($model,'UnidadNegocioID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AutorizacionProceso'); ?>
		<?php echo $form->textField($model,'AutorizacionProceso',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NombreAutoriza'); ?>
		<?php echo $form->textField($model,'NombreAutoriza',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'EmailAutoriza'); ?>
		<?php echo $form->textField($model,'EmailAutoriza',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AreaVicepresidenciaID'); ?>
		<?php echo $form->textField($model,'AreaVicepresidenciaID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RutaArchivoAutorizacion'); ?>
		<?php echo $form->textField($model,'RutaArchivoAutorizacion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroVacantes'); ?>
		<?php echo $form->textField($model,'NumeroVacantes',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NombreCargo'); ?>
		<?php echo $form->textField($model,'NombreCargo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NombreSolicitante'); ?>
		<?php echo $form->textField($model,'NombreSolicitante',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CargoSolicitante'); ?>
		<?php echo $form->textField($model,'CargoSolicitante',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'JefeInmediatoVacante'); ?>
		<?php echo $form->textField($model,'JefeInmediatoVacante',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LugarTrabajo'); ?>
		<?php echo $form->textField($model,'LugarTrabajo',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RutaDescripcionCargo'); ?>
		<?php echo $form->textField($model,'RutaDescripcionCargo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DescripcionCargoPar'); ?>
		<?php echo $form->textField($model,'DescripcionCargoPar',array('size'=>60,'maxlength'=>4000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TurnoTrabajo'); ?>
		<?php echo $form->textField($model,'TurnoTrabajo',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoContratacionID'); ?>
		<?php echo $form->textField($model,'TipoContratacionID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TiempoContratacion'); ?>
		<?php echo $form->textField($model,'TiempoContratacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AspectosImportantes'); ?>
		<?php echo $form->textField($model,'AspectosImportantes',array('size'=>60,'maxlength'=>4000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Observaciones'); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>60,'maxlength'=>2000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RangoSalario'); ?>
		<?php echo $form->textField($model,'RangoSalario',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NivelHAY'); ?>
		<?php echo $form->textField($model,'NivelHAY',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PuntosHAY'); ?>
		<?php echo $form->textField($model,'PuntosHAY',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoProcesoSeleccionID'); ?>
		<?php echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UsuarioIDResponsable'); ?>
		<?php echo $form->textField($model,'UsuarioIDResponsable',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actividadID'); ?>
		<?php echo $form->textField($model,'actividadID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'politicaID'); ?>
		<?php echo $form->textField($model,'politicaID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->