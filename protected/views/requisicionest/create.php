<?php
$this->breadcrumbs=array(
	'Requisicionests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Requisicionest', 'url'=>array('index')),
	array('label'=>'Manage Requisicionest', 'url'=>array('admin')),
);
?>

<h1>Create Requisicionest</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>