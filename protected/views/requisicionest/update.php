<?php
$this->breadcrumbs=array(
	'Requisicionests'=>array('index'),
	$model->RequisicionID=>array('view','id'=>$model->RequisicionID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Requisicionest', 'url'=>array('index')),
	array('label'=>'Create Requisicionest', 'url'=>array('create')),
	array('label'=>'View Requisicionest', 'url'=>array('view', 'id'=>$model->RequisicionID)),
	array('label'=>'Manage Requisicionest', 'url'=>array('admin')),
);
?>

<h1>Update Requisicionest <?php echo $model->RequisicionID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>