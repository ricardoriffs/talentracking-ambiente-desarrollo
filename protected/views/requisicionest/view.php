<?php
$this->breadcrumbs=array(
	'Requisicionests'=>array('index'),
	$model->RequisicionID,
);

$this->menu=array(
	array('label'=>'List Requisicionest', 'url'=>array('index')),
	array('label'=>'Create Requisicionest', 'url'=>array('create')),
	array('label'=>'Update Requisicionest', 'url'=>array('update', 'id'=>$model->RequisicionID)),
	array('label'=>'Delete Requisicionest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->RequisicionID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Requisicionest', 'url'=>array('admin')),
);
?>

<h1>View Requisicionest #<?php echo $model->RequisicionID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'RequisicionID',
		'UnidadNegocioID',
		'AutorizacionProceso',
		'NombreAutoriza',
		'EmailAutoriza',
		'AreaVicepresidenciaID',
		'RutaArchivoAutorizacion',
		'NumeroVacantes',
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
		'LugarTrabajo',
		'RutaDescripcionCargo',
		'DescripcionCargoPar',
		'TurnoTrabajo',
		'TipoContratacionID',
		'TiempoContratacion',
		'AspectosImportantes',
		'Observaciones',
		'RangoSalario',
		'NivelHAY',
		'PuntosHAY',
		'TipoProcesoSeleccionID',
		'UsuarioIDResponsable',
		'actividadID',
		'politicaID',
	),
)); ?>
