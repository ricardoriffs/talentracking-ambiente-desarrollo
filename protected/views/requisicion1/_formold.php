<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicion1-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'UnidadNegocioID'); ?>
		<?php echo $form->textField($model,'UnidadNegocioID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UnidadNegocioID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AutorizacionProceso'); ?>
		<?php echo $form->textField($model,'AutorizacionProceso',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'AutorizacionProceso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreAutoriza'); ?>
		<?php echo $form->textField($model,'NombreAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NombreAutoriza'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EmailAutoriza'); ?>
		<?php echo $form->textField($model,'EmailAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'EmailAutoriza'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AreaVicepresidenciaID'); ?>
		<?php echo $form->textField($model,'AreaVicepresidenciaID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'AreaVicepresidenciaID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RutaArchivoAutorizacion'); ?>
		<?php echo $form->textField($model,'RutaArchivoAutorizacion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'RutaArchivoAutorizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NumeroVacantes'); ?>
		<?php echo $form->textField($model,'NumeroVacantes',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NumeroVacantes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreCargo'); ?>
		<?php echo $form->textField($model,'NombreCargo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'NombreCargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreSolicitante'); ?>
		<?php echo $form->textField($model,'NombreSolicitante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NombreSolicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CargoSolicitante'); ?>
		<?php echo $form->textField($model,'CargoSolicitante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'CargoSolicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'JefeInmediatoVacante'); ?>
		<?php echo $form->textField($model,'JefeInmediatoVacante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'JefeInmediatoVacante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LugarTrabajo'); ?>
		<?php echo $form->textField($model,'LugarTrabajo',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'LugarTrabajo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RutaDescripcionCargo'); ?>
		<?php echo $form->textField($model,'RutaDescripcionCargo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'RutaDescripcionCargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DescripcionCargoPar'); ?>
		<?php echo $form->textField($model,'DescripcionCargoPar',array('size'=>60,'maxlength'=>4000)); ?>
		<?php echo $form->error($model,'DescripcionCargoPar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TurnoTrabajo'); ?>
		<?php echo $form->textField($model,'TurnoTrabajo',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'TurnoTrabajo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoContratacionID'); ?>
		<?php echo $form->textField($model,'TipoContratacionID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'TipoContratacionID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TiempoContratacion'); ?>
		<?php echo $form->textField($model,'TiempoContratacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'TiempoContratacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AspectosImportantes'); ?>
		<?php echo $form->textField($model,'AspectosImportantes',array('size'=>60,'maxlength'=>4000)); ?>
		<?php echo $form->error($model,'AspectosImportantes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Observaciones'); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RangoSalario'); ?>
		<?php echo $form->textField($model,'RangoSalario',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'RangoSalario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NivelHAY'); ?>
		<?php echo $form->textField($model,'NivelHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NivelHAY'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PuntosHAY'); ?>
		<?php echo $form->textField($model,'PuntosHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'PuntosHAY'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoProcesoSeleccionID'); ?>
		<?php echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'TipoProcesoSeleccionID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UsuarioIDResponsable'); ?>
		<?php echo $form->textField($model,'UsuarioIDResponsable',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioIDResponsable'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->