<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>


<div class="form">



<script language="JavaScript1.2">

function ventanaSecundaria (URL, alto, ancho)
{ 
   window.open(URL,"ventana1","width="+ancho+",height="+alto+",scrollbars=YES") 
} 
</script>



<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicion1-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con asterisco <span class="required">(*)</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
          
    <diV class = "divContentBlue">   
        <div class="row">
	<h2 align="center" class="Estilo2">IDENTIFICACIÓN DEL CARGO</h2>
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'UnidadNegocioID'); ?>
		<?php echo $form->dropDownList($model,'UnidadNegocioID', CHtml::listData(Unidadnegocio::model()->findAll(), 'UnidadNegocioID', 'Nombre')); ?>
		<?php echo $form->error($model,'UnidadNegocioID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AutorizacionProceso'); ?>
		<?php echo $form->dropDownList($model,'AutorizacionProceso', CHtml::listData(Autorizaproceso::model()->findAll(), 'id', 'id')); ?> 
		<?php echo $form->error($model,'AutorizacionProceso'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'NombreAutoriza'); ?>
		<?php //echo $form->textField($model,'NombreAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'NombreAutoriza'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'EmailAutoriza'); ?>
		<?php //echo $form->textField($model,'EmailAutoriza',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'EmailAutoriza'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AreaVicepresidenciaID'); ?>
		<?php echo $form->dropDownList($model,'AreaVicepresidenciaID', CHtml::listData(Areavicepresidencia::model()->findAll(), 'AreaVicepresidenciaID', 'Nombre')); ?>
		<?php echo $form->error($model,'AreaVicepresidenciaID'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'RutaArchivoAutorizacion'); ?>        
		<?php //echo $form->textField($model,'RutaArchivoAutorizacion',array('size'=>60,'maxlength'=>255)); ?>
                <a href="javascript:ventanaSecundaria('\popup_noticia1.php?r=f<?echo $model->RequisicionID ?>' ,400,400)">Ver autorización de la requisición de personal ...</a>
		<?php echo $form->error($model,'RutaArchivoAutorizacion'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'NumeroVacantes'); ?>
		<?php echo $form->textField($model,'NumeroVacantes',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NumeroVacantes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreCargo'); 
	                $criteria=new CDbCriteria;
			$criteria->select='id';  // only select the 'title' column
			$criteria->condition='tipocargo=:tipocargo';
			$criteria->params=array(':tipocargo'=>R);
			//$post=Post::model()->find($criteria); // $params is not needed 

                ?>

                <?php //echo $form->dropDownList($model,'NombreCargo', CHtml::listData(cargo::model()->findall($criteria), 'id', 'id')); ?> 
		<?php// echo $form->textField($model,'NombreCargo',array('size'=>60,'maxlength'=>255)); ?>

                <?php	$this->widget('ext.combobox.EJuiComboBox', array(
    			'model' => $model,
    			'attribute' => 'NombreCargo',
    			// data to populate the select. Must be an array.
    			'data' =>CHtml::listData(Cargo::model()->findall($criteria), 'id', 'id'), 
                        //array('yii','is','fun','!'),
    			// options passed to plugin
    			'options' => array(
        		// JS code to execute on 'select' event, the selected item is
        		// available through the 'item' variable.
        		//'onSelect' => 'alert("selected value : " + item.value);',
        		// JS code to be executed on 'change' event, the input is available
        		// through the '$(this)' variable.
        		'onChange' => 'alert("changed value : " + $(this).val());',
        		// If false, field value must be present in the select.
        		// Defaults to true.
        		'allowText' => false,
    			),
    			// Options passed to the text input
    			'htmlOptions' => array('size' => 70),
		));	
        ?>

		<?php 

              Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/requisicion.js', CClientScript::POS_END); 
              echo $form->error($model,'NombreCargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreSolicitante'); ?>
		<?php echo $form->textField($model,'NombreSolicitante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NombreSolicitante'); ?>
	</div>

	<div class="row">
               <?php echo $form->labelEx($model,'CargoSolicitante');                           
                        $criteria=new CDbCriteria;
			$criteria->select='id';  // only select the 'title' column
			$criteria->condition='tipocargo=:tipocargo';
			$criteria->params=array(':tipocargo'=>V);
			//$post=Post::model()->find($criteria); // $params is not needed 		<?php echo $form->labelEx($model,'CargoSolicitante'); 


                ?>
		<?php //echo $form->textField($model,'CargoSolicitante',array('size'=>50,'maxlength'=>50)); ?>
                <?php //echo $form->dropDownList($model,'CargoSolicitante', CHtml::listData(cargo::model()->findall($criteria), 'id', 'id')); ?>   


		<?php	$this->widget('ext.combobox.EJuiComboBox', array(
    			'model' => $model,
    			'attribute' => 'CargoSolicitante',
    			// data to populate the select. Must be an array.
    			'data' =>CHtml::listData(Cargo::model()->findall($criteria), 'id', 'id'), 
                        //array('yii','is','fun','!'),
    			// options passed to plugin
    			'options' => array(
        		// JS code to execute on 'select' event, the selected item is
        		// available through the 'item' variable.
        		//'onSelect' => 'alert("selected value : " + item.value);',
        		// JS code to be executed on 'change' event, the input is available
        		// through the '$(this)' variable.
        		'onChange' => 'alert("changed value : " + $(this).val());',
        		// If false, field value must be present in the select.
        		// Defaults to true.
        		'allowText' => false,
    			),
    			// Options passed to the text input
    			'htmlOptions' => array('size' => 70),
		));	
        ?> 


		<?php echo $form->error($model,'CargoSolicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'JefeInmediatoVacante'); ?>
		<?php echo $form->textField($model,'JefeInmediatoVacante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'JefeInmediatoVacante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LugarTrabajo'); ?>
		<?php echo $form->dropDownList($model,'LugarTrabajo', CHtml::listData(Sedescompanias::model()->findAll(), 'SedeCompaniaID', 'Nombre')); ?>
		<?php echo $form->error($model,'LugarTrabajo'); ?>
	</div>
        
   </div>

   <div>     
		<br> </br>
  
  </div>

        
 <diV class = "divContentBlue">   
        <div class="row">
		<h2 align="center" class="Estilo2">INFORMACIÓN DEL CARGO</h2>
        </div>
        
	<div class="row">
		    <?php //echo $form->labelEx($model,'RutaDescripcionCargo'); ?>
                    <a href="javascript:ventanaSecundaria('\popup_noticia1.php?r=d<?echo $model->RequisicionID ?>' ,400,400)">Ver descripción de cargo ...</a>
             
                    <?php //echo $form->textField($model,'RutaDescripcionCargo',array('size'=>60,'maxlength'=>255)); ?>
		    <?php echo $form->error($model,'RutaDescripcionCargo'); ?>
	</div>

	<div class="row">
                
		<?php echo $form->labelEx($model,'DescripcionCargoPar'); ?>
		<?php echo $form->textarea($model,'DescripcionCargoPar',array('size'=>60,'maxlength'=>4000,'cols'=>50 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'DescripcionCargoPar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TurnoTrabajo'); ?>
		<?php// echo $form->textField($model,'TurnoTrabajo',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->dropDownList($model,'TurnoTrabajo', CHtml::listData(Turno::model()->findall(), 'id', 'id')); ?>    
		<?php echo $form->error($model,'TurnoTrabajo'); ?>
	</div>

        <div id="TipoContratacionID1" class="row">
		<?php echo $form->labelEx($model,'TipoContratacionID'); ?>
		<?php echo $form->dropDownList($model,'TipoContratacionID', CHtml::listData(Tipocontratacion::model()->findAll(), 'TipoContratacionID', 'Nombre')); ?>
                <?php echo $form->error($model,'TipoContratacionID'); ?>
	</div>

	<div id="TiempoContratacion1" class="row">
         
		
                <?php echo $form->labelEx($model,'TiempoContratacion'); ?>
                <?php echo $form->textField($model,'TiempoContratacion',array('size'=>50,'maxlength'=>50)); ?> 		
		<?php echo $form->error($model,'TiempoContratacion') . '-MESES-'; ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AspectosImportantes'); ?>
		<?php echo $form->textarea($model,'AspectosImportantes',array('size'=>60,'maxlength'=>4000,'cols'=>50 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'AspectosImportantes'); ?>
	</div>
</div>

<div>     
		<br> </br>
  
  </div>

 <diV class = "divContentBlue">   
        <div class="row">
	<h2 align="center" class="Estilo2">CAMPOS A DEFINIR POR EL GERENTE DE TALENTO HUMANO</h2>
        </div>


	<div class="row">
		<?php  echo $form->labelEx($model,'Observaciones'); ?>
		<?php echo $form->textarea($model,'Observaciones',array('size'=>60,'maxlength'=>2000,'cols'=>50 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RangoSalario' . " _____ INFERIOR ( Millones-Miles-Cientos )"); ?>
                <?php //echo $form->dropDownList($model,'RangoSalario', CHtml::listData(rangosalario::model()->findall(), 'id', 'id')); ?>    
		<?php //echo $form->textField($r,'RangoSalario1',array('size'=>3,'maxlength'=>3)); ?>
               
                <?php //echo $form->textField($model,'si1',array('size'=>3,'maxlength'=>3)); ?>
                <?php echo $form->dropDownList($model,'si1',array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30')); ?> 
                <?php echo $form->dropDownList($model,'si2',array("000"=>"000","100"=>"100","200"=>"200","300"=>"300","400"=>"400","500"=>"500","600"=>"600","700"=>"700","800"=>"800","900"=>"900")); ?> 
                <?php echo $form->dropDownList($model,'si3',array("000"=>"000","100"=>"100","200"=>"200","300"=>"300","400"=>"400","500"=>"500","600"=>"600","700"=>"700","800"=>"800","900"=>"900")); ?> 

                <?php echo "<BR>"; ?> 

                <?php $model->si = Yii::app()->format->number($model->si1 . $model->si2 . $model->si3); ?> 
                <?php echo $form->textField($model,'si',array('size'=>10,'maxlength'=>10)); ?>


                <?php echo $form->labelEx($model,'RangoSalario' . " _____ SUPERIOR ( Millones-Miles-Cientos )"); ?>

                <?php echo $form->dropDownList($model,'ss1',array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30')); ?> 
                <?php echo $form->dropDownList($model,'ss2',array("000"=>"000","100"=>"100","200"=>"200","300"=>"300","400"=>"400","500"=>"500","600"=>"600","700"=>"700","800"=>"800","900"=>"900")); ?> 
                <?php echo $form->dropDownList($model,'ss3',array("000"=>"000","100"=>"100","200"=>"200","300"=>"300","400"=>"400","500"=>"500","600"=>"600","700"=>"700","800"=>"800","900"=>"900")); ?> 

                <?php echo "<BR>"; ?> 
                <?php $model->ss = Yii::app()->format->number($model->ss1 . $model->ss2 . $model->ss3); ?> 
                <?php echo $form->textField($model,'ss',array('size'=>10,'maxlength'=>10)); ?>

		<?php echo $form->error($model,'RangoSalario'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'perfilsalarial'); ?>
                <?php echo $form->dropDownList($model,'perfilsalarial', CHtml::listData(Perfilsalarial::model()->findall(), 'id', 'id')); ?>    
		<?php //echo $form->textField($model,'RangoSalario',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'perfilsalarial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NivelHAY'); ?>
		<?php echo $form->textField($model,'NivelHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'NivelHAY'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PuntosHAY'); ?>
		<?php echo $form->textField($model,'PuntosHAY',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'PuntosHAY'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'TipoProcesoSeleccionID'); ?>
		<?php //echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'TipoProcesoSeleccionID'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioIDResponsable'); ?>
		<?php //echo $form->textField($model,'UsuarioIDResponsable',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'UsuarioIDResponsable'); ?>
                <?php //echo 'Manifiesto que he leido las politicas de la compania en cuanto a seleccion y vinculacion'; ?>   
                <?php //echo $form->checkbox($model,'Observaciones',array('name'=>politica, 'value'=>no )); ?> 
                  
 
	</div>


</div>

	

        <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : '       Previsualizar Requisición       '); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->