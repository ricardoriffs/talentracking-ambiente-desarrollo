<?php
$this->breadcrumbs=array(
	'Requisicion1s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista Requisiciones', 'url'=>array('index')),
	array('label'=>'Administrador Requisiciones', 'url'=>array('admin')),
);
?>

<h1>Actualizar Requisiciones Gerente Talento Humano</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>