<?php
$this->breadcrumbs=array(
	'Requisicion1s'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Lista Requisiciones', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicion1-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrador Requisiciones Gerente Talento Humano</h1>

<p>
</p>

<?php echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); 
  $view = $_GET['view']; 
  //echo $view;
  $id = $view;
if ($id == 'subsubr2n1'){ 
	echo "<script language='JavaScript'>alert('Esta requisicion sera devuelta al cliente interno, antes de hacerlo asegurese de diligenciar el campo de observaciones destinado para este fin');</script>";

}

if ($id == 'subsubr2n2'){ 
	echo "<script language='JavaScript'>alert('Esta requisicion sera enviada al lider de seleccion para iniciar el proceso de seleccion, para hacer seguimiento del proceso haga click en seguimiento en linea desde el menu principal');</script>";

}


?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicion1-grid',
	'dataProvider'=>$model->search($view),
	'filter'=>$model,
	'columns'=>array(
		'RequisicionID',
		array ('name'=>'UnidadNegocioID','value'=>'$data->unidadnegocio->Nombre','type'=>'text',),
                array ('name'=>'AreaVicepresidenciaID','value'=>'$data->areasvicepresidencia->Nombre','type'=>'text', 'htmlOptions'=>array('width'=>240,'style'=>'font-size:8pt'), ),  
		//'AutorizacionProceso',
		//'NombreAutoriza',
		//'EmailAutoriza',
		//'AreaVicepresidenciaID',
		'fechacreacion',                
                'usuariocreacion',
                array ('name'=>'NombreCargo','value'=>'$data->NombreCargo','type'=>'text', 'htmlOptions'=>array('width'=>230,'style'=>'font-size:8pt'), ),    

                array(
                'header'=>'Comentarios',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode(Comentarios),Yii::app()->baseurl."/index.php?r=requisicionprocesocomentarios/admin&re=".$data->RequisicionID."&rp=0", array("target"=>"_self"))',  
                ),

                //'NombreCargo',  
		/*
		'RutaArchivoAutorizacion',
		'NumeroVacantes',
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
		'LugarTrabajo',
		'RutaDescripcionCargo',
		'DescripcionCargoPar',
		'TurnoTrabajo',
		'TipoContratacionID',
		'TiempoContratacion',
		'AspectosImportantes',
		'Observaciones',
		'RangoSalario',
		'NivelHAY',
		'PuntosHAY',
		'TipoProcesoSeleccionID',
		'UsuarioIDResponsable',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}{update}',  
		),
	),
)); ?>