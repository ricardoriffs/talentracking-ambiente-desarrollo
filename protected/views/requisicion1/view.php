<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>


<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/requisicion1.js', CClientScript::POS_END);

$this->breadcrumbs=array(
	'Requisicion1s'=>array('index'),
	$model->RequisicionID,
);

$this->menu=array(
	array('label'=>'Lista Requisiciones', 'url'=>array('index')),
	array('label'=>'Crear Requisicion', 'url'=>array('create')),
	array('label'=>'Actulizar Requisicion', 'url'=>array('update', 'id'=>$model->RequisicionID)),
	array('label'=>'Borrar Requisicion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->RequisicionID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Requisicion', 'url'=>array('admin&view=subsubr2')),
);
?>

<h1>Vista Requisicion Gerente Talento Humano #<?php echo $model->RequisicionID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'RequisicionID',
		 array(
                                'name'=>'Unidad de Negocio',
                                'value'=>CHtml::encode($model->unidadnegocio->Nombre),
                        ), 
		'AutorizacionProceso',
		//'NombreAutoriza',
		//'EmailAutoriza',
		array(
                                'name'=>'Area - Vicepresidencia',
                                'value'=>CHtml::encode($model->areasvicepresidencia->Nombre),
                        ),
		//'RutaArchivoAutorizacion',
		'NumeroVacantes',
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
		array(
                                'name'=>'Lugar de Trabajo',
                                'value'=>CHtml::encode($model->sedescompanias->Nombre),
                        ),
		//'RutaDescripcionCargo',
		'DescripcionCargoPar',
		'TurnoTrabajo',
		                array(
                                'name'=>'Tipo de Contracion',
                                'value'=>CHtml::encode($model->tipocontratacion->Nombre),
                        ),		

		'TiempoContratacion',
		'AspectosImportantes',
		'Observaciones',
		 array(
                                'name'=>'si',
                                'value'=>CHtml::encode($model->si1.",".$model->si2.",".$model->si3),
                        ), 
                 array(
                                'name'=>'ss',
                                'value'=>CHtml::encode($model->ss1.",".$model->ss2.",".$model->ss3),
                        ),  

                'perfilsalarial',
		'NivelHAY',
		'PuntosHAY',
		//'TipoProcesoSeleccionID',
		//'UsuarioIDResponsable',
	),
)); ?>
       



<div id="pol" class="row">



  <?php echo 'Si desea devolver esta requisicion al cliente interno seleccione checkbox situado en la parte final de este parrafo,'; ?> 
  <?php echo 'esto significa que la informacion consignada en la solicitud de personal no esta completa,'; ?>  
  <?php echo 'por favor diligencie el campo de observaciones para informar al cliente interno los temas especificos que debe modificar'; ?>  
   <?= CHtml::checkBox('SI',false,array('id' => 'polres')); ?> 
   <?php //CHtml::checkBox('SI',false,array('id' => 'polres', 'onchange' => 'javascript:$("#b1").toggle(); javascript:$("#b2").toggle()')); ?>
   <?php //echo CHtml::dropDownList('pol','', array('NO','SI')); array('onchange'=>'alert(value);')?>
                  
</div>








<div id="b1" class="row buttons">
            
            <?php
 
                    echo nl2br("\n"); 
	            echo nl2br("\n");  		
                    if ($model->RequisicionID!= "") {?>
                       <a class="boton"  href=<?php echo 'index.php?r=requisicion1/devolverejecucion&id=' . $model->RequisicionID.'>' ?>       Devolver-----------Cliente------------Interno          </a> 
                    
 </div>                     


<div id="b2" class="row buttons">
                 <?php   
                    echo nl2br("\n"); 
                    echo nl2br("\n"); 
                      
                    
                  ?>
                         <a class="boton"  href=<?php echo 'index.php?r=requisicion1/crearejecucion&id=' . $model->RequisicionID.'>' ?> Enviar requisicion a Lider de Seleccion </a>                       
                  <?php   
                    echo nl2br("\n"); 
                    echo nl2br("\n");  
                    
                    ?> 
                   
                         <a class="boton"  href=<?php echo 'index.php?r=requisicion1/update&id=' . $model->RequisicionID.'>' ?> Editar Requisicion </a>                        

                  <?php  }
                    
                  ?>
             
         		
 </div>                     
