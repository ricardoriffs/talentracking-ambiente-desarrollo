<?php
$this->breadcrumbs=array(
	'Requisicion1s'=>array('index'),
	$model->RequisicionID=>array('view','id'=>$model->RequisicionID),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista Requisiciones', 'url'=>array('index')),
	array('label'=>'Crear Requisiciones', 'url'=>array('create')),
	array('label'=>'Vista Requisiciones', 'url'=>array('view', 'id'=>$model->RequisicionID)),
	array('label'=>'Administrador Requisiciones', 'url'=>array('admin')),
);
?>

<h1>Actualizar Requisiciones Gerente Talento Humano <?php echo $model->RequisicionID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php //echo CHtml::button('Enviar requisicion a Lider Seleccion', array('submit' => array('Crearejecucion', 'id'=>$model->RequisicionID))); ?>