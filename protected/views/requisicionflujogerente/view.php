<?php
$this->breadcrumbs=array(
	'Requisicionflujogerentes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Requisicionflujogerente', 'url'=>array('index')),
	array('label'=>'Create Requisicionflujogerente', 'url'=>array('create')),
	array('label'=>'Update Requisicionflujogerente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Requisicionflujogerente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Requisicionflujogerente', 'url'=>array('admin')),
);
?>

<h1>View Requisicionflujogerente #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'UnidadNegocioID',
		'AreaVicepresidenciaID',
		'usuarioasignacion',
	),
)); ?>
