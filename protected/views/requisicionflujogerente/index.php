<?php
$this->breadcrumbs=array(
	'Requisicionflujogerentes',
);

$this->menu=array(
	array('label'=>'Create Requisicionflujogerente', 'url'=>array('create')),
	array('label'=>'Manage Requisicionflujogerente', 'url'=>array('admin')),
);
?>

<h1>Requisicionflujogerentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
