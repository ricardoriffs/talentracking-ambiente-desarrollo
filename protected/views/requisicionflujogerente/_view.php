<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UnidadNegocioID')); ?>:</b>
	<?php echo CHtml::encode($data->UnidadNegocioID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AreaVicepresidenciaID')); ?>:</b>
	<?php echo CHtml::encode($data->AreaVicepresidenciaID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuarioasignacion')); ?>:</b>
	<?php echo CHtml::encode($data->usuarioasignacion); ?>
	<br />


</div>