<?php
$this->breadcrumbs=array(
	'Requisicionflujogerentes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Requisicionflujogerente', 'url'=>array('index')),
	array('label'=>'Create Requisicionflujogerente', 'url'=>array('create')),
	array('label'=>'View Requisicionflujogerente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Requisicionflujogerente', 'url'=>array('admin')),
);
?>

<h1>Update Requisicionflujogerente <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>