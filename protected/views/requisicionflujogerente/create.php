<?php
$this->breadcrumbs=array(
	'Requisicionflujogerentes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Requisicionflujogerente', 'url'=>array('index')),
	array('label'=>'Manage Requisicionflujogerente', 'url'=>array('admin')),
);
?>

<h1>Create Requisicionflujogerente</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>