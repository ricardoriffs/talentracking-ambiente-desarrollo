<?php
$this->breadcrumbs=array(
	'Requisicionflujogerentes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Requisicionflujogerente', 'url'=>array('index')),
	array('label'=>'Create Requisicionflujogerente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicionflujogerente-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Requisicionflujogerentes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionflujogerente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'UnidadNegocioID',
            array ('name'=>'UnidadNegocioID','value'=>'$data->unidadnegocio->Nombre','type'=>'text',), 
            array ('name'=>'AreaVicepresidenciaID','value'=>'$data->areavicepresidencia->Nombre','type'=>'text',), 
            //array ('name'=>'usuarioasignacion','value'=>'$data->usuario->username','type'=>'text',), 
 
		//'AreaVicepresidenciaID',
		'usuarioasignacion',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
