<?php
/* @var $this TipocontratacionController */
/* @var $model Tipocontratacion */

$this->breadcrumbs=array(
	'Tipocontratacions'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Tipocontratacion', 'url'=>array('index')),
	array('label'=>'Administrar tipos de contratación', 'url'=>array('admin')),
);
?>

<h1>Crear tipo de contratación</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>