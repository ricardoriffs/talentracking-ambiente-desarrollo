<?php
/* @var $this TipocontratacionController */
/* @var $model Tipocontratacion */

$this->breadcrumbs=array(
	'Tipocontratacions'=>array('index'),
	$model->TipoContratacionID=>array('view','id'=>$model->TipoContratacionID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Tipocontratacion', 'url'=>array('index')),
//	array('label'=>'Create Tipocontratacion', 'url'=>array('create')),
	array('label'=>'Ver tipo de contratación', 'url'=>array('view', 'id'=>$model->TipoContratacionID)),
	array('label'=>'Administar tipo de contratación', 'url'=>array('admin')),
);
?>

<h1>Editar tipo de contratación "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>