<?php
/* @var $this TipocontratacionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipocontratacions',
);

$this->menu=array(
	array('label'=>'Create Tipocontratacion', 'url'=>array('create')),
	array('label'=>'Manage Tipocontratacion', 'url'=>array('admin')),
);
?>

<h1>Tipocontratacions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
