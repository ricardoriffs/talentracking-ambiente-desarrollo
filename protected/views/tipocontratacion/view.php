<?php
/* @var $this TipocontratacionController */
/* @var $model Tipocontratacion */

$this->breadcrumbs=array(
	'Tipocontratacions'=>array('index'),
	$model->TipoContratacionID,
);

$this->menu=array(
//	array('label'=>'List Tipocontratacion', 'url'=>array('index')),
//	array('label'=>'Create Tipocontratacion', 'url'=>array('create')),
	array('label'=>'Editar tipo de contratación', 'url'=>array('update', 'id'=>$model->TipoContratacionID)),
	array('label'=>'Borrar tipo de contratación', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TipoContratacionID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar tipos de contratación', 'url'=>array('admin')),
);
?>

<h1>Ver tipo de contratación "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'TipoContratacionID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
                array(
                        'name'=>'ModuloID',
                        'value'=>CHtml::encode($model->modulo->Nombre),
                ), 
//		'CompaniaID',
	),
)); ?>
