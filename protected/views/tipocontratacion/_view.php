<?php
/* @var $this TipocontratacionController */
/* @var $data Tipocontratacion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoContratacionID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TipoContratacionID), array('view', 'id'=>$data->TipoContratacionID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ModuloID')); ?>:</b>
	<?php echo CHtml::encode($data->ModuloID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::encode($data->CompaniaID); ?>
	<br />

	*/ ?>

</div>