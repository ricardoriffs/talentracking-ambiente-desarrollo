<?php
/* @var $this RangosalarioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rangosalarios',
);

$this->menu=array(
	array('label'=>'Create Rangosalario', 'url'=>array('create')),
	array('label'=>'Manage Rangosalario', 'url'=>array('admin')),
);
?>

<h1>Rangosalarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
