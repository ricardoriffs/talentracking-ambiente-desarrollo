<?php
/* @var $this RangosalarioController */
/* @var $model Rangosalario */

$this->breadcrumbs=array(
	'Rangosalarios'=>array('index'),
	$model->RangoSalarioID=>array('view','id'=>$model->RangoSalarioID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Rangosalario', 'url'=>array('index')),
//	array('label'=>'Create Rangosalario', 'url'=>array('create')),
	array('label'=>'Ver Rango de Salario', 'url'=>array('view', 'id'=>$model->RangoSalarioID)),
	array('label'=>'Administrar Rangos de Salario', 'url'=>array('admin')),
);
?>

<h1>Editar Rango de Salario "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>