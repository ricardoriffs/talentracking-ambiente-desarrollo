<?php
/* @var $this RangosalarioController */
/* @var $model Rangosalario */

$this->breadcrumbs=array(
	'Rangosalarios'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Rangosalario', 'url'=>array('index')),
	array('label'=>'Administrar Rangos de Salario', 'url'=>array('admin')),
);
?>

<h1>Crear Rango de Salario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>