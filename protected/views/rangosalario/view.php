<?php
/* @var $this RangosalarioController */
/* @var $model Rangosalario */

$this->breadcrumbs=array(
	'Rangosalarios'=>array('index'),
	$model->RangoSalarioID,
);

$this->menu=array(
//	array('label'=>'List Rangosalario', 'url'=>array('index')),
//	array('label'=>'Create Rangosalario', 'url'=>array('create')),
	array('label'=>'Editar Rango de Salario', 'url'=>array('update', 'id'=>$model->RangoSalarioID)),
	array('label'=>'Borrar Rango de Salario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->RangoSalarioID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Rangos de Salario', 'url'=>array('admin')),
);
?>

<h1>Ver Rango de Salario "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'RangoSalarioID',
		'id',
//		'CompaniaID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),  
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
