<?php
/* @var $this EjecucionactividadController */
/* @var $model Ejecucionactividad */

$this->breadcrumbs=array(
	'Ejecucionactividads'=>array('index'),
	$model->requisicionactividadID=>array('view','id'=>$model->requisicionactividadID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ejecucionactividad', 'url'=>array('index')),
	array('label'=>'Create Ejecucionactividad', 'url'=>array('create')),
	array('label'=>'View Ejecucionactividad', 'url'=>array('view', 'id'=>$model->requisicionactividadID)),
	array('label'=>'Manage Ejecucionactividad', 'url'=>array('admin')),
);
?>

<h1>Update Ejecucionactividad <?php echo $model->requisicionactividadID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>