<?php
/* @var $this EjecucionactividadController */
/* @var $data Ejecucionactividad */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisicionactividadID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->requisicionactividadID), array('view', 'id'=>$data->requisicionactividadID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RequisionID')); ?>:</b>
	<?php echo CHtml::encode($data->RequisionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actividadflujoID')); ?>:</b>
	<?php echo CHtml::encode($data->actividadflujoID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuarioID')); ?>:</b>
	<?php echo CHtml::encode($data->usuarioID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacion')); ?>:</b>
	<?php echo CHtml::encode($data->observacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::encode($data->CompaniaID); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	*/ ?>

</div>