<?php
/* @var $this EjecucionactividadController */
/* @var $model Ejecucionactividad */

$this->breadcrumbs=array(
	'Ejecucionactividads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ejecucionactividad', 'url'=>array('index')),
	array('label'=>'Manage Ejecucionactividad', 'url'=>array('admin')),
);
?>

<h1>Create Ejecucionactividad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>