<?php
/* @var $this EjecucionactividadController */
/* @var $model Ejecucionactividad */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ejecucionactividad-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'RequisionID'); ?>
		<?php echo $form->textField($model,'RequisionID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'RequisionID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actividadflujoID'); ?>
		<?php echo $form->textField($model,'actividadflujoID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'actividadflujoID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuarioID'); ?>
		<?php echo $form->textField($model,'usuarioID',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'usuarioID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observacion'); ?>
		<?php echo $form->textField($model,'observacion',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'observacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CompaniaID'); ?>
		<?php echo $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'CompaniaID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaCreacion'); ?>
		<?php echo $form->textField($model,'FechaCreacion'); ?>
		<?php echo $form->error($model,'FechaCreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UsuarioCreacion'); ?>
		<?php echo $form->textField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioCreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaModificacion'); ?>
		<?php echo $form->textField($model,'FechaModificacion'); ?>
		<?php echo $form->error($model,'FechaModificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UsuarioModificacion'); ?>
		<?php echo $form->textField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioModificacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->