<?php
/* @var $this EjecucionactividadController */
/* @var $model Ejecucionactividad */

$this->breadcrumbs=array(
	'Ejecucionactividads'=>array('index'),
	'Manage',
);

//$this->menu=array(
////	array('label'=>'List Ejecucionactividad', 'url'=>array('index')),
////	array('label'=>'Crear Ejecución de actividad', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ejecucionactividad-grid', {
		data: $(this).serialize()
	});
	return false;
});
");


//$criteria=new CDbCriteria;



//if (Yii::app()->user->name == 'admin'){
//} else { 
//        $selectd = 'RequisicionID IN (Select Distinct RequisionID from ejecucionactividad Where actividadflujoID = 1 And usuarioID ='. "'" . Yii::app()->user->name . "')";
//        $criteria->condition= $selectd;
//}


?>

<h1>Seguimiento en Línea Requisiciones y Procesos de Selección</h1>

<p>NOTA: En este pagina se podra realizar seguimiento a las requisiciones actualmente activas, utilice el menu desplegable del campo requisicion para realizar consultas.</p> 

<p>
Opcionalmente usted puede ingresar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de los valores de búsqueda para especificar parametros de búsqueda mas exactos.
</p>

<?php echo CHtml::link('Búsqueda avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); 


//$v = $_GET['v']; 
//
//
//if ($v == 'A')		
//                        $selectd2 = 'RequisicionID IN (Select Distinct RequisionID from ejecucionactividad Where actividadflujoID = 1 And usuarioID ='. "'" . Yii::app()->user->name . "') and RequisicionID not in (select Distinct RequisionID  from ejecucionactividad where  actividadflujoID = 6  group by requisionID)";
//
//                 else
//                        $selectd2 = 'RequisicionID IN (Select Distinct RequisionID from ejecucionactividad Where actividadflujoID = 1 And usuarioID ='. "'" . Yii::app()->user->name . "') and RequisicionID in (select Distinct RequisionID  from ejecucionactividad where  actividadflujoID = 6  group by requisionID)";
//
//                        $criteria->condition= $selectd2;
//
//


?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ejecucionactividad-grid',
	'dataProvider'=>$model->search($v),
	'filter'=>$model,
        //'cssFile'=>'ie.css', 
        'htmlOptions' => array('style'=>'width:880px;'),
        //'wide'=> 800,
    
	'columns'=>array(
                array (
                    'name'=>'RequisionID', 
                    'value'=>'$data->requision->NombreCargo',
                    'type'=>'text',                
//                    'filter' => CHtml::listData(Requisicion::model()->findAll($criteria), 'RequisicionID', 'NombreCargo'),
                    'htmlOptions'=>array('width'=>440,'style'=>'font-size:8pt'),                 

                ),             
                array ('name'=>'actividadflujoID','value'=>'$data->actividadflujo->Nombre','type'=>'text','htmlOptions'=>array('width'=>150,'style'=>'font-size:8pt') ),  
                array ('name'=>'fecha','value'=>'$data->fecha','type'=>'text','htmlOptions'=>array('width'=>80,'style'=>'font-size:8pt'),  ),  
                array ('name'=>'usuarioID','value'=>'$data->usuarioID','type'=>'text', 'htmlOptions'=>array('width'=>80,'style'=>'font-size:8pt') ),  
//		'observacion',
  

                //array ('header'=>'Consulta Proceso','value'=>'$data->requisicionproceso->ofertaid','type'=>'text','htmlOptions'=>array('width'=>80,'style'=>'font-size:8pt') ),
//                array(
//                    'header'=>'Avance Proceso',
//                    'type'=>'raw',     
//                    'value'=>'CHtml::link(CHtml::encode($data->requisicionproceso->ofertaid),"http://www.talentracking.com/enc/procesosVer.php?idP=".$data->requisicionproceso->ofertaid."&APP=R", array("target"=>"_blank"))',  
//                    ), 
                array(
                    'header'=>'Graficos Comentarios',
                    'type'=>'raw',     
                    'value'=>'CHtml::link(CHtml::encode("Mas detalles"),"index.php?r=ejecucionactividad/view&id=".$data->requisicionactividadID, array("target"=>"_blank"))',   
                    ),             
//		'requisicionactividadID',
		/*
		'CompaniaID',
		'FechaCreacion',
		'UsuarioCreacion',
		'FechaModificacion',
		'UsuarioModificacion',
		*/
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>

<p>

	NOTA: Si en la columna "Consulta Proceso" no tiene el link es porque este proceso no ha sido activado

</p>

