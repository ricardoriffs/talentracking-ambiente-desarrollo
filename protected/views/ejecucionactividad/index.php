<?php
/* @var $this EjecucionactividadController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ejecucionactividads',
);

$this->menu=array(
	array('label'=>'Create Ejecucionactividad', 'url'=>array('create')),
	array('label'=>'Manage Ejecucionactividad', 'url'=>array('admin')),
);
?>

<h1>Ejecucionactividads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
