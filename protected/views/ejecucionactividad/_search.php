<?php
/* @var $this EjecucionactividadController */
/* @var $model Ejecucionactividad */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'RequisionID'); ?>
                <?php 
                    echo $form->dropdownlist(
                            $model,
                            'RequisionID',
                            CHtml::listData(Requisicion::model()->findAll(array('order'=>'NombreCargo ASC')), 'RequisicionID', 'NombreCargo'),
                            array('empty'=>'-- Seleccione una opción --')
                            ); 
                ?>  
	</div>

	<div class="row">
		<?php echo $form->label($model,'actividadflujoID'); ?>
                <?php 
                    echo $form->dropdownlist(
                            $model,
                            'actividadflujoID',
                            CHtml::listData(Actividadesflujos::model()->findAll(), 'ActividadFlujoID', 'Nombre'),
                            array('empty'=>'-- Seleccione una opción --')
                            ); 
                ?>  
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'fecha',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)              
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'usuarioID'); ?>
		<?php //echo $form->textField($model,'usuarioID',array('size'=>60,'maxlength'=>128)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'observacion'); ?>
		<?php //echo $form->textField($model,'observacion',array('size'=>60,'maxlength'=>2000)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'requisicionactividadID'); ?>
		<?php //echo $form->textField($model,'requisicionactividadID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'CompaniaID'); ?>
		<?php //echo $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FechaCreacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaCreacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)     
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'UsuarioCreacion'); ?>
		<?php //echo $form->textField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FechaModificacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaModificacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)   
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'UsuarioModificacion'); ?>
		<?php //echo $form->textField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->