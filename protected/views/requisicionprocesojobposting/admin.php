<?php
$this->breadcrumbs=array(
	'Requisicionprocesojobposting'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Lista Proc. Jobposting', 'url'=>array('index')),
	array('label'=>'Crear Proc. Jobposting', 'url'=>array('create')),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicionprocesojobposting-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrador Job Posting</h1>

<p>

</p>

<?php echo CHtml::link('Busquedas avanzadas','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionprocesojobposting-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'requisionprocesoID',
		'datosofertacargo',
		'datosofertareportaa',
                 array ('name'=>'datosofertaunidadnegocio','value'=>'$data->unidadnegocio->Nombre','type'=>'text',), 
                 array ('name'=>'TipoProcesoSeleccionID','value'=>'$data->tipoprocesoseleccion->Nombre','type'=>'text',),   
		/*
		'datosparapublicar',
		'TipoProcesoSeleccionID',
		'datosofertacargo',
		'datosofertaunidadnegocio',
		'datosofertareportaa',
		'datosofertamision',
		'datosofertaresponsabilidades',
		'datosofertaformacion',
		'datosofertaconocimientos',
		'datosofertacompetencias',
		'datosofertaexperiencia',
		'datosofertafechalimite',
		'datosafertaregistroaplicar',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
));


//Aqui voy a agregar.... los otros formatos 







 ?>
