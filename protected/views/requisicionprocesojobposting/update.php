<?php
$this->breadcrumbs=array(
	'Requisicionprocesojobpostings'=>array('index'),
	$model->requisionprocesoID=>array('view','id'=>$model->requisionprocesoID),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista Proc. JobPosting', 'url'=>array('index')),
	array('label'=>'Crear Proc. JobPosting', 'url'=>array('create')),
	array('label'=>'Vista Proc. JobPosting', 'url'=>array('view', 'id'=>$model->requisionprocesoID)),
	array('label'=>'Administrar Proc. JobPosting', 'url'=>array('admin')),
);
?>

<h1>Actualizar Formato Job Posting <?php echo $model->requisionprocesoID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>