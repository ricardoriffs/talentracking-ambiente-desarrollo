<?php
$this->breadcrumbs=array(
	'Requisicionprocesojobpostings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista Proc. Jobposting', 'url'=>array('index')),
	array('label'=>'Administrador Proc. Jobposting', 'url'=>array('admin')),
);
?>

<h1>Crear Proc. Jobposting</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>