<?php
$this->breadcrumbs=array(
	'Requisicionprocesojobpostings',
);

$this->menu=array(
	array('label'=>'Crear Proc.Jobposting', 'url'=>array('create')),
	array('label'=>'Administrador Proc. Jobposting', 'url'=>array('admin')),
);
?>

<h1>Proc. Jobposting</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
