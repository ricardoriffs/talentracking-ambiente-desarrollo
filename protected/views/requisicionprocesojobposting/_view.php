<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisionprocesoID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->requisionprocesoID), array('view', 'id'=>$data->requisionprocesoID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RequisicionID')); ?>:</b>
	<?php echo CHtml::encode($data->RequisicionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refiere')); ?>:</b>
	<?php echo CHtml::encode($data->refiere); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailrefiere')); ?>:</b>
	<?php echo CHtml::encode($data->emailrefiere); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referido')); ?>:</b>
	<?php echo CHtml::encode($data->referido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailreferido')); ?>:</b>
	<?php echo CHtml::encode($data->emailreferido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosparapublicar')); ?>:</b>
	<?php echo CHtml::encode($data->datosparapublicar); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoProcesoSeleccionID')); ?>:</b>
	<?php echo CHtml::encode($data->TipoProcesoSeleccionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertacargo')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertacargo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertaunidadnegocio')); ?>:</b>
	<?php echo CHtml::encode($data->getAttributeLabel('Unidad de Negocio')); ?>
        <?php echo CHtml::encode($data->unidadnegocio->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertareportaa')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertareportaa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertamision')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertamision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertaresponsabilidades')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertaresponsabilidades); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertaformacion')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertaformacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertaconocimientos')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertaconocimientos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertacompetencias')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertacompetencias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertaexperiencia')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertaexperiencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosofertafechalimite')); ?>:</b>
	<?php echo CHtml::encode($data->datosofertafechalimite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosafertaregistroaplicar')); ?>:</b>
	<?php echo CHtml::encode($data->datosafertaregistroaplicar); ?>
	<br />

	*/ ?>

</div>