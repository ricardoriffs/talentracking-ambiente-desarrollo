<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicionprocesojobposting-form',
	'enableAjaxValidation'=>false,
));



Yii::import('application.extensions.calendar.SCalendar');

 ?>




	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<diV class = "divContentBlue"> 

        <div class="row">
		<h2 align="center" class="Estilo2">INFORMACION BASICA PROCESO	    </h2>
	</div>  


	<div class="row">
		<?php echo $form->labelEx($model,'RequisicionID'); ?>
		<?php echo $form->textField($model,'RequisicionID',array('size'=>100,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'RequisicionID'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'refiere'); ?>
		<?php //echo $form->textField($model,'refiere',array('size'=>60,'maxlength'=>200)); ?>
		<?php //echo $form->error($model,'refiere'); ?>
	</div>

	<div class="row">
		<?php// echo $form->labelEx($model,'emailrefiere'); ?>
		<?php// echo $form->textField($model,'emailrefiere',array('size'=>60,'maxlength'=>100)); ?>
		<?php// echo $form->error($model,'emailrefiere'); ?>
	</div>

	<div class="row">
		<?php// echo $form->labelEx($model,'referido'); ?>
		<?php// echo $form->textField($model,'referido',array('size'=>60,'maxlength'=>200)); ?>
		<?php// echo $form->error($model,'referido'); ?>
	</div>

	<div class="row">
		<?php// echo $form->labelEx($model,'emailreferido'); ?>
		<?php// echo $form->textField($model,'emailreferido',array('size'=>60,'maxlength'=>100)); ?>
		<?php// echo $form->error($model,'emailreferido'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'TipoProcesoSeleccionID'); ?>
                <?php echo $form->dropDownList($model,'TipoProcesoSeleccionID', CHtml::listData(Tipoprocesoseleccion::model()->findAll(), 'TipoProcesoSeleccionID', 'Nombre'),array('disabled'=> true)); ?> 
                <?php //echo $form->dropDownList($model,'LugarTrabajo', CHtml::listData(Sedescompanias::model()->findAll(), 'SedeCompaniaID', 'Nombre'),array('disabled'=> true)); ?>
		<?php //echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>100,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'TipoProcesoSeleccionID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertacargo'); ?>
		<?php echo $form->textField($model,'datosofertacargo',array('size'=>100,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'datosofertacargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaunidadnegocio'); ?>
                <?php echo $form->dropDownList($model,'datosofertaunidadnegocio', CHtml::listData(Unidadnegocio::model()->findAll(), 'UnidadNegocioID', 'Nombre'),array('disabled'=> true)); ?>  
		<?php //echo $form->textField($model,'datosofertaunidadnegocio',array('size'=>100,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'datosofertaunidadnegocio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertareportaa'); ?>
		<?php echo $form->textField($model,'datosofertareportaa',array('size'=>100,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'datosofertareportaa'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'ofertaid'); ?>
		<?php echo 'R' .$model->RequisicionID . '-P' .$model->requisionprocesoID; //echo $form->textField($model,'ofertaid',array('size'=>60,'maxlength'=>200));  ?>
                <?php //echo $form->textarea($model,'emailrefiere',array('size'=>60,'maxlength'=>4000,'cols'=>50 ,'rows'=>10)); ?> 
		<?php echo $form->error($model,'ofertaid '); ?>
	</div> 

</div>

<div>     
		<br> </br>
  
</div>

<diV class = "divContentBlue"> 


        <div class="row">
		<h2 align="center" class="Estilo2">FORMATOS PARA PUBLICACION    </h2>
	</div>  



    	<div class="row">
		<?php echo $form->labelEx($model,'datosparapublicar'); ?>
		<?php // echo $form->textarea($model,'datosparapublicar',array('size'=>60,'maxlength'=>2000)); ?>
            <?php echo $form->textarea($model,'datosparapublicar',array('size'=>100,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?>
		<?php echo $form->error($model,'datosparapublicar'); ?>
	</div>

        <div class="row">
		<h2 align="center" class="Estilo2">FORMATOS PARA PUBLICAR OFERTA INTERNA DE VACANTES   </h2>
	</div>  
 

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertamision'); ?>
		<?php // echo $form->textField($model,'datosofertamision',array('size'=>60,'maxlength'=>2000)); ?>
            <?php echo $form->textarea($model,'datosofertamision',array('size'=>60,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?>

		<?php echo $form->error($model,'datosofertamision'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaresponsabilidades'); ?>
		<?php // echo $form->textField($model,'datosofertaresponsabilidades',array('size'=>60,'maxlength'=>10000)); ?>
            <?php echo $form->textarea($model,'datosofertaresponsabilidades',array('size'=>60,'maxlength'=>6000,'cols'=>85 ,'rows'=>30)); ?>

		<?php echo $form->error($model,'datosofertaresponsabilidades'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaformacion'); ?>
		<?php //echo $form->textField($model,'datosofertaformacion',array('size'=>60,'maxlength'=>1000)); ?>
            <?php echo $form->textarea($model,'datosofertaformacion',array('size'=>60,'maxlength'=>2000,'cols'=>85 ,'rows'=>10)); ?>

		<?php echo $form->error($model,'datosofertaformacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaconocimientos'); ?>
		<?php //echo $form->textField($model,'datosofertaconocimientos',array('size'=>60,'maxlength'=>1000)); ?>
            <?php echo $form->textarea($model,'datosofertaconocimientos',array('size'=>60,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?>

		<?php echo $form->error($model,'datosofertaconocimientos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertacompetencias'); ?>
		<?php //echo $form->textField($model,'datosofertacompetencias',array('size'=>60,'maxlength'=>1000)); ?>
            <?php echo $form->textarea($model,'datosofertacompetencias',array('size'=>60,'maxlength'=>4000,'cols'=>85 ,'rows'=>10)); ?>
 
		<?php echo $form->error($model,'datosofertacompetencias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaexperiencia'); ?>
		<?php echo $form->textField($model,'datosofertaexperiencia',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'datosofertaexperiencia'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaexperienciaind'); ?>
		<?php echo $form->textField($model,'datosofertaexperienciaind',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'datosofertaexperienciaind'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaexperienciacar'); ?>
		<?php echo $form->textField($model,'datosofertaexperienciacar',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'datosofertaexperienciacar'); ?>
	</div>

       
       <div class="row">
 		
              <?php echo CHtml::activeLabelEx($model,'datosofertafechainicio'); ?> 
              <?php //$this->widget('ext.simple-calendar.SimpleCalendarWidget'); ?>   
              <?php //echo CHtml::activeTextField($model,'datosofertafechainicio',array("id"=>"datosofertafechainicio")); ?> 
              <?php //echo CHtml::image(Yii::app()->baseUrl."/images/calendar.jpg","calendar", array("id"=>"ds_button","class"=>"pointer")); ?> 
              <?php //$this->widget('application.extensions.calendar.SCalendar', array( 'inputField'=>'datosofertafechainicio', 'button'=>'ds_button', 'skin'=>$skin, 'stylesheet'=>$style, 'ifFormat'=>'%Y-%m-%d', 'range'=>'[2000,2050]', 'cache'=>true,'language'=>'en', )); ?>
              
              <?php //echo CHtml::activeLabelEx($event,'end_date'); ?>
    	      <?php //echo CHtml::activeTextField($event,'end_date',array("id"=>"end_date")); ?>

              <?php $this->widget('application.extensions.timepicker.timepicker', array(
		     'model'=>$model,
		     'name'=>'datosofertafechainicio',
	
		   )); ?>  
      	



                    &nbsp;(Formato de la fecha : AAAA-MM-DD)
              
              <?php echo $form->error($model,'datosofertafechainicio'); ?>
	</div>



       <div class="row">
 	      	
              <?php echo CHtml::activeLabelEx($model,'datosofertafechalimite'); ?> 
              <?php //echo CHtml::activeTextField($model,'datosofertafechalimite',array('id'=>'datosofertafechalimite')); ?> 
              <?php //echo CHtml::image(Yii::app()->baseUrl."/images/calendar.jpg","calendar", array("id"=>"ds_button1","class"=>"pointer")); ?> 
              <?php //$this->widget('application.extensions.calendar.SCalendar', array( 'inputField'=>'datosofertafechalimite', 'button'=>'ds_button1', 'skin'=>$skin, 'stylesheet'=>$style, 'ifFormat'=>'%Y-%m-%d', 'range'=>'[2000,2050]', 'cache'=>true,'language'=>'en', )); ?>

              <?php $this->widget('application.extensions.timepicker.timepicker', array(
		     'model'=>$model,
		     'name'=>'datosofertafechalimite',
	
		   )); ?>  
                  &nbsp;(Formato de la fecha : AAAA-MM-DD) 
      	
	      <?php //echo CHtml::activeTextField($model,'datosofertafechalimite',array("id"=>"datosofertafechalimite")); ?>
                
      	<?php /*  &nbsp;(calendar appears when textbox is clicked)
                 $this->widget('application.extensions.calendar.SCalendar',
        	array(
        	'inputField'=>'datosofertafechalimite',
        	'ifFormat'=>'%Y-%m-%d',
        	));*/
      	?>
        


                
              <?php echo $form->error($model,'datosofertafechalimite'); ?>






	</div>
	





	<div class="row">
		<?php //echo $form->labelEx($model,'datosafertaregistroaplicar'); ?>
		<?php //echo $form->textField($model,'datosafertaregistroaplicar',array('size'=>100,'maxlength'=>1000)); ?>
		<?php //echo $form->error($model,'datosafertaregistroaplicar'); ?>
	</div>
</div>


<div>     
		<br> </br>
  
</div>

<diV class = "divContentBlue"> 


        <div class="row">
		<h2 align="center" class="Estilo2">INFORMACION ASPIRANTE </h2>
	</div>  

        <div class="row">
		<?php echo 'Nombre Completo'; ?>
		<?php echo $form->textField($model,'referido',array('size'=>100,'maxlength'=>1000, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div> 
                     
        <div class="row">
		<?php echo 'Cargo Actual'; ?>
		<?php echo $form->textField($model,'referido',array('size'=>100,'maxlength'=>1000, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div> 
               
        <div class="row">
		<?php echo 'Tiempo en el cargo Actual'; ?>
		<?php echo $form->textField($model,'referido',array('size'=>100,'maxlength'=>1000, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div> 
               
        <div class="row">
		<?php echo 'Fecha de inicio de publicacion'; ?>
		<?php echo $form->textField($model,'referido',array('size'=>100,'maxlength'=>1000, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div> 

        <div class="row">
		<?php echo 'Tipo de contrato'; ?>
		<?php echo $form->textField($model,'referido',array('size'=>100,'maxlength'=>1000, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div> 

        <div class="row">
		<?php echo 'Unidad de negocios       '; ?>
		<?php echo $form->textField($model,'referido',array('size'=>100,'maxlength'=>1000, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div> 
        <div class="row">
		<?php echo 'Observaciones       '; ?>
		<?php echo $form->textarea($model,'referido',array('size'=>100,'maxlength'=>1000, 'cols'=> 85, 'rows'=> 15, 'disabled'=> true)); ?>
		<?php echo $form->error($model,'referido'); ?>
	</div>  
               
       <div>     
		<br>   
       </div>
                
       ..............___________________________________.............___________________________________.............

       <div>     
		<br>   
       </div>
       .................................Firma aspirante.................................................Firma vicepresidente.................................
 


</div>  


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Previsualizar FORMULARIO JOBPOSTING'); ?>
                

	</div>

	<div class="row buttons">
		<?php 
                     //$imagePath="images\administrator4.png";
                     //$image = CHtml::image($imagePath,'Admin', array('title'=>'Resumen Procesos Requisiciones')); 
                     //echo CHtml::link($image,array('requisicionproceso/admin' )) 
                
                ?>
             <a class="boton"  href=<?php echo 'index.php?r=requisicionproceso/admin>' ?> Resumen Procesos Requisiciones </a>                           

	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->