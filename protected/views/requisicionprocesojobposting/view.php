<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>


<?php
$this->breadcrumbs=array(
	'Requisicionprocesojobpostings'=>array('index'),
	$model->requisionprocesoID,
);

$this->menu=array(
	//array('label'=>'Lista Proc. JobPosting', 'url'=>array('index')),
	//array('label'=>'Crear Proc. JobPosting', 'url'=>array('create')),
	array('label'=>'Actualizar Proc. JobPosting', 'url'=>array('update', 'id'=>$model->requisionprocesoID)),
	//array('label'=>'Borrar Proc. JobPosting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->requisionprocesoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Proc. JobPosting', 'url'=>array('admin')),
);
?>

<h1>Vista Formato Job Posting </h1>
<?php echo $model->datosofertacargo; ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'requisionprocesoID',
		'datosofertacargo',
		//'refiere',
		//'emailrefiere',
		//'referido',
		//'emailreferido',
		'datosparapublicar',
		'tipoprocesoseleccion.Nombre',
		'datosofertacargo',
                 array(
                                'name'=>'Unidad de Negocio',
                                'value'=>CHtml::encode($model->unidadnegocio->Nombre),
                        ),  		
		'datosofertareportaa',
		'datosofertamision',
		'datosofertaresponsabilidades',
		'datosofertaformacion',
		'datosofertaconocimientos',
		'datosofertacompetencias',
		'datosofertaexperiencia',
		'datosofertafechalimite',
		array(
                                'name'=>'Oferta ID',
                                'value'=>CHtml::encode('R'.$model->RequisicionID.'-P'.$model->requisionprocesoID),
                        ),
	),
)); ?>
<?php //echo CHtml::button('Publicar Correo Oferta Interna', array('submit' => array('publicarcorreo', 'id'=>$model->RequisicionID))); ?>   
<?php //echo CHtml::button('Activar Proceso de seleccion', array('submit' => array('activarproceso', 'id'=>$model->RequisicionID))); ?>   

<div id="b1" class="row buttons">
            
            <?php
              echo nl2br("\n");
              echo nl2br("\n");
                    //alert $politicaid->value;
                    if (($model->RequisicionID)!= "") {?>
                    
                      <a class="boton"  href=<?php echo 'index.php?r=requisicionprocesojobposting/publicapdf&id='.$model->requisionprocesoID. ' title= "Esta accion publica la oferta en formato PDF" >'; ?> Publicar Oferta Interna </a>                        
                      
                      <?php 
                        echo nl2br("\n");  
                        echo nl2br("\n");                          
                        //Aqui buscamos la Vicepresidencia de la requisicion

                        
        
        $results = Requisicion::model()->findAll(array('condition'=> "RequisicionID =" . $model->RequisicionID ,'order' => 'UnidadNegocioID ASC'));
        
        foreach ($results as $result)
        {



          $arevic=$result->AreaVicepresidenciaID; 

                  $resultsA = Areavicepresidencia::model()->findAll(array('condition'=> "AreaVicepresidenciaID =" . $result->AreaVicepresidenciaID ,'order' => 'Nombre ASC'));  
                  foreach ($resultsA as $resultA)
                  {
                  	$nareavic=$resultA->Descripcion; 
                   
                  } 
         }

                      ?>





                      

                      <A class="boton" HREF="mailto:rrhh@pacificrubiales.com.co?cc=jtatis@yahoo.com,fgutierrez@talentracking.com&subject=JOB POSTING CARGO :  <?php echo $model->datosofertacargo  ?>
                       &body= <?php echo 'Buenos dias,'. '%0D%0A'.
                       ''. '%0D%0A'.  
                       'Los invitamos a revisar estas nuevas ofertas de vacantes que surgen en la corporacion,'. '%0D%0A'.  
                       'con el objetivo que todos los empleados puedan acceder a las mejores oportunidades de desarrollo'. '%0D%0A'.  
                       'que ofrece Pacific Rubiales Energy. Para ver informacion detallada de la vacante ingrese aqui  '. '%0D%0A'. 'http:\\\www.talentracking.com\\3t\\' .'R'.$model->RequisicionID.'-P'.$model->requisionprocesoID.'.pdf'. '%0D%0A'.                         
                       ''. '%0D%0A'.                       
                       'CARGO:'. $model->datosofertacargo . '%0D%0A'.
                       'VICEPRESIDENCIA:'.$nareavic . '%0D%0A'. '%0D%0A'.
                       'Tener en cuenta lo siguiente:'. '%0D%0A'.
                       ''. '%0D%0A'.
                       '1. Para aplicar a un proceso de oferta interna debe llevar en el cargo actual minimo un ano como empleado interno.' . '%0D%0A'. 
                       '2. El candidato interno que desee aplicar debe completar el formato de aplicacion a la oferta interna, obtener la' . '%0D%0A'.  
                       'aprobacion de su Supervisor y Vicepresidente funcional.'. '%0D%0A'. 
                       '3. Cargar el formato diligenciado en la plataforma de TalenTracking en esta direccion www.talentracking.com/enc/personal.php ' .
                       'y digite el codigo '. 'R'.$model->RequisicionID.'-P'.$model->requisionprocesoID . ' en el campo respectivo del formulario.'. '%0D%0A'.   
                       '4. Los candidatos deben anexar a la solicitud una hoja de vida actualizada.' . '%0D%0A'. 
                       '5. El departamento de Talento Humano enviara las aplicaciones con los anexos al Gerente que publico la vacante.' . '%0D%0A'.                           
                       ''. '%0D%0A'.                       
                       ''. '%0D%0A'.'Cualquier inquietud comunicarse con Sandra Martinez o Carlos Moreno : 5112000 Ext 2621 - 2772'. '%0D%0A'. '%0D%0A'.
                       'Cordialmente,' . '%0D%0A'.'TALENTO HUMANO'. '%0D%0A'.                       
                       ''. '%0D%0A'  
                        ?> " >  Previsualizar el correo </A>  
                       
                      <?php 
                        echo nl2br("\n");  
                        echo nl2br("\n");                          
                       
                      ?>
                      
                      
                      
                      <a class="boton"  href=<?php echo 'index.php?r=requisicionprocesojobposting/activarproceso&id=' . $model->requisionprocesoID.' title= "Esta accion activara un nuevo proceso de seleccion en la plataforma de preseleccion" >'; ?> Activar Proceso de seleccion </a>  



                      <?php 
                        echo nl2br("\n");
                        echo nl2br("\n");                             
                      ?>  
  
                      <a class="boton"  href=<?php echo 'index.php?r=requisicionprocesojobposting/update&id=' . $model->requisionprocesoID.'>'; ?> Editar Formato </a>                         
                      

                 <?php   
                    echo nl2br("\n"); 
                      }
                    
                  ?>
             
</div>  	

