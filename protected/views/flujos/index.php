<?php
$this->breadcrumbs=array(
	'Flujoses',
);

$this->menu=array(
	array('label'=>'Create Flujos', 'url'=>array('create')),
	array('label'=>'Manage Flujos', 'url'=>array('admin')),
);
?>

<h1>Flujoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
