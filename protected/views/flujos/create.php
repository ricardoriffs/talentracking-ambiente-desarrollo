<?php
$this->breadcrumbs=array(
	'Flujoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Flujos', 'url'=>array('index')),
	array('label'=>'Manage Flujos', 'url'=>array('admin')),
);
?>

<h1>Create Flujos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>