<?php
$this->breadcrumbs=array(
	'Flujoses'=>array('index'),
	$model->FlujoID=>array('view','id'=>$model->FlujoID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Flujos', 'url'=>array('index')),
	array('label'=>'Create Flujos', 'url'=>array('create')),
	array('label'=>'View Flujos', 'url'=>array('view', 'id'=>$model->FlujoID)),
	array('label'=>'Manage Flujos', 'url'=>array('admin')),
);
?>

<h1>Update Flujos <?php echo $model->FlujoID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>