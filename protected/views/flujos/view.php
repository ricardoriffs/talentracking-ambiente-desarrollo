<?php
$this->breadcrumbs=array(
	'Flujoses'=>array('index'),
	$model->FlujoID,
);

$this->menu=array(
	array('label'=>'List Flujos', 'url'=>array('index')),
	array('label'=>'Create Flujos', 'url'=>array('create')),
	array('label'=>'Update Flujos', 'url'=>array('update', 'id'=>$model->FlujoID)),
	array('label'=>'Delete Flujos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->FlujoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Flujos', 'url'=>array('admin')),
);
?>

<h1>View Flujos #<?php echo $model->FlujoID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'FlujoID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
		'UsuarioCreacion',
	),
)); ?>
