<?php
$this->breadcrumbs=array(
	'Tipoprocesoseleccionresumens',
);

$this->menu=array(
	array('label'=>'Create Tipoprocesoseleccionresumen', 'url'=>array('create')),
	array('label'=>'Manage Tipoprocesoseleccionresumen', 'url'=>array('admin')),
);
?>

<h1>Tipoprocesoseleccionresumens</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
