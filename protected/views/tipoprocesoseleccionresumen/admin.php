<?php
$this->breadcrumbs=array(
	'Tipoprocesoseleccionresumens'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tipoprocesoseleccionresumen-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Resumen Procesos Formatos</h1>

<h1>Job Posting</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionprocesojobposting-grid',
	'dataProvider'=>$modelj->search(),
	'filter'=>$modelj,
	'columns'=>array(
		'requisionprocesoID',
		'datosofertacargo',
		'datosofertareportaa',
                 array ('name'=>'datosofertaunidadnegocio','value'=>'$data->unidadnegocio->Nombre','type'=>'text',), 
                 array ('name'=>'TipoProcesoSeleccionID','value'=>'$data->tipoprocesoseleccion->Nombre','type'=>'text',),   
		/*
		'datosparapublicar',
		'TipoProcesoSeleccionID',
		'datosofertacargo',
		'datosofertaunidadnegocio',
		'datosofertareportaa',
		'datosofertamision',
		'datosofertaresponsabilidades',
		'datosofertaformacion',
		'datosofertaconocimientos',
		'datosofertacompetencias',
		'datosofertaexperiencia',
		'datosofertafechalimite',
		'datosafertaregistroaplicar',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
));

?>
