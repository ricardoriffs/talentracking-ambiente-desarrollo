<?php
$this->breadcrumbs=array(
	'Tipoprocesoseleccionresumens'=>array('index'),
	$model->TipoProcesoSeleccionID=>array('view','id'=>$model->TipoProcesoSeleccionID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tipoprocesoseleccionresumen', 'url'=>array('index')),
	array('label'=>'Create Tipoprocesoseleccionresumen', 'url'=>array('create')),
	array('label'=>'View Tipoprocesoseleccionresumen', 'url'=>array('view', 'id'=>$model->TipoProcesoSeleccionID)),
	array('label'=>'Manage Tipoprocesoseleccionresumen', 'url'=>array('admin')),
);
?>

<h1>Update Tipoprocesoseleccionresumen <?php echo $model->TipoProcesoSeleccionID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>