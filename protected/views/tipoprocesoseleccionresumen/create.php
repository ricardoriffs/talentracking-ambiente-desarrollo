<?php
$this->breadcrumbs=array(
	'Tipoprocesoseleccionresumens'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tipoprocesoseleccionresumen', 'url'=>array('index')),
	array('label'=>'Manage Tipoprocesoseleccionresumen', 'url'=>array('admin')),
);
?>

<h1>Create Tipoprocesoseleccionresumen</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>