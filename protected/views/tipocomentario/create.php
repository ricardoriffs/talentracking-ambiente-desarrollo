<?php
/* @var $this TipocomentarioController */
/* @var $model Tipocomentario */

$this->breadcrumbs=array(
	'Tipocomentarios'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Tipocomentario', 'url'=>array('index')),
	array('label'=>'Administrar Tipos de comentario', 'url'=>array('admin')),
);
?>

<h1>Crear Tipo de comentario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>