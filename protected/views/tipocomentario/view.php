<?php
/* @var $this TipocomentarioController */
/* @var $model Tipocomentario */

$this->breadcrumbs=array(
	'Tipocomentarios'=>array('index'),
	$model->TipoComentarioID,
);

$this->menu=array(
//	array('label'=>'List Tipocomentario', 'url'=>array('index')),
//	array('label'=>'Create Tipocomentario', 'url'=>array('create')),
	array('label'=>'Editar Tipo de comentario', 'url'=>array('update', 'id'=>$model->TipoComentarioID)),
	array('label'=>'Borrar Tipo de comentario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TipoComentarioID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Tipos de comentario', 'url'=>array('admin')),
);
?>

<h1>Ver Tipo de comentario "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'TipoComentarioID',
		'id',
		'orden',
//		'CompaniaID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),    
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
