<?php
/* @var $this TipocomentarioController */
/* @var $model Tipocomentario */

$this->breadcrumbs=array(
	'Tipocomentarios'=>array('index'),
	$model->TipoComentarioID=>array('view','id'=>$model->TipoComentarioID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Tipocomentario', 'url'=>array('index')),
//	array('label'=>'Create Tipocomentario', 'url'=>array('create')),
	array('label'=>'Ver Tipo de comentario', 'url'=>array('view', 'id'=>$model->TipoComentarioID)),
	array('label'=>'Administrar Tipos de comentario', 'url'=>array('admin')),
);
?>

<h1>Editar Tipo de comentario "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>