<?php
/* @var $this TipocomentarioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipocomentarios',
);

$this->menu=array(
	array('label'=>'Create Tipocomentario', 'url'=>array('create')),
	array('label'=>'Manage Tipocomentario', 'url'=>array('admin')),
);
?>

<h1>Tipocomentarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
