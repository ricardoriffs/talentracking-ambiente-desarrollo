<?php
/* @var $this CompaniasController */
/* @var $model Companias */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<!--	<div class="row">
		<?php //echo $form->label($model,'CompaniaID'); ?>
		<?php //echo $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Descripcion'); ?>
		<?php echo $form->textField($model,'Descripcion',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaInicioVigencia'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaInicioVigencia',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)  
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaFinVigencia'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaFinVigencia',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)  
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'TipoServicioID'); ?>
		<?php //echo $form->textField($model,'TipoServicioID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'FechaCreacion'); ?>
		<?php //echo $form->textField($model,'FechaCreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'UsuarioCreacion'); ?>
		<?php //echo $form->textField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'FechaModificacion'); ?>
		<?php //echo $form->textField($model,'FechaModificacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'UsuarioModificacion'); ?>
		<?php //echo $form->textField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'ID'); ?>
		<?php //echo $form->textField($model,'ID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->