<?php
/* @var $this CompaniasController */
/* @var $data Companias */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CompaniaID), array('view', 'id'=>$data->CompaniaID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaInicioVigencia')); ?>:</b>
	<?php echo CHtml::encode($data->FechaInicioVigencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaFinVigencia')); ?>:</b>
	<?php echo CHtml::encode($data->FechaFinVigencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoServicioID')); ?>:</b>
	<?php echo CHtml::encode($data->TipoServicioID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::encode($data->ID); ?>
	<br />

	*/ ?>

</div>