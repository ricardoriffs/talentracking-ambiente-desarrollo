<?php
/* @var $this CompaniasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Companiases',
);

$this->menu=array(
	array('label'=>'Create Companias', 'url'=>array('create')),
	array('label'=>'Manage Companias', 'url'=>array('admin')),
);
?>

<h1>Companiases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
