<?php
/* @var $this CompaniasController */
/* @var $model Companias */

$this->breadcrumbs=array(
	'Companiases'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Companias', 'url'=>array('index')),
	array('label'=>'Administrar Compañias', 'url'=>array('admin')),
);
?>

<h1>Crear Compañia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>