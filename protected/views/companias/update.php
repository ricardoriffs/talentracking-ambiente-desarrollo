<?php
/* @var $this CompaniasController */
/* @var $model Companias */

$this->breadcrumbs=array(
	'Companiases'=>array('index'),
	$model->CompaniaID=>array('view','id'=>$model->CompaniaID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Companias', 'url'=>array('index')),
//	array('label'=>'Create Companias', 'url'=>array('create')),
	array('label'=>'Ver Compañia', 'url'=>array('view', 'id'=>$model->CompaniaID)),
	array('label'=>'Administrar Compañias', 'url'=>array('admin')),
);
?>

<h1>Editar Compañia "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>