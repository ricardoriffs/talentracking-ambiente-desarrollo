<?php
/* @var $this CompaniasController */
/* @var $model Companias */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'companias-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con asterisco <span class="required">(*)</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Nombre'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'NIT'); ?>
		<?php echo $form->textField($model,'NIT',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'NIT'); ?>
	</div>        

	<div class="row">
		<?php echo $form->labelEx($model,'Representante'); ?>
		<?php echo $form->textField($model,'Representante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Representante'); ?>
	</div>    

	<div class="row">
		<?php echo $form->labelEx($model,'Descripcion'); ?>
		<?php echo $form->textArea($model,'Descripcion',array('size'=>60,'maxlength'=>100, 'rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'Descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaInicioVigencia'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaInicioVigencia',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD) 
		<?php echo $form->error($model,'FechaInicioVigencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaFinVigencia'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaFinVigencia',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD) 
		<?php echo $form->error($model,'FechaFinVigencia'); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->labelEx($model,'TipoServicioID'); ?>
		<?php //echo $form->textField($model,'TipoServicioID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'TipoServicioID'); ?>
	</div>-->

	<div class="row">
		<?php //echo $form->labelEx($model,'FechaCreacion'); ?>
		<?php echo $form->hiddenField($model,'FechaCreacion'); ?>
		<?php echo $form->error($model,'FechaCreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioCreacion'); ?>
		<?php echo $form->hiddenField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioCreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'FechaModificacion'); ?>
		<?php echo $form->hiddenField($model,'FechaModificacion'); ?>
		<?php echo $form->error($model,'FechaModificacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioModificacion'); ?>
		<?php echo $form->hiddenField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioModificacion'); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->labelEx($model,'ID'); ?>
		<?php //echo $form->textField($model,'ID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'ID'); ?>
	</div>-->

	<div class="row">            
                <?php echo $form->labelEx($model,'soporteLegal'); ?>
		<?php echo $form->FileField($model,'soporteLegal'); ?>                        
                <?php echo $form->error($model,'soporteLegal'); ?>   
                <p class="hint">
                        Nota: Anexe un archivo de WORD, EXCEL o PDF con un tamaño máximo de 2MB. 
                </p>            
	</div> 

	<div class="row">
		<span style="font-weight: bold;"><?php echo 'Activo '; ?></span>
                <?php echo $form->checkBox($model, 'Estado'); ?>
		<?php echo $form->error($model,'Estado'); ?>
	</div>  

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->