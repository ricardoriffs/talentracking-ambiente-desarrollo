<?php
/* @var $this CompaniasController */
/* @var $model Companias */

$this->breadcrumbs=array(
	'Companiases'=>array('index'),
	$model->CompaniaID,
);

$this->menu=array(
//	array('label'=>'List Companias', 'url'=>array('index')),
//	array('label'=>'Create Companias', 'url'=>array('create')),
	array('label'=>'Editar Compañias', 'url'=>array('update', 'id'=>$model->CompaniaID)),
	array('label'=>'Borrar Compañias', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->CompaniaID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Compañias', 'url'=>array('admin')),
);
?>

<h1>Ver Compañia "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'CompaniaID',
		'Nombre',
                'NIT',
                'Representante',
		'Descripcion',
		'FechaInicioVigencia',
		'FechaFinVigencia',
//		'TipoServicioID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),   
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
		array(
                    'name'=>'Estado',
                    'value'=>CHtml::encode($model->Estado==1?'Activo':'Inactivo'),
                        ),                
//		'ID',
	),
)); ?>
