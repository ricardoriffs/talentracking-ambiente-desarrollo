<?php
/* @var $this CompaniasController */
/* @var $model Companias */

$this->breadcrumbs=array(
	'Companiases'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List Companias', 'url'=>array('index')),
	array('label'=>'Crear Compañia', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#companias-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Compañias</h1>

<p>
Opcionalmente usted puede ingresar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de los valores de búsqueda para especificar parametros de búsqueda mas exactos.
</p>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'companias-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'CompaniaID',
		'Nombre',
		'Descripcion',
		'FechaInicioVigencia',
		'FechaFinVigencia',
                array ('name'=>'Estado','value'=>'$data->obtenerEstado($data->Estado)','type'=>'text',),              
		/*'TipoServicioID',
		'FechaCreacion',
		'UsuarioCreacion',
		'FechaModificacion',
		'UsuarioModificacion',
		'ID',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
