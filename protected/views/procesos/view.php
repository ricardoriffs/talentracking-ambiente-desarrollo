<?php
$this->breadcrumbs=array(
	'Procesoses'=>array('index'),
	$model->id,
);

/*
$this->menu=array(
	array('label'=>'List Procesos', 'url'=>array('index')),
	array('label'=>'Create Procesos', 'url'=>array('create')),
	array('label'=>'Update Procesos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Procesos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procesos', 'url'=>array('admin')),
      
);*/
?>

<h1><?php echo "<br>".$model->nombre ."<br>" ; ?></h1>


<?php 
        //Vista de Proceso # Graficas Estado Candidatos y tipo contrato 
        $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		//'nombre',
		'descripcion',
		'activo',                 
                /*
		'fechaInsert',*/
		'ofertaid',
               array(
                'name'=>'Ver Candidatos',
                'type'=>'url',     
                'value'=>"http://www.talentracking.com/enc/procesosVer.php?idP=". $model->id ,  
                ), 
                              array(
                'name'=>'Ver Evaluaciones',
                'type'=>'url',     
                'value'=>"http://www.talentracking.com/enc/prefiltro.php?idP=". $model->id ,  
                ), 

	),
)); ?>

<h1> Estadisticas de candidatos seleccionados-descartados</h1>

<?php   $results = Candidatos::model()->findAll(array('condition'=> "id_proceso =" . $model->id . "" ,'order' => 'estado ASC'));
        $escribir = 0;       
        $lcv = 0;
        $arealcv = 0;
        $procesoseleccion = array();
        $procesoseleccion2 = array();
        $counts = array();
        $color = array(); 
        $temp = "X";
        $tempU = "X";
        
       echo '<div class="item">';   
        echo "<table border='0'>";
         
        echo "<tr>";
           echo "<th>Estado</th>";
           echo "<th>Cantidad</th>"; 
           echo "<th>"." ". "</th>";
        echo "</tr>"; 
        
        foreach ($results as $result)
        {
            
           
            if ($tempU != $result->estado) {
                 
                       if ($tempU == "X") {
                              $escribir = 0;
                       } else {
                              $escribir = 1;
                       }   
                  
                  $procesoseleccion[$arealcv] = $result->estado;                  
                  $temp = $result->estado;
                  
                   
                  if (($lcv > 0) && ($escribir != 0)) {  
                        echo "<tr>";
                        if ($tempU==0){$U = "No Seleccionado";} 
                        if ($tempU==1){$U = "Descartado";}
                        if ($tempU==2){$U = "Continua";}
           			echo "<td>". $U ."</td>";
           			echo "<td>".$lcv."</td>"; 
        		echo "</tr>"; 
                  	$counts[] =array($U, $lcv);
                        $procesoseleccion2[$arealcv] = array($U); 
                        $color[$arealcv] = array($arealcv);

                        $arealcv++; 
 
	                $escribir = 0;
                        $lcv = 0;  
                   }
                  
/*
0	no seleccionado
1	descartado
2	contin�a
*/

                  if ($temU == "X") {
                     $tempU = "*Sin Estado Asignado*";
                  } else {  
                   /* if ($result->estado==0) $tempU = "No Seleccionado"; 
                    if ($result->estado==1) $tempU = "Descartado";
                    if ($result->estado==2) $tempU = "Continua"; 
                   */ 
                   $tempU = $result->estado;
 
                  } 



            } 

            $lcv++;
        }

                   
                        echo "<tr>";
                        if ($tempU==0){$U = "No Seleccionado";} 
                        if ($tempU==1){$U = "Descartado";}
                        if ($tempU==2){$U = "Continua";}

           			echo "<td>". $U ."</td>";
           			echo "<td>".$lcv."</td>"; 
        		echo "</tr>";

                  	$counts[] =array($U, $lcv);
                        //$procesoseleccion2[$arealcv] = array($tempU);
                        $procesoseleccion2[$arealcv] = array($U); 
                        $color[$arealcv] = array($arealcv); 
                        $arealcv++;
 
	                $escribir = 0;
                        $lcv = 0;  

echo "<tr>";
echo "</tr>";
echo "<tr>";
echo "</tr>";


echo "<tr>";

   

echo "</tr>";
echo "</table>";
echo '</div>';

$this->Widget('ext.highcharts.HighchartsWidget', array(
        'options'=>array(
            'theme' => 'grid', 
            'chart'=> array('type'=>'column','width'=>'720', 'height'=>'350', 
                      'spacingBottom'=>10, 'margin'=>array(50,50, 100, 80)),
            'title' => array('text'=>''),
            //'legend'=> array('enabled'=>true),
            'plotOptions'=>array('pie'=>array('dataLabels'=>array('enabled'=>true,'color'=>'#000000','formatter'=>  'js:function(){ return "("+this.point.y + "): " +this.percentage.toFixed(2) + "%"; }' ),'showInLegend'=>true)),
            'xAxis' => array('categories'=>$procesoseleccion2, 'labels'=>array('rotation'=>'0','align'=>'right','style'=>array('color'=>$color,'font'=>'normal 10x arial, Verdana, sans-serif'))),
            'yAxis' => array('title'=>array('text'=>'Cantidad de Candidatos')),
            'legend' => array('layout'=> 'horizontal','verticalAlign'=>'bottom'), 
            'series' => array(array('type' => 'column','name' => 'Candidatos', 'data' => $counts , 
                        datalabels=>array('enabled'=>'true','rotation'=>'-90','color'=>'#FFFFFF','align'=>'right','x'=>'-3', 'y'=>'10','formatter'=>  'js:function(){ return "("+this.point.y + ")";}')),
        ))
     ));


?>
<h1> Estadisticas  Tipo de Cantidatos Registrados Para este proceso</h1>

<?php   $results = candidatos::model()->findAll(array('condition'=> "id_proceso=" . $model->id . "" ,'order' => 'tipocontrato ASC'));
        $escribir = 0;       
        $lcv = 0;
        $arealcv = 0;
        $procesoseleccion = array();
        $procesoseleccion2 = array();
        $counts = array();
        $color = array(); 
        $temp = "X";
        $tempU = "X";
        
       echo '<div class="item">';   
        echo "<table border='0'>";
         
        echo "<tr>";
           echo "<th>Tipo Contrato</th>";
           echo "<th>Cantidad</th>"; 
           echo "<th>"." ". "</th>";
        echo "</tr>"; 
        
        foreach ($results as $result)
        {
            
           
            if ($tempU != $result->tipocontrato) {
                 
                       if ($tempU == "X") {
                              $escribir = 0;
                       } else {
                              $escribir = 1;
                       }   
                  
                  $procesoseleccion[$arealcv] = $result->tipocontrato;                  
                  $temp = $result->tipocontrato;
                  
                   
                  if (($lcv > 0) && ($escribir != 0)) {  
                        echo "<tr>";
           			echo "<td>". $tempU ."</td>";
           			echo "<td>".$lcv."</td>"; 
        		echo "</tr>"; 
                  	$counts[] =array($tempU, $lcv);
                        $procesoseleccion2[$arealcv] = array($tempU); 
                        $color[$arealcv] = array($arealcv);

                        $arealcv++; 
 
	                $escribir = 0;
                        $lcv = 0;  
                   }
                  if ($temU == "X") {
                     $tempU = "*Sin Tipo Contrato Asignado*";
                  } else {  
                    $tempU = $result->tipocontrato; 
                  } 



            } 

            $lcv++;
        }

                   
                        echo "<tr>";
           			echo "<td>". $tempU ."</td>";
           			echo "<td>".$lcv."</td>"; 
        		echo "</tr>";

                  	$counts[] =array($tempU, $lcv);
                        //$procesoseleccion2[$arealcv] = array($tempU);
                        $procesoseleccion2[$arealcv] = array($tempU); 
                        $color[$arealcv] = array($arealcv); 
                        $arealcv++;
 
	                $escribir = 0;
                        $lcv = 0;  

echo "<tr>";
echo "</tr>";
echo "<tr>";
echo "</tr>";


echo "<tr>";

   

echo "</tr>";
echo "</table>";
echo '</div>';

$this->Widget('ext.highcharts.HighchartsWidget', array(
        'options'=>array(
            'theme' => 'grid', 
            'chart'=> array('type'=>'column','width'=>'720', 'height'=>'350', 
                      'spacingBottom'=>10, 'margin'=>array(50,50, 100, 80)),
            'title' => array('text'=>''),
            //'legend'=> array('enabled'=>true),
            'plotOptions'=>array('pie'=>array('dataLabels'=>array('enabled'=>true,'color'=>'#000000','formatter'=>  'js:function(){ return "("+this.point.y + "): " +this.percentage.toFixed(2) + "%"; }' ),'showInLegend'=>true)),
            'xAxis' => array('categories'=>$procesoseleccion2, 'labels'=>array('rotation'=>'0','align'=>'right','style'=>array('color'=>$color,'font'=>'normal 10x arial, Verdana, sans-serif'))),
            'yAxis' => array('title'=>array('text'=>'Cantidad de Candidatos')),
            'legend' => array('layout'=> 'horizontal','verticalAlign'=>'bottom'), 
            'series' => array(array('type' => 'column','name' => 'Candidatos', 'data' => $counts , 
                        datalabels=>array('enabled'=>'true','rotation'=>'-90','color'=>'#FFFFFF','align'=>'right','x'=>'-3', 'y'=>'10','formatter'=>  'js:function(){ return "("+this.point.y + ")";}')),
        ))
     ));

     
?>


