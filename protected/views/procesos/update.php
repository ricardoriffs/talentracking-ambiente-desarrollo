<?php
$this->breadcrumbs=array(
	'Procesoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procesos', 'url'=>array('index')),
	array('label'=>'Create Procesos', 'url'=>array('create')),
	array('label'=>'View Procesos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Procesos', 'url'=>array('admin')),
);
?>

<h1>Update Procesos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>