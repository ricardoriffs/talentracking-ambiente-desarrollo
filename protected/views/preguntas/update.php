<?php
/* @var $this PreguntasController */
/* @var $model Preguntas */

$this->breadcrumbs=array(
	'Preguntases'=>array('index'),
	$model->idPregunta=>array('view','id'=>$model->idPregunta),
	'Update',
);

$this->menu=array(
	array('label'=>'List Preguntas', 'url'=>array('index')),
	array('label'=>'Create Preguntas', 'url'=>array('create')),
	array('label'=>'View Preguntas', 'url'=>array('view', 'id'=>$model->idPregunta)),
	array('label'=>'Manage Preguntas', 'url'=>array('admin')),
);
?>

<h1>Update Preguntas <?php echo $model->idPregunta; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>