<?php
/* @var $this PreguntasController */
/* @var $data Preguntas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPregunta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idPregunta), array('view', 'id'=>$data->idPregunta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idGrupo')); ?>:</b>
	<?php echo CHtml::encode($data->idGrupo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idTipo')); ?>:</b>
	<?php echo CHtml::encode($data->idTipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('texto')); ?>:</b>
	<?php echo CHtml::encode($data->texto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orden')); ?>:</b>
	<?php echo CHtml::encode($data->orden); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('borrada')); ?>:</b>
	<?php echo CHtml::encode($data->borrada); ?>
	<br />


</div>