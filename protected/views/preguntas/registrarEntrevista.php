
<?php
$cs=Yii::app()->clientScript;
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.calculation.min.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.format.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/template.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/common.js', CClientScript::POS_END);

Yii::app()->getComponent("bootstrap");

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'preguntas-form',
	'enableAjaxValidation'=>false,
)); ?>
    
<div id="subtitulo_proceso">
    <h3><?php echo $encuestas->nombre ?></h3>
</div>    
    
<div class="item">   
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >PROCESO DE SELECCIOŃ: <?php echo $proceso->nombre ?></th> 
	</tr>
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >NO. DE PROCESO: <?php echo $proceso->ofertaid ?></th> 
	</tr>        
    </table>
    <div style ="float: left; padding: 10px;">
        <img width="80" height="110" alt="foto" src="/3t/verFoto.php?idc=<?php echo $candidato->idCandidato ?>" style="border:#626262 solid 1px;float:left;" />
    </div>
    <div style="float: left; padding: 10px;">
        <table>
            <tr>
                <th>Nombres:</th>
                <td><?php echo $candidato->nombres ?></td>
            </tr>
            <tr>
                <th>Apellidos:</th>
                <td><?php echo $candidato->apellidos ?></td>
            </tr> 
            <tr>
                <th>Edad:</th>
                <td><?php echo $candidato->edad ?></td>
            </tr>           
        </table>
    </div> 
    <table style="float: none;">
        <tr>
            <th>Nombre del cargo que desempeña actualmente:</th>
            <td><?php echo $candidato->ultimoCargo ?></td>
            <th>Tipo de Vinculación :</th>
            <td><?php echo $candidato->tipocontrato ?></td>                
        </tr>
        <tr>
            <th>Total años de experiencia profesional:</th>
            <td><?php echo $candidato->aniosProfesional ?></td>
            <th>Salario actual (Incluye bonificaciones, beneficios, etc.):</th>
            <td><?php echo $candidato->sueldoActual ?></td>                
        </tr> 
        <tr>
            <th>Total años de experiencia en cargos similares:</th>
            <td><?php echo $candidato->aniosRequeridos ?></td>
            <th>Aspiración salarial:</th>
            <td><?php echo $candidato->sueldoAspirado ?></td>
        </tr>
        <tr>
            <th>Total años de experiencia en el sector:</th>
            <td><?php echo $candidato->aniosExperienciaSector ?></td>
            <th>Tiene familiares trabajando con nosotros:</th>
            <td><?php echo $candidato->nom_familiar ?></td>
        </tr>   
        <tr>
            <th>Nivel Educativo:</th>
            <td><?php if(!empty($candidato->ne_doctorado)){ echo 'Doctorado'; } elseif(!empty($candidato->ne_maestria)){ echo 'Maestría'; } elseif(!empty($candidato->ne_especializacion)){ echo 'Especialización'; } elseif(!empty($candidato->ne_profecional)){ echo 'Profesional'; } elseif(!empty($candidato->ne_tecnologo)){ echo 'Tecnólogo'; } elseif(!empty($candidato->ne_tecnico)){ echo 'Técnico'; } elseif(!empty($candidato->ne_secundaria)){ echo 'Secundaria'; } elseif(!empty($candidato->ne_primaria)){ echo 'Primaria'; } ?></td>
            <th></th>
            <td></td>
        </tr>             
    </table>
    <div class="grid-view">
    <?php $i = 1; ?>
    <?php foreach($competencias as $competencia): ?>
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >COMPETENCIA No. <?php echo $i++; ?></th> 
	</tr>        
    </table>        
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->nombre ?></td>
            <td><?php echo $competencia->descripcion ?></td>
            <td><?php echo $competencia->preguntaSugerida ?></td>
        </tr>
    </table>
    
    <table>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>            
            <th></th>
            <th></th>
            <th></th>            
        </tr>
    </table>    
    <?php endforeach; ?>
    </div>
</div>  

<?php $this->endWidget(); ?>

</div><!-- form -->
