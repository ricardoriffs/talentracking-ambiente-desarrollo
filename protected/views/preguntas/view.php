<?php
/* @var $this PreguntasController */
/* @var $model Preguntas */

$this->breadcrumbs=array(
	'Preguntases'=>array('index'),
	$model->idPregunta,
);

$this->menu=array(
	array('label'=>'List Preguntas', 'url'=>array('index')),
	array('label'=>'Create Preguntas', 'url'=>array('create')),
	array('label'=>'Update Preguntas', 'url'=>array('update', 'id'=>$model->idPregunta)),
	array('label'=>'Delete Preguntas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idPregunta),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Preguntas', 'url'=>array('admin')),
);
?>

<h1>View Preguntas #<?php echo $model->idPregunta; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idPregunta',
		'idGrupo',
		'idTipo',
		'texto',
		'descripcion',
		'orden',
		'borrada',
	),
)); ?>
