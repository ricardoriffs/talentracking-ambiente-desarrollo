<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo 'Talent tracking -tool- Personnel Request' ?></title>



<link type="text/css" href="menu.css" rel="stylesheet" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="menu.js"></script>


</script>

</head>

<body>



<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo ''?><img src="images/logo.png" width="910" height="60" /></div>
	</div><!-- header -->

        <div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
                        'id'=>'megamenu1',
                        'htmlOptions'=>array('class'=>'megamenu'),
                        'items'=>array(
				array('label'=>'Incio', 'url'=>array('/site/index')),
				array('label'=>'Opciones Requisicion', 'url'=>array('/site/page', 'view'=>'requisicionadm')),
                                array('label'=>'Administracion', 'url'=>array('/site/page', 'view'=>'administracion')), 
                                array('label'=>'Proceso de Negocio', 'url'=>array('/site/page', 'view'=>'about')),
                                array('label'=>'Contactenos', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->

        

	<?php /*if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif*/?>

	<?php echo $content; ?>

	<div class="clear"></div>
        
    
    
        
  
	<div id="footer">
		Plataforma desarrollada por TalenTracking, todos los derechos reservados. Prohibida su reproduccion total o parcial. info@talenttracking.com
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
