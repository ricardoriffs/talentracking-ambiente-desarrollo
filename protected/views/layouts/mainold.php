<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo 'Talent tracking -tool- Personnel Request' ?></title>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/js/jkmegamenu.css" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jkmegamenu.js">
<script type="text/javascript">

jkmegamenu.definemenu("megaanchor", "megamenu1", "mouseover")

</script>

</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo 'Talent tracking -tool- Personnel Request' ?></div>
	</div><!-- header -->

	
        <div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
                        'id'=>'megamenu1',
                        'htmlOptions'=>array('class'=>'megamenu'),
                        'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Requisicion Cliente Interno', 'url'=>array('/requisicion/admin')),
                                array('label'=>'Requisicion Gte Talento Humano', 'url'=>array('/requisicion1/admin')),
                                array('label'=>'Admin', 'url'=>array('/site/page', 'view'=>'administracion')), 
                                array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                                array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->


	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by P&T.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
