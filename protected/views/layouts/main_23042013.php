<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo 'Talent tracking -tool- Personnel Request' ?></title>




<link type="text/css" href="menu.css" rel="stylesheet" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="menu.js"></script>


</script>

</head>

<body>
<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo ''?><img src="images/logo.png" width="910" height="60" /></div>
	</div><!-- header -->


<?php



                  $Criteria = new CDbCriteria();
                  $Criteria->condition = "username ='" .Yii::app()->user->name. "'" ;

                  $resultusuarios = Usuario::model()->findAll($Criteria);
                  foreach ($resultusuarios  as $resultusuario)
                      {  
                         $rol = $resultusuario->rol;
                         $nameU = $resultusuario->nombrecompleto; 

                  } 
                  
                  $Controlador = Yii::app()->controller->id;
 
      if ($rol == 'admin')  {
          $this->widget('application.extensions.mbmenu.MbMenu',array('encodeLabel'=>false,
            'items'=>array(
                //'label'=>'<img src="'.Yii::app()->baseUrl.'/images/login.gif" />Login/>'
                array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/home1.png" />    Inicio', 'url'=>array('/site/index')), 
                //array('label'=>'Inicio', 'url'=>array('/site/index'),'image'=>Yii::app()->request->baseUrl.'/images/home1.png'),
                //array('label'=>'Inicio', 'url'=>array('/site/index')),
                array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/requ1.png" />    Requisicion',
                  
                   'items'=>array(array('label'=>'Requisicion Aprobaciones',
                      'items'=>array(
                        array('label'=>'Cliente Interno', 'url'=>array('requisicion/admin','view'=>'subsubr1')),
                        array('label'=>'Gerente Talento Humano - METAPETROLEUM', 'url'=>array('requisicion1/admin','view'=>'subsubr2')),
                        array('label'=>'Gerente Talento Humano - Filiales & PACIFIC STRATUS ENERGY', 'url'=>array('requisicion1/admin','view'=>'subsubr3')),
                        array('label'=>'Lider de Seleccion', 'url'=>array('requisicion2/admin','view'=>'subsubr4')), 
                      ),
                     ),
   
                    array('label'=>'Formatos Procesos',
                      'items'=>array(
                        array('label'=>'Coordinadores Seleccion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1')),
                        
                      ),
                    ),
                  ),
                ),
             

                array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/admi1.png" />    Administracion',
                  
                   'items'=>array(array('label'=>'Estructura',
                      'items'=>array(
                        array('label'=>'Unidad de Negocio', 'url'=>array('Unidadnegocio/admin','view'=>'subsube1')),
                        array('label'=>'Vicepresidencias', 'url'=>array('vicepresidencias/admin','view'=>'subsube2')),             
                        array('label'=>'Areas Vicepresidencias', 'url'=>array('areavicepresidencia/admin','view'=>'subsube3')), 
                        array('label'=>'Sedes Compania', 'url'=>array('sedescompanias/admin','view'=>'subsube43')),                          
                      ),
                     ),
                    
                    array('label'=>'Otras Tablas Auxiliares',
                      'items'=>array(
                        array('label'=>'Autoriza Proceso', 'url'=>array('Autorizaproceso/admin','view'=>'subsuo1')),
                        array('label'=>'Tipo de Contratacion', 'url'=>array('tipocontratacion/admin','view'=>'subsuo2')),
                        array('label'=>'Cargos a Requerir(R) o Cargos Vigentes(V)', 'url'=>array('cargo/admin','view'=>'subsuo3')), 
                        array('label'=>'Turnos', 'url'=>array('turno/admin','view'=>'subsuo4')), 
                        array('label'=>'Rango de Salarios', 'url'=>array('rangosalario/admin','view'=>'subsuo5')),  
                        //array('label'=>'Tipo de Procesos de seleccion', 'url'=>array('tipoprocesoseleccion/admin','view'=>'subsuo6')),    
                        array('label'=>'Companias Head Hunter', 'url'=>array('headhunter/admin','view'=>'subsuo7')),    
                        array('label'=>'Companias Procesos Externos', 'url'=>array('companiasprocesosexternos/admin','view'=>'subsuo8')),    
                        array('label'=>'Actividades Definidas Requisicion ', 'url'=>array('actividadesflujos/admin','view'=>'subsuo11')),    
                        array('label'=>'Ejecucion Actividad por Requisicion ', 'url'=>array('ejecucionactividad/admin','view'=>'subsuo9')),    
                        array('label'=>'Perfil Salarial', 'url'=>array('perfilsalarial/admin','view'=>'subsuo10')),    

                      ),
                    ),
                    array('label'=>'Procesos',
                      'items'=>array(
                        array('label'=>'Candidatos', 'url'=>array('Candidatos/admin','view'=>'subsuo1')),
                        array('label'=>'Tipo de Procesos de seleccion', 'url'=>array('tipoprocesoseleccion/admin','view'=>'subsuo6')),    
                        array('label'=>'Procesos Publicados', 'url'=>array('procesos/admin','view'=>'subsuo10')),    
                        array('label'=>'Tipo Comentarios Requisicion Proceso', 'url'=>array('tipocomentario/admin','view'=>'subsuo11')),     
                      ),
                    ),

                  array('label'=>'Accesos y Parametros',
                      'items'=>array(
                        array('label'=>'Usuarios', 'url'=>array('usuario/admin','view'=>'subsuu1')),
                        array('label'=>'Parametro Flujo Gerente', 'url'=>array('requisicionflujogerente/admin','view'=>'subsuu2')), 

                      ),
                    ),


                  ),
                ),

             array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/esta1.png" />    Estadistica',
                  
                   'items'=>array(array('label'=>'Requisiciones',
                      'items'=>array(
                        array('label'=>'Unidad de Negocios', 'url'=>array('requisicionest/admin','view'=>'est01')),
                        array('label'=>'Areas Vicepresidencia', 'url'=>array('requisicionest/admin','view'=>'est02')),
                        array('label'=>'Autorizacion Procesos', 'url'=>array('requisicionest/admin','view'=>'est03')),
                        array('label'=>'Numero de Vacantes', 'url'=>array('requisicionest/admin','view'=>'est04')), 
                        //array('label'=>'Cargo', 'url'=>array('requisicionest/admin','view'=>'est05')),
                        //array('label'=>'Lugas de Trabajo', 'url'=>array('requisicionest/admin','view'=>'est06')),
                        array('label'=>'Tipo Contratacion', 'url'=>array('requisicionest/admin','view'=>'est07')),
                        array('label'=>'Coordinador asignado', 'url'=>array('requisicionest/admin','view'=>'est08')),
                        array('label'=>'Tipo Proceso Seleccion', 'url'=>array('requisicionest/admin','view'=>'est09')), 
                        array('label'=>'Comentarios por Fecha', 'url'=>array('requisicionest/admin','view'=>'est10')),  
                      ), 
                     ),


                   array('label'=>'Procesos',
                      'items'=>array(
                        array('label'=>'Candidatos por procesos', 'url'=>array('requisicionest/admin','view'=>'estp01')),
                        array('label'=>'Tipos de Candidatos Registrados', 'url'=>array('requisicionest/admin','view'=>'estp02')),
                        array('label'=>'Cuadro de Vacantes-', 'url'=>array('requisicionest/admin','view'=>'estp03')),
                        //array('label'=>'Abiertos por Vicepresidencia -En Gte, Lider o Coordinador-', 'url'=>array('requisicionest/admin','view'=>'estp04')),
                        //array('label'=>'Estado de candidatos', 'url'=>array('requisicionest/admin','view'=>'estp03')),
                        //array('label'=>'Tipo de Candidatos por procesos', 'url'=>array('requisicionest/admin','view'=>'estp04')), 
                      ),
                     ),

   
                  ),
                ),   

             array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/proc1.png" />    Proceso de Negocio', 'url'=>array('/site/page','view'=>'about')),
             array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/cont1.png" />    Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             //array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest) 
             array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/logi1.png" /> Logout (' . $nameU. ')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)   
             //array('label'=>'Logout ('.Yii::app()->user->name. '-' . $nameU .')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest) 
             //array('label'=>'<img src="'.Yii::app()->baseUrl.'/images/logi1.png" /> Logout ('.Yii::app()->user->name.'), 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));} elseif ($rol == 'clienteinterno') {


  	   $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
             array('label'=>'Inicio', 'url'=>array('/site/index')),
             array('label'=>'Requisicion',

                   'items'=>array(array('label'=>'Requisicion Aprobaciones',
                      'items'=>array(
                        array('label'=>'Cliente Interno', 'url'=>array('requisicion/admin','view'=>'A')),
                        
                      ),
                     ),
                    ),
                   ),
                                 
	     array('label'=>'Seg. en Linea', 'url'=>array('ejecucionactividad/admin','v'=>'A')),
             array('label'=>'Enviados a Contratacion', 'url'=>array('ejecucionactividad/admin','v'=>'C')),
             array('label'=>'Adm. Cargos', 'url'=>array('cargo/admin','view'=>'subsuo10')),
             array('label'=>'Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             array('label'=>'Logout ('. $nameU.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));






}elseif ($rol == 'gerente1') {


  	   $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
             array('label'=>'Inicio', 'url'=>array('/site/index')),
             array('label'=>'Requisicion',

                   'items'=>array(array('label'=>'Requisicion Aprobaciones',
                      'items'=>array(
                          array('label'=>'Gerente Talento Humano', 'url'=>array('requisicion1/admin','view'=>'subsubr2')),
                          array('label'=>'Coordinadores Seleccion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'A','fr'=>'G')), 
                          array('label'=>'Coordinadores Seleccion Enviados a Contratacion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'C')), 
                        
                      ),
                     ),
                    ),
                   ),
                                 
             array('label'=>'Proceso de Negocio', 'url'=>array('/site/page','view'=>'about')),
             array('label'=>'Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             array('label'=>'Logout ('.$nameU.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));






} elseif ($rol == 'gerente2') {


  	   $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
             array('label'=>'Inicio', 'url'=>array('/site/index')),
             array('label'=>'Requisicion',

                   'items'=>array(array('label'=>'Requisicion Aprobaciones',
                      'items'=>array(
                         array('label'=>'Gerente Talento Humano ', 'url'=>array('requisicion1/admin','view'=>'subsubr3')),
                         array('label'=>'Coordinadores Seleccion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'A','fr'=>'G')), 
                         array('label'=>'Coordinadores Seleccion Enviados a Contratacion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'C')),                          
                        
                      ),
                     ),
                    ),
                   ),
                                 
             array('label'=>'Proceso de Negocio', 'url'=>array('/site/page','view'=>'about')),
             array('label'=>'Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             array('label'=>'Logout ('.$nameU.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));






}


elseif ($rol == 'liderseleccion') {


  	   $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
             array('label'=>'Inicio', 'url'=>array('/site/index')),
             array('label'=>'Requisicion',

                   'items'=>array(array('label'=>'Requisiciones Aprobaciones',
                      'items'=>array(
                         array('label'=>'Lider de Seleccion', 'url'=>array('requisicion2/admin','view'=>'subsubr4')), 
                         array('label'=>'Coordinadores Seleccion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'A')), 
                         array('label'=>'Coordinadores Seleccion Enviados a Contratacion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'C')), 
                        
                      ),
                     ),
                    ),
                   ),
           array('label'=>'Estadisticas',
                  
                   'items'=>array(array('label'=>'Requisiciones',
                      'items'=>array(
                        array('label'=>'Unidad de Negocios', 'url'=>array('requisicionest/admin','view'=>'est01')),
                        array('label'=>'Areas Vicepresidencia', 'url'=>array('requisicionest/admin','view'=>'est02')),
                        array('label'=>'Autorizacion Procesos', 'url'=>array('requisicionest/admin','view'=>'est03')),
                        array('label'=>'Numero de Vacantes', 'url'=>array('requisicionest/admin','view'=>'est04')), 
                        //array('label'=>'Cargo', 'url'=>array('requisicionest/admin','view'=>'est05')),
                        //array('label'=>'Lugas de Trabajo', 'url'=>array('requisicionest/admin','view'=>'est06')),
                        array('label'=>'Tipo Contratacion', 'url'=>array('requisicionest/admin','view'=>'est07')),
                        array('label'=>'Coordinador asignado', 'url'=>array('requisicionest/admin','view'=>'est08')),
                        array('label'=>'Tipo Proceso Seleccion', 'url'=>array('requisicionest/admin','view'=>'est09')), 
                        array('label'=>'Comentarios por Fecha', 'url'=>array('requisicionest/admin','view'=>'est10')),  
                      ),
                     ),


                   array('label'=>'Procesos',
                      'items'=>array(
                        array('label'=>'Candidatos por procesos', 'url'=>array('requisicionest/admin','view'=>'estp01')),
                        array('label'=>'Tipos de Candidatos Registrados', 'url'=>array('requisicionest/admin','view'=>'estp02')),
                        //array('label'=>'Estado de candidatos', 'url'=>array('requisicionest/admin','view'=>'estp03')),
                        //array('label'=>'Tipo de Candidatos por procesos', 'url'=>array('requisicionest/admin','view'=>'estp04')), 
                      ),
                     ),

   
                  ),
                ),   
 
                                 
             array('label'=>'Proceso de Negocio', 'url'=>array('/site/page','view'=>'about')),
             array('label'=>'Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             array('label'=>'Logout ('. $nameU .')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));






}


elseif ($rol == 'coordinador'|| $rol == 'coordinador1' || $rol == 'coordinador2') {


  	   $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
             array('label'=>'Inicio', 'url'=>array('/site/index')),
             array('label'=>'Formatos Procesos',

                      'items'=>array(
                        array('label'=>'Coordinadores Seleccion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'A')),

                    ),
                   ),
             array('label'=>'Enviados a Contratacion', 'url'=>array('requisicionproceso/admin','view'=>'subsubp1','v'=>'C')),                  
             array('label'=>'Proceso de Negocio', 'url'=>array('/site/page','view'=>'about')),
             array('label'=>'Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             array('label'=>'Logout ('.$nameU.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));






}




 elseif($Controlador == 'candidatos'){
     
 } else 

{
          $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
                array('label'=>'Inicio', 'url'=>array('/site/index')),                
             array('label'=>'Proceso de Negocio', 'url'=>array('/site/page','view'=>'about')),
             array('label'=>'Contactenos', 'url'=>array('/site/contact')),
             array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
             array('label'=>'Logout ('.$nameU.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

            ),
    ));
                  
}

?>

        

	<?php /*if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif*/?>

	<?php echo $content; ?>

	<div class="clear"></div>
        
    
    
        
  
	<div id="footer">
		Plataforma desarrollada por TalenTracking, todos los derechos reservados. Prohibida su reproduccion total o parcial. info@talenttracking.com
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
