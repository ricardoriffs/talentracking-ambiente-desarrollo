
<?php
$cs = Yii::app()->clientScript;
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.calculation.min.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.format.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/template.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/common.js', CClientScript::POS_END);

$image_url = Yii::app()->request->baseUrl . "/images/add.gif";
// Demo objects
$MetadatosRel = array();
$MetadatosAdi = array();
Yii::app()->getComponent("bootstrap");
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'candidatos-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    ?>

    <p class="note">Por favor llene todos los Campos que se muestran a continuación:</p>

<?php echo $form->errorSummary($model); ?>


    <table id="ancho_label_tabla">
        <tr>
            <th>
<?php echo $form->labelEx($model, 'tipoContratoID'); ?>
            </th>
            <td>
                <div>
                    <?php
                    /* echo $form->dropdownlist(
                      $model,
                      'tipoContratoID',
                      CHtml::listData(TipoContratacion::model()->findAll(), 'TipoContratacionID', 'Nombre'));
                     */
                    ?>
                    <?php
                    echo $form->dropdownlist(
                            $model, 'tipoContratoID',
                            //CHtml::listData(Tipocontratacion::model()->findAll(array('condition'=> "ModuloID =2")), 'TipoContratacionID', 'Nombre'),
                            CHtml::listData(Tipocontratacion::model()->findAll(array('condition' => "TipoContratacionID=9")), 'TipoContratacionID', 'Nombre')
//                                    array('empty'=>'-- Seleccione una opción --')
//                                    array(
//                                        'ajax' => array(
//                                        'type' => 'POST',
//                                        'url' => $this->createUrl('Candidatos/obtenerTipoProcesoSelAjax'),
//                                        'success' => 'js:function(data) { $("#newid").val(data.id);$("#message").val(data.msg); }'
//                                        ))
                    );
                    ?>
                    <p class="hint">
                        Nota: Seleccione el típo de vinculación.
                    </p>
                    <?php echo $form->error($model, 'tipoContratoID'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo $form->labelEx($model, 'id_proceso'); ?>
            </th>
            <td>
                <div>
                    <?php
                    echo $form->textField(
                            $model, 'id_proceso');
//                                    array(
//                                        'ajax' => array(
//                                        'type' => 'POST',
//                                        'url' => $this->createUrl('Candidatos/obtenerTipoProcesoSelAjax'),
//                                        //'success' => 'function(data)'
//                                        )
//                                    ));
//                                array(
//                                    'onChange'=>CHtml::ajax(array(
//                                        'type'=>'POST',
//                                        'url'=> $this->createUrl('Candidatos/obtenerTipoProcesoSelAjax'),
//                                        'update'=>'#data',
//                                        'success'=>new CJavaScriptExpression("function(){ alert('hola') }")
//                                        ))
//                                    ));
//                                    'onchange'=>'js:showUser(this.value)',
//                                    'allowText' => false,
//                                ));
                    ?>
                    <?php echo $form->error($model, 'id_proceso'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <?php //echo $form->labelEx($model,'id_proceso'); ?>
            </th>
            <td>
                <div>
                    <?php /* echo $form->textField($model,
                      array(
                      'ajax' => array(
                      'type' => 'POST',
                      'url' => $this->createUrl('Candidatos/obtenerTipoProcesoSelAjax'),
                      'success' => 'function(data)'
                      )
                      )); ?>
                      <?php echo $form->error($model,'id_proceso'); */ ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo $form->labelEx($model, 'identificacion'); ?>
            </th>
            <td>
                <div>
                    <?php
                    echo $form->textField(
                            $model, 'identificacion'
//                                array(
//                                    'onChange'=>CHtml::ajax(array(
//                                        'type'=>'POST',
//                                        'url'=> 'Candidatos/verificarRegAnteriorCandidato',
////                                        'update'=>'#description_id',
//                                        'data' => array('identificacion'=>'js:this.value'),
////                                        'success'=> "mostrarAdvertencia(this.value, 'hola')",
//                                        ))
//                                    )
                    );
                    ?>
                    <?php echo $form->error($model, 'identificacion'); ?>
                    <!--                        <div id="description_id">
                                            </div>                        -->
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo $form->labelEx($model, 'nombres'); ?>
            </th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'nombres', array('size' => 50, 'maxlength' => 50)); ?>
                    <?php echo $form->error($model, 'nombres'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo $form->labelEx($model, 'apellidos'); ?>
            </th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'apellidos', array('size' => 50, 'maxlength' => 50)); ?>
                    <?php echo $form->error($model, 'apellidos'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo $form->labelEx($model, 'edad'); ?>
            </th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'edad'); ?>
                    <?php echo $form->error($model, 'edad'); ?>
                </div>
            </td>
        </tr>

        <tr>
            <th><?php echo $form->labelEx($model, 'ciudad_id'); ?></th>
            <td>
                <div>
                    <?php echo $form->hiddenField($model, 'ciudad_id'); ?>
                    <?php
                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'model' => $model,
                        'name' => 'ciudad',
//         'attribute' => 'ciudad_id',
                        'source' => 'js: function(request, response) {
                $.ajax({
                    url:"' . $this->createUrl('admin/suggestCity') . '",
                    dataType:"json",
                    data:{q:request.term},
                    success:function(data){response(data)}
                })
            }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'select' => "js:function(event,ui){ $('#RegistroCandidatos_ciudad_id').val(ui.item.id)}",
                            'change' => "js:function(event,ui){if(!ui.item){ $('#RegistroCandidatos_ciudad_id').val('')}}"
                        )
                    ));
                    ?>
                    <?php /* $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      //'model'=>$model,
                      //'attribute'=>'name',
                      'id'=>'ciudad_id',
                      'name'=>'ciudad_id',
                      'source'=>'http://www.eha.ee/labs/yiiplay/index.php/en/request/suggestCountry',
                      //    'source'=>$this->createUrl('request/suggestCountry'),
                      'htmlOptions'=>array(
                      'size'=>'40'
                      ),
                      )); */ ?>
                    <?php //echo $form->textField($model,'ciudad_id'); ?>
<?php //echo $form->error($model,'ciudad_id');  ?>
                </div>
            </td>
        </tr>

        <tr>
            <th><?php echo $form->labelEx($model, 'fuente_reclutamiento'); ?></th>
            <td>
                <?php //echo $form->dropDownList($model,'fuente_reclutamiento', array(
                $form->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'model' => $model,
                    'name' => 'RegistroCandidatos[fuente_reclutamiento]',
                    'source' => array(
                    'El empleo',
                    'Computrabajo',
                    'Zona jobs',
                    'Trabajando',
                    'Facebook',
                    'Universidades',
                    'Oferta interna',
                    'LinkedIn',
                    'Otro'
                )
                )); ?>
                <div id="otro" style="display:none">
                    <?php echo $form->labelEx($model, 'fuente_reclutamiento_otro'); ?>
                    <?php echo $form->textArea($model, 'fuente_reclutamiento_otro', array('maxlength' => 100)); ?>
                    <script type="text/javascript">var c="#RegistroCandidatos_fuente_reclutamiento";var o="#otro";$(c).click(function(){a($(this))});$(c).blur(function(){a($(this))});$(c).keyup(function(e){if(e.keyCode===13){e.preventDefault();a($(this))}else{$(o).hide()}});function a(p){var v=p.val();if(v.length!==0&&v==="Otro"){$(o).show()}else{$(o).hide()}}</script>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'direccion'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'direccion', array('size' => 60, 'maxlength' => 300)); ?>
<?php echo $form->error($model, 'direccion'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo 'Teléfonos de Contacto' ?></th>
            <td>
                <table>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'teloficina'); ?></th>
                        <td>
                            <div>
                                <?php echo $form->textField($model, 'teloficina'); ?>
<?php echo $form->error($model, 'teloficina'); ?>
                            </div>
                        </td>
                        <th><?php echo $form->labelEx($model, 'teloficinaext'); ?></th>
                        <td>
                            <div>
                                <?php echo $form->textField($model, 'teloficinaext', array('size' => 8, 'maxlength' => 8)); ?>
<?php echo $form->error($model, 'teloficinaext'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'telcasa'); ?></th>
                        <td colspan="3">
                            <div>
                                <?php echo $form->textField($model, 'telcasa'); ?>
<?php echo $form->error($model, 'telcasa'); ?>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'telcel1'); ?></th>
                        <td colspan="3">
                            <div>
                                <?php echo $form->textField($model, 'telcel1'); ?>
<?php echo $form->error($model, 'telcel1'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'telcel2'); ?></th>
                        <td colspan="3">
                            <div>
                                <?php echo $form->textField($model, 'telcel2'); ?>
<?php echo $form->error($model, 'telcel2'); ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo 'Correo electrónico' ?></th>
            <td>
                <table>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'mail1'); ?></th>
                        <td>
                            <div>
                                <?php echo $form->textField($model, 'mail1'); ?>
<?php echo $form->error($model, 'mail1'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'mail2'); ?></th>
                        <td>
                            <div>
                                <?php echo $form->textField($model, 'mail2'); ?>
<?php echo $form->error($model, 'mail2'); ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'ultimoCargo'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'ultimoCargo', array('size' => 60, 'maxlength' => 300)); ?>
<?php echo $form->error($model, 'ultimoCargo'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'aniosProfesional'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'aniosProfesional', array('size' => 10, 'maxlength' => 10)); ?>
<?php echo $form->error($model, 'aniosProfesional'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'aniosRequeridos'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'aniosRequeridos', array('size' => 10, 'maxlength' => 10)); ?>
<?php echo $form->error($model, 'aniosRequeridos'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'aniosExperienciaSector'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'aniosExperienciaSector', array('size' => 10, 'maxlength' => 10)); ?>
<?php echo $form->error($model, 'aniosExperienciaSector'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'sueldoActual'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'sueldoActual', array('size' => 10, 'maxlength' => 10)); ?>
<?php echo $form->error($model, 'sueldoActual'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'sueldoAspirado'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'sueldoAspirado', array('size' => 10, 'maxlength' => 10)); ?>
<?php echo $form->error($model, 'sueldoAspirado'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>
<?php //echo $form->labelEx($model,'familiarEnEmpresa');  ?>
            </th>
            <td>
                <div>
                    <?php /* echo $form->radioButton($model, 'familiarEnEmpresa', array(
                      'value'=>'S',
                      'id'=>'btnname1',
                      'uncheckValue'=>null
                      )); */ ?>
                    <?php /* echo $form->radioButton($model, 'familiarEnEmpresa', array(
                      'value'=>'N',
                      'id'=>'btnname2',
                      'uncheckValue'=>null
                      )); ?>
                      <?php
                      $accountStatus = array('S'=>'SI', 'N'=>'NO');
                      echo $form->radioButtonList($model,'familiarEnEmpresa',$accountStatus,
                      array(
                      'separator'=>' ',
                      'labelOptions'=>array('style'=>'display:inline; margin-right: 20px;'),
                      ));
                      ?>
                      <?php echo $form->error($model,'familiarEnEmpresa'); */ ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'familiarEnEmpresa'); ?></th>
            <td>
                <div class="rpanel">
                    <?php
                    echo $form->radioButton($model, 'familiarEnEmpresa', array(
                        'value' => 'S',
                        'id' => 'RegistroCandidatos_familiarEnEmpresa_0',
                        'uncheckValue' => null
                    ));
                    ?>
                    <label for="RegistroCandidatos_familiarEnEmpresa_0" style="display: inline; margin-right: 20px;">SI</label>

                    <span class="rpanelContent">
                        <?php echo $form->labelEx($model, 'nom_familiar'); ?>
                        <?php echo $form->textField($model, 'nom_familiar'); ?>
<?php echo $form->error($model, 'nom_familiar'); ?>
                    </span>
                </div>
                <div class="rpanel">
                    <?php
                    echo $form->radioButton($model, 'familiarEnEmpresa', array(
                        'value' => 'N',
                        'id' => 'RegistroCandidatos_familiarEnEmpresa_1',
                        'uncheckValue' => null
                    ));
                    ?>
                    <label for="RegistroCandidatos_familiarEnEmpresa_1" style="display: inline; margin-right: 20px;">NO</label>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'referenciadoInterno'); ?></th>
            <td>
                <div class="rpanel">
                    <?php
                    echo $form->radioButton($model, 'referenciadoInterno', array(
                        'value' => 'S',
                        'id' => 'RegistroCandidatos_referenciadoInterno_0',
                        'uncheckValue' => null
                    ));
                    ?>
                    <label for="RegistroCandidatos_referenciadoInterno_0" style="display: inline; margin-right: 20px;">SI</label>

                    <span class="rpanelContent">
                        <?php echo $form->labelEx($model, 'nom_referenciado'); ?>
                        <?php echo $form->textField($model, 'nom_referenciado'); ?>
<?php echo $form->error($model, 'nom_referenciado'); ?>
                    </span>
                </div>
                <div class="rpanel">
                    <?php
                    echo $form->radioButton($model, 'referenciadoInterno', array(
                        'value' => 'N',
                        'id' => 'RegistroCandidatos_referenciadoInterno_1',
                        'uncheckValue' => null
                    ));
                    ?>
                    <label for="RegistroCandidatos_referenciadoInterno_1" style="display: inline; margin-right: 20px;">NO</label>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'nombreCargo'); ?></th>
            <td>
                <div>
                    <?php echo $form->textField($model, 'nombreCargo', array('size' => 60, 'maxlength' => 300)); ?>
<?php echo $form->error($model, 'nombreCargo'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'foto'); ?></th>
            <td>
                <div>
                    <?php echo $form->FileField($model, 'foto'); ?>
<?php echo $form->error($model, 'foto'); ?>
                    <p class="hint">
                        Nota: Anexe una foto en formato JPG, GIF, PNG o BMP con un tamaño máximo de 1MB.
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'hojavida'); ?></th>
            <td>
                <div>
                    <?php echo $form->FileField($model, 'hojavida'); ?>
<?php echo $form->error($model, 'hojavida'); ?>
                    <p class="hint">
                        Nota: Anexe un archivo de WORD, EXCEL o PDF con un tamaño máximo de 2MB. Este archivo debe ir sin anexos, solo su perfil y/o currículo para que el tamaño del archivo sea pequeño.
                    </p>
                </div>
            </td>
        </tr>
        <tr>
        <th><div class="formaprobacion"><?php echo $form->labelEx($model, 'formaprobacion'); ?></div></th>
        <td>
            <div class="formaprobacion">
                <?php echo $form->FileField($model, 'formaprobacion'); ?>
<?php echo $form->error($model, 'formaprobacion'); ?>
                <p class="hint">
                    Nota: Si el proceso de selección al que aplica es interno (Job Posting) debe anexar el formato de aplicación a la oferta interna, además, debe incluir la firma de aprobación de su Supervisor y del Gerente de área. Este archivo debe tener extensión JPG, PNG, BMP, TIFF, PDF.
                </p>
            </div>
        </td>
        </tr>
        <tr>
            <th><?php //echo 'Nivel Educativo'  ?></th>
            <td>
                <div>
                    <?php /*
                      $nivel_educativo = array('ne_primaria' => 'Primaria','ne_secundaria'=> 'Secundaria','ne_tecnico' => 'Técnico','ne_tecnologo' => 'Tecnologo', 'ne_profecional' => 'Profesional', 'ne_especializacion' => 'Especialización', 'ne_maestria' => 'Maestria', 'ne_doctorado' => 'Doctorado');
                      echo $form->checkboxList($model, 'idCandidato', $nivel_educativo,
                      array('template'=>'{input}{label}', 'labelOptions'=>array('style'=>'display:inline; margin-left: 20px;'))
                      );
                      ?>
                      <?php //echo $form->checkBox($model,'ne_primaria', array('value'=>1, 'uncheckValue'=>0)); ?>
                      <?php echo $form->error($model,'idCandidato'); */ ?>
                </div>
            </td>
        </tr>
        <?php $opcionesEstudio = array(
	'Administración',
	'Agronomía',
	'Antropología, Artes Liberales',
	'Arquitectura y afines',
	'Artes Plásticas, Visuales y afines',
	'Artes Representativas',
	'Bacteriología',
	'Bibliotecología, otros de Ciencias Sociales y Humanas',
	'Biología, Microbiología y afines',
	'Ciencia Política, Relaciones Internacionales',
	'Comunicación Social, Periodismo y afines',
	'Contaduría Pública',
	'Deportes, Educación Física y Recreación',
	'Derecho y afines',
	'Diseño',
	'Economía',
	'Educación',
	'Enfermería',
	'Filosofía, Teología y afines',
	'Física',
	'Formación relacionada con el campo militar o policial',
	'Geografía, Historia',
	'Geología, otros programas de Ciencias Naturales',
	'Ingeniería Administrativa y afines',
	'Ingeniería Agrícola, Forestal y afines',
	'Ingeniería Agroindustrial, Alimentos y afines',
	'Ingeniería Agronómica, Pecuaria y afines',
	'Ingeniería Ambiental, Sanitaria y afines',
	'Ingeniería Biomédica y afines',
	'Ingeniería Civil y afines',
	'Ingeniería de Minas, Metalurgia y afines',
	'Ingeniería de Sistemas, Telemática y afines',
	'Ingeniería Eléctrica y afines',
	'Ingeniería Electrónica, Telecomunicaciones y afines',
	'Ingeniería Industrial y afines',
	'Ingeniería Mecánica y afines',
	'Ingeniería Química y afines',
	'Instrumentación Quirúrgica',
	'Lenguas Modernas, Literatura, Lingüística y afines',
	'Matemáticas, Estadística y afines',
	'Medicina',
	'Medicina Veterinaria',
	'Música',
	'Nutrición y Dietética',
	'Odontología',
	'Optometría, otros programas de Ciencias de la Salud',
	'Otras Ingenierías',
	'Otros programas asociados a Bellas Artes',
	'Psicología',
	'Publicidad y afines',
	'Química y afines',
	'Salud Pública',
	'Sociología, Trabajo Social y afines',
	'Terapias',
	'Zootecnia'
        );
        $opcionesEstudio = array_merge(array('' => 'Por favor seleccione'),
                array_combine($opcionesEstudio, $opcionesEstudio));
        $opcionesTecnico = array(
            'Técnico de Mantenimiento',
            'Técnico en desarrollo y mantenimiento de software',
            'Técnico en Gestión Contable',
            'Técnico en Gestión Empresarial',
            'Técnico en Logística',
            'Técnico en radiología e imágenes diagnosticas',
            'Técnico en salud ocupacional'
        );
        $opcionesTecnologo = array(
            'Tecn Administración de personal',
            'Tecn Electricista',
            'Tecn Electrónico',
            'Tecn En seguros',
            'Tecn Mecánico',
            'Tecn Metalmecánico',
            'Tecn Química',
            'Tecn Regencia Farmacia',
            'Tecn Relaciones Industriales',
            'Tecn Seguridad industrial',
            'Tecn Sistemas  Computación',
            'Tecnología de alimentos',
            'Tecnología en electrónica',
        )
        ?>
        <tr>
            <th><?php echo 'Nivel Educativo' ?></th>
            <td>
                <p class="hint">Incluya pregrados o posgrados que haya realizado o est&eacute; cursando. Registre todos los niveles de estudios que ha logrado: bachillerato, t&eacute;cnico, tecnol&oacute;gico, universitario, especializaci&oacute;n, maestr&iacute;a y doctorado seg&uacute;n sea su caso</p>
                <div>
                    <?php echo $form->label($model, 'ne_primaria') .
                    $form->dropDownList($model,'ne_primaria', array(
                        '' => 'Por favor seleccione', 'Primaria'
                    )) ?>
                </div>
                <div>
                    <?php echo $form->label($model, 'ne_secundaria') .
                    $form->dropDownList($model,'ne_secundaria', array(
                        '' => 'Por favor seleccione', 'Secundaria'
                    )) ?>
                </div>
                <div>
                    <?php echo $form->label($model, 'ne_tecnico') .
                    $form->dropDownList($model,'ne_tecnico', array_merge(
                        array('' => 'Por favor seleccione'),
                        array_combine($opcionesTecnico, $opcionesTecnico)
                    )) ?>
                </div>
                <div class="cpanel">
                    <?php echo $form->label($model, 'ne_tecnologo') .
                    $form->dropDownList($model,'ne_tecnologo', array_merge(
                        array('' => 'Por favor seleccione'),
                        array_combine($opcionesTecnologo, $opcionesTecnologo)
                    )) ?>
                </div>
                <div class="cpanel">
                    <?php echo $form->label($model, 'ne_profecional') .
                    $form->dropDownList($model,'ne_profecional', $opcionesEstudio) ?>
                </div>
                <div class="cpanel">
                    <?php echo $form->label($model, 'ne_especializacion') .
                    $form->dropDownList($model,'ne_especializacion', $opcionesEstudio) ?>
                </div>
                <div class="cpanel">
                    <?php echo $form->label($model, 'ne_maestria') .
                    $form->dropDownList($model,'ne_maestria', $opcionesEstudio) ?>
                </div>
                <div class="cpanel">
                    <?php echo $form->label($model, 'ne_doctorado') .
                    $form->dropDownList($model,'ne_doctorado', $opcionesEstudio) ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo 'Cursos Relacionados con el cargo al que aspira' ?></th>
            <td>
                <div class="complex" style="width: 300px;">
                    <span class="label"><?php echo Yii::t('ui', 'Nombre de Cursos Relacionados') ?></span>
                    <div class="panel">
                        <table class="templateFrame grid" cellspacing="0">
                            <thead class="templateHead"><tr><td><?php //echo $form->labelEx(Metadatos::model(),'valor'); ?></td></tr></thead>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <div class="add"><img width="20" height="20" style="cursor:pointer; vertical-align: bottom;" src='<?php echo $image_url; ?>'/><a><?php echo Yii::t('ui', 'Agregar'); ?></a></div>
                                        <textarea class="template" rows="0" cols="0">
                                            <tr class="templateContent">
                                                <td><?php echo CHtml::textField('MetadatosRel[{0}][valor]', '', array('size' => 60)); ?></td>
                                                <td>
                                                    <input type="hidden" class="rowIndex" value="{0}" />
                                                    <div class="remove"><?php echo Yii::t('ui', '<a>Borrar</a>'); ?></div>
                                                </td>
                                            </tr>
                                        </textarea>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody class="templateTarget">
                                <?php $i = 0; ?>
<?php foreach ($MetadatosRel as $i => $metadatoRel): ?>
                                    <tr class="templateContent">
                                        <td><?php echo $form->textField($metadatoRel, "[$i]valor"); ?></td>
                                        <td>
                                            <input type="hidden" class="rowIndex" value="<?php echo $i; ?>" />
                                            <div class="remove"><?php echo Yii::t('ui', 'Borrar'); ?>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
<?php endforeach; ?>
                            </tbody>
                        </table>
                    </div><!--panel-->
                </div><!--complex-->
            </td>
        </tr>
        <tr>
            <th><?php echo 'Cursos Adicionales en otras materias' ?></th>
            <td>
                <div class="complex" style="width: 300px;">
                    <span class="label"><?php echo Yii::t('ui', 'Nombre de Cursos Adicionales') ?></span>
                    <div class="panel">
                        <table class="templateFrame grid" cellspacing="0">
                            <thead class="templateHead">
                                <tr >
                                    <td>
<?php //echo $form->labelEx(Metadatos::model(),'valor'); ?>
                                    </td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <div class="add"><img width="20" height="20" style="cursor:pointer; vertical-align: bottom;" src='<?php echo $image_url; ?>'/><a><?php echo Yii::t('ui', 'Agregar'); ?></a></div>
                                        <textarea class="template" rows="0" cols="0">
                                            <tr class="templateContent">
                                                <td><?php echo CHtml::textField('MetadatosAdi[{0}][valor]', '', array('size' => 60)); ?></td>
                                                <td>
                                                    <input type="hidden" class="rowIndex" value="{0}" />
                                                    <div class="remove"><?php echo Yii::t('ui', '<a>Borrar</a>'); ?></div>
                                                </td>
                                            </tr>
                                        </textarea>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody class="templateTarget">
                                <?php $j = 0; ?>
<?php foreach ($MetadatosAdi as $j => $metadatoAdi): ?>
                                    <tr class="templateContent">
                                        <td>
    <?php echo $form->textField($metadatoAdi, "[$j]valor"); ?>
                                        </td>
                                        <td>
                                            <input type="hidden" class="rowIndex" value="<?php echo $j; ?>" />
                                            <div class="remove"><?php echo Yii::t('ui', 'Borrar'); ?>
                                        </td>
                                    </tr>
                                    <?php $j++; ?>
<?php endforeach; ?>
                            </tbody>
                        </table>
                        <p class="hint">Nota: Adicione uno a uno los cursos realizados mediante el enlace llamado "Agregar".</p>
                    </div><!--panel-->
                </div><!--complex-->
            </td>
        </tr>
        <tr>
            <th>
<?php echo 'Nivel de inglés' ?>
            </th>
            <td>
                <table>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'ingles_lectura'); ?></th>
                        <td>
                            <div>
                                <?php
                                $accountStatus = array('0%' => '0%', '25%' => '25%', '50%' => '50%', '75%' => '75%', '100%' => '100%');
                                echo $form->radioButtonList($model, 'ingles_lectura', $accountStatus, array(
                                    'separator' => ' ',
                                    'labelOptions' => array('style' => 'display:inline; margin-right: 20px;'),
                                ));
                                ?>
                                <?php echo $form->error($model, 'ingles_lectura'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'ingles_escritura'); ?></th>
                        <td>
                            <div>
                                <?php
                                $accountStatus = array('0%' => '0%', '25%' => '25%', '50%' => '50%', '75%' => '75%', '100%' => '100%');
                                echo $form->radioButtonList($model, 'ingles_escritura', $accountStatus, array(
                                    'separator' => ' ',
                                    'labelOptions' => array('style' => 'display:inline; margin-right: 20px;'),
                                ));
                                ?>
                                <?php echo $form->error($model, 'ingles_escritura'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'ingles_hablado'); ?></th>
                        <td>
                            <div>
                                <?php
                                $accountStatus = array('0%' => '0%', '25%' => '25%', '50%' => '50%', '75%' => '75%', '100%' => '100%');
                                echo $form->radioButtonList($model, 'ingles_hablado', $accountStatus, array(
                                    'separator' => ' ',
                                    'labelOptions' => array('style' => 'display:inline; margin-right: 20px;'),
                                ));
                                ?>
                                <?php echo $form->error($model, 'ingles_hablado'); ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th>
<?php echo 'Manejo de Sistemas empresariales' ?>
            </th>
            <td>
                <div>
                    <table style="margin: 0px;">
                        <tr>
                            <td style="width: 5%;">
                                <div>
<?php echo $form->checkBox($model, 'so_ninguno', array('value' => 'Si', 'uncheckValue' => 0)); ?>
                                    <?php echo $form->error($model, 'so_ninguno'); ?>
                                </div>
                            </td>
                            <th><?php echo $form->labelEx($model, 'so_ninguno'); ?></th>
                        </tr>
                    </table>
                </div>
                <div>
                    <table style="margin: 0px;">
                        <tr>
                            <td style="width: 5%;">
                                <div>
<?php echo $form->checkBox($model, 'so_sap', array('value' => 'Si', 'uncheckValue' => NULL)); ?>
                                    <?php echo $form->error($model, 'so_sap'); ?>
                                </div>
                            </td>
                            <th><?php echo $form->labelEx($model, 'so_sap'); ?></th>
                        </tr>
                    </table>
                </div>
                <div>
                    <table style="margin: 0px;">
                        <tr>
                            <td style="width: 5%;">
                                <div>
<?php echo $form->checkBox($model, 'so_jdedwar', array('value' => 'Si', 'uncheckValue' => NULL)); ?>
                                    <?php echo $form->error($model, 'so_jdedwar'); ?>
                                </div>
                            </td>
                            <th><?php echo $form->labelEx($model, 'so_jdedwar'); ?></th>
                        </tr>
                    </table>
                </div>
                <div>
                    <table style="margin: 0px;">
                        <tr>
                            <td style="width: 5%;">
                                <div>
<?php echo $form->checkBox($model, 'so_oracle', array('value' => 'Si', 'uncheckValue' => NULL)); ?>
                                    <?php echo $form->error($model, 'so_oracle'); ?>
                                </div>
                            </td>
                            <th><?php echo $form->labelEx($model, 'so_oracle'); ?></th>
                        </tr>
                    </table>
                </div>
                <div>
                    <table>
                        <tr>
                            <th><?php echo $form->labelEx($model, 'so_otro'); ?></th>
                            <td>
                                <div>
<?php echo $form->textField($model, 'so_otro'); ?>
                                    <?php echo $form->error($model, 'so_otro'); ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--                    <div>
<?php /*
  $sis_empresariales = array('so_ninguno' => 'Ninguno','so_sap'=> 'SAP','so_jdedwar' => 'JD Edwards','so_oracle' => 'Oracle');
  echo $form->checkboxList($model, 'idCandidato', $sis_empresariales,
  array('template'=>'{input}{label}', 'labelOptions'=>array('style'=>'display:inline; margin-left: 20px;'))
  );
  ?>
  <?php //echo $form->checkBox($model,'ne_primaria', array('value'=>1, 'uncheckValue'=>0)); ?>
  <?php echo $form->error($model,'idCandidato'); ?>
  <br/>
  <table>
  <tr>
  <th><?php echo $form->labelEx($model,'so_otro'); ?></th>
  <td>
  <div>
  <?php echo $form->textField($model,'so_otro'); ?>
  <?php echo $form->error($model,'so_otro'); */ ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>-->
            </td>
        </tr>
    </table>

    <!--	<div class="row buttons">
<?php //echo CHtml::submitButton('Enviar');  ?>
            </div>-->
    <div class="action">
<?php echo CHtml::submitButton(Yii::t('ui', $model->isNewRecord ? 'Previsualizar' : 'Previsualizar')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->