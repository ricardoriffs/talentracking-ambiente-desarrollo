<script language="JavaScript1.2">
    function ventanaSecundaria(URL, alto, ancho) {
        window.open(URL, "ventana1", "width=" + ancho + ",height=" + alto + ",scrollbars=YES")
    }
</script>

<?php
//Yii::app()->getComponent("bootstrap");

$this->breadcrumbs = array(
    'Candidatoses' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Ver Pre-filtro', 'url' => array('verPrefiltro', 'idP' => $proceso->id)),
    array('label' => 'Reporte consolidado de gestión y conocimiento técnico', 'url' => array('verCandidatosPruebas', 'idP' => $proceso->id)),
    array('label' => 'Reporte candidatos entrevistados', 'url' => array('verCandidatosEntrevista', 'idP' => $proceso->id)),
    array('label' => 'Reporte candidato(s) elegido(s)', 'url' => array('verCandidatosElegidos', 'idP' => $proceso->id)),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('candidatos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="subtitulo_proceso">
    <h3>PROCESO DE SELECCIÓN: <?php echo $proceso->nombre; ?></h3>
</div>

<div class="item">
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >No. de Proceso: "<?php echo $proceso->ofertaid; ?>"</th>
                    </tr>
                    </table>
                    </div>

                    <p><?php echo 'NOTA: Seleccione en la casilla superior de cada columna, los candidatos que continúan en el proceso de selección para este cargo, posteriormente haga click en el botón "Enviar a Pruebas".'; ?></p>

                    <div class="item">
                        <table cellspacing="0" border="0" cellpadding="0" align="center">
                            <tr>
                                <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >CANDIDATOS ASIGNADOS AL PROCESO</strong></th>
                            </tr>
                            <tr>
                                <th>Estado</th>
                                <th>Cantidad</th>
                            </tr>
                            <tr>
                                <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos no Seleccionados:</td>
                                <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[0]; ?></td>
                            </tr>
                            <tr>
                                <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos Descartados:</td>
                                <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[2]; ?></td>
                            </tr>
                            <tr>
                                <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos enviados a Pruebas / Preselección:</td>
                                <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[1]; ?></td>
                            </tr>
                            <tr>
                                <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos citados a Entrevista:</td>
                                <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[3]; ?></td>
                            </tr>
                            <tr>
                                <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos elegidos / elegibles:</td>
                                <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[4]; ?></td>
                            </tr>
                            <tr>
                                <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos Contratados:</td>
                                <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[5]; ?></td>
                            </tr>
                        </table>
                    </div>

                    <?php echo CHtml::link('Búsqueda Avanzada', '#', array('class' => 'search-button')); ?>
                    <div class="search-form" style="display:none">
                        <?php
                        $this->renderPartial('_search_prefiltro', array(
                            'model' => $candidatos,
                        ));
                        ?>
                    </div><!-- search-form -->
                    <br/>
                    <br/>
                    <br/>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
//	'id'=>'candidatos-form',
                        'enableAjaxValidation' => true,
                    ));
                    ?>

<?php echo CHtml::ajaxSubmitButton('Filter', array('candidatos/ajaxEnviarPruebas'), array(), array("style" => "display:none;")); ?>
                    <?php echo CHtml::ajaxSubmitButton('Enviar a Pruebas', array('candidatos/ajaxEnviarPruebas', 'act' => 'doEnviarPruebas', 'idP' => $proceso->id), array('success' => 'reloadGrid'), array('submit' => array('Candidatos/ajaxEnviarPruebas', 'act' => 'doEnviarPruebas', 'idP' => $proceso->id))); ?>
                    <?php echo CHtml::ajaxSubmitButton('Descartar', array('candidatos/ajaxEnviarPruebas', 'act' => 'doDescartar', 'idP' => $proceso->id), array('success' => 'reloadGrid'), array('submit' => array('Candidatos/ajaxEnviarPruebas', 'act' => 'doDescartar', 'idP' => $proceso->id))); ?>

                    <!--<div style="width:700px; overflow-x: auto;">-->
                    <?php
                    $this->widget('application.extensions.PrefiltroGridView', array(
                        'id' => 'candidatos-grid',
                        'dataProvider' => $candidatos->searchPrefiltro($proceso->id),
                        'filter' => $candidatos,
//        'pager'=>array(
//            'header'=>'PAGE',
//            'cssFile'=>Yii::app()->theme->baseUrl."/css/pagination.css",
//            'prevPageLabel'=>'',
//            'nextPageLabel'=>'',
//            'firstPageLabel'=>'',
//            'lastPageLabel'=>'',
//        ),
//        'pager'=>array(
//            'class'=>'CLinkPager',
//            'header'=>'Idi na->',
//        ),
//	'selectionChanged'=>'gridselchange',
                        'itemsCssClass' => 'gridtablecss',
//        'htmlOptions' => array('style'=>'width:880px;overflow: auto;'),
                        'htmlOptions' => array('style' => 'min-width:800px;'),
                        'columns' => array(
                            array(
                                'id' => 'idCandidato',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => '50',
//                    'value'          => 'CHtml::encode($data->idCandidato)',
                                'checkBoxHtmlOptions' => array('name' => 'idCandidatos[]'),
                            //'htmlOptions'=>array('style'=>'width: 150px;'),
                            ),
                            array(
                                'name' => 'foto',
                                'value' => 'CHtml::image(Yii::app()->baseurl."/verFoto.php?idc=".$data->idCandidato, "foto", array("idc"=>$data->idCandidato, "width"=>80, "height"=>110, "style"=>"border:#626262 solid 1px;"))',
                                'type' => 'raw',),
//			'apellidos',
//			'nombres',
                            array(
                                'header' => 'Nombre completo',
                                'name' => 'nombres',
                                'value' => 'CHtml::encode($data->nombres." ".$data->apellidos)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            'identificacion',
                            'fechaInsert',
                            'edad',
                            array(
                                'header' => 'Ciudad de residencia (Pais)',
                                'name' => 'ciudad_id',
                                'value' => '$data->ciudad ? CHtml::encode($data->ciudad . " (" . $data->ciudad->pais . ")") : ""',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'ultimoCargo',
                                'value' => 'CHtml::encode($data->ultimoCargo)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'header' => 'Ver documentos',
                                'value' => 'CHtml::image(Yii::app()->request->baseUrl."/images/libro.png", "Documentos", array("width"=>42, "heigth"=>42, "align"=>"middle", "onclick"=>"javascript:ventanaSecundaria(\'\popup_noticia1.php?r=x$data->idCandidato\',400,600)", "style"=>"cursor:pointer; cursor: hand"))',
                                'type' => 'raw',),
                            array(
                                'header' => 'Fuente de reclutamiento',
                                'value' => 'CHtml::encode($data->fuente_reclutamiento)',
                                'type' => 'raw'),
                            array(
                                'header' => 'Estado del candidato',
                                'name' => 'estado',
                                'value' => 'CHtml::encode( $data->estado==0?"No seleccionado":($data->estado==1?"En pruebas":($data->estado==2?"Descartado":($data->estado==3?"En Entrevista":($data->estado==4?"Elegido":($data->estado==5?"Contratado":" "))))))',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'color: #0066A4; font-weight: bold;'),
                            ),
                            'aniosProfesional',
                            'aniosRequeridos',
                            'aniosExperienciaSector',
                            array(
                                'name' => 'tipoContratoID',
                                'value' => 'CHtml::encode($data->tipoContrato->Nombre)',
                                'type' => 'raw',),
//			'sueldoActual',
                            'sueldoAspirado',
//			'familiarEnEmpresa',
//                        'referenciadoInterno',
                            array(
                                'name' => 'nom_familiar',
                                'value' => 'CHtml::encode($data->nom_familiar)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'nom_referenciado',
                                'value' => 'CHtml::encode($data->nom_referenciado)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
//                array (
//                    'name'=>'nombreCargo',
//                    'value'=>'CHtml::encode($data->nombreCargo)',
//                    'type'=>'raw',
//                    'htmlOptions'=>array('style'=>'text-transform: uppercase'),
//                    ),
//			'fechaInsert',
//			'id_proceso',
                            'ne_primaria',
                            'ne_secundaria',
                            array(
                                'name' => 'ne_tecnico',
                                'value' => 'CHtml::encode($data->ne_tecnico)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'ne_tecnologo',
                                'value' => 'CHtml::encode($data->ne_tecnologo)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'ne_profecional',
                                'value' => 'CHtml::encode($data->ne_profecional)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'ne_especializacion',
                                'value' => 'CHtml::encode($data->ne_especializacion)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'ne_maestria',
                                'value' => 'CHtml::encode($data->ne_maestria)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            array(
                                'name' => 'ne_doctorado',
                                'value' => 'CHtml::encode($data->ne_doctorado)',
                                'type' => 'raw',
                                'htmlOptions' => array('style' => 'text-transform: uppercase'),
                            ),
                            'ingles_lectura',
                            'ingles_escritura',
                            'ingles_hablado',
//			'so_ninguno',
                            'so_sap',
                            'so_jdedwar',
                            'so_oracle',
                            'so_otro',
                            'observaciones',
//                array (
//                    'name'=>'Ingresar observaciones',
//                    'value'=>'CHtml::textarea($data->observaciones)',
//                    'type'=>'raw',
//                    ),
//			'direccion',
//			'teloficina',
//			'teloficinaext',
//			'telcasa',
//			'telcel1',
//			'telcel2',
//			'mail1',
//			'mail2',
                        /* faltan cursos y el text area de observaciones */
                        ),
                    ));
                    ?>
                    <!--</div>-->
                    <script>
                        function reloadGrid(data) {
                            $.fn.yiiGridView.update('candidatos-grid');
                    //    $.fn.yiiGridView.update('candidatos-grid', {
                    //		data: $(this).serialize()
                    //	});
                    //        	return false;
                        }
                    </script>
<?php $this->endWidget(); ?>
