<?php
$this->breadcrumbs=array(
	'Candidatoses'=>array('index'),
	$model->idCandidato=>array('view','id'=>$model->idCandidato),
	'Update',
);

$this->menu=array(
	array('label'=>'List Candidatos', 'url'=>array('index')),
	array('label'=>'Create Candidatos', 'url'=>array('create')),
	array('label'=>'View Candidatos', 'url'=>array('view', 'id'=>$model->idCandidato)),
	array('label'=>'Manage Candidatos', 'url'=>array('admin')),
);
?>

<h1>Update Candidatos <?php echo $model->idCandidato; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>