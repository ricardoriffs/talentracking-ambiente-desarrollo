<?php
$this->breadcrumbs=array(
	'Candidatoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Candidatos', 'url'=>array('index')),
	array('label'=>'Create Candidatos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('candidatos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administracion de Candidatos</h1>

<p>
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'candidatos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectionChanged'=>'gridselchange',
        'htmlOptions' => array('style'=>'width:880px;'),       
	'columns'=>array(
		'idCandidato',
		'apellidos',
		'nombres',
		'identificacion',
		'edad',
		'ultimoCargo',
            'id_proceso',
            'nombreCargo', 
            array (
                        'header'=>'Comunicación',
                        'value'=>'CHtml::link(CHtml::encode(Enviar_Correo),Yii::app()->baseurl."/index.php?r=Candidatos/enviarCorreo&id=".$data->idCandidato, array("id"=>_.$data->idCandidato,"target"=>"_self", "readonly"=>"True", "style"=>"display:none"))',  
                        'type'=>'raw',),             
            array (
                        'header'=>'(*)Estado',
                        'value'=>'CHtml::checkbox("UserEst_valide", $u["est_valide"], array("value"=>$data->idCandidato, "id"=>$data->idCandidato, "onChange" => "javascript:gridselchange(this.value)"))', 
                        'type'=>'raw',),               
		/*
		'aniosProfesional',
		'aniosRequeridos',
		'aniosExperienciaSector',
		'sueldoActual',
		'sueldoAspirado',
		'familiarEnEmpresa',
		'foto',		
		'fechaInsert',		
		'ne_primaria',
		'ne_secundaria',
		'ne_tecnico',
		'ne_tecnologo',
		'ne_profecional',
		'ne_especializacion',
		'ne_maestria',
		'ne_doctorado',
		'ingles_lectura',
		'ingles_escritura',
		'ingles_hablado',
		'so_ninguno',
		'so_sap',
		'so_jdedwar',
		'so_oracle',
		'so_otro',
		'direccion',
		'teloficina',
		'teloficinaext',
		'telcasa',
		'telcel1',
		'telcel2',
		'mail1',
		'mail2',
		'tipocontrato',
		'hojavida',
		'formaprobacion',
		'nom_familiar',
		'nom_referenciado',
		'estado',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); 

echo "(*) Haga clic sobre la columna de ---Estado--- si desea enviar una comunicación a los postulantes Elegibles.";

?>

<script>
	
	function gridselchange(id) {
  var valor = id;
  var id_es = "_"+valor;
  if(document.getElementById(valor).checked)
   {//alert('seleccionado '+valor);
    document.getElementById(id_es).style.display = "";
   }
   else{//alert('no seleccionado');
		document.getElementById(id_es).style.display = "none";
		}  
}

</script>