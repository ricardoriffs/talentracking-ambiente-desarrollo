<script language="JavaScript1.2">

function ventanaSecundaria (URL, alto, ancho)
{ 
   window.open(URL,"ventana1","width="+ancho+",height="+alto+",scrollbars=YES") 
} 


</script>
<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>

<?php
$this->breadcrumbs=array(
	'Candidatoses'=>array('index'),
	$model->idCandidato,
);
?>

<?php if($obj_candidato != NULL){
    echo '<script languaje="javascript">';
    echo "var oferta ='".$proceso->ofertaid."';";
    echo "var estado ='".$estado."';";    
    echo 'alert("ADVERTENCIA: Actualmente usted está registrado en el Proceso No. "+oferta+", el cual se encuentra en un estado "+estado+". Si usted confirma el registro de este formulario, quedará automáticamente Descartado de cualquier proceso anterior al que se haya inscrito.");'; 
    echo '</script>';
    
 } 

 
 ?>

<h1>Confirmación de registro del Candidato "<?php echo $model->nombres. ' '. $model->apellidos; ?>" para la oferta No. <?php echo $oferta->ofertaid; ?></h1>

<p class="note">Por favor verifique que todos los datos de encuentren diligenciados correctamente.</p>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'idCandidato',
		array(
                    'name'=>'Tipo de Vinculación',
                    'value'=>CHtml::encode($model->tipoContrato->Nombre),
                        ),            
		array(
                    'name'=>'Número de Proceso',
                    'value'=>CHtml::encode($oferta->ofertaid),
                        ),
		'identificacion',            
		'nombres',            
		'apellidos',
		'edad',
                array(
                    'name'=>'Ciudad (Pais)',
                    'value'=>CHtml::encode($model->ciudad . " (" . $model->ciudad->pais . ")"),
                        ),
		'direccion',  
		array(
                    'name'=>'Teléfono de oficina',
                    'value'=>CHtml::encode($model->teloficina),
                        ),              
		array(
                    'name'=>'Extensión de oficina',
                    'value'=>CHtml::encode($model->teloficinaext),
                        ),              
		array(
                    'name'=>'Teléfono de la casa',
                    'value'=>CHtml::encode($model->telcasa),
                        ),             
		array(
                    'name'=>'Teléfono Celular # 1',
                    'value'=>CHtml::encode($model->telcel1),
                        ),  
		array(
                    'name'=>'Teléfono Celular # 2',
                    'value'=>CHtml::encode($model->telcel2),
                        ),  
		array(
                    'name'=>'Correo electrónico # 1',
                    'value'=>CHtml::encode($model->mail1),
                        ),               
		array(
                    'name'=>'Correo electrónico # 2',
                    'value'=>CHtml::encode($model->mail2),
                        ),                
		'ultimoCargo',
		'aniosProfesional',
		'aniosRequeridos',
		'aniosExperienciaSector',
		'sueldoActual',
		'sueldoAspirado',
		array(
                    'name'=>'familiarEnEmpresa',
                    'value'=>CHtml::encode($model->familiarEnEmpresa=='N'?'NO':('S'?'NO':'SI')),
                        ),   
		'nom_familiar',            
//		'foto',
		array(
                    'name'=>'referenciadoInterno',
                    'value'=>CHtml::encode($model->familiarEnEmpresa=='N'?'NO':('S'?'NO':'SI')),
                        ),                           
                'nom_referenciado',
		'nombreCargo',
//		'fechaInsert',
		array(
                    'name'=>'Nivel educativo primaria',
                    'value'=>CHtml::encode($model->ne_primaria=='Si'?'SI':'NO'),
                        ),         
		array(
                    'name'=>'Nivel educativo secundaria',
                    'value'=>CHtml::encode($model->ne_secundaria=='Si'?'SI':'NO'),
                        ),      
		array(
                    'name'=>'Nivel educativo técnico',
                    'value'=>CHtml::encode($model->ne_tecnico=='Si'?'SI':$model->ne_tecnico),
                        ),      
		array(
                    'name'=>'Nivel educativo tecnólogo',
                    'value'=>CHtml::encode($model->ne_tecnologo=='Si'?'SI':$model->ne_tecnologo),
                        ), 
		array(
                    'name'=>'Nivel educativo profesional',
                    'value'=>CHtml::encode($model->ne_profecional=='Si'?'SI':$model->ne_profecional),
                        ), 
		array(
                    'name'=>'Nivel educativo Especialización',
                    'value'=>CHtml::encode($model->ne_especializacion=='Si'?'SI':$model->ne_especializacion),
                        ),    
		array(
                    'name'=>'Nivel educativo Maestría',
                    'value'=>CHtml::encode($model->ne_maestria=='Si'?'SI':$model->ne_maestria),
                        ),  
		array(
                    'name'=>'Nivel educativo Doctorado',
                    'value'=>CHtml::encode($model->ne_doctorado=='Si'?'SI':$model->ne_doctorado),
                        ),             
		array(
                    'name'=>'Nivel de Inglés - Lectura',
                    'value'=>CHtml::encode($model->ingles_lectura),
                        ),               
		array(
                    'name'=>'Nivel de Inglés - Escritura',
                    'value'=>CHtml::encode($model->ingles_escritura),
                        ),    
		array(
                    'name'=>'Nivel de Inglés - Hablado',
                    'value'=>CHtml::encode($model->ingles_hablado),
                        ),                 
		array(
                    'name'=>'Manejo Sist Empresariales - Ninguno',
                    'value'=>CHtml::encode($model->so_ninguno),
                        ),      
		array(
                    'name'=>'Manejo Sist Empresariales - SAP',
                    'value'=>CHtml::encode($model->so_sap),
                        ),              
		array(
                    'name'=>'Manejo Sist Empresariales - JD Edwards',
                    'value'=>CHtml::encode($model->so_jdedwar),
                        ),  
		array(
                    'name'=>'Manejo Sist Empresariales - Oracle',
                    'value'=>CHtml::encode($model->so_oracle),
                        ),             
		array(
                    'name'=>'Manejo Sist Empresariales - Otro',
                    'value'=>CHtml::encode($model->so_otro),
                        ),                      
//		'hojavida',
//		'formaprobacion',
//		'estado',
//                array(            
//                    'label'=>'Documentos',
//                    'type'=>'raw',
//                    'value'=>'<a href="javascript:ventanaSecundaria(\'\popup_noticia1.php?r=x' . $model->idCandidato .  '\',400,600)">Ver documentos de candidato</a>',
//
//                    ),
            
	),
)); ?>


<div id="b2" class="row buttons">
                 <?php   
                    echo nl2br("\n"); 
                    echo nl2br("\n");     
                  ?>
                          <a class="boton" href="<?php echo 'index.php?r=Candidatos/agradecimiento&id=' . $model->idCandidato. '&of=' . $oferta->id ?>"> Confirmar registro de candidato </a>                                 
                  <?php   
                    echo nl2br("\n"); 
                    echo nl2br("\n");                      
                    ?> 
                          <a class="boton" href="<?php echo 'index.php?r=Candidatos/actualizarCandidatos&id=' . $model->idCandidato ?>"> Corregir registro de candidato </a>  
                          
                <?php /* 
    echo CHtml::Button('Delete',array(

          'submit'=> array('Candidatos/actualizarCandidatos'),
          'class' => 'boton',
          'params'=>array('id'=>1365),
          'confirm'=>"Are you sure to delete #{$oferta->ofertaid}?")); */ ?>                
 </div>                     

