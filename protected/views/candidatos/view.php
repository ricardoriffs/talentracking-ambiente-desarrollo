<script language="JavaScript1.2">

function ventanaSecundaria (URL, alto, ancho)
{ 
   window.open(URL,"ventana1","width="+ancho+",height="+alto+",scrollbars=YES") 
} 


</script>

<?php
$this->breadcrumbs=array(
	'Candidatoses'=>array('index'),
	$model->idCandidato,
);

$this->menu=array(
	array('label'=>'List Candidatos', 'url'=>array('index')),
	array('label'=>'Create Candidatos', 'url'=>array('create')),
	array('label'=>'Update Candidatos', 'url'=>array('update', 'id'=>$model->idCandidato)),
	array('label'=>'Delete Candidatos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idCandidato),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Candidatos', 'url'=>array('admin')),
);
?>

<h1>View Candidatos #<?php echo $model->idCandidato; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idCandidato',
		'apellidos',
		'nombres',
		'identificacion',
		'edad',
		'ultimoCargo',
		'aniosProfesional',
		'aniosRequeridos',
		'aniosExperienciaSector',
		'sueldoActual',
		'sueldoAspirado',
		'familiarEnEmpresa',
		'foto',
		'nombreCargo',
		'fechaInsert',
		'id_proceso',
		'ne_primaria',
		'ne_secundaria',
		'ne_tecnico',
		'ne_tecnologo',
		'ne_profecional',
		'ne_especializacion',
		'ne_maestria',
		'ne_doctorado',
		'ingles_lectura',
		'ingles_escritura',
		'ingles_hablado',
		'so_ninguno',
		'so_sap',
		'so_jdedwar',
		'so_oracle',
		'so_otro',
		'direccion',
		'teloficina',
		'teloficinaext',
		'telcasa',
		'telcel1',
		'telcel2',
		'mail1',
		'mail2',
		'tipocontrato',
		'hojavida',
		'formaprobacion',
		'nom_familiar',
		'nom_referenciado',
		'estado',
                array(            
                    'label'=>'Documentos',
                    'type'=>'raw',
                    'value'=>'<a href="javascript:ventanaSecundaria(\'\popup_noticia1.php?r=x' . $model->idCandidato .  '\',400,600)">Ver documentos de candidato</a>',

                    ),
            
	),
)); ?>
