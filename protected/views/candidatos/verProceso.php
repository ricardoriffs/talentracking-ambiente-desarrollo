<?php
//Yii::app()->getComponent("bootstrap");

$this->breadcrumbs=array(
	//'Procesoses'=>array('index'),
	'Candidatoses'=>array('index'),    
        'Manage',
	//$proceso->id,
);

$this->menu=array(
	array('label'=>'Ver Pre-filtro', 'url'=>array('verPrefiltro', 'idP'=>$proceso->id)),
	array('label'=>'Reporte consolidado de gestión y conocimiento técnico', 'url'=>array('verCandidatosPruebas', 'idP'=>$proceso->id)),    
	array('label'=>'Reporte candidatos entrevistados', 'url'=>array('verCandidatosEntrevista', 'idP'=>$proceso->id)),    
	array('label'=>'Reporte candidato(s) elegido(s)', 'url'=>array('verCandidatosElegidos', 'idP'=>$proceso->id)),    
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('candidatos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>
<div id="subtitulo_proceso">
    <h3>PROCESO DE SELECCIÓN: <?php echo $proceso->nombre; ?></h3>
</div>


<div class="item">   
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >INFORMACIÓN DEL PROCESO</th> 
	</tr>
    <tr>
        <th>Proceso ID:</th>
        <td><?php echo $proceso->id ?></td>
    </tr>
    <tr>
        <th>No. de Proceso:</th>
        <td><?php echo $proceso->ofertaid ?></td>
    </tr>    
    <tr>
        <th>Nombre:</th>
        <td><?php echo $proceso->nombre ?></td>
    </tr>  
    <tr>
        <th>Descripción:</th>
        <td><?php echo $proceso->descripcion ?></td>
    </tr> 
    <tr>
        <th>Activo:</th>
        <td><?php 
            if($proceso->activo == 'S'){
                echo('Si');
            }else{
                echo('No');
            } ?>
        </td>
    </tr>     
</table>
<table cellspacing="0" border="0" cellpadding="0" align="center">
    <tr>
        <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >CANDIDATOS ASIGNADOS AL PROCESO</th> 
    </tr>
</table>
</div>   
<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$candidatos,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'candidatos-grid',
	'dataProvider'=>$candidatos->search($proceso->id),
	'filter'=>$candidatos,
	'columns'=>array(
//		'nombres',
//                array ( 
//                    'name'=>'foto',
//                    'value'=>'CHtml::image(Yii::app()->baseurl."/verFoto.php?idc=".$data->idCandidato, "foto", array("idc"=>$data->idCandidato, "width"=>80, "height"=>110, "style"=>"border:#626262 solid 1px;float:left;"))', 
//                    'type'=>'raw',),          
                array ( 
                    'header'=>'Nombre completo',
                    'value'=>'CHtml::link(CHtml::encode($data->nombres." ".$data->apellidos),Yii::app()->baseurl."/index.php?r=Candidatos/index&id=".$data->idCandidato, array("id"=>$data->idCandidato))',  
                    'type'=>'raw',),               
//		'apellidos',
                'identificacion',
//                'sueldoAspirado',
                array(
                    'name'=>'sueldoAspirado', 
//                    'type'=>'number',
                    'value'=>'"$ ".Yii::app()->numberFormatter->formatCurrency($data->sueldoAspirado, " ")',),
                'fechaInsert',         
	),
));             

?>
            
