<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php /*echo $form->label($model,'idCandidato'); ?>
		<?php   echo $form->textField($model,'idCandidato'); */ ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>50,'maxlength'=>50)); ?>
	</div>    

	<div class="row">
		<?php echo $form->label($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'identificacion'); ?>
		<?php echo $form->textField($model,'identificacion'); ?>
	</div>   
    
<!--        <label style="color: #0066A4; font-weight: bold; margin: 10px 0px 10px 0px;">Ordenar por:</label>    
        <table style="clear: both; width: 80%;">
            <tr>
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'aniosProfesional'); ?>
                            <?php //echo $form->checkBox($model, 'aniosProfesional', array('value'=>'1', 'uncheckValue'=>0)); ?>               
                    </div>                    
                </td>
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'aniosRequeridos'); ?>
                            <?php //echo $form->checkBox($model, 'aniosRequeridos', array('value'=>'1', 'uncheckValue'=>0)); ?>             
                    </div>                      
                </td>
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'aniosExperienciaSector'); ?>
                            <?php //echo $form->checkBox($model, 'aniosExperienciaSector', array('value'=>'1', 'uncheckValue'=>0)); ?>             
                    </div>                      
                </td>                
            </tr>
        </table>
        <label style="color: #0066A4; font-weight: bold; margin: 10px 0px 10px 0px;">Filtrar por:</label>            
        <table style="clear: both; width: 80%;">
            <tr>
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'ne_profecional'); ?>
                            <?php //echo $form->checkBox($model, 'ne_profecional', array('value'=>'1', 'uncheckValue'=>0)); ?>             
                    </div>                     
                </td>
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'ne_especializacion'); ?>
                            <?php //echo $form->checkBox($model, 'ne_especializacion', array('value'=>'1', 'uncheckValue'=>0)); ?>             
                    </div>                      
                </td>  
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'ne_maestria'); ?>
                            <?php //echo $form->checkBox($model, 'ne_maestria', array('value'=>'1', 'uncheckValue'=>0)); ?>             
                    </div>                         
                </td>                
            </tr>
            <tr>
                <td>
                    <div class="row" style="margin-left: 40px;">
                            <?php //echo $form->label($model,'ne_tecnico'); ?>
                            <?php //echo $form->checkBox($model, 'ne_tecnico', array('value'=>'1', 'uncheckValue'=>0)); ?>             
                    </div>                   
                </td>
            </tr>
        </table>     -->
    <div class="row">
            <?php echo $form->label($model,'estado'); ?>
            <?php 
                echo $form->dropdownlist(
                        $model,
                        'estado',
                        array(0 => 'No Seleccionado',1 => 'En pruebas', 2 => 'Descartado', 3 => 'En Entrevista', 4 => 'Elegible', 5 => 'Contratado'),
                        array('empty'=>'-- Seleccione una opción --')
                        ); 
            ?>              
    </div> 
    
	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->