<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php /*echo $form->label($model,'idCandidato'); ?>
		<?php   echo $form->textField($model,'idCandidato'); */ ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>50,'maxlength'=>50)); ?>
	</div>    

	<div class="row">
		<?php echo $form->label($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'identificacion'); ?>
		<?php echo $form->textField($model,'identificacion'); ?>
	</div>

	<div class="row">
		<?php /* echo $form->label($model,'edad'); ?>
		<?php echo $form->textField($model,'edad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ultimoCargo'); ?>
		<?php echo $form->textField($model,'ultimoCargo',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aniosProfesional'); ?>
		<?php echo $form->textField($model,'aniosProfesional'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aniosRequeridos'); ?>
		<?php echo $form->textField($model,'aniosRequeridos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aniosExperienciaSector'); ?>
		<?php echo $form->textField($model,'aniosExperienciaSector'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sueldoActual'); ?>
		<?php echo $form->textField($model,'sueldoActual'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sueldoAspirado'); ?>
		<?php echo $form->textField($model,'sueldoAspirado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'familiarEnEmpresa'); ?>
		<?php echo $form->textField($model,'familiarEnEmpresa',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'foto'); ?>
		<?php echo $form->textField($model,'foto',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombreCargo'); ?>
		<?php echo $form->textField($model,'nombreCargo',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaInsert'); ?>
		<?php echo $form->textField($model,'fechaInsert'); */ ?>
<!--	</div>

	<div class="row">-->
		<?php /*echo $form->label($model,'id_proceso'); ?>
		<?php echo $form->textField($model,'id_proceso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_primaria'); ?>
		<?php echo $form->textField($model,'ne_primaria',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_secundaria'); ?>
		<?php echo $form->textField($model,'ne_secundaria',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_tecnico'); ?>
		<?php echo $form->textField($model,'ne_tecnico',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_tecnologo'); ?>
		<?php echo $form->textField($model,'ne_tecnologo',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_profecional'); ?>
		<?php echo $form->textField($model,'ne_profecional',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_especializacion'); ?>
		<?php echo $form->textField($model,'ne_especializacion',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_maestria'); ?>
		<?php echo $form->textField($model,'ne_maestria',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ne_doctorado'); ?>
		<?php echo $form->textField($model,'ne_doctorado',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ingles_lectura'); ?>
		<?php echo $form->textField($model,'ingles_lectura',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ingles_escritura'); ?>
		<?php echo $form->textField($model,'ingles_escritura',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ingles_hablado'); ?>
		<?php echo $form->textField($model,'ingles_hablado',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'so_ninguno'); ?>
		<?php echo $form->textField($model,'so_ninguno',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'so_sap'); ?>
		<?php echo $form->textField($model,'so_sap',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'so_jdedwar'); ?>
		<?php echo $form->textField($model,'so_jdedwar',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'so_oracle'); ?>
		<?php echo $form->textField($model,'so_oracle',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'so_otro'); ?>
		<?php echo $form->textField($model,'so_otro',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'teloficina'); ?>
		<?php echo $form->textField($model,'teloficina',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'teloficinaext'); ?>
		<?php echo $form->textField($model,'teloficinaext',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telcasa'); ?>
		<?php echo $form->textField($model,'telcasa',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telcel1'); ?>
		<?php echo $form->textField($model,'telcel1',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telcel2'); ?>
		<?php echo $form->textField($model,'telcel2',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mail1'); ?>
		<?php echo $form->textField($model,'mail1',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mail2'); ?>
		<?php echo $form->textField($model,'mail2',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipocontrato'); ?>
		<?php echo $form->textField($model,'tipocontrato',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hojavida'); ?>
		<?php echo $form->textField($model,'hojavida',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'formaprobacion'); ?>
		<?php echo $form->textField($model,'formaprobacion',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nom_familiar'); ?>
		<?php echo $form->textField($model,'nom_familiar',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nom_referenciado'); ?>
		<?php echo $form->textField($model,'nom_referenciado',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->textField($model,'estado'); */ ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->