<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'candidatos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'apellidos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nombres'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'identificacion'); ?>
		<?php echo $form->textField($model,'identificacion'); ?>
		<?php echo $form->error($model,'identificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'edad'); ?>
		<?php echo $form->textField($model,'edad'); ?>
		<?php echo $form->error($model,'edad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ultimoCargo'); ?>
		<?php echo $form->textField($model,'ultimoCargo',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'ultimoCargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aniosProfesional'); ?>
		<?php echo $form->textField($model,'aniosProfesional'); ?>
		<?php echo $form->error($model,'aniosProfesional'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aniosRequeridos'); ?>
		<?php echo $form->textField($model,'aniosRequeridos'); ?>
		<?php echo $form->error($model,'aniosRequeridos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aniosExperienciaSector'); ?>
		<?php echo $form->textField($model,'aniosExperienciaSector'); ?>
		<?php echo $form->error($model,'aniosExperienciaSector'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sueldoActual'); ?>
		<?php echo $form->textField($model,'sueldoActual'); ?>
		<?php echo $form->error($model,'sueldoActual'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sueldoAspirado'); ?>
		<?php echo $form->textField($model,'sueldoAspirado'); ?>
		<?php echo $form->error($model,'sueldoAspirado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'familiarEnEmpresa'); ?>
		<?php echo $form->textField($model,'familiarEnEmpresa',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'familiarEnEmpresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'foto'); ?>
		<?php echo $form->textField($model,'foto',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'foto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombreCargo'); ?>
		<?php echo $form->textField($model,'nombreCargo',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'nombreCargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaInsert'); ?>
		<?php echo $form->textField($model,'fechaInsert'); ?>
		<?php echo $form->error($model,'fechaInsert'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_proceso'); ?>
		<?php echo $form->textField($model,'id_proceso'); ?>
		<?php echo $form->error($model,'id_proceso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_primaria'); ?>
		<?php echo $form->textField($model,'ne_primaria',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_primaria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_secundaria'); ?>
		<?php echo $form->textField($model,'ne_secundaria',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_secundaria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_tecnico'); ?>
		<?php echo $form->textField($model,'ne_tecnico',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_tecnico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_tecnologo'); ?>
		<?php echo $form->textField($model,'ne_tecnologo',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_tecnologo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_profecional'); ?>
		<?php echo $form->textField($model,'ne_profecional',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_profecional'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_especializacion'); ?>
		<?php echo $form->textField($model,'ne_especializacion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_especializacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_maestria'); ?>
		<?php echo $form->textField($model,'ne_maestria',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_maestria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ne_doctorado'); ?>
		<?php echo $form->textField($model,'ne_doctorado',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ne_doctorado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ingles_lectura'); ?>
		<?php echo $form->textField($model,'ingles_lectura',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ingles_lectura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ingles_escritura'); ?>
		<?php echo $form->textField($model,'ingles_escritura',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ingles_escritura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ingles_hablado'); ?>
		<?php echo $form->textField($model,'ingles_hablado',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ingles_hablado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'so_ninguno'); ?>
		<?php echo $form->textField($model,'so_ninguno',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'so_ninguno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'so_sap'); ?>
		<?php echo $form->textField($model,'so_sap',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'so_sap'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'so_jdedwar'); ?>
		<?php echo $form->textField($model,'so_jdedwar',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'so_jdedwar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'so_oracle'); ?>
		<?php echo $form->textField($model,'so_oracle',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'so_oracle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'so_otro'); ?>
		<?php echo $form->textField($model,'so_otro',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'so_otro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'teloficina'); ?>
		<?php echo $form->textField($model,'teloficina',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'teloficina'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'teloficinaext'); ?>
		<?php echo $form->textField($model,'teloficinaext',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'teloficinaext'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telcasa'); ?>
		<?php echo $form->textField($model,'telcasa',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telcasa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telcel1'); ?>
		<?php echo $form->textField($model,'telcel1',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telcel1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telcel2'); ?>
		<?php echo $form->textField($model,'telcel2',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telcel2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail1'); ?>
		<?php echo $form->textField($model,'mail1',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'mail1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail2'); ?>
		<?php echo $form->textField($model,'mail2',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'mail2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipocontrato'); ?>
		<?php echo $form->textField($model,'tipocontrato',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'tipocontrato'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hojavida'); ?>
		<?php echo $form->textField($model,'hojavida',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'hojavida'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'formaprobacion'); ?>
		<?php echo $form->textField($model,'formaprobacion',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'formaprobacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nom_familiar'); ?>
		<?php echo $form->textField($model,'nom_familiar',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nom_familiar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nom_referenciado'); ?>
		<?php echo $form->textField($model,'nom_referenciado',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nom_referenciado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model,'estado'); ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->