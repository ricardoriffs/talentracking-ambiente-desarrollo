<?php
//Yii::app()->getComponent("bootstrap");

$this->breadcrumbs=array(
  'Procesoses'=>array('index'),
  $proceso->id,
);

$this->menu=array(
  array('label'=>'Ver Pre-filtro', 'url'=>array('verPrefiltro', 'idP'=>$proceso->id)),
  array('label'=>'Reporte consolidado de gestión y conocimiento técnico', 'url'=>array('verCandidatosPruebas', 'idP'=>$proceso->id)),    
  array('label'=>'Reporte candidatos entrevistados', 'url'=>array('verCandidatosEntrevista', 'idP'=>$proceso->id)),    
  array('label'=>'Reporte candidato(s) elegido(s)', 'url'=>array('verCandidatosElegidos', 'idP'=>$proceso->id)),    
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
});
$('.search-form form').submit(function(){
  $.fn.yiiGridView.update('candidatos-grid', {
    data: $(this).serialize()
  });
  return false;
});
");

?>
<div id="subtitulo_proceso">
    <h3>PROCESO DE SELECCIÓN: <?php echo $proceso->nombre; ?> Candidato(s) Elegido(s)</h3>
</div>

<div class="item">   
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >No. de Proceso: "<?php echo $proceso->ofertaid; ?>"</th> 
  </tr>
    </table>
</div>

<p><?php echo 'NOTA: En esta página podrá visualizar la información consolidada de reportes y evaluaciones del (los) candidato(s) a contratar. Si desea visualizar o agregar más documentos diríjase al bloque de documentos adjuntos, luego haga click en el botón "Enviar Documentos".'; ?></p>

<div class="item">   
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >CANDIDATOS ASIGNADOS AL PROCESO</strong></th> 
        </tr>            
        <tr>
            <th>Estado</th>
            <th>Cantidad</th>        
        </tr>
        <tr>
            <td width="50%" style="text-transform: uppercase; color: #0066A4; background-color: #EFFDFF; font-size: 11px;">Candidatos elegidos:</td> 
            <td style="color: #0066A4; background-color: #EFFDFF; font-size: 11px;"><?php echo $conteo_estado[4]; ?></td> 
        </tr>         
    </table>
</div>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
  'model'=>$candidatos,
)); ?>
</div><!-- search-form -->
<br/>
<br/>
<br/>
<?php $form=$this->beginWidget('CActiveForm', array(
//  'id'=>'candidatos-form',    
    'enableAjaxValidation'=>true,
)); ?>

<?php echo CHtml::ajaxSubmitButton('Filter',array('candidatos/ajaxEnviarDocumentos'), array(),array("style"=>"display:none;")); ?>
<?php echo CHtml::ajaxSubmitButton('Enviar Documentos',array('candidatos/ajaxEnviarDocumentos','act'=>'doEnviarDocumentos','idP'=>$proceso->id), array('success'=>'reloadGrid'), array('submit' => array('Candidatos/ajaxEnviarDocumentos','act'=>'doEnviarDocumentos','idP'=>$proceso->id))); ?>


<?php //$this->widget('application.extensions.FlujoSeleccionGridView', array(
        $this->widget('zii.widgets.grid.CGridView', array(
  'id'=>'candidatos-grid',
  'dataProvider'=>$candidatos->searchFlujoSeleccion($proceso->id, $flujo), //AND (estado=1 OR estado=3)
  'filter'=>$candidatos,
        //'itemsCssClass'=>'gridtablecss',    
  'columns'=>array(
//    'nombres',
//                'foto',
                array(
                    'id'=>'idCandidato',
                    'class'=>'CCheckBoxColumn',
                    'selectableRows' => '50', 
//                    'value'          => 'CHtml::encode($data->idCandidato)',
                    'checkBoxHtmlOptions' => array('name' => 'idCandidatos[]'),
                    //'htmlOptions'=>array('style'=>'width: 150px;'),                    
                ),                  
                array ( 
                    'name'=>'foto',
                    'value'=>'CHtml::image(Yii::app()->baseurl."/verFoto.php?idc=".$data->idCandidato, "foto", array("idc"=>$data->idCandidato, "width"=>80, "height"=>110, "style"=>"border:#626262 solid 1px;float:left;"))', 
                    'type'=>'raw',),          
                array ( 
                    'header'=>'Nombre completo',
                    'value'=>'CHtml::link(CHtml::encode($data->nombres." ".$data->apellidos),Yii::app()->baseurl."/index.php?r=Candidatos/index&id=".$data->idCandidato, array("id"=>$data->idCandidato))',  
                    'type'=>'raw',),               
//    'apellidos',
                'identificacion',
//                'sueldoAspirado',
                array(
                    'name'=>'sueldoAspirado', 
//                    'type'=>'number',
                    'value'=>'"$ ".Yii::app()->numberFormatter->formatCurrency($data->sueldoAspirado, " ")',),
                'fechaInsert',
                array (
                        'header'=>'Estado',
                        'value'=>'CHtml::encode( $data->estado==4?"Elegido":($data->estado==5?"Contratado":" "))', 
                        'type'=>'raw',
                        'htmlOptions'=>array('style'=>'color: #0066A4; font-weight: bold;'),
                    ),            
  ),
));             

?>

<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('candidatos-grid');
//    $.fn.yiiGridView.update('candidatos-grid', {
//    data: $(this).serialize()
//  });
//          return false;
}
</script>
<?php $this->endWidget(); ?>
            