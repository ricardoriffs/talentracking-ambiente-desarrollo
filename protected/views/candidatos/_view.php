<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCandidato')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idCandidato), array('view', 'id'=>$data->idCandidato)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellidos')); ?>:</b>
	<?php echo CHtml::encode($data->apellidos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombres')); ?>:</b>
	<?php echo CHtml::encode($data->nombres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('identificacion')); ?>:</b>
	<?php echo CHtml::encode($data->identificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('edad')); ?>:</b>
	<?php echo CHtml::encode($data->edad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ultimoCargo')); ?>:</b>
	<?php echo CHtml::encode($data->ultimoCargo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aniosProfesional')); ?>:</b>
	<?php echo CHtml::encode($data->aniosProfesional); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('aniosRequeridos')); ?>:</b>
	<?php echo CHtml::encode($data->aniosRequeridos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aniosExperienciaSector')); ?>:</b>
	<?php echo CHtml::encode($data->aniosExperienciaSector); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sueldoActual')); ?>:</b>
	<?php echo CHtml::encode($data->sueldoActual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sueldoAspirado')); ?>:</b>
	<?php echo CHtml::encode($data->sueldoAspirado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('familiarEnEmpresa')); ?>:</b>
	<?php echo CHtml::encode($data->familiarEnEmpresa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto')); ?>:</b>
	<?php echo CHtml::encode($data->foto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreCargo')); ?>:</b>
	<?php echo CHtml::encode($data->nombreCargo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaInsert')); ?>:</b>
	<?php echo CHtml::encode($data->fechaInsert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_proceso')); ?>:</b>
	<?php echo CHtml::encode($data->id_proceso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_primaria')); ?>:</b>
	<?php echo CHtml::encode($data->ne_primaria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_secundaria')); ?>:</b>
	<?php echo CHtml::encode($data->ne_secundaria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_tecnico')); ?>:</b>
	<?php echo CHtml::encode($data->ne_tecnico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_tecnologo')); ?>:</b>
	<?php echo CHtml::encode($data->ne_tecnologo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_profecional')); ?>:</b>
	<?php echo CHtml::encode($data->ne_profecional); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_especializacion')); ?>:</b>
	<?php echo CHtml::encode($data->ne_especializacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_maestria')); ?>:</b>
	<?php echo CHtml::encode($data->ne_maestria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ne_doctorado')); ?>:</b>
	<?php echo CHtml::encode($data->ne_doctorado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ingles_lectura')); ?>:</b>
	<?php echo CHtml::encode($data->ingles_lectura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ingles_escritura')); ?>:</b>
	<?php echo CHtml::encode($data->ingles_escritura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ingles_hablado')); ?>:</b>
	<?php echo CHtml::encode($data->ingles_hablado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('so_ninguno')); ?>:</b>
	<?php echo CHtml::encode($data->so_ninguno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('so_sap')); ?>:</b>
	<?php echo CHtml::encode($data->so_sap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('so_jdedwar')); ?>:</b>
	<?php echo CHtml::encode($data->so_jdedwar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('so_oracle')); ?>:</b>
	<?php echo CHtml::encode($data->so_oracle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('so_otro')); ?>:</b>
	<?php echo CHtml::encode($data->so_otro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('teloficina')); ?>:</b>
	<?php echo CHtml::encode($data->teloficina); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('teloficinaext')); ?>:</b>
	<?php echo CHtml::encode($data->teloficinaext); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telcasa')); ?>:</b>
	<?php echo CHtml::encode($data->telcasa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telcel1')); ?>:</b>
	<?php echo CHtml::encode($data->telcel1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telcel2')); ?>:</b>
	<?php echo CHtml::encode($data->telcel2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail1')); ?>:</b>
	<?php echo CHtml::encode($data->mail1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail2')); ?>:</b>
	<?php echo CHtml::encode($data->mail2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipocontrato')); ?>:</b>
	<?php echo CHtml::encode($data->tipocontrato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hojavida')); ?>:</b>
	<?php echo CHtml::encode($data->hojavida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('formaprobacion')); ?>:</b>
	<?php echo CHtml::encode($data->formaprobacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom_familiar')); ?>:</b>
	<?php echo CHtml::encode($data->nom_familiar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom_referenciado')); ?>:</b>
	<?php echo CHtml::encode($data->nom_referenciado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	*/ ?>

</div>