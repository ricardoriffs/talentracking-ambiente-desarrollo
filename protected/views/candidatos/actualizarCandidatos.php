<?php
$this->breadcrumbs=array(
	'Candidatoses'=>array('index'),
	$model->idCandidato=>array('view','id'=>$model->idCandidato),
	'Update',
);

?>

<h1>Actualizar registro de Candidato</h1>

<?php echo $this->renderPartial('_registrarCandidatos', array('model'=>$model)); ?>
