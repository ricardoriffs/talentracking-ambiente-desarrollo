<?php
$this->breadcrumbs=array(
	'Candidatoses',
);

$this->menu=array(
	array('label'=>'Create Candidatos', 'url'=>array('create')),
	array('label'=>'Manage Candidatos', 'url'=>array('admin')),
);
?>

<h1>Candidatoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
