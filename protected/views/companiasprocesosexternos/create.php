<?php
/* @var $this CompaniasprocesosexternosController */
/* @var $model Companiasprocesosexternos */

$this->breadcrumbs=array(
	'Companiasprocesosexternoses'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Companiasprocesosexternos', 'url'=>array('index')),
	array('label'=>'Administrar Compañias Procesos externos', 'url'=>array('admin')),
);
?>

<h1>Crear Compañia Procesos externos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>