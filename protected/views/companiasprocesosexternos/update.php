<?php
/* @var $this CompaniasprocesosexternosController */
/* @var $model Companiasprocesosexternos */

$this->breadcrumbs=array(
	'Companiasprocesosexternoses'=>array('index'),
	$model->CompaniasProcesosExternosID=>array('view','id'=>$model->CompaniasProcesosExternosID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Companiasprocesosexternos', 'url'=>array('index')),
//	array('label'=>'Create Companiasprocesosexternos', 'url'=>array('create')),
	array('label'=>'Ver Compañia Procesos externos', 'url'=>array('view', 'id'=>$model->CompaniasProcesosExternosID)),
	array('label'=>'Administrar Compañias Procesos externos', 'url'=>array('admin')),
);
?>

<h1>Editar Compañia Procesos externos "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>