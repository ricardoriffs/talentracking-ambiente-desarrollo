<?php
/* @var $this CompaniasprocesosexternosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Companiasprocesosexternoses',
);

$this->menu=array(
	array('label'=>'Create Companiasprocesosexternos', 'url'=>array('create')),
	array('label'=>'Manage Companiasprocesosexternos', 'url'=>array('admin')),
);
?>

<h1>Companiasprocesosexternoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
