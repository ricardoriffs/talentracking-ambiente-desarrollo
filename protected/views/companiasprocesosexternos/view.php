<?php
/* @var $this CompaniasprocesosexternosController */
/* @var $model Companiasprocesosexternos */

$this->breadcrumbs=array(
	'Companiasprocesosexternoses'=>array('index'),
	$model->CompaniasProcesosExternosID,
);

$this->menu=array(
//	array('label'=>'List Companiasprocesosexternos', 'url'=>array('index')),
//	array('label'=>'Create Companiasprocesosexternos', 'url'=>array('create')),
	array('label'=>'Editar Compañia Procesos externos', 'url'=>array('update', 'id'=>$model->CompaniasProcesosExternosID)),
	array('label'=>'Borrar Compañia Procesos externos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->CompaniasProcesosExternosID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Compañias Procesos externos', 'url'=>array('admin')),
);
?>

<h1>Ver Compañia Procesos externos "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'CompaniasProcesosExternosID',
		'id',
//		'CompaniaID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
