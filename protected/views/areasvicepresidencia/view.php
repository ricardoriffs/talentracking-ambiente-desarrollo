<?php
/* @var $this AreasvicepresidenciaController */
/* @var $model Areasvicepresidencia */

$this->breadcrumbs=array(
	'Areasvicepresidencias'=>array('index'),
	$model->AreaVicepresidenciaID,
);

$this->menu=array(
//	array('label'=>'List Areasvicepresidencia', 'url'=>array('index')),
//	array('label'=>'Create Areasvicepresidencia', 'url'=>array('create')),
	array('label'=>'Editar Área Vicepresidencia', 'url'=>array('update', 'id'=>$model->AreaVicepresidenciaID)),
	array('label'=>'Borrar Área Vicepresidencia', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->AreaVicepresidenciaID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Áreas Vicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Ver Área Vicepresidencia "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'AreaVicepresidenciaID',
                array(
                        'name'=>'Vice Presidencia',
                        'value'=>CHtml::encode($model->vicePresidencia->Nombre),
                ),             
//		'CompaniaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ),  
	),
)); ?>
