<?php
/* @var $this AreasvicepresidenciaController */
/* @var $model Areasvicepresidencia */

$this->breadcrumbs=array(
	'Areasvicepresidencias'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Areasvicepresidencia', 'url'=>array('index')),
	array('label'=>'Administrar Áreas Vicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Crear Áreas Vicepresidencia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'compania'=>$compania)); ?>