<?php
/* @var $this AreasvicepresidenciaController */
/* @var $model Areasvicepresidencia */

$this->breadcrumbs=array(
	'Areasvicepresidencias'=>array('index'),
	$model->AreaVicepresidenciaID=>array('view','id'=>$model->AreaVicepresidenciaID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Areasvicepresidencia', 'url'=>array('index')),
//	array('label'=>'Create Areasvicepresidencia', 'url'=>array('create')),
	array('label'=>'Ver Área Vicepresidencia', 'url'=>array('view', 'id'=>$model->AreaVicepresidenciaID)),
	array('label'=>'Administrar Áreas Vicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Editar Área Vicepresidencia "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'compania'=>$compania)); ?>