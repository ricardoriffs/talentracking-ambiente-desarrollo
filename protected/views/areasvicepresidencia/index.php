<?php
/* @var $this AreasvicepresidenciaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Areasvicepresidencias',
);

$this->menu=array(
	array('label'=>'Create Areasvicepresidencia', 'url'=>array('create')),
	array('label'=>'Manage Areasvicepresidencia', 'url'=>array('admin')),
);
?>

<h1>Areasvicepresidencias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
