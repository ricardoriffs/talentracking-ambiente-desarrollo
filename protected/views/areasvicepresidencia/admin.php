<?php
/* @var $this AreasvicepresidenciaController */
/* @var $model Areasvicepresidencia */

$this->breadcrumbs=array(
	'Areasvicepresidencias'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List Areasvicepresidencia', 'url'=>array('index')),
	array('label'=>'Crear Área Vicepresidencia', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#areasvicepresidencia-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Áreas Vicepresidencias</h1>

<p>
Opcionalmente usted puede ingresar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de los valores de búsqueda para especificar parametros de búsqueda mas exactos.
</p>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
	'compania'=>$compania,    
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'areasvicepresidencia-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'AreaVicepresidenciaID',
//		'VicePresidenciaID',
                array ('name'=>'VicePresidenciaID','value'=>'$data->vicePresidencia->Nombre','type'=>'text',),            
//		'CompaniaID',
		'Nombre',
		'Descripcion',
                array ('name'=>'UsuarioCreacion','value'=>'$data->usuarioCreacion->username','type'=>'text',),            
		/*
		'UsuarioCreacion',
		'FechaModificacion',
		'UsuarioModificacion',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
