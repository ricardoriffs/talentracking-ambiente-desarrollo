<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuario-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con asterisco <span class="required">(*)</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->labelEx($model,'mail'); ?>
		<?php //echo $form->textField($model,'mail',array('size'=>45,'maxlength'=>45)); ?>
		<?php //echo $form->error($model,'mail'); ?>
	</div>-->

	<div class="row">
		<?php //echo $form->labelEx($model,'fechacreacion'); ?>
		<?php echo $form->hiddenField($model,'fechacreacion'); ?>
		<?php echo $form->error($model,'fechacreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'usuariocreacion'); ?>
		<?php echo $form->hiddenField($model,'usuariocreacion',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'usuariocreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombrecompleto'); ?>
		<?php echo $form->textField($model,'nombrecompleto',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'nombrecompleto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cargo'); ?>
		<?php echo $form->textField($model,'cargo',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'cargo'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'rol'); ?>
		<?php echo $form->dropdownlist(
                            $model,
                            'rol',
                            array('admincompania' => 'admincompania','gerente1' => 'gerente1','gerente2' => 'gerente2','coordinador' => 'coordinador','coordinador1' => 'coordinador1','coordinador2' => 'coordinador2','liderseleccion' => 'liderseleccion','clienteinterno' => 'clienteinterno'),
                            array('empty'=>'-- Seleccione una opción --')
                        ); 
                ?>
		<?php echo $form->error($model,'rol'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'CompaniaID'); ?>
		<?php echo $form->dropdownlist(
                            $model,
                            'CompaniaID',
                            CHtml::listData(Companias::model()->findAll(), 'CompaniaID', 'Nombre'),
                            array('empty'=>'-- Seleccione una opción --')
                        ); 
                ?>
		<?php echo $form->error($model,'CompaniaID'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'usuariobloqueado'); ?>
                <?php echo $form->checkBox($model, 'usuariobloqueado', array('value'=>'0', 'uncheckValue'=>1, 'checked'=>'checked')); ?>
		<?php echo $form->error($model,'usuariobloqueado'); ?>
	</div>        

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->