<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<!--	<div class="row">
		<?php //echo $form->label($model,'id'); ?>
		<?php //echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'mail'); ?>
		<?php //echo $form->textField($model,'mail',array('size'=>45,'maxlength'=>45)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'fechacreacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'fechacreacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)   
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuariocreacion'); ?>
		<?php echo $form->textField($model,'usuariocreacion',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuariobloqueado'); ?>
                <?php echo $form->checkBox($model, 'usuariobloqueado', array('value'=>'0', 'uncheckValue'=>1, 'checked'=>'checked')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombrecompleto'); ?>
		<?php echo $form->textField($model,'nombrecompleto',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cargo'); ?>
		<?php echo $form->textField($model,'cargo',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rol'); ?>
		<?php echo $form->dropdownlist(
                            $model,
                            'rol',
                            array('admin' => 'admin', 'admincompania' => 'admincompania','gerente1' => 'gerente1','gerente2' => 'gerente2','coordinador' => 'coordinador','coordinador1' => 'coordinador1','coordinador2' => 'coordinador2','liderseleccion' => 'liderseleccion','clienteinterno' => 'clienteinterno'),
                            array('empty'=>'-- Seleccione una opción --')
                        ); 
                ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CompaniaID'); ?>
                <?php 
                    echo $form->dropdownlist(
                            $model,
                            'CompaniaID',
                            CHtml::listData(Companias::model()->findAll(), 'CompaniaID', 'Nombre'),
                            array('empty'=>'-- Seleccione una opción --')
                            ); 
                ?>              
	</div> 

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->