<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List Usuario', 'url'=>array('index')),
//	array('label'=>'Create Usuario', 'url'=>array('create')),
	array('label'=>'Editar Usuario', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Usuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
);
?>

<h1>Ver Usuario "<?php echo $model->username; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'fechacreacion',
		'usuariocreacion',
		'usuariobloqueado',
		'nombrecompleto',
		'cargo',
                'rol',
                'admin',
                'primerAcceso', 	
                'fechaPrimerAcceso',
                array(
                        'name'=>'Compañia',
                        'value'=>CHtml::encode($model->compania->Nombre),
                ),                 
	),
)); ?>
