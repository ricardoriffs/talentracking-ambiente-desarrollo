<?php
$this->breadcrumbs=array(
	'Requisicion2s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Requisicion2', 'url'=>array('index')),
	array('label'=>'Manage Requisicion2', 'url'=>array('admin')),
);
?>

<h1>Create Requisicion2</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>