<?php
$this->breadcrumbs=array(
	'Requisicion2s'=>array('index'),
	'Manage',
);

//$this->menu=array(
//	array('label'=>'Lista Requisiciones', 'url'=>array('index')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicion2-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$id = $_GET['view'];


if ($id == 'subsubr4n1'){ 
	echo "<script language='JavaScript'>alert('Este proceso fue asignado al coordinador de seleccion para hacer seguimiento ingrese a Requisicion-Requisicion Aprobaciones - Coordinadores Seleccion');</script>";

}



?>

<h1>Administrador Requisiciones Lider de Seleccion</h1>

<p>
</p>

<?php echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicion2-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'RequisicionID',
                array ('name'=>'UnidadNegocioID','value'=>'$data->unidadnegocio->Nombre','type'=>'text',),
                array ('name'=>'AreaVicepresidenciaID','value'=>'$data->areasvicepresidencia->Nombre','type'=>'text', 'htmlOptions'=>array('width'=>240,'style'=>'font-size:8pt'), ),   
		'fechacreacion',                     
                'usuariocreacion', 
                'NombreCargo',  
		//'UnidadNegocioID',
		//'AutorizacionProceso',
		//'NombreAutoriza',
		//'EmailAutoriza',
		//'AreaVicepresidenciaID',
		/*
		'RutaArchivoAutorizacion',
		'NumeroVacantes',
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
		'LugarTrabajo',
		'RutaDescripcionCargo',
		'DescripcionCargoPar',
		'TurnoTrabajo',
		'TipoContratacionID',
		'TiempoContratacion',
		'AspectosImportantes',
		'Observaciones',
		'RangoSalario',
		'NivelHAY',
		'PuntosHAY',
		'TipoProcesoSeleccionID',
		'UsuarioIDResponsable',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}{update}',  
		),
	),
)); ?>