<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('RequisicionID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->RequisicionID), array('view', 'id'=>$data->RequisicionID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreCargo')); ?>:</b>
	<?php echo CHtml::encode($data->NombreCargo); ?>
	<br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('CargoSolicitante')); ?>:</b>
	<?php echo CHtml::encode($data->CargoSolicitante); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('UnidadNegocioID')); ?>:</b>
	<?php echo CHtml::encode($data->UnidadNegocioID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AutorizacionProceso')); ?>:</b>
	<?php echo CHtml::encode($data->AutorizacionProceso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreAutoriza')); ?>:</b>
	<?php echo CHtml::encode($data->NombreAutoriza); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EmailAutoriza')); ?>:</b>
	<?php echo CHtml::encode($data->EmailAutoriza); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AreaVicepresidenciaID')); ?>:</b>
	<?php echo CHtml::encode($data->AreaVicepresidenciaID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RutaArchivoAutorizacion')); ?>:</b>
	<?php echo CHtml::encode($data->RutaArchivoAutorizacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroVacantes')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroVacantes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreCargo')); ?>:</b>
	<?php echo CHtml::encode($data->NombreCargo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreSolicitante')); ?>:</b>
	<?php echo CHtml::encode($data->NombreSolicitante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CargoSolicitante')); ?>:</b>
	<?php echo CHtml::encode($data->CargoSolicitante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JefeInmediatoVacante')); ?>:</b>
	<?php echo CHtml::encode($data->JefeInmediatoVacante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LugarTrabajo')); ?>:</b>
	<?php echo CHtml::encode($data->LugarTrabajo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RutaDescripcionCargo')); ?>:</b>
	<?php echo CHtml::encode($data->RutaDescripcionCargo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DescripcionCargoPar')); ?>:</b>
	<?php echo CHtml::encode($data->DescripcionCargoPar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TurnoTrabajo')); ?>:</b>
	<?php echo CHtml::encode($data->TurnoTrabajo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoContratacionID')); ?>:</b>
	<?php echo CHtml::encode($data->TipoContratacionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TiempoContratacion')); ?>:</b>
	<?php echo CHtml::encode($data->TiempoContratacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AspectosImportantes')); ?>:</b>
	<?php echo CHtml::encode($data->AspectosImportantes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->Observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RangoSalario')); ?>:</b>
	<?php echo CHtml::encode($data->RangoSalario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NivelHAY')); ?>:</b>
	<?php echo CHtml::encode($data->NivelHAY); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PuntosHAY')); ?>:</b>
	<?php echo CHtml::encode($data->PuntosHAY); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoProcesoSeleccionID')); ?>:</b>
	<?php echo CHtml::encode($data->TipoProcesoSeleccionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioIDResponsable')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioIDResponsable); ?>
	<br />

	*/ ?>

</div>