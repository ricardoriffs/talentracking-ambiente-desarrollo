<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>

<?php
$this->breadcrumbs=array(
	'Requisicion2s'=>array('index'),
	$model->RequisicionID,
);

$this->menu=array(
//	array('label'=>'Lista Requisiciones', 'url'=>array('index')),
//	array('label'=>'Crear Requisicion', 'url'=>array('create')),
	array('label'=>'Actualizar Requisicion', 'url'=>array('update', 'id'=>$model->RequisicionID)),
	array('label'=>'Borrar Requisicion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->RequisicionID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Requisicion', 'url'=>array('admin')),
);
?>

<h1>Vista Requisicion Lider de Seleccion #<?php echo $model->RequisicionID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'RequisicionID',
		'unidadnegocio.Nombre',
		'AutorizacionProceso',
		//'NombreAutoriza',
		//'EmailAutoriza',
		array(
                                'name'=>'Area - Vicepresidencia',
                                'value'=>CHtml::encode($model->areavicepresidencia->Nombre),
                        ),
		//'RutaArchivoAutorizacion',
		'NumeroVacantes',
		'NombreCargo',
		'NombreSolicitante',
		'CargoSolicitante',
		'JefeInmediatoVacante',
		array(
                                'name'=>'Lugar de Trabajo',
                                'value'=>CHtml::encode($model->sedescompanias->Nombre),
                        ),
		//'RutaDescripcionCargo',
		'DescripcionCargoPar',
		'TurnoTrabajo',
		   array(
                                'name'=>'Tipo de Contracion',
                                'value'=>CHtml::encode($model->tipocontratacion->Nombre),
                        ),
		'TiempoContratacion',
		'AspectosImportantes',
		'Observaciones',
		 array(
                                'name'=>'si',
                                'value'=>CHtml::encode($model->si1.",".$model->si2.",".$model->si3),
                        ), 
                 array(
                                'name'=>'ss',
                                'value'=>CHtml::encode($model->ss1.",".$model->ss2.",".$model->ss3),
                        ),  

		//'NivelHAY',
		//'PuntosHAY',
                array(
                                'name'=>'Tipo Proceso Seleccion',
                                'value'=>CHtml::encode($model->tipoprocesoseleccion->Nombre),
                        ), 
		//'TipoProcesoSeleccionID',
		'UsuarioIDResponsable',
                array(
                                'name'=>'Coordinador Responsable',
                                'value'=>CHtml::encode($model->usuario->nombrecompleto),
                        ),   
	),
)); ?>

  <div class="row buttons">
            
            <?php
                    echo nl2br("\n"); 
			  echo nl2br("\n");  
                    if ($model->RequisicionID!= "") {?>
                       <a class="boton"  href=<?php echo 'index.php?r=requisicion2/crearejecucion&id=' . $model->RequisicionID.'>' ?> Asignar proceso a coordinador de seleccion </a>                        
                    
                 <?php   
                    echo nl2br("\n"); 
                    echo nl2br("\n"); 
                  ?>
                      <a class="boton"  href=<?php echo 'index.php?r=requisicion2/update&id=' . $model->RequisicionID.'>' ?> Editar Requisicion </a>                        
 
                   <?php     }
                    
                  ?>
             
         </div>  		
