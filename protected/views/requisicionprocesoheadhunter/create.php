<?php
$this->breadcrumbs=array(
	'Requisicionprocesoheadhunters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista Proc. Head Hunter', 'url'=>array('index')),
	array('label'=>'Administrar Proc. Head Hunter', 'url'=>array('admin')),
);
?>

<h1>Crear Proc. Head Hunter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>