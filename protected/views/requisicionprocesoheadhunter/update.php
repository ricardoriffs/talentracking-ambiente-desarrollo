<?php
$this->breadcrumbs=array(
	'Requisicionprocesoheadhunters'=>array('index'),
	$model->requisionprocesoID=>array('view','id'=>$model->requisionprocesoID),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista Proc.Headhunter', 'url'=>array('index')),
	array('label'=>'Crear Proc.Headhunter', 'url'=>array('create')),
	array('label'=>'Vista Proc.Headhunter', 'url'=>array('view', 'id'=>$model->requisionprocesoID)),
	array('label'=>'Administrar Proc.Headhunter', 'url'=>array('admin')),
);
?>

<h1>Actualizar Proc.Headhunter<?php echo $model->requisionprocesoID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>