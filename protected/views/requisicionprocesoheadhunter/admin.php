<?php
$this->breadcrumbs=array(
	'Requisicionprocesoheadhunters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Lista Proc. Head Hunter', 'url'=>array('index')),
	array('label'=>'Crear Proc. Head Hunter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicionprocesoheadhunter-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Procesos Head Hunter</h1>

<p>
</p>

<?php echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionprocesoheadhunter-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'requisionprocesoID',
		'RequisicionID',
		'datosofertacargo',
		'datosofertareportaa',
            'datosofertaunidadnegocio',
		/*
		'datosparapublicar',
		'TipoProcesoSeleccionID',
		'datosofertacargo',
		'datosofertaunidadnegocio',
		'datosofertareportaa',
		'datosofertamision',
		'datosofertaresponsabilidades',
		'datosofertaformacion',
		'datosofertaconocimientos',
		'datosofertacompetencias',
		'datosofertaexperiencia',
		'datosofertafechalimite',
		'datosafertaregistroaplicar',
		'ofertaid',
		'publicacionmedios',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
