<?php
$this->breadcrumbs=array(
	'Requisicionprocesoheadhunters',
);

$this->menu=array(
	array('label'=>'Crear Proc. Head Hunter', 'url'=>array('create')),
	array('label'=>'Administrar Proc. Head Hunter', 'url'=>array('admin')),
);
?>

<h1>Proc. Head Hunter</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
