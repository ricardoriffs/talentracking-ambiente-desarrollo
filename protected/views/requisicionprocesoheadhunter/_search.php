<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'requisionprocesoID'); ?>
		<?php echo $form->textField($model,'requisionprocesoID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RequisicionID'); ?>
		<?php echo $form->textField($model,'RequisicionID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'refiere'); ?>
		<?php echo $form->textField($model,'refiere',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emailrefiere'); ?>
		<?php echo $form->textField($model,'emailrefiere',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'referido'); ?>
		<?php echo $form->textField($model,'referido',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emailreferido'); ?>
		<?php echo $form->textField($model,'emailreferido',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosparapublicar'); ?>
		<?php echo $form->textField($model,'datosparapublicar',array('size'=>60,'maxlength'=>2000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoProcesoSeleccionID'); ?>
		<?php echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertacargo'); ?>
		<?php echo $form->textField($model,'datosofertacargo',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertaunidadnegocio'); ?>
		<?php echo $form->textField($model,'datosofertaunidadnegocio',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertareportaa'); ?>
		<?php echo $form->textField($model,'datosofertareportaa',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertamision'); ?>
		<?php echo $form->textField($model,'datosofertamision',array('size'=>60,'maxlength'=>2000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertaresponsabilidades'); ?>
		<?php echo $form->textField($model,'datosofertaresponsabilidades',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertaformacion'); ?>
		<?php echo $form->textField($model,'datosofertaformacion',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertaconocimientos'); ?>
		<?php echo $form->textField($model,'datosofertaconocimientos',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertacompetencias'); ?>
		<?php echo $form->textField($model,'datosofertacompetencias',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertaexperiencia'); ?>
		<?php echo $form->textField($model,'datosofertaexperiencia',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertafechalimite'); ?>
		<?php echo $form->textField($model,'datosofertafechalimite',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosafertaregistroaplicar'); ?>
		<?php echo $form->textField($model,'datosafertaregistroaplicar',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ofertaid'); ?>
		<?php echo $form->textField($model,'ofertaid',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'publicacionmedios'); ?>
		<?php echo $form->textField($model,'publicacionmedios',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->