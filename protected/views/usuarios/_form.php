<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuarios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario'); ?>
		<?php echo $form->textField($model,'usuario',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clave'); ?>
		<?php echo $form->textField($model,'clave',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'clave'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CambioRequeridoPassword'); ?>
		<?php echo $form->textField($model,'CambioRequeridoPassword'); ?>
		<?php echo $form->error($model,'CambioRequeridoPassword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IntentosErrados'); ?>
		<?php echo $form->textField($model,'IntentosErrados',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'IntentosErrados'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Bloqueado'); ?>
		<?php echo $form->textField($model,'Bloqueado'); ?>
		<?php echo $form->error($model,'Bloqueado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaUltimoLogeo'); ?>
		<?php echo $form->textField($model,'FechaUltimoLogeo'); ?>
		<?php echo $form->error($model,'FechaUltimoLogeo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaCreacion'); ?>
		<?php echo $form->textField($model,'FechaCreacion'); ?>
		<?php echo $form->error($model,'FechaCreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UsuarioCreacion'); ?>
		<?php echo $form->textField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioCreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaModificacion'); ?>
		<?php echo $form->textField($model,'FechaModificacion'); ?>
		<?php echo $form->error($model,'FechaModificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UsuarioModificacion'); ?>
		<?php echo $form->textField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioModificacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->