<?php
$this->breadcrumbs=array(
	'Usuarioses'=>array('index'),
	$model->usuarioID=>array('view','id'=>$model->usuarioID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Usuarios', 'url'=>array('index')),
	array('label'=>'Create Usuarios', 'url'=>array('create')),
	array('label'=>'View Usuarios', 'url'=>array('view', 'id'=>$model->usuarioID)),
	array('label'=>'Manage Usuarios', 'url'=>array('admin')),
);
?>

<h1>Update Usuarios <?php echo $model->usuarioID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>