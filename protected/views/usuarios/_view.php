<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuarioID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->usuarioID), array('view', 'id'=>$data->usuarioID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario')); ?>:</b>
	<?php echo CHtml::encode($data->usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clave')); ?>:</b>
	<?php echo CHtml::encode($data->clave); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CambioRequeridoPassword')); ?>:</b>
	<?php echo CHtml::encode($data->CambioRequeridoPassword); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IntentosErrados')); ?>:</b>
	<?php echo CHtml::encode($data->IntentosErrados); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Bloqueado')); ?>:</b>
	<?php echo CHtml::encode($data->Bloqueado); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaUltimoLogeo')); ?>:</b>
	<?php echo CHtml::encode($data->FechaUltimoLogeo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	*/ ?>

</div>