<?php
$this->breadcrumbs=array(
	'Unidadnegocios'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Unidadnegocio', 'url'=>array('index')),
	array('label'=>'Administrar Unidades de Negocio', 'url'=>array('admin')),
);
?>

<h1>Crear Unidad de Negocio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>