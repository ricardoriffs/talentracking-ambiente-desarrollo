<?php
$this->breadcrumbs=array(
	'Unidadnegocios'=>array('index'),
	$model->UnidadNegocioID,
);

$this->menu=array(
//	array('label'=>'List Unidadnegocio', 'url'=>array('index')),
//	array('label'=>'Create Unidadnegocio', 'url'=>array('create')),
	array('label'=>'Editar Unidad de Negocio', 'url'=>array('update', 'id'=>$model->UnidadNegocioID)),
	array('label'=>'Borrar Unidad de Negocio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->UnidadNegocioID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Unidades de Negocio', 'url'=>array('admin')),
);
?>

<h1>Ver Unidad de Negocio "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'UnidadNegocioID',
//		'CompaniaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),            
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ),   
            
	),
)); ?>
