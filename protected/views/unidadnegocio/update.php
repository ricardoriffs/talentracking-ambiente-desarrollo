<?php
$this->breadcrumbs=array(
	'Unidadnegocios'=>array('index'),
	$model->UnidadNegocioID=>array('view','id'=>$model->UnidadNegocioID),
	'Update',
);

$this->menu=array(
//	array('label'=>'Listar Unidades Neg.', 'url'=>array('index')),
//	array('label'=>'Crear Unidad de Negocio', 'url'=>array('create')),
	array('label'=>'Ver Unidad de Negocio', 'url'=>array('view', 'id'=>$model->UnidadNegocioID)),
	array('label'=>'Administrar Unidades de Negocio', 'url'=>array('admin')),
);
?>

<h1>Editar Unidad de Negocio "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>