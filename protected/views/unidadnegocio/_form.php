<style type="text/css">
<!--
.Estilo1 {
	color: #000033;
	font-weight: bold;
}
.Estilo2 {
	color: #000066;
	font-weight: bold;
}

a.boton {
   text-decoration: none;
   background: #EEE;
   color: #222;
   border: 1px outset #CCC;
   padding: .1em .5em;
}
a.boton:hover {
   background: #CCB;
}
a.boton:active {
   border: 1px inset #000;
}

-->
</style>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'unidadnegocio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con asterisco <span class="required">(*)</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
 <div class = "divContentBlue">   
   <div class="row">
	<!--<h2 align="center" class="Estilo2">DATOS GENERALES UNIDAD NEGOCIO	</h2>-->
        </div>
	<div class="row">
		<?php //echo $form->labelEx($model,'CompaniaID'); ?>
            <?php //echo $form->dropDownList($model,'CompaniaID', CHtml::listData(Companias::model()->findAll(), 'CompaniaID', 'Nombre'),array('length'=>350)); ?>  
		<?php echo $form->hiddenField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'CompaniaID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Descripcion'); ?>
		<?php echo $form->textArea($model,'Descripcion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Descripcion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'FechaCreacion'); ?>
		<?php echo $form->hiddenField($model,'FechaCreacion'); ?>
		<?php echo $form->error($model,'FechaCreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioCreacion'); ?>
		<?php echo $form->hiddenField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioCreacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'FechaModificacion'); ?>
		<?php echo $form->hiddenField($model,'FechaModificacion'); ?>
		<?php echo $form->error($model,'FechaModificacion'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UsuarioModificacion'); ?>
		<?php echo $form->hiddenField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'UsuarioModificacion'); ?>
	</div>
      <div class="row">
		<?php echo $form->labelEx($model,'UsernameApruebaRequisicion'); ?>
		<?php echo $form->textField($model,'UsernameApruebaRequisicion',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'UsernameApruebaRequisicion'); ?>
	</div>
                
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>
   </div>
<?php $this->endWidget(); ?>

</div><!-- form -->