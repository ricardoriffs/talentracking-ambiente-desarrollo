<?php
$this->breadcrumbs=array(
	'Unidadnegocios',
);

$this->menu=array(
	array('label'=>'Create Unidadnegocio', 'url'=>array('create')),
	array('label'=>'Manage Unidadnegocio', 'url'=>array('admin')),
);
?>

<h1>Unidadnegocios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
