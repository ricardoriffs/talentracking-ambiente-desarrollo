<?php
$this->breadcrumbs=array(
	'Unidadnegocios'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List Unidadnegocio', 'url'=>array('index')),
	array('label'=>'Crear Unidad de Negocio', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('unidadnegocio-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administración de Unidades de Negocio</h1>

<p>
Opcionalmente usted puede ingresar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de los valores de búsqueda para especificar parametros de búsqueda mas exactos.
</p>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'unidadnegocio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'UnidadNegocioID',
//		'CompaniaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array ('name'=>'UsuarioCreacion','value'=>'$data->usuarioCreacion->username','type'=>'text',),
		/*
		'FechaModificacion',
		'UsuarioModificacion',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
