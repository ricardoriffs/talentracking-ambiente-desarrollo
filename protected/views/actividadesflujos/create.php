<?php
/* @var $this ActividadesflujosController */
/* @var $model Actividadesflujos */

$this->breadcrumbs=array(
	'Actividadesflujoses'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Actividadesflujos', 'url'=>array('index')),
	array('label'=>'Administrar Actividades de flujos', 'url'=>array('admin')),
);
?>

<h1>Crear Actividades de flujos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>