<?php
/* @var $this ActividadesflujosController */
/* @var $model Actividadesflujos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<!--	<div class="row">
		<?php //echo $form->label($model,'ActividadFlujoID'); ?>
		<?php //echo $form->textField($model,'ActividadFlujoID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>50,'maxlength'=>50)); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'Descripcion'); ?>
		<?php //echo $form->textField($model,'Descripcion',array('size'=>60,'maxlength'=>100)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FlujoID'); ?>
                <?php 
                    echo $form->dropdownlist(
                            $model,
                            'FlujoID',
                            CHtml::listData(Flujos::model()->findAll(), 'FlujoID', 'Nombre'),
                            array('empty'=>'-- Seleccione una opción --')
                            ); 
                ?>          
	</div>

	<div class="row">
		<?php echo $form->label($model,'TiempoEstimado'); ?>
		<?php echo $form->textField($model,'TiempoEstimado',array('size'=>10,'maxlength'=>10)); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'RolIDResponsable'); ?>
		<?php //echo $form->textField($model,'RolIDResponsable',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'UsuarioIDResponsable'); ?>
		<?php //echo $form->textField($model,'UsuarioIDResponsable',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'Orden'); ?>
		<?php //echo $form->textField($model,'Orden',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

<!--	<div class="row">
		<?php //echo $form->label($model,'UsuarioCreacion'); ?>
		<?php //echo $form->textField($model,'UsuarioCreacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FechaCreacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaCreacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)            
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'UsuarioModificacion'); ?>
		<?php //echo $form->textField($model,'UsuarioModificacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row">
		<?php echo $form->label($model,'FechaModificacion'); ?>
                <?php $this->widget('application.extensions.timepicker.timepicker', array(
                       'model'=>$model,
                       'name'=>'FechaModificacion',
                     )); ?>              
                    &nbsp;(Formato de la fecha : AAAA-MM-DD)            
	</div>

<!--	<div class="row">
		<?php //echo $form->label($model,'CompaniaID'); ?>
		<?php //echo $form->textField($model,'CompaniaID',array('size'=>10,'maxlength'=>10)); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->