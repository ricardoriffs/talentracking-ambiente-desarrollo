<?php
/* @var $this ActividadesflujosController */
/* @var $data Actividadesflujos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ActividadFlujoID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ActividadFlujoID), array('view', 'id'=>$data->ActividadFlujoID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FlujoID')); ?>:</b>
	<?php echo CHtml::encode($data->FlujoID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TiempoEstimado')); ?>:</b>
	<?php echo CHtml::encode($data->TiempoEstimado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RolIDResponsable')); ?>:</b>
	<?php echo CHtml::encode($data->RolIDResponsable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioIDResponsable')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioIDResponsable); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Orden')); ?>:</b>
	<?php echo CHtml::encode($data->Orden); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsuarioModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->UsuarioModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaModificacion')); ?>:</b>
	<?php echo CHtml::encode($data->FechaModificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CompaniaID')); ?>:</b>
	<?php echo CHtml::encode($data->CompaniaID); ?>
	<br />

	*/ ?>

</div>