<?php
/* @var $this ActividadesflujosController */
/* @var $model Actividadesflujos */

$this->breadcrumbs=array(
	'Actividadesflujoses'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List Actividadesflujos', 'url'=>array('index')),
	array('label'=>'Crear Actividad de flujo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('actividadesflujos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administración de Actividades de Flujos</h1>

<p>
Opcionalmente usted puede ingresar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de los valores de búsqueda para especificar parametros de búsqueda mas exactos.
</p>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'actividadesflujos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'ActividadFlujoID',
		'Orden',            
		'Nombre',
		'Descripcion',
                array ('name'=>'FlujoID','value'=>'$data->flujo->Nombre','type'=>'text',),   
		'TiempoEstimado',
                array ('name'=>'CompaniaID','value'=>'$data->compania->Nombre','type'=>'text',), 
		/*'RolIDResponsable',
		'UsuarioIDResponsable',
		'UsuarioCreacion',
		'FechaCreacion',
		'UsuarioModificacion',
		'FechaModificacion',
		'CompaniaID',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
