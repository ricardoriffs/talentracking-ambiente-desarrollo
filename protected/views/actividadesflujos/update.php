<?php
/* @var $this ActividadesflujosController */
/* @var $model Actividadesflujos */

$this->breadcrumbs=array(
	'Actividadesflujoses'=>array('index'),
	$model->ActividadFlujoID=>array('view','id'=>$model->ActividadFlujoID),
	'Update',
);

$this->menu=array(
	//array('label'=>'List Actividadesflujos', 'url'=>array('index')),
	//array('label'=>'Create Actividadesflujos', 'url'=>array('create')),
	array('label'=>'Ver Actividad de flujo', 'url'=>array('view', 'id'=>$model->ActividadFlujoID)),
	array('label'=>'Administrar Actividades de flujos', 'url'=>array('admin')),
);
?>

<h1>Editar Actividad de flujo "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>