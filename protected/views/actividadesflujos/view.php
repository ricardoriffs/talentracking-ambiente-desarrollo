<?php
/* @var $this ActividadesflujosController */
/* @var $model Actividadesflujos */

$this->breadcrumbs=array(
	'Actividadesflujoses'=>array('index'),
	$model->ActividadFlujoID,
);

$this->menu=array(
	//array('label'=>'List Actividadesflujos', 'url'=>array('index')),
	//array('label'=>'Create Actividadesflujos', 'url'=>array('create')),
	array('label'=>'Editar Actividad de flujo', 'url'=>array('update', 'id'=>$model->ActividadFlujoID)),
	array('label'=>'Borrar Actividad de flujo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ActividadFlujoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Actividades de flujos', 'url'=>array('admin')),
);
?>

<h1>Ver Actividad de flujo "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'ActividadFlujoID',
		'Nombre',
		'Descripcion',
                array(
                        'name'=>'Flujo',
                        'value'=>CHtml::encode($model->flujo->Nombre),
                ),              
		'TiempoEstimado',
//		'RolIDResponsable',
//		'UsuarioIDResponsable',
                array(
                        'name'=>'Usuario Responsable',
                        'value'=>CHtml::encode($model->usuarioIDResponsable->username),
                ),                 
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
		'FechaCreacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ),  
		'FechaModificacion',
		'Orden',            
                array(
                        'name'=>'Compañia',
                        'value'=>CHtml::encode($model->compania->Nombre),
                ), 
	),
)); ?>
