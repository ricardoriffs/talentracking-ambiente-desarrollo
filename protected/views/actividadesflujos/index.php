<?php
/* @var $this ActividadesflujosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Actividadesflujoses',
);

$this->menu=array(
	array('label'=>'Create Actividadesflujos', 'url'=>array('create')),
	array('label'=>'Manage Actividadesflujos', 'url'=>array('admin')),
);
?>

<h1>Actividadesflujoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
