<?php
/* @var $this TurnoController */
/* @var $model Turno */

$this->breadcrumbs=array(
	'Turnos'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Turno', 'url'=>array('index')),
	array('label'=>'Administrar Turnos', 'url'=>array('admin')),
);
?>

<h1>Crear Turno</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>