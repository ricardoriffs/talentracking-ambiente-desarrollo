<?php
/* @var $this TurnoController */
/* @var $model Turno */

$this->breadcrumbs=array(
	'Turnos'=>array('index'),
	$model->TurnoID=>array('view','id'=>$model->TurnoID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Turno', 'url'=>array('index')),
//	array('label'=>'Create Turno', 'url'=>array('create')),
	array('label'=>'Ver Turno', 'url'=>array('view', 'id'=>$model->TurnoID)),
	array('label'=>'Administrar Turnos', 'url'=>array('admin')),
);
?>

<h1>Editar Turno "<?php echo $model->id; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>