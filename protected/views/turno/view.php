<?php
/* @var $this TurnoController */
/* @var $model Turno */

$this->breadcrumbs=array(
	'Turnos'=>array('index'),
	$model->TurnoID,
);

$this->menu=array(
//	array('label'=>'List Turno', 'url'=>array('index')),
//	array('label'=>'Create Turno', 'url'=>array('create')),
	array('label'=>'Editar Turno', 'url'=>array('update', 'id'=>$model->TurnoID)),
	array('label'=>'Borrar Turno', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TurnoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Turnos', 'url'=>array('admin')),
);
?>

<h1>Ver Turno "<?php echo $model->id; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'TurnoID',
		'id',
//		'CompaniaID',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ),  
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ), 
	),
)); ?>
