<?php
/* @var $this TipoprocesoseleccionController */
/* @var $model Tipoprocesoseleccion */

$this->breadcrumbs=array(
	'Tipoprocesoseleccions'=>array('index'),
	$model->TipoProcesoSeleccionID,
);

$this->menu=array(
//	array('label'=>'List Tipoprocesoseleccion', 'url'=>array('index')),
//	array('label'=>'Create Tipoprocesoseleccion', 'url'=>array('create')),
	array('label'=>'Editar Tipo de proceso de selección', 'url'=>array('update', 'id'=>$model->TipoProcesoSeleccionID)),
	array('label'=>'Borrar Tipo de proceso de selección', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TipoProcesoSeleccionID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Tipos de proceso de selección', 'url'=>array('admin')),
);
?>

<h1>Ver Tipo de proceso de selección "<?php echo $model->Nombre; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'TipoProcesoSeleccionID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array(
                        'name'=>'Usuario Creación',
                        'value'=>CHtml::encode($model->usuarioCreacion->username),
                ), 
//		'CompaniaID',
		'FechaModificacion',
                array(
                        'name'=>'Usuario Modificación',
                        'value'=>CHtml::encode($model->usuarioModificacion->username),
                ),  
	),
)); ?>
