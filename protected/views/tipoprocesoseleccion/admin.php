<?php
/* @var $this TipoprocesoseleccionController */
/* @var $model Tipoprocesoseleccion */

$this->breadcrumbs=array(
	'Tipoprocesoseleccions'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List Tipoprocesoseleccion', 'url'=>array('index')),
	array('label'=>'Crear Tipo de proceso de selección', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tipoprocesoseleccion-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administración de Tipos de proceso de selección</h1>

<p>
Opcionalmente usted puede ingresar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de los valores de búsqueda para especificar parametros de búsqueda mas exactos.
</p>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipoprocesoseleccion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'TipoProcesoSeleccionID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
                array ('name'=>'UsuarioCreacion','value'=>'$data->usuarioCreacion->username','type'=>'text',),
//		'CompaniaID',
		/*
		'FechaModificacion',
		'UsuarioModificacion',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
