<?php
/* @var $this TipoprocesoseleccionController */
/* @var $model Tipoprocesoseleccion */

$this->breadcrumbs=array(
	'Tipoprocesoseleccions'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Tipoprocesoseleccion', 'url'=>array('index')),
	array('label'=>'Administrar Tipos de proceso de selección', 'url'=>array('admin')),
);
?>

<h1>Crear Tipo de proceso de selección</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>