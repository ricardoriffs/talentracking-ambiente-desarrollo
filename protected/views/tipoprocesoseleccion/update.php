<?php
/* @var $this TipoprocesoseleccionController */
/* @var $model Tipoprocesoseleccion */

$this->breadcrumbs=array(
	'Tipoprocesoseleccions'=>array('index'),
	$model->TipoProcesoSeleccionID=>array('view','id'=>$model->TipoProcesoSeleccionID),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Tipoprocesoseleccion', 'url'=>array('index')),
//	array('label'=>'Create Tipoprocesoseleccion', 'url'=>array('create')),
	array('label'=>'Ver Tipo de proceso de selección', 'url'=>array('view', 'id'=>$model->TipoProcesoSeleccionID)),
	array('label'=>'Administrar Tipos de proceso de selección', 'url'=>array('admin')),
);
?>

<h1>Editar Tipo de proceso de selección "<?php echo $model->Nombre; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>