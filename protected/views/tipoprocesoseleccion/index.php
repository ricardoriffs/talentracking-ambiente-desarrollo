<?php
/* @var $this TipoprocesoseleccionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipoprocesoseleccions',
);

$this->menu=array(
	array('label'=>'Create Tipoprocesoseleccion', 'url'=>array('create')),
	array('label'=>'Manage Tipoprocesoseleccion', 'url'=>array('admin')),
);
?>

<h1>Tipoprocesoseleccions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
