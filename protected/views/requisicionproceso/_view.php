<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisionprocesoID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->requisionprocesoID), array('view', 'id'=>$data->requisionprocesoID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RequisicionID')); ?>:</b>
	<?php echo CHtml::encode($data->RequisicionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refiere')); ?>:</b>
	<?php echo CHtml::encode($data->refiere); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailrefiere')); ?>:</b>
	<?php echo CHtml::encode($data->emailrefiere); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referido')); ?>:</b>
	<?php echo CHtml::encode($data->referido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailreferido')); ?>:</b>
	<?php echo CHtml::encode($data->emailreferido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosparapublicar')); ?>:</b>
	<?php echo CHtml::encode($data->datosparapublicar); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoProcesoSeleccionID')); ?>:</b>
	<?php echo CHtml::encode($data->TipoProcesoSeleccionID); ?>
	<br />

	*/ ?>

</div>