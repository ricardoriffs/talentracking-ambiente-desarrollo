<?php
$this->breadcrumbs=array(
	'Requisicionprocesos'=>array('index'),
	$model->requisionprocesoID=>array('view','id'=>$model->requisionprocesoID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Requisicionproceso', 'url'=>array('index')),
	array('label'=>'Create Requisicionproceso', 'url'=>array('create')),
	array('label'=>'View Requisicionproceso', 'url'=>array('view', 'id'=>$model->requisionprocesoID)),
	array('label'=>'Manage Requisicionproceso', 'url'=>array('admin')),
);
?>

<h1>Update Requisicionproceso <?php echo $model->requisionprocesoID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>