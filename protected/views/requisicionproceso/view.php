<?php
$this->breadcrumbs=array(
	'Requisicionprocesos'=>array('index'),
	$model->requisionprocesoID,
);

$this->menu=array(
	array('label'=>'List Requisicionproceso', 'url'=>array('index')),
	array('label'=>'Create Requisicionproceso', 'url'=>array('create')),
	array('label'=>'Update Requisicionproceso', 'url'=>array('update', 'id'=>$model->requisionprocesoID)),
	array('label'=>'Delete Requisicionproceso', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->requisionprocesoID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Requisicionproceso', 'url'=>array('admin')),
);
?>

<h1>View Requisicionproceso #<?php echo $model->requisionprocesoID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'requisionprocesoID',
		'RequisicionID',
		'refiere',
		'emailrefiere',
		'referido',
		'emailreferido',
		'datosparapublicar',
		'TipoProcesoSeleccionID',
	),
)); ?>
