<?php
$this->breadcrumbs=array(
	'Requisicionprocesos',
);

$this->menu=array(
	array('label'=>'Create Requisicionproceso', 'url'=>array('create')),
	array('label'=>'Manage Requisicionproceso', 'url'=>array('admin')),
);
?>

<h1>Requisicionprocesos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
