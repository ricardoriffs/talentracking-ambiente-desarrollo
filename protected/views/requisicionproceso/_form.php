<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisicionproceso-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

  <div class="row">
		<?php echo $form->labelEx($model,'RequisicionID'); ?>
		<?php echo $form->textField($model,'RequisicionID',array('size'=>10,'maxlength'=>10,'disabled'=> true)); ?>
		<?php echo $form->error($model,'RequisicionID'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'refiere'); ?>
		<?php //echo $form->textField($model,'refiere',array('size'=>60,'maxlength'=>200)); ?>
		<?php //echo $form->error($model,'refiere'); ?>
	</div>

	<div class="row">
		<?php// echo $form->labelEx($model,'emailrefiere'); ?>
		<?php// echo $form->textField($model,'emailrefiere',array('size'=>60,'maxlength'=>100)); ?>
		<?php// echo $form->error($model,'emailrefiere'); ?>
	</div>

	<div class="row">
		<?php// echo $form->labelEx($model,'referido'); ?>
		<?php// echo $form->textField($model,'referido',array('size'=>60,'maxlength'=>200)); ?>
		<?php// echo $form->error($model,'referido'); ?>
	</div>

	<div class="row">
		<?php// echo $form->labelEx($model,'emailreferido'); ?>
		<?php// echo $form->textField($model,'emailreferido',array('size'=>60,'maxlength'=>100)); ?>
		<?php// echo $form->error($model,'emailreferido'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'TipoProcesoSeleccionID'); ?>
		<?php echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>10,'maxlength'=>10,'disabled'=> true)); ?>
		<?php echo $form->error($model,'TipoProcesoSeleccionID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertacargo'); ?>
		<?php echo $form->textField($model,'datosofertacargo',array('size'=>60,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'datosofertacargo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertaunidadnegocio'); ?>
		<?php echo $form->textField($model,'datosofertaunidadnegocio',array('size'=>60,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'datosofertaunidadnegocio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosofertareportaa'); ?>
		<?php echo $form->textField($model,'datosofertareportaa',array('size'=>60,'maxlength'=>100,'disabled'=> true)); ?>
		<?php echo $form->error($model,'datosofertareportaa'); ?>
	</div>
	



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->