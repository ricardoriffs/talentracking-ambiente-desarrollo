<?php
$this->breadcrumbs=array(
	'Requisicionprocesos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Requisicionproceso', 'url'=>array('index')),
	array('label'=>'Manage Requisicionproceso', 'url'=>array('admin')),
);
?>

<h1>Create Requisicionproceso</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>