<?php

function getStatus($id, $id2)
{
    $imagePath="images\administrator.png";
    $imagePath2="images\administrator1.png";
    $imagePath3="images\administrator2.png";
    $imagePath4="images\administrator3.png";
    $image = CHtml::image($imagePath,'Admin', array('title'=>'Administrar Formatos Job Posting')); 
    $image2 = CHtml::image($imagePath2,'Admin', array('title'=>'Administrar Formatos Evaluacion'));
    $image3 = CHtml::image($imagePath3,'Admin', array('title'=>'Administrar Head Hunter'));  
    $image4 = CHtml::image($imagePath4,'Admin', array('title'=>'Administrar Proc. Externos'));  
    $urlp = Yii::app()->request->baseUrl . 'requisicionjobposting/admin';
if ($id == 1)
    $content ="<div align='middle'>".CHtml::link($image,array('requisicionprocesojobposting/update&id='.$id2 ),array("target"=>"_blank")) ."</div>";
if ($id == 2)
    $content ="<div align='middle'>".CHtml::link($image2,array('requisicionprocesoeval/update&id='.$id2 ),array("target"=>"_blank")) ."</div>";
if ($id == 3)
    $content ="<div align='middle'>".CHtml::link($image3,array('requisicionprocesoheadhunter/update&id='.$id2 ),array("target"=>"_blank")) ."</div>";
if ($id == 4)
   $content ="<div align='middle'>".CHtml::link($image4,array('requisicionprocesoproexterno/update&id='.$id2 ),array("target"=>"_blank")) ."</div>";





    return $content;
}









$this->breadcrumbs=array(
	'Requisicionprocesos'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicionproceso-grid', {
		data: $(this).serialize()
	});
	return false;
});
");


  $id1 = $_GET['id1']; 
  $cp = $_GET['cp'];
  $v = $_GET['v']; 
  $fr = $_GET['fr']; 
  //echo $fr; 
  $id2 = 'M2';
  //echo $view;
  //$id = $view;
if ($id1 == 'ERROR'){ 
	echo "<script language='JavaScript'>alert('El registro asociado al proceso no existe o ha sido eliminado. Por favor contacte al administrador de la herramienta');</script>";

}

if ($id1 == 'MSJ'){ 
          
	echo "<script language='JavaScript'>var answer = confirm ('DESEA CERRAR EL PROCESO?');

              if (answer)
                     window.location = Yii::app()->baseurl.'/index.php?r=requisicionproceso/uptstatus&idP='.$data->ofertaid.'&idRP='.$data->requisionprocesoID.'&idR='.$data->RequisicionID .'&id1=M1';

</script>";

}

if ($cp == 'MSJ'){ 
          
	echo "<script language='JavaScript'>
            alert('Actualmente NO es posible cerrar este Proceso. Antes de cerrar el Proceso es necesario que usted agregue un comentario de tipo 5.CIERRE PROCESO en la Bitácora.');
        </script>";           
}



?>

<h1>Resumen Procesos Requisiciones </h1>

<p>
</p>

<?php echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); ?>
<?php echo nl2br("\n");?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 




if ($fr == 'G') { 
       $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionproceso-grid',
	'dataProvider'=>$model->search($v),
	'filter'=>$model,
	'selectionChanged'=>'gridselchange',
        'htmlOptions' => array('style'=>'width:880px;'),
      //'order'=>TipoProcesoSeleccionID,  
	'columns'=>array(
                array ('name'=>'TipoProcesoSeleccionID','value'=>'$data->tipoprocesoseleccion->Nombre','type'=>'text','htmlOptions'=>array('width'=>80,'style'=>'font-size:7pt')),      
		//array ('header'=>'TipoProcesoSeleccionID','value'=>'$data->tipoprocesoseleccion->Nombre','type'=>'text','htmlOptions'=>array('width'=>80,'style'=>'font-size:7pt')),   
                //'TipoProcesoSeleccionID',
                array ('name'=>'datosofertaunidadnegocio','value'=>'$data->unidadnegocio->Nombre','type'=>'text','htmlOptions'=>array('width'=>120,'style'=>'font-size:8pt')),               
                array ('name'=>'datosofertacargo','value'=>'$data->datosofertacargo','type'=>'text','htmlOptions'=>array('width'=>250,'style'=>'font-size:8pt')),                   
                //'datosofertacargo',
		'username',
                array ('name'=>'ofertaid','value'=>'R.$data->RequisicionID."-P".$data->requisionprocesoID','type'=>'text',), 
                //array ('name'=>'usuarioID','value'=>'$data->usuarioID','type'=>'text', 'htmlOptions'=>array('width'=>80,'st yle'=>'font-size:8pt') ),     
                array ('header'=>'fechaCreacion', 'name'=>'datosofertafechainicio','value'=>'$data->obtenerFechaCreacion($data->RequisicionID)','type'=>'text',),                   
                array (
                          'header'=>'Cerrar Proceso',
                          'value'=>'CHtml::link(CHtml::encode($data->estado == 0 ? "Cerrar Proc":($data->estado == 2 ? "Cerrar Proc" : "*")),Yii::app()->baseurl."/index.php?r=requisicionproceso/uptstatus&AC=C&idP=".$data->ofertaid."&idRP=".$data->requisionprocesoID."&idR=".$data->RequisicionID ."&id1=".$d2 ."&cp=1", array("id"=>_R.$data->RequisicionID."-P".$data->requisionprocesoID,"target"=>"_self", "readonly"=> "True", "style"=>"display:none"))', 
                          'type'=>'raw',),  
                array (
                          'header'=>'Congelar Proceso',
                          'value'=>'CHtml::link(CHtml::encode($data->estado == 0 ? "Congelar Proc":($data->estado == 2 ? "Abrir Proc" : "*")),Yii::app()->baseurl."/index.php?r=requisicionproceso/uptstatus&AC=F&idP=".$data->ofertaid."&idRP=".$data->requisionprocesoID."&idR=".$data->RequisicionID ."&id1=".$d2 , array("id"=>_R.$data->RequisicionID."-P".$data->requisionprocesoID,"target"=>"_self", ))', 
                          'type'=>'raw',),  
                /*array (
                          'header'=>'Abierto Cerrado',
                          'value'=>'CHtml::link(CHtml::encode($data->estado == 0 ? "Abierto" : ($data->estado == 1 ? "Cerrado" : "Congelado")),Yii::app()->baseurl."/index.php?r=requisicionproceso/uptstatus&idP=".$data->ofertaid."&idRP=".$data->requisionprocesoID."&idR=".$data->RequisicionID ."&id1=".$d2 , array("id"=>_R.$data->RequisicionID."-P".$data->requisionprocesoID,"target"=>"_self", ))', 
                          'type'=>'raw',),  */
                array ('header'=>'Abierto Cerrado','value'=>'($data->estado == 0 ? "Abierto" : ($data->estado == 1 ? "Cerrado" : "Congelado"))','type'=>'text','htmlOptions'=>array('width'=>250,'style'=>'font-size:8pt')),                    

                array (
                          'header'=>'(*)Estado',
                          'value'=>'CHtml::checkbox("UserEst_valide", $u["est_valide"], array("value"=>R.$data->RequisicionID."-P".$data->requisionprocesoID, "id"=>R.$data->RequisicionID."-P".$data->requisionprocesoID, "onChange" => "javascript:gridselchange(this.value)" ))', 
                          'type'=>'raw',),  
		//'estado',
                //'value'=>'$data->estado == 0 ? "Abierto" : "Cerrado"',


               //  CHtml::checkBox('UserEst_valide', $u['est_valide'],
               //                 array(
              //                 'ajax' => array(
                //                'type'=>'POST',
                //                'url'=>CController::createUrl('users/updateUserEst_valide')
                //                )));

                array(
                'header'=>'Requisicion ID',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode($data->RequisicionID),Yii::app()->baseurl."/index.php?r=requisicion2/update&id=".$data->RequisicionID ."&v=1&fr=G", array("target"=>"_blank"))',  
                ),

                array(
                'header'=>'Proceso ID',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode($data->ofertaid), Yii::app()->baseurl."/index.php?r=Candidatos/verProceso&idP=".$data->ofertaid."&APP=R", array("target"=>"_blank"))',  
                ),
                
                //array ('name'=>'ofertaid','value'=>'href="http://www.yiiframework.com/">Yii Framework</a>','type'=>'text',),                    
		//'emailrefiere',
		//'value' => 'CHtml::link(CHtml::encode($data->id),array("example/view","id"=>$data->id))',
            
		/*
		'datosparapublicar',
		'TipoProcesoSeleccionID',
                /index.php?r= 
		*/ 

           array(
                'class'=>'CDataColumn',
                'header'=>'Formatos',                                           
                'type'=>'Raw',
                'value'=>'getStatus("$data->TipoProcesoSeleccionID","$data->requisionprocesoID")',
            ),                     

          array(
                'header'=>'Comentarios',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode(Comentarios),Yii::app()->baseurl."/index.php?r=requisicionprocesocomentarios/admin&re=".$data->RequisicionID."&rp=".$data->requisionprocesoID, array("target"=>"_blank"))',  
                ),
	),
)); 

} else {

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionproceso-grid',
	'dataProvider'=>$model->search($v),
	'filter'=>$model,
	'selectionChanged'=>'gridselchange',
        'htmlOptions' => array('style'=>'width:880px;'),
      //'order'=>TipoProcesoSeleccionID,  
	'columns'=>array(
                array ('name'=>'TipoProcesoSeleccionID','value'=>'$data->tipoprocesoseleccion->Nombre','type'=>'text','htmlOptions'=>array('width'=>80,'style'=>'font-size:7pt')),      
		//array ('header'=>'TipoProcesoSeleccionID','value'=>'$data->tipoprocesoseleccion->Nombre','type'=>'text','htmlOptions'=>array('width'=>80,'style'=>'font-size:7pt')),   
                //'TipoProcesoSeleccionID',
                array ('name'=>'datosofertaunidadnegocio','value'=>'$data->unidadnegocio->Nombre','type'=>'text','htmlOptions'=>array('width'=>120,'style'=>'font-size:8pt')),               
                array ('name'=>'datosofertacargo','value'=>'$data->datosofertacargo','type'=>'text','htmlOptions'=>array('width'=>250,'style'=>'font-size:8pt')),                   
                //'datosofertacargo',
		'username',
                array ('name'=>'ofertaid','value'=>'R.$data->RequisicionID."-P".$data->requisionprocesoID','type'=>'text',), 
                //array ('name'=>'usuarioID','value'=>'$data->usuarioID','type'=>'text', 'htmlOptions'=>array('width'=>80,'st yle'=>'font-size:8pt') ),     
                array ('header'=>'fechaCreacion', 'name'=>'datosofertafechainicio','value'=>'$data->obtenerFechaCreacion($data->RequisicionID)','type'=>'text',),                                   
                array (
                          'header'=>'Cerrar Proceso',
                          'value'=>'CHtml::link(CHtml::encode($data->estado == 0 ? "Cerrar Proc":($data->estado == 2 ? "Cerrar Proc" : "*")),Yii::app()->baseurl."/index.php?r=requisicionproceso/uptstatus&AC=C&idP=".$data->ofertaid."&idRP=".$data->requisionprocesoID."&idR=".$data->RequisicionID ."&id1=".$d2 ."&cp=1", array("id"=>_R.$data->RequisicionID."-P".$data->requisionprocesoID,"target"=>"_self", "readonly"=> "True", "style"=>"display:none"))', 
                          'type'=>'raw',),  
                array (
                          'header'=>'Congelar Proceso',
                          'value'=>'CHtml::link(CHtml::encode($data->estado == 0 ? "Congelar Proc":($data->estado == 2 ? "Abrir Proc" : "*")),Yii::app()->baseurl."/index.php?r=requisicionproceso/uptstatus&AC=F&idP=".$data->ofertaid."&idRP=".$data->requisionprocesoID."&idR=".$data->RequisicionID ."&id1=".$d2 , array("id"=>_R.$data->RequisicionID."-P".$data->requisionprocesoID,"target"=>"_self", ))', 
                          'type'=>'raw',),  
                /*array (
                          'header'=>'Abierto Cerrado',
                          'value'=>'CHtml::link(CHtml::encode($data->estado == 0 ? "Abierto" : ($data->estado == 1 ? "Cerrado" : "Congelado")),Yii::app()->baseurl."/index.php?r=requisicionproceso/uptstatus&idP=".$data->ofertaid."&idRP=".$data->requisionprocesoID."&idR=".$data->RequisicionID ."&id1=".$d2 , array("id"=>_R.$data->RequisicionID."-P".$data->requisionprocesoID,"target"=>"_self", ))', 
                          'type'=>'raw',),  */
                array ('header'=>'Abierto Cerrado','value'=>'($data->estado == 0 ? "Abierto" : ($data->estado == 1 ? "Cerrado" : "Congelado"))','type'=>'text','htmlOptions'=>array('width'=>250,'style'=>'font-size:8pt')),                    

                array (
                          'header'=>'(*)Estado',
                          'value'=>'CHtml::checkbox("UserEst_valide", $u["est_valide"], array("value"=>R.$data->RequisicionID."-P".$data->requisionprocesoID, "id"=>R.$data->RequisicionID."-P".$data->requisionprocesoID, "onChange" => "javascript:gridselchange(this.value)" ))', 
                          'type'=>'raw',),  
		//'estado',
                //'value'=>'$data->estado == 0 ? "Abierto" : "Cerrado"',


               //  CHtml::checkBox('UserEst_valide', $u['est_valide'],
               //                 array(
              //                 'ajax' => array(
                //                'type'=>'POST',
                //                'url'=>CController::createUrl('users/updateUserEst_valide')
                //                )));

                array(
                'header'=>'Requisicion ID',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode($data->RequisicionID),Yii::app()->baseurl."/index.php?r=requisicion2/update&id=".$data->RequisicionID ."&v=1", array("target"=>"_blank"))',  
                ),

                array(
                'header'=>'Proceso ID',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode($data->ofertaid), Yii::app()->baseurl."/index.php?r=Candidatos/verProceso&idP=".$data->ofertaid."&APP=R")',                 
                ),
                
                //array ('name'=>'ofertaid','value'=>'href="http://www.yiiframework.com/">Yii Framework</a>','type'=>'text',),                    
		//'emailrefiere',
		//'value' => 'CHtml::link(CHtml::encode($data->id),array("example/view","id"=>$data->id))',
            
		/*
		'datosparapublicar',
		'TipoProcesoSeleccionID',
                /index.php?r= 
		*/ 

           array(
                'class'=>'CDataColumn',
                'header'=>'Formatos',                                           
                'type'=>'Raw',
                'value'=>'getStatus("$data->TipoProcesoSeleccionID","$data->requisionprocesoID")',
            ),                     

          array(
                'header'=>'Comentarios',
                'type'=>'raw',     
                'value'=>'CHtml::link(CHtml::encode(Comentarios),Yii::app()->baseurl."/index.php?r=requisicionprocesocomentarios/admin&re=".$data->RequisicionID."&rp=".$data->requisionprocesoID, array("target"=>"_blank"))',  
                ),
	),
)); 



}

echo "(*) Haga clic sobre la columna de ---Estado--- si desea cerrar un proceso";
?>
<script>
	
	function gridselchange(id) {
  var valor = id;
  var id_es = "_"+valor;
  if(document.getElementById(valor).checked)
   {//alert('seleccionado '+valor);
    document.getElementById(id_es).style.display = "";
   }
   else{//alert('no seleccionado');
		document.getElementById(id_es).style.display = "none";
		}  
}

</script>
