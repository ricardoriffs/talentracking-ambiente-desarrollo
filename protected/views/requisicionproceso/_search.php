<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php //echo $form->label($model,'requisionprocesoID'); ?>
		<?php //echo $form->textField($model,'requisionprocesoID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RequisicionID'); ?>
		<?php echo $form->textField($model,'RequisicionID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosofertacargo'); ?>
		<?php echo $form->textField($model,'datosofertacargo',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'emailrefiere'); ?>
		<?php //echo $form->textField($model,'emailrefiere',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'referido'); ?>
		<?php //echo $form->textField($model,'referido',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'emailreferido'); ?>
		<?php //echo $form->textField($model,'emailreferido',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php //echo $form->label($model,'datosparapublicar'); ?>
		<?php //echo $form->textField($model,'datosparapublicar',array('size'=>60,'maxlength'=>1500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoProcesoSeleccionID'); ?>
		<?php echo $form->textField($model,'TipoProcesoSeleccionID',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->