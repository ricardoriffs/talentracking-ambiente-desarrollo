<?php
/* @var $this RespuestasController */
/* @var $model Respuestas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'respuestas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idPregunta'); ?>
		<?php echo $form->textField($model,'idPregunta'); ?>
		<?php echo $form->error($model,'idPregunta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idOpcion'); ?>
		<?php echo $form->textField($model,'idOpcion'); ?>
		<?php echo $form->error($model,'idOpcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idCandidato'); ?>
		<?php echo $form->textField($model,'idCandidato'); ?>
		<?php echo $form->error($model,'idCandidato'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor'); ?>
		<?php echo $form->textField($model,'valor',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'valor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textArea($model,'observaciones',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaInsert'); ?>
		<?php echo $form->textField($model,'fechaInsert'); ?>
		<?php echo $form->error($model,'fechaInsert'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->