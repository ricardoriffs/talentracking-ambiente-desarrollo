<?php
/* @var $this RespuestasController */
/* @var $data Respuestas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idRespuesta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idRespuesta), array('view', 'id'=>$data->idRespuesta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPregunta')); ?>:</b>
	<?php echo CHtml::encode($data->idPregunta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idOpcion')); ?>:</b>
	<?php echo CHtml::encode($data->idOpcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCandidato')); ?>:</b>
	<?php echo CHtml::encode($data->idCandidato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor')); ?>:</b>
	<?php echo CHtml::encode($data->valor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaInsert')); ?>:</b>
	<?php echo CHtml::encode($data->fechaInsert); ?>
	<br />


</div>