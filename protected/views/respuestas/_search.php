<?php
/* @var $this RespuestasController */
/* @var $model Respuestas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idRespuesta'); ?>
		<?php echo $form->textField($model,'idRespuesta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idPregunta'); ?>
		<?php echo $form->textField($model,'idPregunta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idOpcion'); ?>
		<?php echo $form->textField($model,'idOpcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idCandidato'); ?>
		<?php echo $form->textField($model,'idCandidato'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor'); ?>
		<?php echo $form->textField($model,'valor',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'observaciones'); ?>
		<?php echo $form->textArea($model,'observaciones',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaInsert'); ?>
		<?php echo $form->textField($model,'fechaInsert'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->