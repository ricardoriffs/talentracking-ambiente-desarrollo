﻿
<?php
$cs=Yii::app()->clientScript;
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.calculation.min.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.format.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/template.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/common.js', CClientScript::POS_END);

Yii::app()->getComponent("bootstrap");

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'respuestas-form',
	'enableAjaxValidation'=>false,
)); ?>
    
<div id="subtitulo_proceso">
    <h3><?php echo $encuestas->nombre ?></h3>
</div>    
    
<div class="item">   
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >PROCESO DE SELECCIOŃ: <?php echo $proceso->nombre ?></th> 
	</tr>
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >NO. DE PROCESO: <?php echo $proceso->ofertaid ?></th> 
	</tr>        
    </table>
    <div style ="float: left; padding: 10px;">
        <img width="80" height="110" alt="foto" src="/3t/verFoto.php?idc=<?php echo $candidato->idCandidato ?>" style="border:#626262 solid 1px;float:left;" />
    </div>
    <div style="float: left; padding: 10px;">
        <table>
            <tr>
                <th>Nombres:</th>
                <td><?php echo $candidato->nombres ?></td>
            </tr>
            <tr>
                <th>Apellidos:</th>
                <td><?php echo $candidato->apellidos ?></td>
            </tr> 
            <tr>
                <th>Edad:</th>
                <td><?php echo $candidato->edad ?></td>
            </tr>           
        </table>
    </div> 
    <table style="float: none;">
        <tr>
            <th>Nombre del cargo que desempeña actualmente:</th>
            <td><?php echo $candidato->ultimoCargo ?></td>
            <th>Tipo de Vinculación :</th>
            <td><?php echo $candidato->tipocontrato ?></td>                
        </tr>
        <tr>
            <th>Total años de experiencia profesional:</th>
            <td><?php echo $candidato->aniosProfesional ?></td>
            <th>Salario actual (Incluye bonificaciones, beneficios, etc.):</th>
            <td><?php echo $candidato->sueldoActual ?></td>                
        </tr> 
        <tr>
            <th>Total años de experiencia en cargos similares:</th>
            <td><?php echo $candidato->aniosRequeridos ?></td>
            <th>Aspiración salarial:</th>
            <td><?php echo $candidato->sueldoAspirado ?></td>
        </tr>
        <tr>
            <th>Total años de experiencia en el sector:</th>
            <td><?php echo $candidato->aniosExperienciaSector ?></td>
            <th>Tiene familiares trabajando con nosotros:</th>
            <td><?php echo $candidato->nom_familiar ?></td>
        </tr>   
        <tr>
            <th>Nivel Educativo:</th>
            <td><?php if(!empty($candidato->ne_doctorado)){ echo 'Doctorado'; } elseif(!empty($candidato->ne_maestria)){ echo 'Maestría'; } elseif(!empty($candidato->ne_especializacion)){ echo 'Especialización'; } elseif(!empty($candidato->ne_profecional)){ echo 'Profesional'; } elseif(!empty($candidato->ne_tecnologo)){ echo 'Tecnólogo'; } elseif(!empty($candidato->ne_tecnico)){ echo 'Técnico'; } elseif(!empty($candidato->ne_secundaria)){ echo 'Secundaria'; } elseif(!empty($candidato->ne_primaria)){ echo 'Primaria'; } ?></td>
            <th></th>
            <td></td>
        </tr>             
    </table>
    <div class="grid-view" style="width: 850px;">
    <?php $k = 0; ?>
    <?php foreach($competencias as $competencia): ?>
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >COMPETENCIA No. <?php echo ++$k; ?></th> 
	</tr>        
    </table> 
    <?php $l = $k-1; ?>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th>COMPETENCIA</th>
            <th>DEFINICIÓN</th>
            <th>PREGUNTA SUGERIDA PARA VALIDAR LA COMPETENCIA</th>            
        </tr>
        <tr>
            <td><?php echo $competencia->idGrupo0->nombre ?></td>
            <td><?php echo $competencia->idGrupo0->descripcion ?></td>
            <td><?php echo $competencia->idGrupo0->preguntaSugerida ?></td>
        </tr>
    </table>    
    <table class="items">
        <tr style="color: #fff; background-color: #459acb; font-size: 12px;">
            <th width="35%">VALIDADORES DE LA COMPETENCIA</th>
            <?php foreach($opciones as $opcion): ?>   
                <th style="font-size: 10px;" width="7%"><?php echo $opcion->descripcion ?></th>
            <?php endforeach; ?>            
<!--            <th width="7%" style="font-size: 10px;">Total</th>-->
            <th width="30%">OBSERVACIONES</th>            
        </tr>
        <tr style="font-size: 12px;">
            <th width="35%"></th>
            <?php foreach($opciones as $opcion): ?>   
                <th style="font-size: 7px;" width="7%"><?php echo $opcion->texto ?></th>
            <?php endforeach; ?>            
<!--            <th width="7%" style="font-size: 10px;"></th>-->
            <th width="30%"></th>            
        </tr>
        <?php $preguntas = Preguntas::model()->findAll(array('condition'=> "idGrupo =" . $competencia->idGrupo ,'order' => 'orden ASC')) ?>
        <?php foreach ($preguntas as $i=>$pregunta): ?>
        <tr style="font-size: 12px;">
            <th width="35%" style="font-weight:normal;"><?php echo $pregunta->texto ?></th>
            <?php foreach($opciones as $opcion): ?>   

                <th style="font-size: 7px;" width="7%">
                    <?php 
                       echo $form->radioButton($model, "idOpcion[$l][$i]", array(
                           'value'=> $opcion->idOpcion,
                           'uncheckValue'=>NULL
                       ));  
                   ?>                     
                </th>              
            <?php endforeach; ?>            
<!--            <th width="7%" style="font-size: 10px;"></th>-->
            <th width="30%">
                <?php echo $form->textarea($model,"observaciones[$l][$i]",array('size'=>100,'maxlength'=>100, 'rows' => 2, 'cols' => 35)); ?>    
            </th>            
        </tr> 
        <?php endforeach; ?>
    </table>    
    <?php endforeach; ?>
        
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th bgcolor='#EFFDFF' width="75%"><font color='#0066A4' face='Arial' size='2' >¿CONSIDERA QUE EL CANDIDATO DEBE CONTINUAR EN EL PROCESO DE SELECCIÓN?:</th> 
            <td bgcolor='#EFFDFF'><font color='#0066A4' face='Arial' size='2' >SI
                <?php 
                echo $form->radioButton($model1, 'valoracion', array(
                    'value'=>'SI',
                    'id'=>'ResultadoEntrevista_valoracion_0',
                    'uncheckValue'=>null
                ));  
                ?>            
            </td>
            <td bgcolor='#EFFDFF'><font color='#0066A4' face='Arial' size='2' >NO
                <?php 
                echo $form->radioButton($model1, 'valoracion', array(
                    'value'=>'NO',
                    'id'=>'ResultadoEntrevista_valoracion_1',                    
                    'uncheckValue'=>null
                ));  
                ?>              
            </td>
	</tr>
    </table>
    <table>
        <tr>
            <th bgcolor='#EFFDFF'><font color='#0066A4' face='Arial' size='2' >Ingrese comentarios sobre la entrevista:</th> 
            <td bgcolor='#EFFDFF'><font color='#0066A4' face='Arial' size='2' >
		<?php echo $form->textArea($model1,'comentarios',array('size'=>500,'maxlength'=>500, 'rows' => 5, 'cols' => 100)); ?>
		<?php echo $form->error($model1,'comentarios'); ?>                
            </td>                
        </tr>
    </table>        
    </div>
</div>  
<div class="action">
        <?php echo CHtml::submitButton(Yii::t('ui','Guardar')); ?>
</div>       

<?php $this->endWidget(); ?>

</div><!-- form -->
