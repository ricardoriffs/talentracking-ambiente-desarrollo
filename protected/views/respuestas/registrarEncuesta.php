﻿
<?php
$cs=Yii::app()->clientScript;
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.calculation.min.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.format.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/template.js', CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/common.js', CClientScript::POS_END);

Yii::app()->getComponent("bootstrap");

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'respuestas-form',
	'enableAjaxValidation'=>false,
)); ?>
    
<div id="subtitulo_proceso">
    <h3><?php echo $encuestas->nombre ?></h3>
</div>    
    
<div class="item">   
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >PROCESO DE SELECCIOŃ: <?php echo $proceso->nombre ?></th> 
	</tr>
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >NO. DE PROCESO: <?php echo $proceso->ofertaid ?></th> 
	</tr>
    </table>    
    <div style ="float: left; padding: 10px;">
        <img width="80" height="110" alt="foto" src="/3t/verFoto.php?idc=<?php echo $candidato->idCandidato ?>" style="border:#626262 solid 1px;float:left;" />
    </div>
    <div style="float: left; padding: 10px;">
        <table>
            <tr>
                <th>Nombres:</th>
                <td><?php echo $candidato->nombres ?></td>
            </tr>
            <tr>
                <th>Apellidos:</th>
                <td><?php echo $candidato->apellidos ?></td>
            </tr> 
            <tr>
                <th>No. de Identificación:</th>
                <td><?php echo $candidato->identificacion ?></td>
            </tr>              
            <tr>
                <th>Edad:</th>
                <td><?php echo $candidato->edad ?></td>
            </tr>           
        </table>
    </div> 
    <p style="clear: both; padding-top: 20px;"><?php echo 'NOTA: '.$encuestas->descripcion ?></p>
    <div class="grid-view" style="width: 850px;">
        
        <?php foreach ($preguntas as $i=>$pregunta): ?>
    <table cellspacing="0" border="0" cellpadding="0" align="center">
        <tr>
            <th colspan="2" bgcolor='#EFFDFF'><strong><font color='#0066A4' face='Arial' size='2' >PREGUNTA No. <?php echo $i+1;  ?></th> 
	</tr>        
    </table>  
    <table>    
        <tr style="font-size: 14px;">
            <td style="font-weight:normal; padding: 0px;"><?php echo $pregunta->texto ?>
                <table>
                    <?php foreach($opciones[$i] as $opcion): ?>   
                    <tr>        
                        <td width="10%" style="font-weight:normal; padding: 0px 0px 0px 20px;">
                            <?php 
                            echo $form->radioButton($model, "idOpcion[$i]", array(
                                'value'=> $opcion->idOpcion,
                                'uncheckValue'=>NULL
                            )); 

                            ?> 
                        </td>
                        <td style="font-weight:normal; padding: 0px;"><?php echo $opcion->texto ?></td>                        
                    </tr>                          
                    <?php endforeach; ?>  
                </table>                                 
         
            </td>            
        </tr> 
    </table>  
        <?php endforeach; ?>                
             
    </div>
</div>  
<div class="action">
        <?php echo CHtml::submitButton(Yii::t('ui','Guardar')); ?>
</div>       

<?php $this->endWidget(); ?>

</div><!-- form -->
