<BR/>
<div id="subtitulo_proceso">
    <h3><?php echo 'ENCUESTAS DE EVALUACIÓN'?></h3>
</div> 
<div class="item">   
    <div style ="float: left; padding: 10px;">
        <img width="80" height="110" alt="foto" src="/3t/verFoto.php?idc=<?php echo $candidato->idCandidato ?>" style="border:#626262 solid 1px;float:left;" />
    </div>
    <div style="float: left; padding: 10px;">
        <table>
            <tr>
                <th>Nombres:</th>
                <td><?php echo $candidato->nombres ?></td>
            </tr>
            <tr>
                <th>Apellidos:</th>
                <td><?php echo $candidato->apellidos ?></td>
            </tr>
            <tr>
                <th>No. de Identificación:</th>
                <td><?php echo $candidato->identificacion ?></td>
            </tr>             
            <tr>
                <th>Edad:</th>
                <td><?php echo $candidato->edad ?></td>
            </tr>           
        </table>
    </div> 
    <p style="clear: both; padding-top: 20px;"><?php echo 'NOTA: A continuación se visualiza un listado de encuestas asignadas a usted, que evaluarán su perfil para el proceso de selección al cual se encuentra inscrito.' ?></p>
</div>    
<?php echo 'estado:'.$estado ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'respuestas-grid',
	'dataProvider'=>$encuestas->search($candidato->id_proceso),
	//'filter'=>$encuestas,
	'columns'=>array(
		'nombre',
		'descripcion',
                array ( 
                    'header'=>'Proceso',
                    'value'=>'CHtml::encode($data->idProceso0->ofertaid)',  
                    'type'=>'raw',),               
		'fechaInsert',
                array ( 
                    'header'=>'Estado',
                    'value'=>$estado,  
                    'type'=>'text',
                    'htmlOptions'=>array('style'=>'font-weight: bold;'),),             
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{responder}',
                        'buttons'=>array(
                            'responder' => array(
                                'label'=>'Responder',
                                //'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
                                'visible' => '('.$estado.'=="Sin_resolver" OR '.$estado.'=="Incompleta") ? true : false',
                                'url'=>'Yii::app()->createUrl("Respuestas/registrarEncuesta", array("idE"=>$data->idEncuesta))',
                            ),
                        ),                    
		),                      
	),
)); ?>
