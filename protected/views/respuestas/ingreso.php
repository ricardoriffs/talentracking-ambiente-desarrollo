<div class="form">

<h1>Control de Acceso a Encuestas</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'candidatos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Por favor ingrese su número de identificación para ver sus encuestas.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'identificacion'); ?>
		<?php echo $form->textField($model,'identificacion'); ?>
		<?php echo $form->error($model,'identificacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Acceder'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->        