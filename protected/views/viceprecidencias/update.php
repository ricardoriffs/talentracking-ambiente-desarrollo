<?php
$this->breadcrumbs=array(
	'Viceprecidenciases'=>array('index'),
	$model->VicePresidenciaID=>array('view','id'=>$model->VicePresidenciaID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Viceprecidencias', 'url'=>array('index')),
	array('label'=>'Create Viceprecidencias', 'url'=>array('create')),
	array('label'=>'View Viceprecidencias', 'url'=>array('view', 'id'=>$model->VicePresidenciaID)),
	array('label'=>'Manage Viceprecidencias', 'url'=>array('admin')),
);
?>

<h1>Update Viceprecidencias <?php echo $model->VicePresidenciaID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>