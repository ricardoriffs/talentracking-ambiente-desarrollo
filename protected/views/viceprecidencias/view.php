<?php
$this->breadcrumbs=array(
	'Viceprecidenciases'=>array('index'),
	$model->VicePresidenciaID,
);

$this->menu=array(
	array('label'=>'List Viceprecidencias', 'url'=>array('index')),
	array('label'=>'Create Viceprecidencias', 'url'=>array('create')),
	array('label'=>'Update Viceprecidencias', 'url'=>array('update', 'id'=>$model->VicePresidenciaID)),
	array('label'=>'Delete Viceprecidencias', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->VicePresidenciaID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Viceprecidencias', 'url'=>array('admin')),
);
?>

<h1>View Viceprecidencias #<?php echo $model->VicePresidenciaID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'VicePresidenciaID',
		'CompañiaID',
		'Nombre',
		'Descripcion',
		'FechaCreacion',
		'UsuarioCreacion',
		'FechaModificacion',
		'UsuarioModificacion',
	),
)); ?>
