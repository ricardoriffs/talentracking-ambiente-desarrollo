<?php
$this->breadcrumbs=array(
	'Viceprecidenciases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Viceprecidencias', 'url'=>array('index')),
	array('label'=>'Manage Viceprecidencias', 'url'=>array('admin')),
);
?>

<h1>Create Viceprecidencias</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>