<?php
$this->breadcrumbs=array(
	'Viceprecidenciases',
);

$this->menu=array(
	array('label'=>'Create Viceprecidencias', 'url'=>array('create')),
	array('label'=>'Manage Viceprecidencias', 'url'=>array('admin')),
);
?>

<h1>Viceprecidenciases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
