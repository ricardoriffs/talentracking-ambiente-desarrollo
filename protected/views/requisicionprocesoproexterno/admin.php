<?php
$this->breadcrumbs=array(
	'Requisicionprocesoproexternos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Lista Proc. Externos', 'url'=>array('index')),
	array('label'=>'Crear Proc. Externo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisicionprocesoproexterno-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrador Procesos Externos</h1>

<p>
</p>

<?php echo CHtml::link('Busquedas Avanzadas','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisicionprocesoproexterno-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'datosofertacargo',
		'RequisicionID',		
		'datosofertareportaa',
            'datosofertaunidadnegocio',
		/*
		'datosparapublicar',
		'TipoProcesoSeleccionID',
		'datosofertacargo',
		'datosofertaunidadnegocio',
		'datosofertareportaa',
		'datosofertamision',
		'datosofertaresponsabilidades',
		'datosofertaformacion',
		'datosofertaconocimientos',
		'datosofertacompetencias',
		'datosofertaexperiencia',
		'datosofertafechalimite',
		'datosafertaregistroaplicar',
		'ofertaid',
		'publicacionmedios',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
