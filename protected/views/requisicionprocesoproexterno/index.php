<?php
$this->breadcrumbs=array(
	'Requisicionprocesoproexternos',
);

$this->menu=array(
	array('label'=>'Crear Proc.Externo', 'url'=>array('create')),
	array('label'=>'Administrador Proc. Externo', 'url'=>array('admin')),
);
?>

<h1>Proc. Externo</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
