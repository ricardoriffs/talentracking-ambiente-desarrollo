<?php
$this->breadcrumbs=array(
	'Requisicionprocesoproexternos'=>array('index'),
	$model->requisionprocesoID=>array('view','id'=>$model->requisionprocesoID),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista Proc. Externo', 'url'=>array('index')),
	array('label'=>'Crear Proc. Externo', 'url'=>array('create')),
	array('label'=>'Vista Proc. Externo', 'url'=>array('view', 'id'=>$model->requisionprocesoID)),
	array('label'=>'Administrar Proc. Externo', 'url'=>array('admin')),
);
?>

<h1>Actualizar Procesos Externos<?php echo $model->requisionprocesoID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
