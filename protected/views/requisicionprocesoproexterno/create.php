<?php
$this->breadcrumbs=array(
	'Requisicionprocesoproexternos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista Proc. Externo', 'url'=>array('index')),
	array('label'=>'Administrador Proc. Externo', 'url'=>array('admin')),
);
?>

<h1>Crear Proc. Externo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>