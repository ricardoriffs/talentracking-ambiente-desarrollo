<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome to the <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Role Based Access Manager (RBAM) is a Yii module that provides complete management of Authorisation Data (Authorisation Items, Authorisation Hierarchy, and Authorisation Assignments) for Yii’s Role Based Access Control system via a browser interface; it is intended for use in development and end-user administration environments.</p>
<p>RBAM has an intuitive “Web 2.0” interface to easily manage Authorisation Items (Roles, Tasks, and Operations), their hierarchy, and Authorisation Assignments.  It presents all of an Authorisation Item’s information in one place providing a comprehensive overview and complete management of the item.</p>
<p>RBAM’s “Drill-down” and “Drill-up” features quickly show an item’s position in the Authorisation Hierarchy, what permissions it inherits (Drill down) and which Roles inherit its permissions (Drill up).</p>
<p>RBAM is built on top of Yii’s CAuthManager component and supports both of Yii’s built-in Authorisation Managers, CDbAuthManager and CPhpAuthManager, and authorisation managers extended from them.</p>
<?php if (Yii::app()->getUser()->getIsGuest()): ?>
<p>To use RBAM you must first <a href="<?php echo $this->createUrl('site/login'); ?>">login</a></p>
<?php else: ?>
<p><a href="<?php echo $this->createUrl('rbam/rbam/index'); ?>">Use RBAM</a><br/>
<b>Note: </b>This link is slightly different from the menu link. This link will take you to the RBAM home page. Because the demo is configured to initialise RBAC, the menu link will take you to the initialisation page.</p>
<?php endif; ?>
<p><a href="http://www.yiiframework.com/extension/rbam/">Download RBAM</a></p>
<p><a href="/documents/rbam_manual.pdf">The RBAM manual (PDF - 2.2MB)</a></p>
<p><a href="http://www.yiiframework.com/forum/index.php?/topic/14235-rbam-role-based-access-control-manager/">Comments, suggestions, &amp; bug reports</a></p>