<?php
return array (
  'RBAC Manager' => 
  array (
    'type' => 2,
    'description' => 'Manages Auth Items and Role Assignments. RBAM required role.',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'Auth Items Manager',
      1 => 'Auth Assignments Manager',
    ),
    'assignments' => 
    array (
      'admin' => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      1 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'Auth Items Manager' => 
  array (
    'type' => 2,
    'description' => 'Manages Auth Items. RBAM required role.',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Auth Assignments Manager' => 
  array (
    'type' => 2,
    'description' => 'Manages Role Assignments. RBAM required role.',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Authenticated' => 
  array (
    'type' => 2,
    'description' => 'Default role for users that are logged in. RBAC default role.',
    'bizRule' => 'return !Yii::app()->getUser()->getIsGuest();',
    'data' => NULL,
    'children' => 
    array (
      0 => 'Post Author',
    ),
  ),
  'Guest' => 
  array (
    'type' => 2,
    'description' => 'Default role for users that are not logged in. RBAC default role.',
    'bizRule' => 'return Yii::app()->getUser()->getIsGuest();',
    'data' => NULL,
  ),
  'RBAM Demo' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'Site',
    ),
  ),
  'Site' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'Site:Captcha',
      1 => 'Site:CCaptchaAction',
      2 => 'Site:Index',
      3 => 'Site:Error',
      4 => 'Site:Contact',
      5 => 'Site:Login',
      6 => 'Site:Logout',
    ),
  ),
  'Site:Captcha' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Site:CCaptchaAction' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Site:Index' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Site:Error' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Site:Contact' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Site:Login' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Site:Logout' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Post Author' => 
  array (
    'type' => '2',
    'description' => 'Writes and updates their own posts.
Because this role is a child of the Authenticated default role, all authenticated users are Post Authors and it is not necessary to explicitly assign this role to users.',
    'bizRule' => '',
    'data' => '',
    'children' => 
    array (
      0 => 'Create Post',
      1 => 'Edit Own Post',
    ),
  ),
  'Blog Editor' => 
  array (
    'type' => '2',
    'description' => 'Manages blog posts',
    'bizRule' => '',
    'data' => '',
    'children' => 
    array (
      0 => 'Manage Posts',
    ),
    'assignments' => 
    array (
      1 => 
      array (
        'bizRule' => 'return true;',
        'data' => NULL,
      ),
    ),
  ),
  'Create Post' => 
  array (
    'type' => '0',
    'description' => 'Authenticated users can create a new post',
    'bizRule' => '',
    'data' => '',
  ),
  'Delete Post' => 
  array (
    'type' => '0',
    'description' => 'Permanently remove a post',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Edit Own Post' => 
  array (
    'type' => '0',
    'description' => 'Users can only edit their own posts, and only if not approved',
    'bizRule' => 'return $params[\'post\']->authorId==Yii::app()->getUser()->id && !$params[\'post\']->approved;',
    'data' => NULL,
  ),
  'Approve Post' => 
  array (
    'type' => '0',
    'description' => 'Approve a post for publication',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'Manage Posts' => 
  array (
    'type' => '1',
    'description' => 'Manage posts',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'Disable Post',
      1 => 'Approve Post',
      2 => 'Delete Post',
    ),
  ),
  'Disable Post' => 
  array (
    'type' => '0',
    'description' => 'Remove, but don\'t delete, a post',
    'bizRule' => NULL,
    'data' => NULL,
  ),
);
