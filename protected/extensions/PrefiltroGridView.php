<?php

Yii::import('zii.widgets.grid.CGridView');

/**
* A Grid View that groups rows by any column(s)
*
* @category       User Interface
* @package        extensions
* @author         Vitaliy Potapov <noginsk@rambler.ru>
* @version        1.1
*/
class PrefiltroGridView extends CGridView {
    
	public function renderTableHeader()
	{

	}    
    
       public function renderTableBody(){
           
		$data=$this->dataProvider->getData();
		$n=count($data);
                
//                $this->renderFilter();
                        
		echo "<tbody>\n";

		if($n>0)
		{   
                        $this->renderTableRow($n);
                }
		else
		{
			echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
			$this->renderEmptyText();
			echo "</td></tr>\n";
		}
		echo "</tbody>\n";
        }
        
	public function renderTableRow($row){
 
            
		if($this->rowCssClassExpression!==null)
		{
			$data=$this->dataProvider->data[$row];
			$class=$this->evaluateExpression($this->rowCssClassExpression,array('row'=>$row,'data'=>$data));
		}
		else if(is_array($this->rowCssClass) && ($n=count($this->rowCssClass))>0)
			$class=$this->rowCssClass[$row%$n];
		else
			$class='';

		foreach($this->columns as $column){
//                        echo empty($class) ? '<tr>' : '<tr class="'.$class.'">'; 

                          echo empty($class) ? '<tr>' : '<tr>';
                          
                        $column->renderHeaderCell();
                                    
                        for($r=0;$r<$row;$r++){
                                $column->renderDataCell($r);
                        }                

                        echo "</tr>\n";                        
                }
	}    
    
    
}