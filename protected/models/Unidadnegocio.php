<?php

/**
 * This is the model class for table "unidadnegocio".
 *
 * The followings are the available columns in table 'unidadnegocio':
 * @property string $UnidadNegocioID
 * @property string $CompaniaID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 * @property string $UsernameApruebaRequisicion
 *
 * The followings are the available model relations:
 * @property Companias $compania
 * @property Usuario $usuarioCreacion
 * @property Usuario $usuarioModificacion
 */
class Unidadnegocio extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Unidadnegocio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unidadnegocio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, FechaCreacion, UsuarioCreacion', 'required'),
			array('CompaniaID, UsuarioCreacion, UsuarioModificacion', 'length', 'max'=>10),
			array('Nombre', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>100),
			array('UsernameApruebaRequisicion', 'length', 'max'=>128),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('UnidadNegocioID, CompaniaID, Nombre, Descripcion, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion, UsernameApruebaRequisicion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'UnidadNegocioID' => 'Unidad Negocio',
			'CompaniaID' => 'Compañia',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioCreacion' => 'Usuario Creación',
			'FechaModificacion' => 'Fecha Modificación',
			'UsuarioModificacion' => 'Usuario Modificación',
			'UsernameApruebaRequisicion' => 'Username Aprueba Requisición',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('UnidadNegocioID',$this->UnidadNegocioID,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);
		$criteria->compare('UsernameApruebaRequisicion',$this->UsernameApruebaRequisicion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}