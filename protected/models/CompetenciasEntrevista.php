<?php

/**
 * This is the model class for table "competenciasEntrevista".
 *
 * The followings are the available columns in table 'competenciasEntrevista':
 * @property integer $idCompetenciasEntrevista
 * @property integer $idGrupo
 * @property integer $idCompania
 *
 * The followings are the available model relations:
 * @property Grupospreguntas $idGrupo0
 */
class CompetenciasEntrevista extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompetenciasEntrevista the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'competenciasEntrevista';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idGrupo, idCompania', 'required'),
			array('idGrupo, idCompania', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCompetenciasEntrevista, idGrupo, idCompania', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idGrupo0' => array(self::BELONGS_TO, 'Grupospreguntas', 'idGrupo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCompetenciasEntrevista' => 'Id Competencias Entrevista',
			'idGrupo' => 'Competencia',
			'idCompania' => 'Compañia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCompetenciasEntrevista',$this->idCompetenciasEntrevista);
		$criteria->compare('idGrupo',$this->idGrupo);
		$criteria->compare('idCompania',$this->idCompania);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function afterValidate() {
                            
                $criteria = new CDbCriteria;
                $criteria->select = 'id, CompaniaID';
                $criteria->condition = 'username=:username';
                $criteria->params = array(':username' => Yii::app()->user->name);                        
                $usuario = Usuario::model()->find($criteria);                  
                
                $competenciasAsignadas = CompetenciasEntrevista::model()->findAll(array('condition'=> "idCompania =".$usuario->CompaniaID));
                $elecciongrupo = $this->idGrupo;
                $i = 0;
                
                foreach ($competenciasAsignadas as $competenciaAsignada){
                        if($competenciaAsignada->idGrupo==$elecciongrupo){
                            $this->addError('idGrupo', 'Usted ya ha elegido esta competencia, eliga una competencia que NO se encuentre asignada.');
                            return false;                        
                        }
                        $i++;
                }
                
                if($i>=3){
                        $this->addError('idGrupo', 'Solamente es posible asignar 3 competencias como máximo.');
                        return false;                       
                }                
                
                return parent::afterValidate();                
        }         
}