<?php

/**
 * This is the model class for table "companias".
 *
 * The followings are the available columns in table 'companias':
 * @property string $CompaniaID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FechaInicioVigencia
 * @property string $FechaFinVigencia
 * @property string $TipoServicioID
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 * @property string $ID
 *
 * The followings are the available model relations:
 * @property Cargo[] $cargos
 * @property Areasvicepresidencia[] $areasvicepresidencias
 * @property Autorizaproceso[] $autorizaprocesos
 * @property Usuario $usuarioCreacion
 * @property Usuario $usuarioModificacion
 * @property Companiasprocesosexternos[] $companiasprocesosexternoses
 * @property Ejecucionactividad[] $ejecucionactividads
 * @property Headhunter[] $headhunters
 * @property Rangosalario[] $rangosalarios
 * @property Sedescompanias[] $sedescompaniases
 * @property Tipocomentario[] $tipocomentarios
 * @property Tipocontratacion[] $tipocontratacions
 * @property Tipoprocesoseleccion[] $tipoprocesoseleccions
 * @property Turno[] $turnos
 * @property Unidadnegocio[] $unidadnegocios
 * @property Usuario[] $usuarios
 * @property Vicepresidencias[] $vicepresidenciases
 */
class Companias extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Companias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, NIT, Representante, Descripcion, FechaInicioVigencia, FechaCreacion, UsuarioCreacion', 'required'),
			array('Nombre', 'length', 'max'=>50),
			array('Estado', 'numerical', 'integerOnly'=>true),
			array('Nombre, Representante', 'length', 'max'=>50),
			array('NIT', 'length', 'max'=>20),                    
			array('Descripcion', 'length', 'max'=>100),
			array('TipoServicioID, UsuarioCreacion, UsuarioModificacion, ID', 'length', 'max'=>10),
			array('FechaFinVigencia, FechaModificacion, soporteLegal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('CompaniaID, Nombre, NIT, Representante, Descripcion, FechaInicioVigencia, FechaFinVigencia, TipoServicioID, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion, ID, Estado, soporteLegal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cargos' => array(self::HAS_MANY, 'Cargo', 'CompaniaID'),
                        'actividadesflujoses' => array(self::HAS_MANY, 'Actividadesflujos', 'CompaniaID'),
			'areasvicepresidencias' => array(self::HAS_MANY, 'Areasvicepresidencia', 'CompaniaID'),
			'autorizaprocesos' => array(self::HAS_MANY, 'Autorizaproceso', 'CompaniaID'),
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),
			'companiasprocesosexternoses' => array(self::HAS_MANY, 'Companiasprocesosexternos', 'CompaniaID'),
			'ejecucionactividads' => array(self::HAS_MANY, 'Ejecucionactividad', 'CompaniaID'),
			'headhunters' => array(self::HAS_MANY, 'Headhunter', 'CompaniaID'),
			'rangosalarios' => array(self::HAS_MANY, 'Rangosalario', 'CompaniaID'),
			'sedescompaniases' => array(self::HAS_MANY, 'Sedescompanias', 'CompaniaID'),
			'tipocomentarios' => array(self::HAS_MANY, 'Tipocomentario', 'CompaniaID'),
			'tipocontratacions' => array(self::HAS_MANY, 'Tipocontratacion', 'CompaniaID'),
			'tipoprocesoseleccions' => array(self::HAS_MANY, 'Tipoprocesoseleccion', 'CompaniaID'),
			'turnos' => array(self::HAS_MANY, 'Turno', 'CompaniaID'),
			'unidadnegocios' => array(self::HAS_MANY, 'Unidadnegocio', 'CompaniaID'),
			'usuarios' => array(self::HAS_MANY, 'Usuario', 'CompaniaID'),
			'vicepresidenciases' => array(self::HAS_MANY, 'Vicepresidencias', 'CompaniaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CompaniaID' => 'CompaniaID',
			'Nombre' => 'Nombre de Compañia',
			'NIT' => 'Nit',
			'Representante' => 'Representante Legal o Comercial',                    
			'Descripcion' => 'Descripción',
			'FechaInicioVigencia' => 'Fecha Inicio Vigencia',
			'FechaFinVigencia' => 'Fecha Fin Vigencia',
			'TipoServicioID' => 'Tipo Servicio',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioCreacion' => 'Usuario Creación',
			'FechaModificacion' => 'Fecha Modificación',
			'UsuarioModificacion' => 'Usuario Modificación',
			'ID' => 'ID',
                        'Estado' => 'Estado',
			'soporteLegal' => 'Soporte Legal',                    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CompaniaID',$this->CompaniaID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('NIT',$this->NIT,true);
		$criteria->compare('Representante',$this->Representante,true);                
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaInicioVigencia',$this->FechaInicioVigencia,true);
		$criteria->compare('FechaFinVigencia',$this->FechaFinVigencia,true);
		$criteria->compare('TipoServicioID',$this->TipoServicioID,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);
		$criteria->compare('ID',$this->ID,true);
                $criteria->compare('Estado',$this->Estado);  
		$criteria->compare('soporteLegal',$this->soporteLegal,true);                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function obtenerEstado($estado){
            
            if($estado == '1'){
                return 'Activo';
            } 
            elseif($estado == '0'){
                return 'Inactivo';
            }
        }        
}