<?php

/**
 * This is the model class for table "tipocomentario".
 *
 * The followings are the available columns in table 'tipocomentario':
 * @property string $TipoComentarioID
 * @property string $id
 * @property integer $orden
 * @property string $CompaniaID
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 *
 * The followings are the available model relations:
 * @property Companias $compania
 * @property Usuario $usuarioCreacion
 * @property Usuario $usuarioModificacion
 */
class Tipocomentario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tipocomentario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tipocomentario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, orden, FechaCreacion, UsuarioCreacion', 'required'),
			array('orden', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>200),
			array('CompaniaID, UsuarioCreacion, UsuarioModificacion', 'length', 'max'=>10),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('TipoComentarioID, id, orden, CompaniaID, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TipoComentarioID' => 'TipoComentarioID',
			'id' => 'Tipo de Comentario',
			'orden' => 'Orden',
			'CompaniaID' => 'Compañia',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioCreacion' => 'Usuario Creación',
			'FechaModificacion' => 'Fecha Modificación',
			'UsuarioModificacion' => 'Usuario Modificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TipoComentarioID',$this->TipoComentarioID,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('orden',$this->orden);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}