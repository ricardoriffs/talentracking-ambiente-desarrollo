<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property string $nombre
 * @property string $mail
 * @property string $publicacionmedios
 * @property string $idperson
 * @property string $requisionprocesoID
 */
class Person extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, mail, publicacionmedios, requisionprocesoID', 'required'),
			array('nombre', 'length', 'max'=>200),
			array('mail, publicacionmedios', 'length', 'max'=>45),
			array('requisionprocesoID', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nombre, mail, publicacionmedios, idperson, requisionprocesoID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nombre' => 'Nombre',
			'mail' => 'Mail',
			'publicacionmedios' => 'Publicacionmedios',
			'idperson' => 'Idperson',
			'requisionprocesoID' => 'Requisionproceso',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($ida)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('mail',$this->mail,true);
		$criteria->compare('publicacionmedios',$this->publicacionmedios,true);
		$criteria->compare('idperson',$this->idperson,true);
		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
               
               $criteria->condition='requisionprocesoID ='. $ida;   
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}