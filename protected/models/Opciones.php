<?php

/**
 * This is the model class for table "opciones".
 *
 * The followings are the available columns in table 'opciones':
 * @property integer $idOpcion
 * @property integer $idPregunta
 * @property integer $idGrupo
 * @property string $texto
 * @property string $descripcion
 * @property string $tipoOpcion
 * @property double $peso
 * @property integer $orden
 *
 * The followings are the available model relations:
 * @property Grupospreguntas $idGrupo0
 */
class Opciones extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Opciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'opciones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPregunta, idGrupo, orden', 'numerical', 'integerOnly'=>true),
			array('peso', 'numerical'),
			array('texto', 'length', 'max'=>250),
			array('descripcion', 'length', 'max'=>1200),
			array('tipoOpcion', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idOpcion, idPregunta, idGrupo, texto, descripcion, tipoOpcion, peso, orden', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idGrupo0' => array(self::BELONGS_TO, 'Grupospreguntas', 'idGrupo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idOpcion' => 'Id Opcion',
			'idPregunta' => 'Id Pregunta',
			'idGrupo' => 'Id Grupo',
			'texto' => 'Texto',
			'descripcion' => 'Descripcion',
			'tipoOpcion' => 'Tipo Opcion',
			'peso' => 'Peso',
			'orden' => 'Orden',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idOpcion',$this->idOpcion);
		$criteria->compare('idPregunta',$this->idPregunta);
		$criteria->compare('idGrupo',$this->idGrupo);
		$criteria->compare('texto',$this->texto,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('tipoOpcion',$this->tipoOpcion,true);
		$criteria->compare('peso',$this->peso);
		$criteria->compare('orden',$this->orden);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}