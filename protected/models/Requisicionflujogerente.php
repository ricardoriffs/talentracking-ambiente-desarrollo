<?php

/**
 * This is the model class for table "requisicionflujogerente".
 *
 * The followings are the available columns in table 'requisicionflujogerente':
 * @property string $id
 * @property string $UnidadNegocioID
 * @property string $AreaVicepresidenciaID
 * @property string $usuarioasignacion
 */
class Requisicionflujogerente extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicionflujogerente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicionflujogerente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UnidadNegocioID, AreaVicepresidenciaID, usuarioasignacion', 'required'),
			array('UnidadNegocioID, AreaVicepresidenciaID', 'length', 'max'=>10),
			array('usuarioasignacion', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, UnidadNegocioID, AreaVicepresidenciaID, usuarioasignacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                       return array('unidadnegocio' => array(self::BELONGS_TO, 'Unidadnegocio', 'UnidadNegocioID'),
                             'areavicepresidencia' => array(self::BELONGS_TO, 'Areasvicepresidencia', 'AreaVicepresidenciaID'), 
                             'tipocontratacion' => array(self::BELONGS_TO, 'Tipocontratacion', 'TipoContratacionID'), 
                             'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuarioasignacion'),   
                             'sedescompanias' => array(self::BELONGS_TO, 'Sedescompanias', 'LugarTrabajo'),);		

//return array(
		//);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'UnidadNegocioID' => 'Unidad Negocio',
			'AreaVicepresidenciaID' => 'Area Vicepresidencia',
			'usuarioasignacion' => 'Usuarioasignacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('UnidadNegocioID',$this->UnidadNegocioID,true);
		$criteria->compare('AreaVicepresidenciaID',$this->AreaVicepresidenciaID,true);
		$criteria->compare('usuarioasignacion',$this->usuarioasignacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}