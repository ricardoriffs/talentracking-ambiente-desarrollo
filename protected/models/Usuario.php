<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $mail
 * @property string $fechacreacion
 * @property string $usuariocreacion
 * @property integer $usuariobloqueado
 * @property string $nombrecompleto
 * @property string $cargo
 * @property string $rol
 * @property integer $admin
 * @property integer $primerAcceso
 * @property string $fechaPrimerAcceso
 * @property string $CompaniaID
 *
 * The followings are the available model relations:
 * @property Areasvicepresidencia[] $areasvicepresidencias
 * @property Areasvicepresidencia[] $areasvicepresidencias1
 * @property Companias[] $companiases
 * @property Companias[] $companiases1
 * @property Unidadnegocio[] $unidadnegocios
 * @property Unidadnegocio[] $unidadnegocios1
 * @property Companias $compania
 */
class Usuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, fechacreacion, usuariocreacion, usuariobloqueado, nombrecompleto, cargo, CompaniaID', 'required'),
			array('usuariobloqueado, admin, primerAcceso', 'numerical', 'integerOnly'=>true),
                        array('primerAcceso', 'boolean'),
			array('username, password, usuariocreacion, nombrecompleto, cargo, rol', 'length', 'max'=>128),
			array('mail', 'length', 'max'=>45),
//			array('CompaniaID', 'length', 'max'=>10),
			array('fechaPrimerAcceso', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, mail, fechacreacion, usuariocreacion, usuariobloqueado, nombrecompleto, cargo, rol, admin, primerAcceso, fechaPrimerAcceso, CompaniaID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areasvicepresidencias' => array(self::HAS_MANY, 'Areasvicepresidencia', 'UsuarioCreacion'),
			'areasvicepresidencias1' => array(self::HAS_MANY, 'Areasvicepresidencia', 'UsuarioModificacion'),
			'companiases' => array(self::HAS_MANY, 'Companias', 'UsuarioModificacion'),
			'companiases1' => array(self::HAS_MANY, 'Companias', 'UsuarioCreacion'),
			'unidadnegocios' => array(self::HAS_MANY, 'Unidadnegocio', 'UsuarioModificacion'),
			'unidadnegocios1' => array(self::HAS_MANY, 'Unidadnegocio', 'UsuarioCreacion'),
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'correo electrónico',
			'password' => 'Contraseña',
			'mail' => 'Correo electrónico',
			'fechacreacion' => 'Fecha creación',
			'usuariocreacion' => 'Usuario creación',
			'usuariobloqueado' => 'Activo',
			'nombrecompleto' => 'Nombre completo',
			'cargo' => 'Cargo',
			'rol' => 'Rol',
			'admin' => 'Admin',
			'primerAcceso' => 'Primer Acceso',
			'fechaPrimerAcceso' => 'Fecha Primer Acceso',
			'CompaniaID' => 'Compañia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('mail',$this->mail,true);
		$criteria->compare('fechacreacion',$this->fechacreacion,true);
		$criteria->compare('usuariocreacion',$this->usuariocreacion,true);
		$criteria->compare('usuariobloqueado',$this->usuariobloqueado);
		$criteria->compare('nombrecompleto',$this->nombrecompleto,true);
		$criteria->compare('cargo',$this->cargo,true);
		$criteria->compare('rol',$this->rol,true);
		$criteria->compare('admin',$this->admin);
		$criteria->compare('primerAcceso',$this->primerAcceso);
		$criteria->compare('fechaPrimerAcceso',$this->fechaPrimerAcceso,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	public function validatePassword($password)
	{
		return $this->hashPassword($password)===$this->password;
	}
	public function hashPassword($password)
	{
		return md5($password);
	}        
}