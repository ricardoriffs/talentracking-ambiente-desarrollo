<?php

/**
 * This is the model class for table "encuestas".
 *
 * The followings are the available columns in table 'encuestas':
 * @property integer $idEncuesta
 * @property string $nombre
 * @property string $nombreResumen
 * @property string $descripcion
 * @property integer $proceso
 * @property integer $idProceso
 * @property string $fechaInsert
 *
 * The followings are the available model relations:
 * @property Procesos $idProceso0
 */
class Encuestas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Encuestas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuestas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, nombreResumen, idProceso', 'required'),
			array('proceso, idProceso', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>200),
			array('nombreResumen', 'length', 'max'=>50),
			array('descripcion', 'length', 'max'=>1000),
			array('fechaInsert', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idEncuesta, nombre, nombreResumen, descripcion, proceso, idProceso, fechaInsert', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProceso0' => array(self::BELONGS_TO, 'Procesos', 'idProceso'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEncuesta' => 'Id Encuesta',
			'nombre' => 'Nombre',
			'nombreResumen' => 'Nombre Resumen',
			'descripcion' => 'Descripcion',
			'proceso' => 'Proceso',
			'idProceso' => 'Id Proceso',
			'fechaInsert' => 'Fecha',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($proceso)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

    		if($proceso == NULL)
	          $proceso = 0;

		$criteria=new CDbCriteria;

		$criteria->compare('idEncuesta',$this->idEncuesta);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('nombreResumen',$this->nombreResumen,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('proceso',$this->proceso);
		$criteria->compare('idProceso',$this->idProceso);
		$criteria->compare('fechaInsert',$this->fechaInsert,true);
    		$criteria->addCondition('idProceso ='. $proceso);    

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array('pageSize' => 8),
                        'sort'=>array('defaultOrder'=>'nombre ASC'),   
		));
	}
}
