<?php

/**
 * This is the model class for table "vicepresidencias".
 *
 * The followings are the available columns in table 'vicepresidencias':
 * @property string $VicePresidenciaID
 * @property string $CompañiaID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 *
 * The followings are the available model relations:
 * @property Areasvicepresidencia[] $areasvicepresidencias
 * @property Companias $compañia
 */
class Viceprecidencias extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Viceprecidencias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vicepresidencias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CompañiaID, Nombre, FechaCreacion, UsuarioCreacion', 'required'),
			array('CompañiaID, UsuarioCreacion, UsuarioModificacion', 'length', 'max'=>10),
			array('Nombre', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>100),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('VicePresidenciaID, CompañiaID, Nombre, Descripcion, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areasvicepresidencias' => array(self::HAS_MANY, 'Areasvicepresidencia', 'VicePresidenciaID'),
			'compañia' => array(self::BELONGS_TO, 'Companias', 'CompañiaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'VicePresidenciaID' => 'Vice Presidencia',
			'CompañiaID' => 'Compa�ia',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripcion',
			'FechaCreacion' => 'Fecha Creacion',
			'UsuarioCreacion' => 'Usuario Creacion',
			'FechaModificacion' => 'Fecha Modificacion',
			'UsuarioModificacion' => 'Usuario Modificacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('VicePresidenciaID',$this->VicePresidenciaID,true);
		$criteria->compare('CompañiaID',$this->CompañiaID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}