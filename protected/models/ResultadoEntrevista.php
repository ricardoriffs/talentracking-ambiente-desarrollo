<?php

/**
 * This is the model class for table "resultadoEntrevista".
 *
 * The followings are the available columns in table 'resultadoEntrevista':
 * @property integer $idResultadoEntrevista
 * @property string $valoracion
 * @property string $comentarios
 * @property integer $idProceso
 * @property integer $idCandidato
 * @property integer $idUsuarioEvaluador
 *
 * The followings are the available model relations:
 * @property Candidatos $idCandidato0
 * @property Procesos $idProceso0
 */
class ResultadoEntrevista extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ResultadoEntrevista the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'resultadoEntrevista';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('valoracion, idCandidato, idUsuarioEvaluador', 'required'),
			array('idProceso, idCandidato, idUsuarioEvaluador', 'numerical', 'integerOnly'=>true),
			array('valoracion', 'length', 'max'=>45),
			array('comentarios', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idResultadoEntrevista, valoracion, comentarios, idProceso, idCandidato, idUsuarioEvaluador', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCandidato0' => array(self::BELONGS_TO, 'Candidatos', 'idCandidato'),
			'idProceso0' => array(self::BELONGS_TO, 'Procesos', 'idProceso'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idResultadoEntrevista' => 'Id Resultado Entrevista',
			'valoracion' => 'Valoracion',
			'comentarios' => 'Comentarios',
			'idProceso' => 'Id Proceso',
			'idCandidato' => 'Id Candidato',
			'idUsuarioEvaluador' => 'Id Usuario Evaluador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idResultadoEntrevista',$this->idResultadoEntrevista);
		$criteria->compare('valoracion',$this->valoracion,true);
		$criteria->compare('comentarios',$this->comentarios,true);
		$criteria->compare('idProceso',$this->idProceso);
		$criteria->compare('idCandidato',$this->idCandidato);
		$criteria->compare('idUsuarioEvaluador',$this->idUsuarioEvaluador);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}