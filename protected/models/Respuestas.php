<?php

/**
 * This is the model class for table "respuestas".
 *
 * The followings are the available columns in table 'respuestas':
 * @property integer $idRespuesta
 * @property integer $idPregunta
 * @property integer $idOpcion
 * @property integer $idProceso
 * @property integer $idCandidato
 * @property string $valor
 * @property string $observaciones
 * @property string $fechaInsert
 *
 * The followings are the available model relations:
 * @property Preguntas $idPregunta0
 * @property Candidatos $idCandidato0
 * @property Procesos $idProceso0
 */
class Respuestas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Respuestas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'respuestas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('valor', 'required'),
			array('idPregunta, idOpcion, idProceso, idCandidato', 'numerical', 'integerOnly'=>true),
			array('valor', 'length', 'max'=>1000),
			array('observaciones, fechaInsert', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idRespuesta, idPregunta, idOpcion, idProceso, idCandidato, valor, observaciones, fechaInsert', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPregunta0' => array(self::BELONGS_TO, 'Preguntas', 'idPregunta'),
			'idCandidato0' => array(self::BELONGS_TO, 'Candidatos', 'idCandidato'),
			'idProceso0' => array(self::BELONGS_TO, 'Procesos', 'idProceso'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idRespuesta' => 'Id Respuesta',
			'idPregunta' => 'Id Pregunta',
			'idOpcion' => 'Id Opcion',
			'idProceso' => 'Id Proceso',
			'idCandidato' => 'Id Candidato',
			'valor' => 'Valor',
			'observaciones' => 'Observaciones',
			'fechaInsert' => 'Fecha Insert',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idRespuesta',$this->idRespuesta);
		$criteria->compare('idPregunta',$this->idPregunta);
		$criteria->compare('idOpcion',$this->idOpcion);
		$criteria->compare('idProceso',$this->idProceso);
		$criteria->compare('idCandidato',$this->idCandidato);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('fechaInsert',$this->fechaInsert,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}