<?php

/**
 * This is the model class for table "Cargo".
 *
 * The followings are the available columns in table 'Cargo':
 * @property string $CargoID
 * @property string $id
 * @property string $tipocargo
 * @property string $CompaniaID
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 *
 * The followings are the available model relations:
 * @property Usuario $usuarioCreacion
 * @property Companias $compania
 */
class Cargo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cargo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cargo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, tipocargo, FechaCreacion, UsuarioCreacion', 'required'),
			array('id', 'length', 'max'=>200),
			array('tipocargo', 'length', 'max'=>1),
			array('CompaniaID, UsuarioModificacion, UsuarioCreacion', 'length', 'max'=>10),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('CargoID, id, tipocargo, CompaniaID, FechaModificacion, UsuarioModificacion, FechaCreacion, UsuarioCreacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CargoID' => 'CargoID',
			'id' => 'Nombre de Cargo',
			'tipocargo' => 'Tipo de cargo',
			'CompaniaID' => 'Compañia',
			'FechaModificacion' => 'Fecha Modificación',
			'UsuarioModificacion' => 'Usuario Modificación',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioCreacion' => 'Usuario Creación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CargoID',$this->CargoID,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('tipocargo',$this->tipocargo,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function obtenerTipoCargo($tipocargo){
            
            if($tipocargo == 'R'){
                return 'A requerir (R)';
            } 
            elseif($tipocargo == 'V'){
                return 'Vigente (V)';
            }
        }
}