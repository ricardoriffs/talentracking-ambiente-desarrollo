<?php

/**
 * This is the model class for table "tipoprocesoseleccion".
 *
 * The followings are the available columns in table 'tipoprocesoseleccion':
 * @property string $TipoProcesoSeleccionID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $CompaniaID
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 *
 * The followings are the available model relations:
 * @property Usuario $usuarioModificacion
 * @property Companias $compania
 * @property Usuario $usuarioCreacion
 */
class Tipoprocesoseleccion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tipoprocesoseleccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tipoprocesoseleccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, Descripcion, FechaCreacion, UsuarioCreacion', 'required'),
			array('Nombre', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>100),
			array('UsuarioCreacion, CompaniaID, UsuarioModificacion', 'length', 'max'=>10),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('TipoProcesoSeleccionID, Nombre, Descripcion, FechaCreacion, UsuarioCreacion, CompaniaID, FechaModificacion, UsuarioModificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioCreacion' => 'Usuario Creación',
			'CompaniaID' => 'Compañia',
			'FechaModificacion' => 'Fecha Modificación',
			'UsuarioModificacion' => 'Usuario Modificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}