<?php

/**
 * This is the model class for table "requisicionproceso".
 *
 * The followings are the available columns in table 'requisicionproceso':
 * @property string $requisionprocesoID
 * @property string $RequisicionID
 * @property string $refiere
 * @property string $emailrefiere
 * @property string $referido
 * @property string $emailreferido
 * @property string $datosparapublicar
 * @property string $TipoProcesoSeleccionID
 */
class Requi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicionproceso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RequisicionID', 'required'),
			array('RequisicionID, TipoProcesoSeleccionID', 'length', 'max'=>10),
			array('refiere, referido', 'length', 'max'=>200),
			array('emailrefiere, emailreferido', 'length', 'max'=>100),
			array('datosparapublicar', 'length', 'max'=>1500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('requisionprocesoID, RequisicionID, refiere, emailrefiere, referido, emailreferido, datosparapublicar, TipoProcesoSeleccionID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'requisionprocesoID' => 'Requisionproceso',
			'RequisicionID' => 'Requisicion',
			'refiere' => 'Refiere',
			'emailrefiere' => 'Emailrefiere',
			'referido' => 'Referido',
			'emailreferido' => 'Emailreferido',
			'datosparapublicar' => 'Datosparapublicar',
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('refiere',$this->refiere,true);
		$criteria->compare('emailrefiere',$this->emailrefiere,true);
		$criteria->compare('referido',$this->referido,true);
		$criteria->compare('emailreferido',$this->emailreferido,true);
		$criteria->compare('datosparapublicar',$this->datosparapublicar,true);
		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}