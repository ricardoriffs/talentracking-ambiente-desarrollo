<?php

/**
 * This is the model class for table "requisicion".
 *
 * The followings are the available columns in table 'requisicion':
 * @property string $RequisicionID
 * @property string $UnidadNegocioID
 * @property string $AutorizacionProceso
 * @property string $NombreAutoriza
 * @property string $EmailAutoriza
 * @property string $AreaVicepresidenciaID
 * @property string $RutaArchivoAutorizacion
 * @property string $NumeroVacantes
 * @property string $NombreCargo
 * @property string $NombreSolicitante
 * @property string $CargoSolicitante
 * @property string $JefeInmediatoVacante
 * @property string $LugarTrabajo
 * @property string $RutaDescripcionCargo
 * @property string $DescripcionCargoPar
 * @property string $TurnoTrabajo
 * @property string $TipoContratacionID
 * @property string $TiempoContratacion
 * @property string $AspectosImportantes
 * @property string $Observaciones
 * @property string $RangoSalario
 * @property string $NivelHAY
 * @property string $PuntosHAY
 * @property string $TipoProcesoSeleccionID
 * @property string $UsuarioIDResponsable
 */
class Requisicion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UnidadNegocioID, AutorizacionProceso,  AreaVicepresidenciaID, RutaArchivoAutorizacion, NumeroVacantes, NombreCargo, NombreSolicitante, CargoSolicitante, JefeInmediatoVacante, LugarTrabajo, RutaDescripcionCargo, DescripcionCargoPar, TurnoTrabajo, TipoContratacionID, AspectosImportantes, Observaciones, RangoSalario', 'required'),
                        //NombreAutoriza, EmailAutoriza,
			array('UnidadNegocioID, AreaVicepresidenciaID, NumeroVacantes, TipoContratacionID, TiempoContratacion, NivelHAY, PuntosHAY, TipoProcesoSeleccionID, UsuarioIDResponsable,actividadID', 'length', 'max'=>10),
			array('AutorizacionProceso, NombreAutoriza, EmailAutoriza, NombreSolicitante, CargoSolicitante, JefeInmediatoVacante, LugarTrabajo, TurnoTrabajo, RangoSalario', 'length', 'max'=>50),
			array('RutaArchivoAutorizacion, NombreCargo, RutaDescripcionCargo', 'length', 'max'=>255),
			array('DescripcionCargoPar, AspectosImportantes', 'length', 'max'=>4000),
			array('Observaciones', 'length', 'max'=>2000),
                        //array('actividadID','length', 'max'=>10), 
                        array('actividadID', 'safe'),
                        array('politicaID', 'safe'), 
                        array('fechacreacion', 'safe'), 
                        array('usuariocreacion', 'safe'), 
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('RequisicionID, UnidadNegocioID, AutorizacionProceso, NombreAutoriza, EmailAutoriza, AreaVicepresidenciaID, RutaArchivoAutorizacion, NumeroVacantes, NombreCargo, NombreSolicitante, CargoSolicitante, JefeInmediatoVacante, LugarTrabajo, RutaDescripcionCargo, DescripcionCargoPar, TurnoTrabajo, TipoContratacionID, TiempoContratacion, AspectosImportantes, Observaciones, RangoSalario, NivelHAY, PuntosHAY, TipoProcesoSeleccionID, UsuarioIDResponsable', 'safe', 'on'=>'search'),
		);
                
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array('unidadnegocio' => array(self::BELONGS_TO, 'Unidadnegocio', 'UnidadNegocioID'),
                             'areavicepresidencia' => array(self::BELONGS_TO, 'AreasVicepresidencia', 'AreaVicepresidenciaID'), 
                             'tipocontratacion' => array(self::BELONGS_TO, 'Tipocontratacion', 'TipoContratacionID'), 
                             'sedescompanias' => array(self::BELONGS_TO, 'Sedescompanias', 'LugarTrabajo'),);
                //return array( );  
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'RequisicionID' => 'Requisicion',
			'UnidadNegocioID' => 'Unidad de Negocio',
			'AutorizacionProceso' => 'Autorización del Proceso',
			'NombreAutoriza' => 'Nombre de quien autoriza el proceso',
			'EmailAutoriza' => 'Correo de quien Autoriza',
			'AreaVicepresidenciaID' => 'Vicepresidencia Área',
			'RutaArchivoAutorizacion' => 'Formato de autorización de requisición (puede ser un soporte de correo electrónico, escaner, etc.)',
			'NumeroVacantes' => 'Número Vacantes para este cargo',
			'NombreCargo' => 'Nombre del Cargo',
			'NombreSolicitante' => 'Nombre del Solicitante',
			'CargoSolicitante' => 'Cargo del Solicitante',
			'JefeInmediatoVacante' => 'Cargo del jefe inmediato de la persona que va a ocupar la vacante',
			'LugarTrabajo' => 'Lugar de trabajo de la persona que va a ocupar la vacante',
			'RutaDescripcionCargo' => 'Cargar Descripción de Cargo existente',
			'DescripcionCargoPar' => 'Describir un Cargo Par',
			'TurnoTrabajo' => 'Turno Trabajo',
			'TipoContratacionID' => 'Tipo de Contratación',
			'TiempoContratacion' => 'Tiempo de Contratación',
			'AspectosImportantes' => 'Describa algunos Aspectos del cargo que son importantes a tener en cuenta y que no han sido descritos anteriormente',
			'Observaciones' => 'Observaciones necesarias para poder aprobar la requisición y el proceso de seleccion',
			'RangoSalario' => 'Rango Salario',
			'NivelHAY' => 'Nivel Hay',
			'PuntosHAY' => 'Puntos Hay',
			'TipoProcesoSeleccionID' => 'Tipo Proceso Selección',
			'UsuarioIDResponsable' => 'Usuario Idresponsable',
                        'fechacreacion' => 'Fecha de Creación', 

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		//$criteria->with =array('unidadnegocio');
                //$criteria->addSearchCondition('unidadnegocio.nombre', $this->UnidadNegocioID); 
                $criteria->compare('UnidadNegocioID',$this->UnidadNegocioID,true);   
                $criteria->compare('AutorizacionProceso',$this->AutorizacionProceso,true);
		$criteria->compare('NombreAutoriza',$this->NombreAutoriza,true);
                $criteria->compare('EmailAutoriza',$this->EmailAutoriza,true);
		//$criteria->with =array('area');
                //$criteria->addSearchCondition('area.nombre',$this->AreaVicepresidenciaID);
                $criteria->compare('AreaVicepresidenciaID',$this->AreaVicepresidenciaID,true); 
		$criteria->compare('RutaArchivoAutorizacion',$this->RutaArchivoAutorizacion,true);
		$criteria->compare('NumeroVacantes',$this->NumeroVacantes,true);
		$criteria->compare('NombreCargo',$this->NombreCargo,true);
		$criteria->compare('NombreSolicitante',$this->NombreSolicitante,true);
		$criteria->compare('CargoSolicitante',$this->CargoSolicitante,true);
		$criteria->compare('JefeInmediatoVacante',$this->JefeInmediatoVacante,true);
		$criteria->compare('LugarTrabajo',$this->LugarTrabajo,true);
		$criteria->compare('RutaDescripcionCargo',$this->RutaDescripcionCargo,true);
		$criteria->compare('DescripcionCargoPar',$this->DescripcionCargoPar,true);
		$criteria->compare('TurnoTrabajo',$this->TurnoTrabajo,true);
		$criteria->compare('TipoContratacionID',$this->TipoContratacionID,true);
		$criteria->compare('TiempoContratacion',$this->TiempoContratacion,true);
		$criteria->compare('AspectosImportantes',$this->AspectosImportantes,true);
		$criteria->compare('Observaciones',$this->Observaciones,true);
		$criteria->compare('RangoSalario',$this->RangoSalario,true);
		$criteria->compare('NivelHAY',$this->NivelHAY,true);
		$criteria->compare('PuntosHAY',$this->PuntosHAY,true);
		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		$criteria->compare('UsuarioIDResponsable',$this->UsuarioIDResponsable,true);
                $criteria->compare('actividadID',$this->actividadID,true); 
                $criteria->compare('politicaID',$this->politicaID,true);
                $criteria->compare('fechacreacion',$this->fechacreacion,true);
                $criteria->compare('usuariocreacion',$this->usuariocreacion,true);  

                if (Yii::app()->user->name == 'admin' or Yii::app()->user->name == 'clienteinterno' ){
                   $criteria->condition='actividadID = 1'; 
                } else { 
		   $criteria->condition= "actividadID = 1 and usuariocreacion = '" . Yii::app()->user->name ."'"; 
                } 
                //$criteria->compare('RequisicionID',$this->RequisicionID,true);
                   
		
                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array('defaultOrder'=>'fechacreacion DESC'),                    
		));
	}



}