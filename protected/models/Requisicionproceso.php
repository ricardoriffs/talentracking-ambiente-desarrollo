<?php

/**
 * This is the model class for table "requisicionproceso".
 *
 * The followings are the available columns in table 'requisicionproceso':
 * @property string $requisionprocesoID
 * @property string $RequisicionID
 * @property string $refiere
 * @property string $emailrefiere
 * @property string $referido
 * @property string $emailreferido
 * @property string $datosparapublicar
 * @property string $TipoProcesoSeleccionID
 */
class Requisicionproceso extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicionproceso the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicionproceso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RequisicionID', 'required'),
			array('RequisicionID, TipoProcesoSeleccionID', 'length', 'max'=>10),
			array('refiere, referido', 'length', 'max'=>200),
			array('emailrefiere, emailreferido', 'length', 'max'=>100),
			array('datosparapublicar', 'length', 'max'=>1500),                        
                        array('username', 'safe'),
                        array('estado', 'safe'), 
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('requisionprocesoID, RequisicionID, refiere, emailrefiere, referido, emailreferido, datosparapublicar, TipoProcesoSeleccionID,datosofertacargo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
           return array(
		  'unidadnegocio' => array(self::BELONGS_TO, 'Unidadnegocio','datosofertaunidadnegocio'),
		  'tipoprocesoseleccion' => array(self::BELONGS_TO, 'Tipoprocesoseleccion','TipoProcesoSeleccionID') ); 
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'requisionprocesoID' => 'Requisionproceso',
			'RequisicionID' => 'Requisicion',
			'refiere' => 'Refiere',
			'emailrefiere' => 'Emailrefiere',
			'referido' => 'Referido',
			'emailreferido' => 'Emailreferido',
			'datosparapublicar' => 'Datosparapublicar',
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
                        'username' => 'Nombre Coordinador Asignado',
                        'datosofertacargo'=> 'Oferta Cargo',
                        'datosofertaunidadnegocio'=> 'Unidad de Negocio', 
                        'ofertaid'=> 'Oferta ID',    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($v)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                 
                //$criteria->condition = "TipoProcesoSeleccionID IN ($idSlice)";
                
 
		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('refiere',$this->refiere,true);
		$criteria->compare('emailrefiere',$this->emailrefiere,true);
		$criteria->compare('referido',$this->referido,true);
		$criteria->compare('emailreferido',$this->emailreferido,true);
		$criteria->compare('datosparapublicar',$this->datosparapublicar,true);
		//$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
                
                //$criteria->with =array('datosofertacargo'); 
                $criteria->compare('datosofertacargo',$this->datosofertacargo,true);
                
                $criteria->with =array('tipoprocesoseleccion');   
                $criteria->addSearchCondition('tipoprocesoseleccion.Nombre',$this->TipoProcesoSeleccionID);
                
                //liderseleccion (Yii::app()->user->name == "admin") �� 

                if ((Yii::app()->user->name == "liderseleccion") or (Yii::app()->user->name == "gerente1") or (Yii::app()->user->name =="gerente2")) {
                      //$criteria->compare('username',Yii::app()->user->name); 
                     $criteria->compare('username',$this->username);     
                     
                } else {
                      $criteria->compare('username',Yii::app()->user->name);  
                }  
                  
                if ($v == "A") {
                   $criteria->compare('estado',array(0,2));     
                } else {
                   $criteria->compare('estado',1);      
                }  

 	                
                
                //$criteria->order = "FIELD(TipoProcesoSeleccionID,100)"; 
                // $criteria->order = 'field'=>array('TipoProcesoSeleccionID',array(10));   
                //Yii::app()->user->name
                
               // if (Yii::app()->user->name == 'admin'){
               // } else { 
               // 	$criteria->condition='username ='. "'" . Yii::app()->user->name . "'"; 
               // } 

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array('defaultOrder'=>'requisicionID DESC'),                      
		));
	}
        
        public function obtenerFechaCreacion($requisicionID){
            
            $criteria = new CDbCriteria;
            $criteria->select = 'fechacreacion';
            //$criteria->with =array('requisicion'); 
            $criteria->condition='requisicionID=:requisicionID';
            $criteria->params=array(':requisicionID'=>$requisicionID);
            //$criteria->compare($requisicionID, $this->requisicionID);
            $req_proceso = Requisicion::model()->find($criteria);
                    
            return $req_proceso->fechacreacion;
        }        
}