<?php

/**
 * This is the model class for table "tipoprocesoseleccion".
 *
 * The followings are the available columns in table 'tipoprocesoseleccion':
 * @property string $TipoProcesoSeleccionID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 *
 * The followings are the available model relations:
 * @property Usuarios $usuarioCreacion
 */
class Tipoprocesoseleccionresumen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tipoprocesoseleccionresumen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tipoprocesoseleccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, Descripcion, FechaCreacion, UsuarioCreacion', 'required'),
			array('Nombre', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>100),
			array('UsuarioCreacion', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('TipoProcesoSeleccionID, Nombre, Descripcion, FechaCreacion, UsuarioCreacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuarios', 'UsuarioCreacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripcion',
			'FechaCreacion' => 'Fecha Creacion',
			'UsuarioCreacion' => 'Usuario Creacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function searchj()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

				// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('refiere',$this->refiere,true);
		$criteria->compare('emailrefiere',$this->emailrefiere,true);
		$criteria->compare('referido',$this->referido,true);
		$criteria->compare('emailreferido',$this->emailreferido,true);
		$criteria->compare('datosparapublicar',$this->datosparapublicar,true);
		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		$criteria->compare('datosofertacargo',$this->datosofertacargo,true);
		$criteria->compare('datosofertaunidadnegocio',$this->datosofertaunidadnegocio,true);
		$criteria->compare('datosofertareportaa',$this->datosofertareportaa,true);
		$criteria->compare('datosofertamision',$this->datosofertamision,true);
		$criteria->compare('datosofertaresponsabilidades',$this->datosofertaresponsabilidades,true);
		$criteria->compare('datosofertaformacion',$this->datosofertaformacion,true);
		$criteria->compare('datosofertaconocimientos',$this->datosofertaconocimientos,true);
		$criteria->compare('datosofertacompetencias',$this->datosofertacompetencias,true);
		$criteria->compare('datosofertaexperiencia',$this->datosofertaexperiencia,true);
		$criteria->compare('datosofertafechalimite',$this->datosofertafechalimite,true);
                $criteria->compare('datosofertafechainicio',$this->datosofertafechainicio,true);
                $criteria->compare('datosofertaexperienciaind',$this->datosofertaexperienciaind,true);
                $criteria->compare('datosofertaexperienciacar',$this->datosofertaexperienciacar,true); 
		$criteria->compare('datosafertaregistroaplicar',$this->datosafertaregistroaplicar,true);
                $criteria->condition='TipoProcesoSeleccionID = 1'; 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}