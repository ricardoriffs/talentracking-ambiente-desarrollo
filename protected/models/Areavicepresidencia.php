<?php

/**
 * This is the model class for table "areasvicepresidencia".
 *
 * The followings are the available columns in table 'areasvicepresidencia':
 * @property string $AreaVicepresidenciaID
 * @property string $VicePresidenciaID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 *
 * The followings are the available model relations:
 * @property Usuarios $usuarioCreacion
 * @property Usuarios $usuarioModificacion
 * @property Vicepresidencias $vicePresidencia
 */
class Areavicepresidencia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Areavicepresidencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'areasvicepresidencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('VicePresidenciaID, Nombre, FechaCreacion, UsuarioCreacion', 'required'),
			array('VicePresidenciaID, UsuarioCreacion, UsuarioModificacion', 'length', 'max'=>10),
			array('Nombre', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>100),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('AreaVicepresidenciaID, VicePresidenciaID, Nombre, Descripcion, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuarios', 'UsuarioCreacion'),
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuarios', 'UsuarioModificacion'),
			'vicePresidencia' => array(self::BELONGS_TO, 'Vicepresidencias', 'VicePresidenciaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'AreaVicepresidenciaID' => 'Area Vicepresidencia',
			'VicePresidenciaID' => 'Vice Presidencia',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripcion',
			'FechaCreacion' => 'Fecha Creacion',
			'UsuarioCreacion' => 'Usuario Creacion',
			'FechaModificacion' => 'Fecha Modificacion',
			'UsuarioModificacion' => 'Usuario Modificacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AreaVicepresidenciaID',$this->AreaVicepresidenciaID,true);
		$criteria->compare('VicePresidenciaID',$this->VicePresidenciaID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}