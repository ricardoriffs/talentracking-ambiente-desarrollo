<?php

/**
 * This is the model class for table "modulos".
 *
 * The followings are the available columns in table 'modulos':
 * @property string $ModuloID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $OrdenVisualizacion
 * @property string $UrlImagen
 * @property string $UrlNavegacion
 * @property string $Tag
 *
 * The followings are the available model relations:
 * @property Acciones[] $acciones
 * @property Tipocontratacion[] $tipocontratacions
 */
class Modulos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Modulos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'modulos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, OrdenVisualizacion', 'required'),
			array('Nombre, UrlImagen', 'length', 'max'=>50),
			array('Descripcion, UrlNavegacion', 'length', 'max'=>100),
			array('OrdenVisualizacion', 'length', 'max'=>10),
			array('Tag', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ModuloID, Nombre, Descripcion, OrdenVisualizacion, UrlImagen, UrlNavegacion, Tag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'acciones' => array(self::HAS_MANY, 'Acciones', 'ModuloID'),
			'tipocontratacions' => array(self::HAS_MANY, 'Tipocontratacion', 'ModuloID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ModuloID' => 'Modulo',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripcion',
			'OrdenVisualizacion' => 'Orden Visualizacion',
			'UrlImagen' => 'Url Imagen',
			'UrlNavegacion' => 'Url Navegacion',
			'Tag' => 'Tag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ModuloID',$this->ModuloID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('OrdenVisualizacion',$this->OrdenVisualizacion,true);
		$criteria->compare('UrlImagen',$this->UrlImagen,true);
		$criteria->compare('UrlNavegacion',$this->UrlNavegacion,true);
		$criteria->compare('Tag',$this->Tag,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}