<?php

/**
 * This is the model class for table "preguntas".
 *
 * The followings are the available columns in table 'preguntas':
 * @property integer $idPregunta
 * @property integer $idGrupo
 * @property integer $idTipo
 * @property string $texto
 * @property string $descripcion
 * @property integer $orden
 * @property string $borrada
 */
class Preguntas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Preguntas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'preguntas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idGrupo, idTipo', 'required'),
			array('idGrupo, idTipo, orden', 'numerical', 'integerOnly'=>true),
			array('texto', 'length', 'max'=>300),
			array('descripcion', 'length', 'max'=>1500),
			array('borrada', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPregunta, idGrupo, idTipo, texto, descripcion, orden, borrada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPregunta' => 'Id Pregunta',
			'idGrupo' => 'Id Grupo',
			'idTipo' => 'Id Tipo',
			'texto' => 'Texto',
			'descripcion' => 'Descripcion',
			'orden' => 'Orden',
			'borrada' => 'Borrada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPregunta',$this->idPregunta);
		$criteria->compare('idGrupo',$this->idGrupo);
		$criteria->compare('idTipo',$this->idTipo);
		$criteria->compare('texto',$this->texto,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('orden',$this->orden);
		$criteria->compare('borrada',$this->borrada,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}