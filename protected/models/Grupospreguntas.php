<?php

/**
 * This is the model class for table "grupospreguntas".
 *
 * The followings are the available columns in table 'grupospreguntas':
 * @property integer $idGrupo
 * @property integer $idEncuesta
 * @property string $nombre
 * @property string $descripcion
 * @property string $preguntaSugerida
 * @property integer $orden
 * @property string $borrado
 * @property string $CompaniaID
 *
 * The followings are the available model relations:
 * @property Companias $compania
 */
class Grupospreguntas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Grupospreguntas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'grupospreguntas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEncuesta', 'required'),
			array('idEncuesta, orden', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>200),
			array('descripcion', 'length', 'max'=>2000),
			array('borrado', 'length', 'max'=>1),
			array('CompaniaID', 'length', 'max'=>10),
			array('preguntaSugerida', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idGrupo, idEncuesta, nombre, descripcion, preguntaSugerida, orden, borrado, CompaniaID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idGrupo' => 'Id Grupo',
			'idEncuesta' => 'Id Encuesta',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'preguntaSugerida' => 'Pregunta Sugerida',
			'orden' => 'Orden',
			'borrado' => 'Borrado',
			'CompaniaID' => 'Compania',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idGrupo',$this->idGrupo);
		$criteria->compare('idEncuesta',$this->idEncuesta);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('preguntaSugerida',$this->preguntaSugerida,true);
		$criteria->compare('orden',$this->orden);
		$criteria->compare('borrado',$this->borrado,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}