<?php

/**
 * This is the model class for table "autorizaproceso".
 *
 * The followings are the available columns in table 'autorizaproceso':
 * @property string $AutorizaProcesoID
 * @property string $id
 * @property string $CompaniaID
 *
 * The followings are the available model relations:
 * @property Companias $compania
 */
class Autorizaproceso extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Autorizaproceso the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autorizaproceso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id', 'length', 'max'=>50),
			array('CompaniaID', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('AutorizaProcesoID, id, CompaniaID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'AutorizaProcesoID' => 'AutorizaProcesoID',
			'id' => 'Autoriza proceso',
			'CompaniaID' => 'Compañia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AutorizaProcesoID',$this->AutorizaProcesoID,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}