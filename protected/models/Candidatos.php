<?php

/**
 * This is the model class for table "candidatos".
 *
 * The followings are the available columns in table 'candidatos':
 * @property integer $idCandidato
 * @property string $apellidos
 * @property string $nombres
 * @property integer $identificacion
 * @property integer $edad
 * @property string $ultimoCargo
 * @property double $aniosProfesional
 * @property double $aniosRequeridos
 * @property double $aniosExperienciaSector
 * @property double $sueldoActual
 * @property double $sueldoAspirado
 * @property string $familiarEnEmpresa
 * @property string $foto
 * @property string $nombreCargo
 * @property string $fechaInsert
 * @property integer $id_proceso
 * @property string $ne_primaria
 * @property string $ne_secundaria
 * @property string $ne_tecnico
 * @property string $ne_tecnologo
 * @property string $ne_profecional
 * @property string $ne_especializacion
 * @property string $ne_maestria
 * @property string $ne_doctorado
 * @property string $ingles_lectura
 * @property string $ingles_escritura
 * @property string $ingles_hablado
 * @property string $so_ninguno
 * @property string $so_sap
 * @property string $so_jdedwar
 * @property string $so_oracle
 * @property string $so_otro
 * @property string $direccion
 * @property string $teloficina
 * @property string $teloficinaext
 * @property string $telcasa
 * @property string $telcel1
 * @property string $telcel2
 * @property string $mail1
 * @property string $mail2
 * @property string $tipocontrato
 * @property string $hojavida
 * @property string $formaprobacion
 * @property string $nom_familiar
 * @property string $nom_referenciado
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Procesos $idProceso
 * @property Metadatos[] $metadatoses
 * @property Respuestas[] $respuestases
 */
class Candidatos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Candidatos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'candidatos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fechaInsert', 'required'),
			array('identificacion, edad, id_proceso, estado', 'numerical', 'integerOnly'=>true),
			array('aniosProfesional, aniosRequeridos, aniosExperienciaSector, sueldoActual, sueldoAspirado', 'numerical'),
			array('apellidos, nombres', 'length', 'max'=>50),
			array('ultimoCargo', 'length', 'max'=>150),
			array('familiarEnEmpresa', 'length', 'max'=>1),
			array('foto, mail1, mail2, tipocontrato, hojavida, formaprobacion, nom_familiar, nom_referenciado', 'length', 'max'=>200),
			array('nombreCargo', 'length', 'max'=>250),
			array('ne_primaria, ne_secundaria, ne_tecnico, ne_tecnologo, ne_profecional, ne_especializacion, ne_maestria, ne_doctorado, so_ninguno, so_sap, so_jdedwar, so_oracle, so_otro, teloficina, teloficinaext, telcasa, telcel1, telcel2', 'length', 'max'=>100),
			array('ingles_lectura, ingles_escritura, ingles_hablado', 'length', 'max'=>10),
			array('direccion', 'length', 'max'=>300),
                        array('estado', 'safe'),                     
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCandidato, apellidos, nombres, identificacion, edad, ultimoCargo, aniosProfesional, aniosRequeridos, aniosExperienciaSector, sueldoActual, sueldoAspirado, familiarEnEmpresa, foto, nombreCargo, fechaInsert, id_proceso, ne_primaria, ne_secundaria, ne_tecnico, ne_tecnologo, ne_profecional, ne_especializacion, ne_maestria, ne_doctorado, ingles_lectura, ingles_escritura, ingles_hablado, so_ninguno, so_sap, so_jdedwar, so_oracle, so_otro, direccion, teloficina, teloficinaext, telcasa, telcel1, telcel2, mail1, mail2, tipocontrato, hojavida, formaprobacion, nom_familiar, nom_referenciado, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProceso' => array(self::BELONGS_TO, 'Procesos', 'id_proceso'),
			'metadatoses' => array(self::HAS_MANY, 'Metadatos', 'idcandidatos'),
			'respuestases' => array(self::HAS_MANY, 'Respuestas', 'idCandidato'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCandidato' => 'Id Candidato',
			'apellidos' => 'Apellidos',
			'nombres' => 'Nombres',
			'identificacion' => 'Identificacion',
			'edad' => 'Edad',
			'ultimoCargo' => 'Ultimo Cargo',
			'aniosProfesional' => 'Anios Profesional',
			'aniosRequeridos' => 'Anios Requeridos',
			'aniosExperienciaSector' => 'Anios Experiencia Sector',
			'sueldoActual' => 'Sueldo Actual',
			'sueldoAspirado' => 'Sueldo Aspirado',
			'familiarEnEmpresa' => 'Familiar En Empresa',
			'foto' => 'Foto',
			'nombreCargo' => 'Nombre Cargo',
			'fechaInsert' => 'Fecha Insert',
			'id_proceso' => 'Id Proceso',
			'ne_primaria' => 'Ne Primaria',
			'ne_secundaria' => 'Ne Secundaria',
			'ne_tecnico' => 'Ne Tecnico',
			'ne_tecnologo' => 'Ne Tecnologo',
			'ne_profecional' => 'Ne Profecional',
			'ne_especializacion' => 'Ne Especializacion',
			'ne_maestria' => 'Ne Maestria',
			'ne_doctorado' => 'Ne Doctorado',
			'ingles_lectura' => 'Ingles Lectura',
			'ingles_escritura' => 'Ingles Escritura',
			'ingles_hablado' => 'Ingles Hablado',
			'so_ninguno' => 'So Ninguno',
			'so_sap' => 'So Sap',
			'so_jdedwar' => 'So Jdedwar',
			'so_oracle' => 'So Oracle',
			'so_otro' => 'So Otro',
			'direccion' => 'Direccion',
			'teloficina' => 'Teloficina',
			'teloficinaext' => 'Teloficinaext',
			'telcasa' => 'Telcasa',
			'telcel1' => 'Telcel1',
			'telcel2' => 'Telcel2',
			'mail1' => 'Mail1',
			'mail2' => 'Mail2',
			'tipocontrato' => 'Tipocontrato',
			'hojavida' => 'Hojavida',
			'formaprobacion' => 'Formaprobacion',
			'nom_familiar' => 'Nom Familiar',
			'nom_referenciado' => 'Nom Referenciado',
			'estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCandidato',$this->idCandidato);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('identificacion',$this->identificacion);
		$criteria->compare('edad',$this->edad);
		$criteria->compare('ultimoCargo',$this->ultimoCargo,true);
		$criteria->compare('aniosProfesional',$this->aniosProfesional);
		$criteria->compare('aniosRequeridos',$this->aniosRequeridos);
		$criteria->compare('aniosExperienciaSector',$this->aniosExperienciaSector);
		$criteria->compare('sueldoActual',$this->sueldoActual);
		$criteria->compare('sueldoAspirado',$this->sueldoAspirado);
		$criteria->compare('familiarEnEmpresa',$this->familiarEnEmpresa,true);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('nombreCargo',$this->nombreCargo,true);
		$criteria->compare('fechaInsert',$this->fechaInsert,true);
		$criteria->compare('id_proceso',$this->id_proceso);
		$criteria->compare('ne_primaria',$this->ne_primaria,true);
		$criteria->compare('ne_secundaria',$this->ne_secundaria,true);
		$criteria->compare('ne_tecnico',$this->ne_tecnico,true);
		$criteria->compare('ne_tecnologo',$this->ne_tecnologo,true);
		$criteria->compare('ne_profecional',$this->ne_profecional,true);
		$criteria->compare('ne_especializacion',$this->ne_especializacion,true);
		$criteria->compare('ne_maestria',$this->ne_maestria,true);
		$criteria->compare('ne_doctorado',$this->ne_doctorado,true);
		$criteria->compare('ingles_lectura',$this->ingles_lectura,true);
		$criteria->compare('ingles_escritura',$this->ingles_escritura,true);
		$criteria->compare('ingles_hablado',$this->ingles_hablado,true);
		$criteria->compare('so_ninguno',$this->so_ninguno,true);
		$criteria->compare('so_sap',$this->so_sap,true);
		$criteria->compare('so_jdedwar',$this->so_jdedwar,true);
		$criteria->compare('so_oracle',$this->so_oracle,true);
		$criteria->compare('so_otro',$this->so_otro,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('teloficina',$this->teloficina,true);
		$criteria->compare('teloficinaext',$this->teloficinaext,true);
		$criteria->compare('telcasa',$this->telcasa,true);
		$criteria->compare('telcel1',$this->telcel1,true);
		$criteria->compare('telcel2',$this->telcel2,true);
		$criteria->compare('mail1',$this->mail1,true);
		$criteria->compare('mail2',$this->mail2,true);
		$criteria->compare('tipocontrato',$this->tipocontrato,true);
		$criteria->compare('hojavida',$this->hojavida,true);
		$criteria->compare('formaprobacion',$this->formaprobacion,true);
		$criteria->compare('nom_familiar',$this->nom_familiar,true);
		$criteria->compare('nom_referenciado',$this->nom_referenciado,true);
		$criteria->compare('estado',$this->estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}