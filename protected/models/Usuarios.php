<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property string $usuario
 * @property string $clave
 * @property string $usuarioID
 * @property string $Email
 * @property integer $CambioRequeridoPassword
 * @property string $IntentosErrados
 * @property integer $Bloqueado
 * @property string $FechaUltimoLogeo
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 *
 * The followings are the available model relations:
 * @property Areasvicepresidencia[] $areasvicepresidencias
 * @property Areasvicepresidencia[] $areasvicepresidencias1
 * @property Miembrosrol[] $miembrosrols
 * @property Miembrosrol[] $miembrosrols1
 * @property Modulocompañias[] $modulocompañiases
 * @property Permisos[] $permisoses
 * @property Sedescompañias[] $sedescompañiases
 * @property Sedescompañias[] $sedescompañiases1
 * @property Tipocontratacion[] $tipocontratacions
 * @property Tipoprocesoseleccion[] $tipoprocesoseleccions
 * @property Tiposervicios[] $tiposervicioses
 * @property Compañias[] $compañiases
 */
class Usuarios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario, clave, Email, CambioRequeridoPassword, IntentosErrados, Bloqueado, FechaCreacion, UsuarioCreacion', 'required'),
			array('CambioRequeridoPassword, Bloqueado', 'numerical', 'integerOnly'=>true),
			array('usuario, clave, Email', 'length', 'max'=>50),
			array('IntentosErrados, UsuarioCreacion, UsuarioModificacion', 'length', 'max'=>10),
			array('FechaUltimoLogeo, FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('usuario, clave, usuarioID, Email, CambioRequeridoPassword, IntentosErrados, Bloqueado, FechaUltimoLogeo, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areasvicepresidencias' => array(self::HAS_MANY, 'Areasvicepresidencia', 'UsuarioCreacion'),
			'areasvicepresidencias1' => array(self::HAS_MANY, 'Areasvicepresidencia', 'UsuarioModificacion'),
			'miembrosrols' => array(self::HAS_MANY, 'Miembrosrol', 'UsuarioCreacion'),
			'miembrosrols1' => array(self::HAS_MANY, 'Miembrosrol', 'UsuarioID'),
			'modulocompañiases' => array(self::HAS_MANY, 'Modulocompañias', 'UsarioCreacion'),
			'permisoses' => array(self::HAS_MANY, 'Permisos', 'UsuarioCreacion'),
			'sedescompañiases' => array(self::HAS_MANY, 'Sedescompañias', 'UsuarioCreacion'),
			'sedescompañiases1' => array(self::HAS_MANY, 'Sedescompañias', 'UsuarioModificacion'),
			'tipocontratacions' => array(self::HAS_MANY, 'Tipocontratacion', 'UsuarioCreacion'),
			'tipoprocesoseleccions' => array(self::HAS_MANY, 'Tipoprocesoseleccion', 'UsuarioCreacion'),
			'tiposervicioses' => array(self::HAS_MANY, 'Tiposervicios', 'UsuarioCreacion'),
			'compañiases' => array(self::MANY_MANY, 'Compañias', 'usuariocompañias(UsuarioID, CompañiaID)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'usuario' => 'Usuario',
			'clave' => 'Clave',
			'usuarioID' => 'Usuario',
			'Email' => 'Email',
			'CambioRequeridoPassword' => 'Cambio Requerido Password',
			'IntentosErrados' => 'Intentos Errados',
			'Bloqueado' => 'Bloqueado',
			'FechaUltimoLogeo' => 'Fecha Ultimo Logeo',
			'FechaCreacion' => 'Fecha Creacion',
			'UsuarioCreacion' => 'Usuario Creacion',
			'FechaModificacion' => 'Fecha Modificacion',
			'UsuarioModificacion' => 'Usuario Modificacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('clave',$this->clave,true);
		$criteria->compare('usuarioID',$this->usuarioID,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('CambioRequeridoPassword',$this->CambioRequeridoPassword);
		$criteria->compare('IntentosErrados',$this->IntentosErrados,true);
		$criteria->compare('Bloqueado',$this->Bloqueado);
		$criteria->compare('FechaUltimoLogeo',$this->FechaUltimoLogeo,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}


public function validatePassword($password)
{
return $this->hashPassword($password)===$this->password;
}
public function hashPassword($password)
{
return md5($password);
}