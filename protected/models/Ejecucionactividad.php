<?php

/**
 * This is the model class for table "ejecucionactividad".
 *
 * The followings are the available columns in table 'ejecucionactividad':
 * @property string $RequisionID
 * @property string $actividadflujoID
 * @property string $fecha
 * @property string $usuarioID
 * @property string $observacion
 * @property string $requisicionactividadID
 * @property string $CompaniaID
 * @property string $FechaCreacion
 * @property string $UsuarioCreacion
 * @property string $FechaModificacion
 * @property string $UsuarioModificacion
 *
 * The followings are the available model relations:
 * @property Usuario $usuarioModificacion
 * @property Requisicion $requision
 * @property Actividadesflujos $actividadflujo
 * @property Companias $compania
 * @property Usuario $usuarioCreacion
 */
class Ejecucionactividad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ejecucionactividad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ejecucionactividad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RequisionID, actividadflujoID, FechaCreacion, UsuarioCreacion', 'required'),
			array('RequisionID, actividadflujoID, CompaniaID, UsuarioCreacion, UsuarioModificacion', 'length', 'max'=>10),
			array('usuarioID', 'length', 'max'=>128),
			array('observacion', 'length', 'max'=>200),
			array('fecha, FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('RequisionID, actividadflujoID, fecha, usuarioID, observacion, requisicionactividadID, CompaniaID, FechaCreacion, UsuarioCreacion, FechaModificacion, UsuarioModificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),
			'requision' => array(self::BELONGS_TO, 'Requisicion', 'RequisionID'),
                        'requisicionproceso' => array(self::BELONGS_TO, 'Requisicionproceso', 'RequisionID'),                     
			'actividadflujo' => array(self::BELONGS_TO, 'Actividadesflujos', 'actividadflujoID'),
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'requisicionactividadID' => 'RequisicionactividadID',                    
			'RequisionID' => 'Requisición',
			'actividadflujoID' => 'Actividad flujo',
			'fecha' => 'Fecha Evento',
			'usuarioID' => 'Usuario',
			'observacion' => 'Comentarios Proceso',
			'CompaniaID' => 'Compañia',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioCreacion' => 'Usuario Creación',
			'FechaModificacion' => 'Fecha Modificación',
			'UsuarioModificacion' => 'Usuario Modificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($v)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('RequisionID',$this->RequisionID,true);
		$criteria->compare('actividadflujoID',$this->actividadflujoID,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('usuarioID',$this->usuarioID,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('requisicionactividadID',$this->requisicionactividadID,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);
                
                if (Yii::app()->user->name == 'admin'){
                } else { 
                 
                  //echo "************************". $v;

                if ($v == 'A')		
                        //select * from ejecucionactividad where requisionID in (select requisionID from ejecucionactividad where usuarioID ='clienteinterno' and actividadflujoID = 1) and requisicionactividadID in (select max(requisicionactividadID)  from ejecucionactividad  group by requisionID)
                        $selectd2 = 'RequisionID IN (Select Distinct RequisionID from ejecucionactividad Where actividadflujoID = 1 And usuarioID ='. "'" . Yii::app()->user->name . "') and requisicionactividadID in (select max(requisicionactividadID)  from ejecucionactividad  group by requisionID) and (actividadflujoID < 6 or actividadflujoID = 7)"; 
                 else
                        $selectd2 = 'RequisionID IN (Select Distinct RequisionID from ejecucionactividad Where actividadflujoID = 1 And usuarioID ='. "'" . Yii::app()->user->name . "') and requisicionactividadID in (select max(requisicionactividadID)  from ejecucionactividad  group by requisionID) and actividadflujoID = 6"; 
                        //'RequisionID IN (Select Distinct RequisionID from ejecucionactividad Where actividadflujoID = 1 And usuarioID ='. "'" . Yii::app()->user->name . "')"; 
                        $criteria->condition= $selectd2;
                }
                //echo $selectd2; 
		$criteria->compare('RequisionID',$this->RequisionID,true);                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}