<?php

/**
 * This is the model class for table "requisicionproceso".
 *
 * The followings are the available columns in table 'requisicionproceso':
 * @property string $requisionprocesoID
 * @property string $RequisicionID
 * @property string $refiere
 * @property string $emailrefiere
 * @property string $referido
 * @property string $emailreferido
 * @property string $datosparapublicar
 * @property string $TipoProcesoSeleccionID
 * @property string $datosofertacargo
 * @property string $datosofertaunidadnegocio
 * @property string $datosofertareportaa
 * @property string $datosofertamision
 * @property string $datosofertaresponsabilidades
 * @property string $datosofertaformacion
 * @property string $datosofertaconocimientos
 * @property string $datosofertacompetencias
 * @property string $datosofertaexperiencia
 * @property string $datosofertafechalimite
 * @property string $datosafertaregistroaplicar
 * @property string $ofertaid
 * @property string $publicacionmedios
 */
class Requisicionprocesoeval extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicionprocesoeval the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicionproceso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RequisicionID', 'required'),
			array('RequisicionID, TipoProcesoSeleccionID', 'length', 'max'=>10),
			array('refiere, referido', 'length', 'max'=>200),
			array('emailrefiere, emailreferido, datosofertacargo, datosofertaunidadnegocio, datosofertareportaa, datosofertafechalimite, publicacionmedios', 'length', 'max'=>100),
			array('datosparapublicar, datosofertamision', 'length', 'max'=>2000),
			array('datosofertaresponsabilidades, datosofertaformacion, datosofertaconocimientos, datosofertacompetencias, datosofertaexperiencia, datosafertaregistroaplicar, ofertaid', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('requisionprocesoID, RequisicionID, refiere, emailrefiere, referido, emailreferido, datosparapublicar, TipoProcesoSeleccionID, datosofertacargo, datosofertaunidadnegocio, datosofertareportaa, datosofertamision, datosofertaresponsabilidades, datosofertaformacion, datosofertaconocimientos, datosofertacompetencias, datosofertaexperiencia, datosofertafechalimite, datosafertaregistroaplicar, ofertaid, publicacionmedios', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'requisionprocesoID' => 'ID proceso',
			'RequisicionID' => 'ID Requisicion',
			'refiere' => 'email Refiere',
			'emailrefiere' => 'Emailrefiere',
			'referido' => 'Referidos',
			'emailreferido' => 'Email Referidos',
			'datosparapublicar' => 'Formato de publicacion para envio por correo electronico',
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
			'datosofertacargo' => 'Nombre Cargo',
			'datosofertaunidadnegocio' => 'Unidad Negocio',
			'datosofertareportaa' => 'REPORTA A:',
			'datosofertamision' => 'Mision',
			'datosofertaresponsabilidades' => 'Responsabilidades',
			'datosofertaformacion' => 'Formacion',
			'datosofertaconocimientos' => 'Conocimientos',
			'datosofertacompetencias' => 'Competencias',
			'datosofertaexperiencia' => 'Experiencia',
			'datosofertafechalimite' => 'Fecha Limite Aplicacion',
			'datosafertaregistroaplicar' => 'Para aplicar a esta vacante registre sus datos en:',
			'datosafertaregistroaplicar' => 'FUENTES DE RECLUTAMIENTO INVESTIGACION DE MERCADO - Nombre Medio',
                  'publicacionmedios' => 'Referidor por:',
   
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('refiere',$this->refiere,true);
		$criteria->compare('emailrefiere',$this->emailrefiere,true);
		$criteria->compare('referido',$this->referido,true);
		$criteria->compare('emailreferido',$this->emailreferido,true);
		$criteria->compare('datosparapublicar',$this->datosparapublicar,true);
		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		$criteria->compare('datosofertacargo',$this->datosofertacargo,true);
		$criteria->compare('datosofertaunidadnegocio',$this->datosofertaunidadnegocio,true);
		$criteria->compare('datosofertareportaa',$this->datosofertareportaa,true);
		$criteria->compare('datosofertamision',$this->datosofertamision,true);
		$criteria->compare('datosofertaresponsabilidades',$this->datosofertaresponsabilidades,true);
		$criteria->compare('datosofertaformacion',$this->datosofertaformacion,true);
		$criteria->compare('datosofertaconocimientos',$this->datosofertaconocimientos,true);
		$criteria->compare('datosofertacompetencias',$this->datosofertacompetencias,true);
		$criteria->compare('datosofertaexperiencia',$this->datosofertaexperiencia,true);
		$criteria->compare('datosofertafechalimite',$this->datosofertafechalimite,true);
		$criteria->compare('datosafertaregistroaplicar',$this->datosafertaregistroaplicar,true);
		$criteria->compare('ofertaid',$this->ofertaid,true);
		$criteria->compare('publicacionmedios',$this->publicacionmedios,true);
             $criteria->condition='TipoProcesoSeleccionID = 2';  
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}