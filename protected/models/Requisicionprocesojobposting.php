<?php

/**
 * This is the model class for table "requisicionproceso".
 *
 * The followings are the available columns in table 'requisicionproceso':
 * @property string $requisionprocesoID
 * @property string $RequisicionID
 * @property string $refiere
 * @property string $emailrefiere
 * @property string $referido
 * @property string $emailreferido
 * @property string $datosparapublicar
 * @property string $TipoProcesoSeleccionID
 * @property string $datosofertacargo
 * @property string $datosofertaunidadnegocio
 * @property string $datosofertareportaa
 * @property string $datosofertamision
 * @property string $datosofertaresponsabilidades
 * @property string $datosofertaformacion
 * @property string $datosofertaconocimientos
 * @property string $datosofertacompetencias
 * @property string $datosofertaexperiencia
 * @property string $datosofertafechalimite
 * @property string $datosafertaregistroaplicar
 */
class Requisicionprocesojobposting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicionprocesojobposting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicionproceso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RequisicionID', 'required'),
			array('RequisicionID, TipoProcesoSeleccionID', 'length', 'max'=>10),
			array('refiere, referido', 'length', 'max'=>100),
			array('emailrefiere, emailreferido, datosofertacargo, datosofertaunidadnegocio,datosofertaexperiencia,datosofertaexperienciaind,datosofertaexperienciacar,datosofertareportaa', 'length', 'max'=>100),
			array('datosparapublicar, datosofertamision', 'length', 'max'=>4000),
			array('datosofertaresponsabilidades, datosofertaformacion, datosofertaconocimientos, datosofertacompetencias, datosafertaregistroaplicar', 'length', 'max'=>6000),
			array('datosofertafechalimite', 'safe'),
			array('datosofertafechainicio', 'safe'),
			array('datosofertaexperienciaind', 'safe'),
			array('datosofertaexperienciacar', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('requisionprocesoID, RequisicionID, refiere, emailrefiere, referido, emailreferido, datosparapublicar, TipoProcesoSeleccionID, datosofertacargo, datosofertaunidadnegocio, datosofertareportaa, datosofertamision, datosofertaresponsabilidades, datosofertaformacion, datosofertaconocimientos, datosofertacompetencias, datosofertaexperiencia, datosofertafechalimite, datosafertaregistroaplicar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
            return array('unidadnegocio' => array(self::BELONGS_TO, 'Unidadnegocio','datosofertaunidadnegocio'),'tipoprocesoseleccion' => array(self::BELONGS_TO, 'Tipoprocesoseleccion','TipoProcesoSeleccionID') );
           
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'requisionprocesoID' => 'ID proceso',
			'RequisicionID' => 'ID Requisicion',
			'refiere' => 'Refiere',
			'emailrefiere' => 'Emailrefiere',
			'referido' => 'Referido',
			'emailreferido' => 'Emailreferido',
			'datosparapublicar' => 'Formato de publicacion para envio por correo electronico',
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
			'datosofertacargo' => 'Nombre Cargo',
			'datosofertaunidadnegocio' => 'Unidad Negocio',
			'datosofertareportaa' => 'REPORTA A:',
			'datosofertamision' => 'Mision del Cargo',
			'datosofertaresponsabilidades' => 'Principales Responsabilidades',
			'datosofertaformacion' => 'Formacion Academica',
			'datosofertaconocimientos' => 'Conocimientos y habilidades tecnicas',
			'datosofertacompetencias' => 'Competencias',
			'datosofertaexperiencia' => 'Experiencia Laboral',
                        'datosofertaexperienciaind' => 'Experiencia Industria', 
                        'datosofertaexperienciacar' => 'Experiencia en el Cargo',
			'datosofertafechalimite' => 'Fecha Limite Aplicacion',
                        'datosofertafechainicio' => 'Fecha Inicio', 
			'datosafertaregistroaplicar' => 'Para aplicar a esta vacante registre sus datos en:',
              
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('refiere',$this->refiere,true);
		$criteria->compare('emailrefiere',$this->emailrefiere,true);
		$criteria->compare('referido',$this->referido,true);
		$criteria->compare('emailreferido',$this->emailreferido,true);
		$criteria->compare('datosparapublicar',$this->datosparapublicar,true);
		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		$criteria->compare('datosofertacargo',$this->datosofertacargo,true);
		$criteria->compare('datosofertaunidadnegocio',$this->datosofertaunidadnegocio,true);
		$criteria->compare('datosofertareportaa',$this->datosofertareportaa,true);
		$criteria->compare('datosofertamision',$this->datosofertamision,true);
		$criteria->compare('datosofertaresponsabilidades',$this->datosofertaresponsabilidades,true);
		$criteria->compare('datosofertaformacion',$this->datosofertaformacion,true);
		$criteria->compare('datosofertaconocimientos',$this->datosofertaconocimientos,true);
		$criteria->compare('datosofertacompetencias',$this->datosofertacompetencias,true);
		$criteria->compare('datosofertaexperiencia',$this->datosofertaexperiencia,true);
		$criteria->compare('datosofertafechalimite',$this->datosofertafechalimite,true);
                $criteria->compare('datosofertafechainicio',$this->datosofertafechainicio,true);
                $criteria->compare('datosofertaexperienciaind',$this->datosofertaexperienciaind,true);
                $criteria->compare('datosofertaexperienciacar',$this->datosofertaexperienciacar,true); 
		$criteria->compare('datosafertaregistroaplicar',$this->datosafertaregistroaplicar,true);
                $criteria->condition='TipoProcesoSeleccionID = 1'; 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}