<?php

/**
 * This is the model class for table "candidatos".
 *
 * The followings are the available columns in table 'candidatos':
 * @property integer $idCandidato
 * @property string $apellidos
 * @property string $nombres
 * @property integer $identificacion
 * @property integer $edad
 * @property string $fuente_reclutamiento
 * @property string $fuente_reclutamiento_otro
 * @property string $ultimoCargo
 * @property double $aniosProfesional
 * @property double $aniosRequeridos
 * @property double $aniosExperienciaSector
 * @property double $sueldoActual
 * @property double $sueldoAspirado
 * @property string $familiarEnEmpresa
 * @property string $foto
 * @property string $nombreCargo
 * @property string $fechaInsert
 * @property integer $id_proceso
 * @property string $ne_primaria
 * @property string $ne_secundaria
 * @property string $ne_tecnico
 * @property string $ne_tecnologo
 * @property string $ne_profecional
 * @property string $ne_especializacion
 * @property string $ne_maestria
 * @property string $ne_doctorado
 * @property string $ingles_lectura
 * @property string $ingles_escritura
 * @property string $ingles_hablado
 * @property string $so_ninguno
 * @property string $so_sap
 * @property string $so_jdedwar
 * @property string $so_oracle
 * @property string $so_otro
 * @property string $direccion
 * @property string $teloficina
 * @property string $teloficinaext
 * @property string $telcasa
 * @property string $telcel1
 * @property string $telcel2
 * @property string $mail1
 * @property string $mail2
 * @property string $tipoContratoID
 * @property string $hojavida
 * @property string $formaprobacion
 * @property string $nom_familiar
 * @property string $nom_referenciado
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Procesos $idProceso
 * @property Metadatos[] $metadatoses
 * @property Respuestas[] $respuestases
 */
class RegistroCandidatos extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Candidatos the static model class
     */
    public $nombre_foto;
    public $tipo_foto;
    public $foto;
    public $nombre_hojavida;
    public $tipo_hojavida;
    public $hojavida;
    public $nombre_formaprobacion;
    public $tipo_formaprobacion;
    public $formaprobacion;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'candidatos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fechaInsert', 'default', 'value' => new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on' => 'insert'),
            array('tipoContratoID, fechaInsert, id_proceso, identificacion, nombres, apellidos, edad, ciudad_id, direccion, teloficina, telcel1, mail1, ultimoCargo, aniosProfesional, aniosRequeridos, aniosExperienciaSector, sueldoActual, sueldoAspirado, familiarEnEmpresa, referenciadoInterno, nombreCargo, foto, hojavida, ingles_lectura, ingles_escritura, ingles_hablado, fuente_reclutamiento', 'required'),
            array('identificacion', 'unique', 'message' => 'Este número de cédula ya existe en el Sistema. No es posible volver a registrarse'),
            array('identificacion, edad, ciudad_id, estado, tipoContratoID', 'numerical', 'integerOnly' => true),
            array('aniosProfesional, aniosRequeridos, aniosExperienciaSector, sueldoActual, sueldoAspirado', 'numerical'),
            array('apellidos, nombres', 'length', 'max' => 50),
            array('ultimoCargo', 'length', 'max' => 150),
            array('familiarEnEmpresa, referenciadoInterno', 'length', 'max' => 1),
            array('mail1, mail2, tipoContratoID, nom_familiar, nom_referenciado', 'length', 'max' => 200),
            array('foto', 'file', 'allowEmpty' => true, 'types' => 'jpg, gif, png, bmp', 'maxSize' => 1024 * 1024 * 1, 'tooLarge' => 'La foto cargada tiene un tamaño mayor a 1MB. Por favor cargue una foto de menor tamaño.'),
            array('hojavida', 'file', 'allowEmpty' => true, 'types' => 'doc, docx, xls, xlsx, pdf', 'maxSize' => 1024 * 1024 * 2, 'tooLarge' => 'El archivo cargado de hoja de vida tiene un tamaño mayor a 2MB. Por favor cargue un archivo de menor tamaño.'),
            array('formaprobacion', 'file', 'allowEmpty' => true, 'types' => 'jpg, png, bmp, tiff, pdf', 'maxSize' => 1024 * 1024 * 2, 'tooLarge' => 'El archivo cargado de formato de aprobación tiene un tamaño mayor a 2MB. Por favor cargue un archivo de menor tamaño.'),
            array('nombre_foto, tipo_foto, foto', 'safe'),
            array('estado, nombre_hojavida, tipo_hojavida, hojavida', 'safe'),
            array('nombre_formaprobacion, tipo_formaprobacion, formaprobacion', 'safe'),
            array('nombreCargo', 'length', 'max' => 250),
            array('ne_primaria, ne_secundaria, ne_tecnico, ne_tecnologo, ne_profecional, ne_especializacion, ne_maestria, ne_doctorado, so_ninguno, so_sap, so_jdedwar, so_oracle, so_otro, teloficina, teloficinaext, telcasa, telcel1, telcel2, fuente_reclutamiento, fuente_reclutamiento_otro', 'length', 'max' => 100),
            array('id_proceso, ingles_lectura, ingles_escritura, ingles_hablado', 'length', 'max' => 10),
            array('direccion', 'length', 'max' => 300),
            //array('idCandidato, apellidos, nombres, identificacion, aniosProfesional, aniosRequeridos, aniosExperienciaSector, ne_profecional, ne_especializacion, ne_maestria, estado', 'safe', 'on'=>'search'),
            array('idCandidato, apellidos, nombres, identificacion, edad, ciudad_id, ultimoCargo, aniosProfesional, aniosRequeridos, aniosExperienciaSector, sueldoActual, sueldoAspirado, familiarEnEmpresa, nombre_foto, nombreCargo, fechaInsert, id_proceso, ne_primaria, ne_secundaria, ne_tecnico, ne_tecnologo, ne_profecional, ne_especializacion, ne_maestria, ne_doctorado, ingles_lectura, ingles_escritura, ingles_hablado, so_ninguno, so_sap, so_jdedwar, so_oracle, so_otro, direccion, teloficina, teloficinaext, telcasa, telcel1, telcel2, mail1, mail2, tipocontrato, tipoContratoID, nombre_hojavida, nombre_formaprobacion, nom_familiar, nom_referenciado, estado, observaciones, referenciadoInterno, foto, hojavida, formaprobacion, tipo_foto, tipo_hojavida, tipo_formaprobacion, hv, fa, ft', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idProceso' => array(self::BELONGS_TO, 'Procesos', 'id_proceso'),
            'ciudad' => array(self::BELONGS_TO, 'Ciudad', 'ciudad_id'),
            'tipoContrato' => array(self::BELONGS_TO, 'Tipocontratacion', 'tipoContratoID'),
            'metadatoses' => array(self::HAS_MANY, 'Metadatos', 'idcandidatos'),
            'respuestases' => array(self::HAS_MANY, 'Respuestas', 'idCandidato'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idCandidato' => 'Id Candidato',
            'apellidos' => 'Apellidos',
            'nombres' => 'Nombres',
            'identificacion' => 'No. de Documento',
            'edad' => 'Edad',
            'ciudad_id' => 'Ciudad de residencia (Pais)',
            'fuente_reclutamiento' => '¿Por medio de que fuente se enteró de esta oferta?',
            'fuente_reclutamiento_otro' => 'Especifique cual',
            'ultimoCargo' => 'Nombre del cargo que desempeña actualmente',
            'aniosProfesional' => 'Total años comprobados experiencia profesional',
            'aniosRequeridos' => 'Total años comprobados de experiencia en cargos similares',
            'aniosExperienciaSector' => 'Total años comprobados de experiencia en el sector al cual está aplicando',
            'sueldoActual' => 'Salario actual (Incluye bonificaciones, beneficios, etc.)',
            'sueldoAspirado' => 'Aspiración salarial',
            'familiarEnEmpresa' => 'Tiene familiares trabajando con nosotros ',
            'referenciadoInterno' => 'Está referenciado por algún colaborador de la compañía',
            'foto' => 'Foto',
            'nombreCargo' => 'Escriba el cargo al que desea aplicar',
            'fechaInsert' => 'Fecha del registro en la plataforma',
            'id_proceso' => 'No. de Proceso',
            'ne_primaria' => 'Primaria',
            'ne_secundaria' => 'Secundaria',
            'ne_tecnico' => 'Técnico',
            'ne_tecnologo' => 'Técnologo',
            'ne_profecional' => 'Profesional',
            'ne_especializacion' => 'Especialización',
            'ne_maestria' => 'Maestría',
            'ne_doctorado' => 'Doctorado',
            'ingles_lectura' => 'Lectura',
            'ingles_escritura' => 'Escritura',
            'ingles_hablado' => 'Hablado',
            'so_ninguno' => 'Ninguno',
            'so_sap' => 'SAP',
            'so_jdedwar' => 'JD Edwards',
            'so_oracle' => 'Oracle',
            'so_otro' => 'Otro Cual',
            'direccion' => 'Dirección de Contacto',
            'teloficina' => 'Oficina',
            'teloficinaext' => 'Extensión',
            'telcasa' => 'Casa',
            'telcel1' => 'Celular 1',
            'telcel2' => 'Celular 2',
            'mail1' => 'Correo 1',
            'mail2' => 'Correo 2',
            'tipoContratoID' => 'Tipo de Vinculación',
            'hojavida' => 'Hoja de vida',
            'formaprobacion' => 'Formato de Aprobación',
            'nom_familiar' => 'Escriba el nombre del familiar',
            'nom_referenciado' => 'Escriba el nombre de la persona que lo referenció',
            'estado' => 'Estado',
            'hv' => 'Hv',
            'fa' => 'Fa',
            'ft' => 'Ft',
        );
    }

    public function search($proceso) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        if ($proceso == NULL)
            $proceso = 0;

        $criteria = new CDbCriteria;

        $criteria->compare('idCandidato', $this->idCandidato);
        $criteria->compare('apellidos', $this->apellidos, true);
        $criteria->compare('nombres', $this->nombres, true);
        $criteria->compare('identificacion', $this->identificacion);
        $criteria->compare('edad', $this->edad, true);
        $criteria->compare('ciudad_id', $this->ciudad_id, true);
        $criteria->compare('fuente_reclutamiento', $this->fuente_reclutamiento, true);
        $criteria->compare('fuente_reclutamiento_otro', $this->fuente_reclutamiento_otro, true);
        $criteria->compare('ultimoCargo', $this->ultimoCargo, true);
        $criteria->compare('aniosProfesional', $this->aniosProfesional);
        $criteria->compare('aniosRequeridos', $this->aniosRequeridos);
        $criteria->compare('aniosExperienciaSector', $this->aniosExperienciaSector);
        $criteria->compare('sueldoActual', $this->sueldoActual);
        $criteria->compare('sueldoAspirado', $this->sueldoAspirado);
        $criteria->compare('familiarEnEmpresa', $this->familiarEnEmpresa, true);
        $criteria->compare('nombreCargo', $this->nombreCargo, true);
        $criteria->compare('fechaInsert', $this->fechaInsert, true);
        $criteria->compare('id_proceso', $this->id_proceso);
        $criteria->compare('ne_primaria', $this->ne_primaria, true);
        $criteria->compare('ne_secundaria', $this->ne_secundaria, true);
        $criteria->compare('ne_tecnico', $this->ne_tecnico, true);
        $criteria->compare('ne_tecnologo', $this->ne_tecnologo, true);
        $criteria->compare('ne_profecional', $this->ne_profecional, true);
        $criteria->compare('ne_especializacion', $this->ne_especializacion, true);
        $criteria->compare('ne_maestria', $this->ne_maestria, true);
        $criteria->compare('ne_doctorado', $this->ne_doctorado, true);
        $criteria->compare('ingles_lectura', $this->ingles_lectura, true);
        $criteria->compare('ingles_escritura', $this->ingles_escritura, true);
        $criteria->compare('ingles_hablado', $this->ingles_hablado, true);
        $criteria->compare('so_ninguno', $this->so_ninguno, true);
        $criteria->compare('so_sap', $this->so_sap, true);
        $criteria->compare('so_jdedwar', $this->so_jdedwar, true);
        $criteria->compare('so_oracle', $this->so_oracle, true);
        $criteria->compare('so_otro', $this->so_otro, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('teloficina', $this->teloficina, true);
        $criteria->compare('teloficinaext', $this->teloficinaext, true);
        $criteria->compare('telcasa', $this->telcasa, true);
        $criteria->compare('telcel1', $this->telcel1, true);
        $criteria->compare('telcel2', $this->telcel2, true);
        $criteria->compare('mail1', $this->mail1, true);
        $criteria->compare('mail2', $this->mail2, true);
        $criteria->compare('tipocontrato', $this->tipocontrato, true);
        $criteria->compare('tipoContratoID', $this->tipoContratoID);
        $criteria->compare('nombre_hojavida', $this->nombre_hojavida, true);
        $criteria->compare('nombre_formaprobacion', $this->nombre_formaprobacion, true);
        $criteria->compare('nom_familiar', $this->nom_familiar, true);
        $criteria->compare('nom_referenciado', $this->nom_referenciado, true);
        $criteria->compare('estado', $this->estado);
        $criteria->compare('observaciones', $this->observaciones, true);
        $criteria->compare('referenciadoInterno', $this->referenciadoInterno, true);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('hojavida', $this->hojavida, true);
        $criteria->compare('formaprobacion', $this->formaprobacion, true);
        $criteria->compare('tipo_foto', $this->tipo_foto, true);
        $criteria->compare('tipo_hojavida', $this->tipo_hojavida, true);
        $criteria->compare('tipo_formaprobacion', $this->tipo_formaprobacion, true);
        $criteria->compare('hv', $this->hv, true);
        $criteria->compare('fa', $this->fa, true);
        $criteria->compare('ft', $this->ft, true);
        $criteria->addCondition('id_proceso =' . $proceso . ' AND estado !=2');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 8),
            'sort' => array('defaultOrder' => 'nombres ASC'),
                //'criteria'=>array('condition'=>'id_proceso ='. $proceso.' AND estado !=2'),
        ));
    }

    public function searchPrefiltro($proceso) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        if ($proceso == NULL)
            $proceso = 0;

        $criteria = new CDbCriteria();

        $criteria->compare('idCandidato', $this->idCandidato);
        $criteria->compare('apellidos', $this->apellidos, true);
        $criteria->compare('nombres', $this->nombres, true);
        $criteria->compare('identificacion', $this->identificacion);
        $criteria->compare('edad', $this->edad, true);
        $criteria->compare('ciudad_id', $this->ciudad_id, true);
        $criteria->compare('fuente_reclutamiento', $this->fuente_reclutamiento, true);
        $criteria->compare('fuente_reclutamiento_otro', $this->fuente_reclutamiento_otro, true);
        $criteria->compare('ultimoCargo', $this->ultimoCargo, true);
        $criteria->compare('aniosProfesional', $this->aniosProfesional);
        $criteria->compare('aniosRequeridos', $this->aniosRequeridos);
        $criteria->compare('aniosExperienciaSector', $this->aniosExperienciaSector);
        $criteria->compare('sueldoActual', $this->sueldoActual);
        $criteria->compare('sueldoAspirado', $this->sueldoAspirado);
        $criteria->compare('familiarEnEmpresa', $this->familiarEnEmpresa, true);
        $criteria->compare('nombreCargo', $this->nombreCargo, true);
        $criteria->compare('fechaInsert', $this->fechaInsert, true);
        $criteria->compare('id_proceso', $this->id_proceso);
        $criteria->compare('ne_primaria', $this->ne_primaria, true);
        $criteria->compare('ne_secundaria', $this->ne_secundaria, true);
        $criteria->compare('ne_tecnico', $this->ne_tecnico, true);
        $criteria->compare('ne_tecnologo', $this->ne_tecnologo, true);
        $criteria->compare('ne_profecional', $this->ne_profecional, true);
        $criteria->compare('ne_especializacion', $this->ne_especializacion, true);
        $criteria->compare('ne_maestria', $this->ne_maestria, true);
        $criteria->compare('ne_doctorado', $this->ne_doctorado, true);
        $criteria->compare('ingles_lectura', $this->ingles_lectura, true);
        $criteria->compare('ingles_escritura', $this->ingles_escritura, true);
        $criteria->compare('ingles_hablado', $this->ingles_hablado, true);
        $criteria->compare('so_ninguno', $this->so_ninguno, true);
        $criteria->compare('so_sap', $this->so_sap, true);
        $criteria->compare('so_jdedwar', $this->so_jdedwar, true);
        $criteria->compare('so_oracle', $this->so_oracle, true);
        $criteria->compare('so_otro', $this->so_otro, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('teloficina', $this->teloficina, true);
        $criteria->compare('teloficinaext', $this->teloficinaext, true);
        $criteria->compare('telcasa', $this->telcasa, true);
        $criteria->compare('telcel1', $this->telcel1, true);
        $criteria->compare('telcel2', $this->telcel2, true);
        $criteria->compare('mail1', $this->mail1, true);
        $criteria->compare('mail2', $this->mail2, true);
        $criteria->compare('tipocontrato', $this->tipocontrato, true);
        $criteria->compare('tipoContratoID', $this->tipoContratoID);
        $criteria->compare('nombre_hojavida', $this->nombre_hojavida, true);
        $criteria->compare('nombre_formaprobacion', $this->nombre_formaprobacion, true);
        $criteria->compare('nom_familiar', $this->nom_familiar, true);
        $criteria->compare('nom_referenciado', $this->nom_referenciado, true);
        $criteria->compare('estado', $this->estado);
        $criteria->compare('observaciones', $this->observaciones, true);
        $criteria->compare('referenciadoInterno', $this->referenciadoInterno, true);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('hojavida', $this->hojavida, true);
        $criteria->compare('formaprobacion', $this->formaprobacion, true);
        $criteria->compare('tipo_foto', $this->tipo_foto, true);
        $criteria->compare('tipo_hojavida', $this->tipo_hojavida, true);
        $criteria->compare('tipo_formaprobacion', $this->tipo_formaprobacion, true);
        $criteria->compare('hv', $this->hv, true);
        $criteria->compare('fa', $this->fa, true);
        $criteria->compare('ft', $this->ft, true);
        $criteria->addCondition('id_proceso =' . $proceso);

        //$criteria->with[]='capacitador';
        //$criteria->addSearchCondition('capacitador.nombre',$this->capacitador_id,true);
//                $sort = new CSort();
//                $sort->attributes = array(
//                    'aniosProfesional'=>array(
//                        'asc'=>'t.aniosProfesional asc',
//                        'desc'=>'t.aniosProfesional desc',
//                        ));


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 4),
            'sort' => array('defaultOrder' => 'nombres ASC'),
//                        'sort'=>$sort,
                //'criteria'=>array('condition'=>'id_proceso ='. $proceso), /*, 'order'=>'nombres ASC'),*/
        ));
    }

    public function searchFlujoSeleccion($proceso, $flujo) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        //if($proceso == NULL) //no es necesario restringir por proceso porque esto ya se hace desde la vista "searchFlujoSeleccion(array('condition'=>'id_proceso ='. $proceso->id.''))"
        //    $proceso = 0;

        if ($proceso == NULL)
            $proceso = 0;

        $criteria = new CDbCriteria();

        $criteria->compare('idCandidato', $this->idCandidato);
        $criteria->compare('apellidos', $this->apellidos, true);
        $criteria->compare('nombres', $this->nombres, true);
        $criteria->compare('identificacion', $this->identificacion);
        $criteria->compare('edad', $this->edad, true);
        $criteria->compare('ciudad_id', $this->ciudad_id, true);
        $criteria->compare('fuente_reclutamiento', $this->fuente_reclutamiento, true);
        $criteria->compare('fuente_reclutamiento_otro', $this->fuente_reclutamiento_otro, true);
        $criteria->compare('ultimoCargo', $this->ultimoCargo, true);
        $criteria->compare('aniosProfesional', $this->aniosProfesional);
        $criteria->compare('aniosRequeridos', $this->aniosRequeridos);
        $criteria->compare('aniosExperienciaSector', $this->aniosExperienciaSector);
        $criteria->compare('sueldoActual', $this->sueldoActual);
        $criteria->compare('sueldoAspirado', $this->sueldoAspirado);
        $criteria->compare('familiarEnEmpresa', $this->familiarEnEmpresa, true);
        $criteria->compare('nombreCargo', $this->nombreCargo, true);
        $criteria->compare('fechaInsert', $this->fechaInsert, true);
        $criteria->compare('id_proceso', $this->id_proceso);
        $criteria->compare('ne_primaria', $this->ne_primaria, true);
        $criteria->compare('ne_secundaria', $this->ne_secundaria, true);
        $criteria->compare('ne_tecnico', $this->ne_tecnico, true);
        $criteria->compare('ne_tecnologo', $this->ne_tecnologo, true);
        $criteria->compare('ne_profecional', $this->ne_profecional, true);
        $criteria->compare('ne_especializacion', $this->ne_especializacion, true);
        $criteria->compare('ne_maestria', $this->ne_maestria, true);
        $criteria->compare('ne_doctorado', $this->ne_doctorado, true);
        $criteria->compare('ingles_lectura', $this->ingles_lectura, true);
        $criteria->compare('ingles_escritura', $this->ingles_escritura, true);
        $criteria->compare('ingles_hablado', $this->ingles_hablado, true);
        $criteria->compare('so_ninguno', $this->so_ninguno, true);
        $criteria->compare('so_sap', $this->so_sap, true);
        $criteria->compare('so_jdedwar', $this->so_jdedwar, true);
        $criteria->compare('so_oracle', $this->so_oracle, true);
        $criteria->compare('so_otro', $this->so_otro, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('teloficina', $this->teloficina, true);
        $criteria->compare('teloficinaext', $this->teloficinaext, true);
        $criteria->compare('telcasa', $this->telcasa, true);
        $criteria->compare('telcel1', $this->telcel1, true);
        $criteria->compare('telcel2', $this->telcel2, true);
        $criteria->compare('mail1', $this->mail1, true);
        $criteria->compare('mail2', $this->mail2, true);
        $criteria->compare('tipocontrato', $this->tipocontrato, true);
        $criteria->compare('tipoContratoID', $this->tipoContratoID);
        $criteria->compare('nombre_hojavida', $this->nombre_hojavida, true);
        $criteria->compare('nombre_formaprobacion', $this->nombre_formaprobacion, true);
        $criteria->compare('nom_familiar', $this->nom_familiar, true);
        $criteria->compare('nom_referenciado', $this->nom_referenciado, true);
        $criteria->compare('estado', $this->estado);
        $criteria->compare('observaciones', $this->observaciones, true);
        $criteria->compare('referenciadoInterno', $this->referenciadoInterno, true);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('hojavida', $this->hojavida, true);
        $criteria->compare('formaprobacion', $this->formaprobacion, true);
        $criteria->compare('tipo_foto', $this->tipo_foto, true);
        $criteria->compare('tipo_hojavida', $this->tipo_hojavida, true);
        $criteria->compare('tipo_formaprobacion', $this->tipo_formaprobacion, true);
        $criteria->compare('hv', $this->hv, true);
        $criteria->compare('fa', $this->fa, true);
        $criteria->compare('ft', $this->ft, true);
//                $criteria->compare('id_proceso',$proceso);
//                $criteria->condition='id_proceso ='. $proceso;

        if ($flujo == 1) {
            $criteria->addCondition('id_proceso =' . $proceso . ' AND (estado=1 OR estado=3)');
        } elseif ($flujo == 3) {
            $criteria->addCondition('id_proceso =' . $proceso . ' AND (estado=3 OR estado=4)');
        } elseif ($flujo == 4) {
            $criteria->addCondition('id_proceso =' . $proceso . ' AND (estado=4 OR estado=5)');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 8),
            //'criteria'=>array('condition'=>'id_proceso ='. $proceso), /*, 'order'=>'nombres ASC'),*/
            'sort' => array('defaultOrder' => 'nombres ASC'),
//                        'criteria'=>array('condition'=>'estado !=2 ')
        ));
    }

    public function beforeValidate() {

        if ($file = CUploadedFile::getInstance($this, 'foto')) {
            $this->nombre_foto = $file->name;
            $this->tipo_foto = $file->type;
            //$this->tamaño_foto = $file->size;
            $this->ft = $file;
            $this->foto = file_get_contents($file->tempName);
        }

        if ($file2 = CUploadedFile::getInstance($this, 'hojavida')) {
            $this->nombre_hojavida = $file2->name;
            $this->tipo_hojavida = $file2->type;
            //$this->image_size2 = $file2->size;
            $this->hv = $file2;
            $this->hojavida = file_get_contents($file2->tempName);
        }

        if ($file3 = CUploadedFile::getInstance($this, 'formaprobacion')) {
            $this->nombre_formaprobacion = $file3->name;
            $this->tipo_formaprobacion = $file3->type;
            $this->formaprobacion = file_get_contents($file3->tempName);
        }

        return parent::beforeValidate();
    }

    public function afterValidate() {

        $num_Proceso = $this->id_proceso;

        $criteria = new CDbCriteria;
        $criteria->select = 'id';
        $criteria->condition = 'ofertaid=:ofertaid';
        $criteria->params = array(':ofertaid' => $num_Proceso);
        $idPkOferta = Procesos::model()->find($criteria);

        if ($idPkOferta != NULL) {
            $formato_aprobacion = NULL;

            $tipocontID = $this->tipoContratoID;
            $formato_aprobacion = CUploadedFile::getInstance($this, 'formaprobacion');

            if (($tipocontID == 5 or $tipocontID == 6) and $formato_aprobacion === NULL) {

                $esJobPosting = Requisicionproceso::model()->find(array('condition' => "ofertaid=" . $idPkOferta->id));

                if ($esJobPosting->TipoProcesoSeleccionID == 1) {

                    $this->addError('formaprobacion', 'Tenga en cuenta que si usted es colaborador de la compañía es necesario anexar el formato de aprobación en el formulario.');

                    return false;
                }
            }

            //$this->id_proceso = $idPkOferta->id;
            $this->setAttribute('identificacion', $this->identificacion);

            return parent::afterValidate();
        } else {
            $this->addError('id_proceso', 'El número de proceso NO existe en el Sistema');

            return false;
        }
    }

}
