<?php

/**
 * This is the model class for table "actividadesflujos".
 *
 * The followings are the available columns in table 'actividadesflujos':
 * @property string $ActividadFlujoID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $FlujoID
 * @property string $TiempoEstimado
 * @property string $RolIDResponsable
 * @property string $UsuarioIDResponsable
 * @property string $Orden
 * @property string $UsuarioCreacion
 * @property string $FechaCreacion
 * @property string $UsuarioModificacion
 * @property string $FechaModificacion
 * @property string $CompaniaID
 *
 * The followings are the available model relations:
 * @property Companias $compania
 * @property Flujos $flujo
 * @property Roles $rolIDResponsable
 * @property Usuario $usuarioCreacion
 * @property Usuario $usuarioModificacion
 * @property Usuario $usuarioIDResponsable
 * @property Ejecucionactividad[] $ejecucionactividads
 */
class Actividadesflujos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Actividadesflujos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'actividadesflujos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, FlujoID, TiempoEstimado, RolIDResponsable, UsuarioIDResponsable, Orden, UsuarioCreacion, FechaCreacion', 'required'),
			array('Nombre', 'length', 'max'=>50),
			array('Descripcion', 'length', 'max'=>100),
			array('FlujoID, TiempoEstimado, RolIDResponsable, UsuarioIDResponsable, Orden, UsuarioCreacion, UsuarioModificacion, CompaniaID', 'length', 'max'=>10),
			array('FechaModificacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ActividadFlujoID, Nombre, Descripcion, FlujoID, TiempoEstimado, RolIDResponsable, UsuarioIDResponsable, Orden, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, CompaniaID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compania' => array(self::BELONGS_TO, 'Companias', 'CompaniaID'),
			'flujo' => array(self::BELONGS_TO, 'Flujos', 'FlujoID'),
			'rolIDResponsable' => array(self::BELONGS_TO, 'Roles', 'RolIDResponsable'),
			'usuarioCreacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioCreacion'),
			'usuarioModificacion' => array(self::BELONGS_TO, 'Usuario', 'UsuarioModificacion'),
			'usuarioIDResponsable' => array(self::BELONGS_TO, 'Usuario', 'UsuarioIDResponsable'),
			'ejecucionactividads' => array(self::HAS_MANY, 'Ejecucionactividad', 'actividadflujoID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ActividadFlujoID' => 'Actividad Flujo',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'FlujoID' => 'Flujo',
			'TiempoEstimado' => 'Tiempo Estimado',
			'RolIDResponsable' => 'Rol responsable',
			'UsuarioIDResponsable' => 'Usuario responsable',
			'Orden' => 'Orden',
			'UsuarioCreacion' => 'Usuario Creación',
			'FechaCreacion' => 'Fecha Creación',
			'UsuarioModificacion' => 'Usuario Modificación',
			'FechaModificacion' => 'Fecha Modificación',
			'CompaniaID' => 'Compañia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ActividadFlujoID',$this->ActividadFlujoID,true);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FlujoID',$this->FlujoID,true);
		$criteria->compare('TiempoEstimado',$this->TiempoEstimado,true);
		$criteria->compare('RolIDResponsable',$this->RolIDResponsable,true);
		$criteria->compare('UsuarioIDResponsable',$this->UsuarioIDResponsable,true);
		$criteria->compare('Orden',$this->Orden,true);
		$criteria->compare('UsuarioCreacion',$this->UsuarioCreacion,true);
		$criteria->compare('FechaCreacion',$this->FechaCreacion,true);
		$criteria->compare('UsuarioModificacion',$this->UsuarioModificacion,true);
		$criteria->compare('FechaModificacion',$this->FechaModificacion,true);
		$criteria->compare('CompaniaID',$this->CompaniaID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}