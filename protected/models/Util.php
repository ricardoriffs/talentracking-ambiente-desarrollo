<?php

/**
 * Clase con metodos utiles
 *
 * @author Johan AR <j3valentin@gmail.com>
 */
class Util {

    static public function slugify($text) {

// replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

// trim
        $text = trim($text, '-');

// transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

// lowercase
        $text = strtolower($text);

// remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    static public function getUrlBase() {
// Get HTTP/HTTPS (the possible values for this vary from server to server)
        $url = "http" . ((filter_has_var(INPUT_SERVER, 'HTTPS')
                && filter_input(INPUT_SERVER, 'HTTPS')
                && !in_array(strtolower(filter_input(INPUT_SERVER, 'HTTPS')),
                        array('off', 'no'))) ? 's' : '');
// Get domain portion
        $url .= '://' . filter_input(INPUT_SERVER, 'HTTP_HOST');
// Get path to script
//        $url .= filter_input(INPUT_SERVER, 'REQUEST_URI');
// Add path info, if any
        if (!(filter_has_var(INPUT_SERVER, 'PATH_INFO'))) {
            $url .= filter_input(INPUT_SERVER, 'PATH_INFO');
        }

//        $get = filter_input_array(INPUT_GET); // Create a copy of $_GET
//        unset($get['lg']); // Unset whatever you don't want
//        if (count($get)) { // Only add a query string if there's anything left
//            $url .= '?' . http_build_query($get);
//        }

        return $url;
    }

}
