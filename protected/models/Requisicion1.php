<?php

/**
 * This is the model class for table "requisicion".
 *
 * The followings are the available columns in table 'requisicion':
 * @property string $RequisicionID
 * @property string $UnidadNegocioID
 * @property string $AutorizacionProceso
 * @property string $NombreAutoriza
 * @property string $EmailAutoriza
 * @property string $AreaVicepresidenciaID
 * @property string $RutaArchivoAutorizacion
 * @property string $NumeroVacantes
 * @property string $NombreCargo
 * @property string $NombreSolicitante
 * @property string $CargoSolicitante
 * @property string $JefeInmediatoVacante
 * @property string $LugarTrabajo
 * @property string $RutaDescripcionCargo
 * @property string $DescripcionCargoPar
 * @property string $TurnoTrabajo
 * @property string $TipoContratacionID
 * @property string $TiempoContratacion
 * @property string $AspectosImportantes
 * @property string $Observaciones
 * @property string $RangoSalario
 * @property string $NivelHAY
 * @property string $PuntosHAY
 * @property string $TipoProcesoSeleccionID
 * @property string $UsuarioIDResponsable
 */
class Requisicion1 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicion1 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UnidadNegocioID, AutorizacionProceso, NombreAutoriza, EmailAutoriza, AreaVicepresidenciaID, RutaArchivoAutorizacion, NumeroVacantes, NombreCargo, NombreSolicitante, CargoSolicitante, JefeInmediatoVacante, LugarTrabajo, RutaDescripcionCargo, DescripcionCargoPar, TurnoTrabajo, TipoContratacionID, AspectosImportantes, Observaciones, RangoSalario', 'required'),
			array('UnidadNegocioID, AreaVicepresidenciaID, NumeroVacantes, TipoContratacionID, TiempoContratacion, NivelHAY, PuntosHAY, TipoProcesoSeleccionID, UsuarioIDResponsable', 'length', 'max'=>10),
			array('AutorizacionProceso, NombreAutoriza, EmailAutoriza, NombreSolicitante, CargoSolicitante, JefeInmediatoVacante, LugarTrabajo, TurnoTrabajo, RangoSalario', 'length', 'max'=>50),
			array('RutaArchivoAutorizacion, NombreCargo, RutaDescripcionCargo', 'length', 'max'=>255),
			array('DescripcionCargoPar, AspectosImportantes', 'length', 'max'=>4000),
			array('Observaciones', 'length', 'max'=>2000),
                        array('actividadID', 'safe'), 
                        array('perfilsalarial', 'safe'), 
                        array('fechacreacion', 'safe'), 
                        array('usuariocreacion', 'safe'),   
                        array('si,ss,si1,si2,si3,ss1,ss2,ss3', 'safe'),
                        
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('RequisicionID, UnidadNegocioID, AutorizacionProceso, NombreAutoriza, EmailAutoriza, AreaVicepresidenciaID, RutaArchivoAutorizacion, NumeroVacantes, NombreCargo, NombreSolicitante, CargoSolicitante, JefeInmediatoVacante, LugarTrabajo, RutaDescripcionCargo, DescripcionCargoPar, TurnoTrabajo, TipoContratacionID, TiempoContratacion, AspectosImportantes, Observaciones, RangoSalario, NivelHAY, PuntosHAY, TipoProcesoSeleccionID, UsuarioIDResponsable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                 		return array('unidadnegocio' => array(self::BELONGS_TO, 'Unidadnegocio', 'UnidadNegocioID'),
                             'areasvicepresidencia' => array(self::BELONGS_TO, 'Areasvicepresidencia', 'AreaVicepresidenciaID'), 
                             'tipocontratacion' => array(self::BELONGS_TO, 'Tipocontratacion', 'TipoContratacionID'), 
                             'sedescompanias' => array(self::BELONGS_TO, 'Sedescompanias', 'LugarTrabajo'),);

		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'RequisicionID' => 'Requisicion',
			'UnidadNegocioID' => 'Unidad Negocio',
			'AutorizacionProceso' => 'Autorización Proceso',
			'NombreAutoriza' => 'Nombre Autoriza',
			'EmailAutoriza' => 'Email Autoriza',
			'AreaVicepresidenciaID' => 'Area Vicepresidencia',
			'RutaArchivoAutorizacion' => 'Ruta Archivo Autorizacion',
			'NumeroVacantes' => 'Número Vacantes',
			'NombreCargo' => 'Nombre Cargo',
			'NombreSolicitante' => 'Nombre Solicitante',
			'CargoSolicitante' => 'Cargo Solicitante',
			'JefeInmediatoVacante' => 'Jefe Inmediato Vacante',
			'LugarTrabajo' => 'Lugar Trabajo',
			'RutaDescripcionCargo' => 'Ruta Descripcion Cargo',
			'DescripcionCargoPar' => 'Descripción Cargo Par',
			'TurnoTrabajo' => 'Turno Trabajo',
			'TipoContratacionID' => 'Tipo Contratación',
			'TiempoContratacion' => 'Tiempo Contratacion',
			'AspectosImportantes' => 'Aspectos Importantes',
			'Observaciones' => 'Escriba en este campo las observaciones por las cuales considera que es necesario devolver la requisición al cliente interno',
			'RangoSalario' => 'Rango Salario',
			'NivelHAY' => 'Nivel Hay',
			'PuntosHAY' => 'Puntos Hay',
			'TipoProcesoSeleccionID' => 'Tipo Proceso Seleccion',
			'UsuarioIDResponsable' => 'Lider de Seleccion a asignar',
                        'fechacreacion'=> 'Fecha Creacion', 
                        'usuariocreacion'=> 'Usuario Creacion', 
                        'perfilsalarial'=> 'Perfil Salarial',  
                        'si' => 'Rango Salario Inferior',    
                        'ss' => 'Rango Salario Superior',    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($view)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('UnidadNegocioID',$this->UnidadNegocioID,true);
		$criteria->compare('AutorizacionProceso',$this->AutorizacionProceso,true);
		$criteria->compare('NombreAutoriza',$this->NombreAutoriza,true);
		$criteria->compare('EmailAutoriza',$this->EmailAutoriza,true);
		$criteria->compare('AreaVicepresidenciaID',$this->AreaVicepresidenciaID,true);
		$criteria->compare('RutaArchivoAutorizacion',$this->RutaArchivoAutorizacion,true);
		$criteria->compare('NumeroVacantes',$this->NumeroVacantes,true);
		$criteria->compare('NombreCargo',$this->NombreCargo,true);
		$criteria->compare('NombreSolicitante',$this->NombreSolicitante,true);
		$criteria->compare('CargoSolicitante',$this->CargoSolicitante,true);
		$criteria->compare('JefeInmediatoVacante',$this->JefeInmediatoVacante,true);
		$criteria->compare('LugarTrabajo',$this->LugarTrabajo,true);
		$criteria->compare('RutaDescripcionCargo',$this->RutaDescripcionCargo,true);
		$criteria->compare('DescripcionCargoPar',$this->DescripcionCargoPar,true);
		$criteria->compare('TurnoTrabajo',$this->TurnoTrabajo,true);
		$criteria->compare('TipoContratacionID',$this->TipoContratacionID,true);
		$criteria->compare('TiempoContratacion',$this->TiempoContratacion,true);
		$criteria->compare('AspectosImportantes',$this->AspectosImportantes,true);
		$criteria->compare('Observaciones',$this->Observaciones,true);
		$criteria->compare('RangoSalario',$this->RangoSalario,true);
		$criteria->compare('NivelHAY',$this->NivelHAY,true);
		$criteria->compare('PuntosHAY',$this->PuntosHAY,true);
		$criteria->compare('TipoProcesoSeleccionID',$this->TipoProcesoSeleccionID,true);
		//$criteria->compare('UsuarioIDResponsable',$this->UsuarioIDResponsable,true);
                $criteria->compare('usuariocreacion',$this->usuariocreacion,true);
                
                
                //echo 'VIEW '. $view;
               // if ($view == 'subsubr2') {
                    // $criteria->condition='actividadID = 2';
               //     $criteria->compare('actividadID',2,true); 
         
                //} else {
                   //  $criteria->condition='actividadID = 3';
                  // $criteria->compare('actividadID',3,true); 
                //} 
                $criteria->compare('actividadID','<=3',true);
                $criteria->compare('UsuarioIDResponsable',Yii::app()->user->name); 
                //$criteria->condition= "(actividadID = 3 or actividadID = 2) and UsuarioIDResponsable= '" . Yii::app()->user->name ."'"; 
                   
         		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array('defaultOrder'=>'fechacreacion DESC'), // orden por defecto según el atributo fechacreacion                                   
		));
	}
}