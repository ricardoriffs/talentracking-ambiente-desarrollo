<?php

/**
 * This is the model class for table "requisicionprocesocomentarios".
 *
 * The followings are the available columns in table 'requisicionprocesocomentarios':
 * @property string $id
 * @property string $requisionprocesoID
 * @property string $RequisicionID
 * @property string $comentarios
 * @property string $fecha
 * @property string $username
 */
class Requisicionprocesocomentarios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisicionprocesocomentarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisicionprocesocomentarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs. requisionprocesoID,
		return array(
			array('RequisicionID, comentarios, fecha, username', 'required'),
			array('requisionprocesoID, RequisicionID', 'length', 'max'=>10),
			array('comentarios', 'length', 'max'=>3000),
			array('username', 'length', 'max'=>128),
                        array('tipocomentarioID', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, requisionprocesoID, RequisicionID, comentarios, fecha, username', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		
                 return array(
			'requisicion' => array(self::BELONGS_TO, 'Requisicion', 'RequisicionID'),
                        'tipocomentario' => array(self::BELONGS_TO, 'id','tipocomentarioID'), 
        	);
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'requisionprocesoID' => 'Requisionproceso',
			'RequisicionID' => 'Requisicion',
			'comentarios' => 'Comentarios',
			'fecha' => 'Fecha',
			'username' => 'Username',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($rp,$re)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('requisionprocesoID',$this->requisionprocesoID,true);
		$criteria->compare('RequisicionID',$this->RequisicionID,true);
		$criteria->compare('comentarios',$this->comentarios,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('username',$this->username,true);
            
            //'requisionprocesoID ='. $rp . ' and 
            $criteria->condition= 'requisicionID ='. $re;  
            $criteria->order='tipocomentarioID, fecha ASC';        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function fechaReciente($requisicion){
            
            $connection=Yii::app()->db; 
            
            $sql='SELECT max(fecha) as fechaantigua FROM `requisicionprocesocomentarios` `t` WHERE requisicionID ='.$requisicion;
            $criteria=$connection->createCommand($sql);
            $dataReader = $criteria->query();
           
            foreach ($dataReader as $row):
                $fecha = $row['fechaantigua'];
            endforeach;
            
            return $fecha;
        }        
}