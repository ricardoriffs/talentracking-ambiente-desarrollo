<?php

/**
 * This is the model class for table "candidatos".
 *
 * The followings are the available columns in table 'candidatos':
 * @property integer $identificacion
 */
class IngresoCandidatos extends CActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Candidatos the static model class
   */
  public $identificacion;
  private $_identity;
        
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'candidatos';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('identificacion', 'required'),        
      array('identificacion', 'numerical', 'integerOnly'=>true),   
      array('identificacion', 'authenticateCandidato'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'idProceso' => array(self::BELONGS_TO, 'Procesos', 'id_proceso'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'identificacion' => 'No. de identificación',
    );
  }

//        public function afterValidate() {
//
//                        $candidato = RegistroCandidatos::model()->findByAttributes(array('identificacion'=>$this->identificacion));         
// 
//                        if($candidato === NULL){                            
//                            $this->addError('identificacion', 'La identificación ingresada NO existe en nuestro Sistema. Antes de diligenciar las Encuestas debe estar registrado en algún Proceso de Selección.');
//                            return false;
//                        }
//                            
//                         else {
//                            
//                            //$this->setState('identificacion', $candidato->identificacion);     
//                            return parent::afterValidate();
////                              $this->addError('id_proceso', 'El número de proceso NO existe en el Sistema');
////                              return false;
//                        }
//        }
        
	public function authenticateCandidato($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->identificacion, $this->identificacion);
			if(!$this->_identity->authenticateCandidato($this->identificacion))
				$this->addError('identificacion','La identificación ingresada NO existe en nuestro Sistema. Antes de diligenciar las Encuestas debe estar registrado en algún Proceso de Selección.');
		}
	}        
        
	public function ingreso()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->identificacion);
			$this->_identity->authenticateCandidato($this->identificacion);
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration= 0;
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}        
}
