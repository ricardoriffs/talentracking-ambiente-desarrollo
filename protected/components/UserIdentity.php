<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate() {
        $username = strtolower($this->username);
        $user = Usuario::model()
                ->with('compania')
                ->find('LOWER(username)=?', array($username));
        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (!$user->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $user->id;
            $this->username = $user->username;
            Yii::app()->session['companyName'] = $user->compania->Nombre;
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId() {
        return $this->_id;
    }

    public function authenticateCandidato($id) {

//        $id=$this->identificacion;
        //$candidato = RegistroCandidatos::model()->find('identificacion=?',array($id));
        $candidato = RegistroCandidatos::model()->findByAttributes(array('identificacion' => $id));
        if ($candidato === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else {
            //$this->identificacion=$candidato->identificacion;
            $this->setState('identificacion', $candidato->identificacion);
            $this->errorCode = self::ERROR_NONE;
        }

        return $this->errorCode == self::ERROR_NONE;
    }

}
